const { v2: cloudinary } = require("cloudinary");
const configApp = require("./app");

cloudinary.config({
    cloud_name: configApp.cdnName,
    api_key: configApp.cdnKey,
    api_secret: configApp.cdnSecret,
});

module.exports = cloudinary;
