module.exports = {
    COMMON: {
        // DEFAULT_EXPIRED: 1000 * 10, // 10s
        JWT_EXPIRED_DEFAULT: "4h",
        DEFAULT_EXPIRED: 1000 * 60 * 60 * 4, // 4h
    },
    HTTP_STATUS: {
        OK: 200,
        CREATED: 201,
        OPTIONS: 204,
        FOUND: 302,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        CONFLICT: 409,
        UNPROCESSABLE_ENTITY: 422,
        INTERNAL_SERVER_ERROR: 500,
        BAD_GATEWAY: 502,
        GATEWAY_TIMEOUT: 504,
    },
    DB_CONNECTION: {
        MONGO: "mongodb",
        MYSQL: "mysql",
        POSTGRESQL: "pgsql",
    },
    VALIDATED: {
        HTTP: {
            UNAUTHORIZED: "Authentication failed",
            TOKEN_NOT_FOUND: "Unauthorized",
            FORBIDDEN: "Invalid token",
            INTERNAL_SERVER_ERROR: "Internal server error",
        },
    },
    QUERY_LIMIT_DEFAULT: 10,
    QUERY_PAGE_DEFAULT: 1,
    MEMO: {
        LIST_PAY: {
            BANK_TRANSFER: "Chuyển khoản",
            BANK_QR: "Tạo QR thanh toán",
            COD: "Đưa trực tiếp",
        },
        LIST_QR: {
            MBB: {
                name: "mbb",
                code: "MB",
            },
            VCB: {
                name: "vietcombank",
                code: "VCB",
            },
            TECH: {
                name: "techcombank",
                code: "TCB",
            },
        },
        LIST_NUMBER_BANK: {
            MBB: "0337882657",
            VCB: "1021027019",
            TECH: "19038342371017",
        },
        OWNER_CARD: "NGUYEN THANH TUNG",
    },
    CDN_RESOURCE_TYPE: {
        IMG: "image",
        VIDEO: "video",
        RAW: "raw",
        AUTO: "auto",
    },
    CDN_TYPE: {
        UPLOAD: "upload",
        LIST: "list",
    },
    JOB_PLAN: {
        STATUS: {
            WAITING: "wait",
            ACTIVE: "active",
            CANCEL: "cancel",
        },
    },
    PARAMETER: {
        TYPE: {
            STRING: "string",
            NUMBER: "number",
            ARRAY: "array",
            OBJECT: "object",
        },
        LIST_TYPE: ["string", "number", "array", "object"],
    },
};
