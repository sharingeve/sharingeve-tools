require("dotenv").config();

const setUrlMongo = () => {
    if (process.env.NODE_ENV == "production") {
        if (process.env.BASE_URL == "https://servertools2.onrender.com") {
            return `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_DATABASE}?retryWrites=true&w=majority&appName=${process.env.DB_APP_NAME}`;
        }
        return `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_DATABASE}?retryWrites=true&w=majority`;
    }

    return `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}?authSource=admin`;
};

module.exports = {
    env: process.env.NODE_ENV || "production",
    base_url: process.env.BASE_URL || "http://localhost:3000",
    port: process.env.BASE_PORT || 3000,
    mongoURL: setUrlMongo() || "mongodb://127.0.0.1/my_database",
    JWT_TOKEN_SECRET: process.env.JWT_TOKEN_SECRET || "",
    ADMIN_EMAIL: process.env.ADMIN_EMAIL || "sharingeve1405@gmail.com",
    BASE_URL_FE: process.env.BASE_URL_FE || "http://localhost:3000",
    // ---------------------------
    MAIL_HOST: process.env.MAIL_HOST || "",
    MAIL_PORT: process.env.MAIL_PORT || 456,
    MAIL_USERNAME: process.env.MAIL_USERNAME || "",
    MAIL_PASSWORD: process.env.MAIL_PASSWORD || "",
    MAIL_FROM_ADDRESS: process.env.MAIL_FROM_ADDRESS || "tungnt@sharingeve.com",
    // ---------------------------
    s3AccesskeyId: process.env.AWS_ACCESS_KEY_ID || "",
    s3SecretKey: process.env.AWS_SECRET_ACCESS_KEY || "",
    s3Region: process.env.AWS_DEFAULT_REGION || "",
    s3Bucket: process.env.AWS_BUCKET || "",
    s3UsePathStyleEndPoint: process.env.AWS_USE_PATH_STYLE_ENDPOINT || false,
    s3BaseUrl: process.env.AWS_BASE_URL || "http://localhost:3000/",
    // ---------------------------
    cdnName: process.env.CDN_NAME || "",
    cdnKey: process.env.CDN_KEY || "",
    cdnSecret: process.env.CDN_SECRET || "",
    cdnBaseUrl: process.env.CDN_BASEURL || "http://localhost:3000/",
    // ---------------------------
    telegramApi: process.env.TELEGRAM_API || "",
    telegramKey: process.env.TELEGRAM_KEY || "",
    teleChatID: process.env.TELEGRAM_CHATID || "",
};
