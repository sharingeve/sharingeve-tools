module.exports = {
    PAY_STATUS: {
        WAIT: "WAIT",
        FAIL: "FAIL",
        DONE: "DONE",
    },
    PAY_TYPE: {
        VIETQR: 0,
        QR_BANK: 0,
    },
    PAY_BANK: {
        TECH: "Techcombank",
        MBB: "MBB bank",
        VCB: "Vietcombank",
    },
    LOG_STATUS: {
        SUCCESS: "SUCCESS",
        ERROR: "ERROR",
        PROCESS: "PROCESS",
        WARNING: "WARNING",
    },
    CLOUD_SAVE: {
        AWS: "S3",
        CLOUDINARY: "CDN",
    },
    MEMO_EXTRA_TYPE_OTHER: {
        STRING: "string",
        OBJECT: "object",
    },
};
