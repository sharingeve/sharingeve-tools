const nodemailer = require("nodemailer");
const configApp = require("../config/app");

const transport = nodemailer.createTransport({
    host: configApp.MAIL_HOST,
    port: configApp.MAIL_PORT,
    auth: {
        user: configApp.MAIL_USERNAME,
        pass: configApp.MAIL_PASSWORD,
    },
});

module.exports = transport;
