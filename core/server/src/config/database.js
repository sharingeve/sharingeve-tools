"use strict";

const mongoose = require("mongoose");
const constants = require("./constants");
const app = require("./app");

class Database {
    connect(connection) {
        switch (connection) {
            case constants.DB_CONNECTION.MONGO:
                this.connectMongo();
                break;
            case constants.DB_CONNECTION.MYSQL:
                this.connectMysql();
                break;
            case constants.DB_CONNECTION.POSTGRESQL:
                this.connectPgsql();
                break;
            default:
                console.error(
                    "Database connection not established, for example: mysql, mongo, postgresql...."
                );
                break;
        }
    }

    async connectMongo() {
        try {
            await mongoose.connect(`${app.mongoURL}`).then(() => {
                logger.info("Connect mongodb successfully!!!");
            });
        } catch (error) {
            logger.error("Connect mongodb failure!!!", error);
        }
    }

    connectMysql() {}

    connectPgsql() {}
}

module.exports = new Database();
