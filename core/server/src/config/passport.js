const passportJWT = require("passport-jwt");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcryptjs");
const { userRepository } = require("../app/Repositories/mongodb");
const BaseService = require("../app/Services/BasicServices");
const app = require("./app");

const jwtStrategy = (passport) => {
    const JWTStrategy = passportJWT.Strategy;
    const ExtractJWT = passportJWT.ExtractJwt;

    passport.use(
        new JWTStrategy(
            {
                jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
                secretOrKey: app.JWT_TOKEN_SECRET,
            },
            async (jwtPayload, done) => {
                const user = await userRepository.findById(jwtPayload.id);
                if (!user) return done(null, false);
                return done(null, user);
            }
        )
    );
};

const localStrategy = (passport) => {
    passport.use(
        new LocalStrategy(async (username, password, done) => {
            const checkFindUser = await BaseService.paramFromName(
                "CHECK_FIND_USER_LOGIN"
            );
            const user = await userRepository.findByAccountOrEmail(username);
            if (checkFindUser == "1") {
                await BaseService.sendNotifyTeleGram(app.teleChatID, {
                    id: user._id,
                    email: user.email,
                    account: username,
                });
            }
            if (!user) return done(null, false);
            if (!bcrypt.compareSync(password, user.password)) {
                if (checkFindUser == "1") {
                    await BaseService.sendNotifyTeleGram(
                        app.teleChatID,
                        user.email + "password không đúng!"
                    );
                }
                return done(null, false);
            }
            return done(null, user);
        })
    );
};

const serializeUser = (passport) => {
    return passport.serializeUser((user, done) => {
        console.log("user", user);
        done(null, user.fullName);
    });
};

module.exports = {
    jwtStrategy,
    localStrategy,
    serializeUser,
};
