const { s3Region, s3AccesskeyId, s3SecretKey, s3Bucket } = require("./app");
const { policyPutObject } = require("./aws-policy");
const {
    S3Client,
    PutObjectCommand,
    GetObjectCommand,
    DeleteObjectCommand,
    ListObjectsV2Command,
} = require("@aws-sdk/client-s3");
const { getSignedUrl } = require("@aws-sdk/s3-request-presigner");

/// https://docs.aws.amazon.com/AmazonS3/latest/userguide/service_code_examples_actions.html
const aws = new S3Client({
    region: s3Region,
    credentials: {
        accessKeyId: s3AccesskeyId,
        secretAccessKey: s3SecretKey,
    },
});
const bucketName = s3Bucket;

const s3PutObject = async (Key, Body, ContentType = "") => {
    const params = {
        Bucket: bucketName,
        Key,
        Body,
    };

    if (ContentType) {
        params["ContentType"] = ContentType;
    }

    try {
        const command = new PutObjectCommand(params);

        const response = await aws.send(command);
        return response;
    } catch (error) {
        console.log("Put object in aws s3", error.message);
        return error;
    }
};

const s3GetObject = async (keyObj) => {
    const params = {
        Bucket: bucketName,
        Key: keyObj,
    };

    try {
        const command = new GetObjectCommand(params);

        const response = await aws.send(command);
        const str = await response.Body.transformToString();

        return str; // get thông tin
    } catch (error) {
        console.log("Get object in aws s3", error.message);
    }
};

const s3GetObjectWithSignedUrl = async (keyObj) => {
    const params = {
        Bucket: bucketName,
        Key: keyObj,
    };

    try {
        const response = await getSignedUrl(
            aws,
            new GetObjectCommand(params),
            { expiresIn: 60 } // 60 seconds
        );
        return response;
    } catch (error) {
        console.log("Get object in aws s3", error.message);
    }
};

const s3DeleteObject = async (keyObj) => {
    const params = {
        Bucket: bucketName,
        key: keyObj,
    };

    try {
        const command = new DeleteObjectCommand(params);

        const response = await aws.send(command);
        console.log("response s3DeleteObject>>>", response);
        return response;
    } catch (error) {
        console.log("Get object in aws s3", error.message);
    }
};

const s3ListObject = async (prefix, limit = 100, delimiter = "/, images/") => {
    let tokenContinuation = null;
    let allObjects = [];

    do {
        const params = {
            Bucket: bucketName,
            Prefix: prefix,
            Delimiter: delimiter,
            MaxKeys: limit,
            ContinuationToken: tokenContinuation || undefined,
        };

        try {
            const command = new ListObjectsV2Command(params);
            const results = await aws.send(command);

            const foldersList = results.Contents.filter(
                (item) => item.Size > 0 && item.Key.split("/").length > 1
            );

            allObjects = allObjects.concat(foldersList);

            // command.input.ContinuationToken = results.NextContinuationToken
            tokenContinuation = results.NextContinuationToken;
        } catch (err) {
            console.error(err);
            break; // Dừng vòng lặp nếu có lỗi
        }
    } while (tokenContinuation);

    return allObjects;
};

module.exports = {
    s3PutObject,
    s3GetObject,
    s3DeleteObject,
    s3GetObjectWithSignedUrl,
    s3ListObject,
};
