const express = require("express");
const router = express.Router();
const memoPayRoute = require("./memo-pay.route");

router.use("/memo-pay", memoPayRoute);

module.exports = router;
