const express = require("express");
const router = express.Router();

const memoPayController = require("../../app/Controller/MemoPayController");

router.post("/create", memoPayController.store);

module.exports = router;
