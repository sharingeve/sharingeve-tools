// const morgan = require('morgan');
const siteRoute = require("./site");
const authRoute = require("./auth");
const api = require("./v1/api");
const share = require("./share");
const verifyToken = require("../app/Middleware/VerifyAuthenToken");

function routes(app) {
    // app.use(morgan("combined"));
    app.use("/api/v1", verifyToken.authenToken, api);
    app.use("/api/auth", authRoute);
    app.use("/api/share", share);
    app.use("/", siteRoute);
}

module.exports = routes;
