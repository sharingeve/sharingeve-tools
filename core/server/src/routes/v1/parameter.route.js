const express = require("express");
const router = express.Router();
const ParameterController = require("../../app/Controller/ParameterController");

router.post(
    "/create",
    async (req, res, next) => await ParameterController.create(req, res)
);
router.put(
    "/:id/update",
    async (req, res, next) => await ParameterController.update(req, res)
);
router.delete(
    "/:id/delete",
    async (req, res, next) => await ParameterController.delete(req, res)
);
router.get(
    "/:id",
    async (req, res, next) => await ParameterController.findParamById(req, res)
);
router.get(
    "/",
    async (req, res, next) => await ParameterController.index(req, res)
);
module.exports = router;
