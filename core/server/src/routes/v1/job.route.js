const express = require("express");
const router = express.Router();
const JobController = require("../../app/Controller/JobController");

router.delete(
    "/remove/:id",
    async (req, res, next) => await JobController.removeJob(req, res, next)
);

module.exports = router;
