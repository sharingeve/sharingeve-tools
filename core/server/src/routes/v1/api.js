const express = require("express");
const router = express.Router();
const hasPermission = require("../../app/Middleware/CheckPermission");

const userRoute = require("./user.route");
const memoRoute = require("./memo.route");
const planRoute = require("./plan.route");
const jobRoute = require("./job.route");
const parameterRoute = require("./parameter.route");

router.use("/parameter", hasPermission, parameterRoute);
router.use("/job", jobRoute);
router.use("/plan", planRoute);
router.use("/user", userRoute);
router.use("/memo", memoRoute);

module.exports = router;
