const express = require("express");
const router = express.Router();
const memoController = require("../../app/Controller/MemoController");
const memoPayController = require("../../app/Controller/MemoPayController");
const uploadMiddleware = require("../../app/Middleware/Upload");

router.post(
    "/create",
    uploadMiddleware.single("img_qr"),
    memoController.create
);
router.patch("/confirm/:id/paid", memoPayController.confirmPaid);
router.patch("/upload-qr", uploadMiddleware.single('qr_upload'), memoController.uploadQr);
router.put("/:id/updated", memoController.updated);
router.put("/:id/payment/done", memoController.paymentDone);
router.get("/detail/:id", memoController.show);
router.get("/list", memoController.list);

module.exports = router;
