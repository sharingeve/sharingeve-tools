const express = require("express");
const router = express.Router();
const PlanController = require("../../app/Controller/PlanController");

router.post(
    "/store",
    async (req, res, next) => await PlanController.create(req, res, next)
);
router.post(
    "/check-key",
    async (req, res, next) => await PlanController.checkKeyTelegram(req, res)
);

module.exports = router;
