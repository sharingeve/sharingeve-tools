const express = require("express");
const router = express.Router();

const userController = require("../../app/Controller/UserController");
const uploadMiddleware = require("../../app/Middleware/Upload");

router.get("/find", userController.findUser);
router.get("/:id", userController.show);
router.patch('/:id/upload/avatar', uploadMiddleware.single('update_avatar'), userController.uploadAvatar);
router.patch('/:id/update/pw', userController.updatePw);
router.put("/:id/update", userController.update);
router.delete("/delete/:id", userController.delete);
router.delete("/destroy/:id", userController.destroy);
router.patch("/restore/:id", userController.restore);
router.get("/", userController.index);

module.exports = router;
