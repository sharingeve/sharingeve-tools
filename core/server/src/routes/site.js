const express = require("express");
const router = express.Router();

const siteController = require("../app/Controller/SiteController");
const memoController = require("../app/Controller/MemoController");

router.get("/api/share/:userId/memo/:memoId", memoController.publicShow);
router.get("/aws/:img", siteController.testAws);
router.get("/cdn/:id", siteController.testCDN);
router.get("/api/get/hashKey", siteController.getHashKey);
router.put("/api/restore/job", siteController.restoreJob);
router.get("/api/db/connect", siteController.checkDbConnect);
router.delete("/api/job/all-cancel", siteController.removeAllJob);
router.get("/date_current", siteController.getDateServer);
router.get("/ping", siteController.pingServer);
router.get("/", siteController.index);

module.exports = router;
