const express = require("express");
const router = express.Router();

const userController = require("../app/Controller/UserController");
const siteController = require("../app/Controller/SiteController");

router.post("/create", userController.store);
router.post("/logout", siteController.logout);
router.post("/login", siteController.login);
router.patch("/:id/toggle/admin", userController.toggleAdmin);
router.post("/verify-token", siteController.verifyToken);

module.exports = router;
