const common = require("../../utils/common");
const constant = require("../../config/constants");

function convertDataByType(val, type) {
    switch (type) {
        case constant.PARAMETER.TYPE.OBJECT:
            return common.convertDataToObject(val);
        case constant.PARAMETER.TYPE.ARRAY:
            return common.convertDataToArray(val);
        default:
            return JSON.parse(val);
    }
}

module.exports = function (request, select) {
    if (
        (request instanceof Array && request.length == 0) ||
        Object.keys(request).length == 0
    ) {
        return request;
    }
    if (common.isArrayEmpty(request) && select == "array") {
        return [];
    }

    if (common.isObjectEmpty(request) && select == "obj") {
        return {};
    }

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            const formated = {
                id: item._id,
                name: item.name,
                value: convertDataByType(item.valueParam, item.typeParam),
                type: item.typeParam,
                description: item.description,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    return {
        id: request._id,
        name: request.name,
        value: convertDataByType(request.valueParam, request.typeParam),
        type: request.typeParam,
        description: request.description,
    };
};
