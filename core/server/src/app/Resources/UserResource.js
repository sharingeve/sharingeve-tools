const common = require("../../utils/common");
const configApp = require("../../config/app");

module.exports.usersResource = function (request, select) {
    if ((common.isEmpty(request) || request.length) && select == "array")
        return [];

    if ((common.isEmpty(request) || request.length) && select == "obj")
        return {};

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            let urlAvatar = item?.avatar
                ? `${configApp.cdnBaseUrl}${configApp.cdnName}/image/upload/${item?.avatar}.webp`
                : "";

            const formated = {
                id: item.id,
                full_name: item.fullName,
                email: item.email,
                account: item.account,
                isClient: !item.admin,
                phoneNumber: item.phoneNumber,
                avatar: urlAvatar,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    let urlAvatarObj = request?.avatar
        ? `${configApp.cdnBaseUrl}${configApp.cdnName}/image/upload/${request?.avatar}.webp`
        : "";

    return {
        id: request.id,
        full_name: request.fullName,
        email: request.email,
        account: request.account,
        isClient: !request.admin,
        phoneNumber: request.phoneNumber,
        avatar: urlAvatarObj,
        createdAt: request.createdAt,
        updatedAt: request.updatedAt,
    };
};
