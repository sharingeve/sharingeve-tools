const common = require("../../utils/common");
const constants = require("../../config/constants");
module.exports.memoPayResource = function (request, select) {
    if (common.isArrayEmpty(request) && select == "array") {
        return [];
    }

    if (common.isObjectEmpty(request) && select == "obj") {
        return {};
    }

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            const formated = {
                id: item._id,
                amount: item.amount,
                bankPay: item.bank_pay,
                creditors: item.creditors,
                memoId: item.memo_id,
                payStatus: item.pay_status,
                payType:
                    item.pay_type == 0
                        ? constants.MEMO.LIST_PAY.BANK_QR
                        : constants.MEMO.LIST_PAY.BANK_TRANSFER,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    return {
        id: request._id,
        amount: request.amount,
        bankPay: request.bank_pay,
        creditors: request.creditors,
        memoId: request.memo_id,
        payStatus: request.pay_status,
        payType:
            request.pay_type == 0
                ? constants.MEMO.LIST_PAY.BANK_QR
                : constants.MEMO.LIST_PAY.BANK_TRANSFER,
        createdAt: request.createdAt,
        updatedAt: request.updatedAt,
    };
};
