const common = require("../../utils/common");

const formatStructure = (structure = []) => {
    const cloneStructure = structure.map((item) => ({
        id: item._id,
        price: Number(item.price),
        reason: item.reason,
        createDate: item.create_date,
    }));

    return cloneStructure;
};
module.exports.memoResource = function (request, select) {
    if ((common.isEmpty(request) || request.length) && select == "array")
        return [];

    if ((common.isEmpty(request) || request.length) && select == "obj")
        return {};

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            const formated = {
                id: item._id,
                full_name: item.fullName,
                structure: formatStructure(item.structure),
                amount: item.amountTotal,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    return {
        id: request._id,
        full_name: request.fullName,
        structure: formatStructure(request.structure),
        amount: request.amountTotal,
        createdAt: request.createdAt,
        updatedAt: request.updatedAt,
    };
};
