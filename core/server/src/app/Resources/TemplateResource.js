const common = require("../../utils/common");

module.exports.planResource = function (request, select) {
    if (common.isArrayEmpty(request) && select == "array") {
        return [];
    }

    if (common.isObjectEmpty(request) && select == "obj") {
        return {};
    }

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            const formated = {
                id: item._id,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    return {
        id: request._id,
    };
};
