const common = require("../../utils/common");
const configApp = require("../../config/app");
const modelCs = require("../../config/model-constants");

module.exports.memoExtraResource = function (request, select) {
    if (common.isArrayEmpty(request) && select == "array") {
        return [];
    }

    if (common.isObjectEmpty(request) && select == "obj") {
        return {};
    }

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            let imgQr = item.memo_img_qr
                ? `${configApp.cdnBaseUrl}${configApp.cdnName}/image/upload/${item.memo_img_qr}.webp`
                : "/assets/img/not_found_image.png";
            if (item.memo_cloud == modelCs.CLOUD_SAVE.AWS) {
                imgQr = `${configApp.s3BaseUrl}${item.memo_img_qr}`;
            }
            const formated = {
                id: item._id,
                memoId: item.memo_id,
                imgQr: imgQr,
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    let imgQrObj = request.memo_img_qr
        ? `${configApp.cdnBaseUrl}${configApp.cdnName}/image/upload/${request.memo_img_qr}.webp`
        : "/assets/img/not_found_image.png";
    if (request.memo_cloud == modelCs.CLOUD_SAVE.AWS) {
        imgQrObj = `${configApp.s3BaseUrl}${request.memo_img_qr}`;
    }
    return {
        id: request._id,
        memoId: request.memo_id,
        imgQr: imgQrObj,
        createdAt: request.createdAt,
        updatedAt: request.updatedAt,
    };
};
