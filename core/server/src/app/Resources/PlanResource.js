const common = require("../../utils/common");
const constants = require("../../config/constants");
module.exports.planResource = function (request, select) {
    if (common.isArrayEmpty(request) && select == "array") {
        return [];
    }

    if (common.isObjectEmpty(request) && select == "obj") {
        return {};
    }

    if (common.isArray(request) && request.length > 0) {
        let transformed = [];
        request.map((item) => {
            const formated = {
                id: item._id,
                creditor: item.creditor,
                titlePlan: item.title_plan,
                dateStart: item.date_start,
                planDetail: item.plan_detail,
                typeAction: item.type_action,
                typeCycle: item.type_cycle,
                timeCycle: item.time_cycle,
                reDateCycle: item.re_date_cycle,
                updatedAt: item.updatedAt,
            };
            transformed.push(formated);
        });
        return transformed;
    }

    return {
        id: request._id,
        creditor: request.creditor,
        titlePlan: request.title_plan,
        dateStart: request.date_start,
        planDetail: request.plan_detail,
        typeAction: request.type_action,
        typeCycle: request.type_cycle,
        timeCycle: request.time_cycle,
        reDateCycle: request.re_date_cycle,
        updatedAt: request.updatedAt,
    };
};
