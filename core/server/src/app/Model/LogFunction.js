/**
  * DB design
    id: ObjectId
    type: string
    status: string,
    id_record: ObjectId,
    content: string
    ip: String
    useAgent: String
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const configApp = require("../../config/app");
const utils = require("../../utils/common");
const modelConstants = require("../../config/model-constants");
const Schema = mongoose.Schema;

const LogFunction = new Schema(
    {
        type: { type: String, require: true },
        status: {
            type: String,
            index: true,
            default: modelConstants.LOG_STATUS.SUCCESS,
        },
        id_record: { type: String },
        ip: { type: String },
        useAgent: { type: String },
        content: { type: String, require: true },
    },
    { timestamps: true }
);

module.exports = mongoose.model("LogFunction", LogFunction);
