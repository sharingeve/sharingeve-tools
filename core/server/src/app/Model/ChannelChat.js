/**
  * DB design
    id: ObjectId
    chatId: number,
    channel: string,
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChannelChat = new Schema(
    {
        chatId: { type: Number, index: true, require: true, unique: true },
        channel: { type: String },
    },
    { timestamps: true }
);

module.exports = mongoose.model("ChannelChat", ChannelChat);
