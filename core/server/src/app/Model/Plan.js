/**
  * DB design
    id: ObjectId
    creditor: ObjectId,
    title_plan: string,
    date_start: date,
    type_action: string, // lặp: loop, 1 lần: only
    type_cycle?: string, // minute, hour, day, week, month
    time_cycle?: number,
    plan_detail: Array,
    re_date_cycle?: date,
    channel_tele: string,
    channel_fb: string,
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PlanDetailStructure = new Schema();
PlanDetailStructure.add({
    planName: String,
    planNote: String,
});

const Plan = new Schema(
    {
        creditor: {
            type: Schema.Types.ObjectId,
            require: true,
            ref: "User",
            index: true,
        },
        title_plan: { type: String, required: true, index: true },
        date_start: Date,
        plan_detail: { type: [PlanDetailStructure], required: true },
        type_action: { type: String, required: true, index: true },
        type_cycle: String,
        time_cycle: Number,
        re_date_cycle: { type: Date, default: null },
        channel_tele: {
            type: String,
            require: true,
            index: true,
        },
        channel_fb: {
            type: String,
            require: true,
            index: true,
        },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Plan", Plan);
