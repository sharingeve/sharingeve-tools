/**
  * DB design
    id: ObjectId
    memo_id: ObjectId
    memo_cloud: string
    memo_img_qr: string,
    memo_other_img_type: string, // String, Object
    memo_other_img: String // get data => JSON.parse save data: JSON.stringify if data is obj
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const modelCst = require("../../config/model-constants");
const Schema = mongoose.Schema;

const MemoExtra = new Schema(
    {
        memo_id: {
            type: Schema.Types.ObjectId,
            require: true,
            index: true,
            ref: "Memo",
        },
        memo_cloud: {
            type: String,
            index: true,
            require: true,
            default: modelCst.CLOUD_SAVE.CLOUDINARY,
        },
        memo_img_qr: { type: String, default: null },
        memo_other_img_type: {
            type: String,
            default: modelCst.MEMO_EXTRA_TYPE_OTHER.STRING,
        },
        memo_other_img: { type: String },
    },
    { timestamps: true }
);

module.exports = mongoose.model("MemoExtra", MemoExtra);
