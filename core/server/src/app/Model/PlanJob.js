/**
  * DB design
    id: ObjectId
    planId: ObjectId,
    nameJob: string,
    templateJob: string,
    date_start: date,
    statusJob: string, // wait, active, cancel
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const constants = require("../../config/constants");
const Schema = mongoose.Schema;

const PlanJob = new Schema(
    {
        planId: {
            type: Schema.Types.ObjectId,
            ref: "Plan",
            unique: true,
        },
        templateJob: {type: String, require: true},
        nameJob: { type: String },
        date_start: { require: true, type: Date },
        statusJob: { type: String, default: constants.JOB_PLAN.STATUS.WAITING },
    },
    { timestamps: true }
);

module.exports = mongoose.model("PlanJob", PlanJob);
