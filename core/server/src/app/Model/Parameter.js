/**
  * DB design
    id: ObjectId
    name: String,
    valueParam: String,
    typeParam: String // String|Array|Object
    description: String,
    creditor: ObjectId,
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const parameter = new Schema(
    {
        name: { type: String, require: true, unique: true, index: true },
        typeParam: { type: String, require: true, index: true },
        valueParam: { type: String },
        description: { type: String },
        creditor: {
            type: Schema.Types.ObjectId,
            require: true,
            ref: "User",
            index: true,
        },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Parameter", parameter);
