"use strict";

const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");
const SALT_WORK_FACTOR = 10;

const User = new Schema(
    {
        fullName: { type: String, max: 255, trim: true, required: true },
        email: {
            type: String,
            trim: true,
            max: 255,
            lowercase: true,
            required: true,
            unique: true,
            index: { unique: true },
        },
        account: {
            type: String,
            trim: true,
            unique: true,
            index: {
                unique: true,
                sparse: true,
            },
        },
        password: { type: String, required: true },
        phoneNumber: { type: Number, match: /(84|0[3|5|7|8|9])+([0-9]{8})\b/g },
        type: {
            type: Number,
            default: 0,
        },
        admin: {
            type: Boolean,
            default: false,
        },
        avatar: { type: String },
        avatar_info: { type: String },
    },
    {
        timestamps: true,
    }
);

User.pre("save", function (next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified("password")) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);
            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

User.pre("findOneAndUpdate", function (next) {
    var user = this._update;

    if (!user) next(new Error("Không có dữ liệu update"));

    if (user?.password) {
        // generate a salt
        bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
            if (err) return next(err);

            // hash the password using our new salt
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) return next(err);
                // override the cleartext password with the hashed one
                user.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

User.plugin(mongooseDelete, {
    overrideMethods: "all",
    deletedAt: true,
    indexFields: ["deletedAt"],
});

module.exports = mongoose.model("User", User);
