const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MemoStructure = new mongoose.Schema();
MemoStructure.add({
    price: Number,
    reason: String,
    create_date: Date,
});

const Memo = new Schema(
    {
        fullName: { type: String, require: true, index: true },
        structure: { type: [MemoStructure], required: true },
        memo_extra_id: {
            type: Schema.Types.ObjectId,
            index: true,
            ref: "MemoExtra",
        },
        amountTotal: { type: Number, default: 0 },
        creditors: { type: String, require: true, index: true },
    },
    { timestamps: true }
);

module.exports = mongoose.model("Memo", Memo);
