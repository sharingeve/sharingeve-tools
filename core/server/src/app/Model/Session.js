const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Session = new Schema(
    {
        // _id: Schema.Types.ObjectId,
        expires: { type: Date },
        session: { type: Object },
    },
    { timestamps: false }
);

module.exports = mongoose.model("Session", Session);
