/**
  * DB design
    id: ObjectId
    memo_id: ObjectId
    creditors: ObjectId
    bank_pay: string // Tech | Vcb | Mbb
    amount: number
    pay_type: number // 0: vietqr| 1: qr_bank
    pay_status: “wait” // wait | fail | done
    pay_envidence?: string // image
    created_at: date
    updated_at: date
 */

const mongoose = require("mongoose");
const handlebars = require("handlebars");
const path = require("path");
const modelConstants = require("../../config/model-constants");
const configApp = require("../../config/app");
const mailUtils = require("../../utils/nodemailer-utils");
const utils = require("../../utils/common");
const Schema = mongoose.Schema;

function capitalize(val) {
    if (typeof val !== "string") val = "";
    return val.charAt(0).toUpperCase() + val.substring(1);
}

const MemoPay = new Schema(
    {
        memo_id: {
            type: Schema.Types.ObjectId,
            require: true,
            index: true,
            ref: "Memo",
        },
        creditors: {
            type: Schema.Types.ObjectId,
            require: true,
            ref: "User",
            index: true,
        },
        bank_pay: { type: String, require: true, index: true },
        amount: { type: Number, require: true },
        pay_type: { type: Number, require: true },
        pay_status: { type: String, default: modelConstants.PAY_STATUS.WAIT },
        pay_envidence: { type: String },
    },
    { timestamps: true }
);

MemoPay.path("bank_pay").set(function (v) {
    return capitalize(v);
});

MemoPay.pre("save", function (next, options) {
    mailUtils.renderHtmlFile(
        path.join(__dirname, "../../template-mail/memo-pay.html"),
        function (err, html) {
            if (err) {
                return;
            }
            const template = handlebars.compile(html);
            const replacements = {
                memoId: options.memo_id,
                payBank: capitalize(options.bank),
                payAmount: utils.formatNumberWithThreeDigits(options.amount),
                debtorName: options.debtor,
            };
            const htmlToSend = template(replacements);
            mailUtils.transporterSendMail(
                configApp.MAIL_FROM_ADDRESS,
                options.creditorsEmail,
                `Hóa đơn ${options.memo_id} được thanh toán`,
                "",
                htmlToSend
            );
        }
    );

    next();
});

module.exports = mongoose.model("MemoPay", MemoPay);
