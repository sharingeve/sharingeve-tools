const moment = require("moment");
const utils = require("../../utils/common");
const planRepo = require("../Repositories/mongodb/PlanRepository");
const planJobRepo = require("../Repositories/mongodb/PlanJobRepositories");
const ChannelChatRepo = require("../Repositories/mongodb/ChannelChatRepositories");
const { planResource } = require("../Resources/PlanResource");
const BaseService = require("./BasicServices");
const SocialService = require("./SocialService");
const JobService = require("./JobService");
const teleJob = require("../job/TelegramJob");
const { responseDto } = require("../../utils/response");
const constants = require("../../config/constants");
const timeUtils = require("../../utils/time");

const TYPE_LOG = "PLAN";
const fieldsCreateRequired = [
    "date_start",
    "plan_detail",
    "type_action",
];

const paramsCreateRequired = (data) => {
    if (!utils.isObject(data)) {
        return "Không có dữ liệu";
    }
    if (data?.type_action == "loop") {
        fieldsCreateRequired.push(["time_cycle", "type_cycle"]);
    }
    return fieldsCreateRequired.filter((field) => !data.hasOwnProperty(field));
};
const paramsCreateValid = (data) => {
    if (!data["date_start"]) {
        return "Thiếu thông tin: ngày thực hiện kế hoạch";
    }
    if (!data["title_plan"]) {
        return "Thiếu thông tin: Tên kế hoạch";
    }
    if (
        !utils.isArray(data["plan_detail"]) ||
        utils.isArrayEmpty(data["plan_detail"])
    ) {
        return "Thiếu thông tin: chi tiết kế hoạch";
    }
    if (!utils.isArrayEmpty(data["plan_detail"])) {
        if (data["plan_detail"].filter((item) => !item?.planName).length > 0) {
            return "Thông tin không hợp lệ: kiểm tra lại chi tiết kế hoạch phải có tên chi tiết.";
        }
    }

    if (!data["type_action"]) {
        return "Thiếu thông tin: thực hiện kế hoạch vòng lặp hay một lần.";
    }

    if (data["type_action"] == "loop") {
        if (!data["type_cycle"]) {
            return "Thiếu thông tin: chu kỳ thực hiện như phút, ngày, tuần";
        }
        if (!data["time_cycle"]) {
            return "Thiếu thông tin: thời gian chu kỳ thực hiện như 15 phút, 1 ngày, 1 tuần, ...";
        }
    }

    if (!data["key"]) {
        return "Thiếu thông tin: thiếu mã xác thực.";
    }

    const current = timeUtils.getTime();
    const dateStart = timeUtils.getTimeByParam(data["date_start"]);
    if (timeUtils.compareTwoDateBefore(dateStart, current)) {
        return "Thời gian chạy thông báo phải lớn hơn ngày hiện tại.";
    }

    if (
        current.date() == dateStart.date() &&
        dateStart.hour() - current.hour() < 2
    ) {
        return "Giờ chạy thông báo phải sau 2 giờ kể từ khi tạo.";
    }

    // return "";
};

const displayCycle = (type_cycle, time_cycle) => {
    const nameType = {
        minute: "Phút",
        hour: "Giờ",
        day: "Ngày",
        week: "Tuần",
        month: "Tháng",
    };

    return {
        type: nameType[type_cycle],
        time: `${time_cycle} ${nameType[type_cycle]}`,
    };
};

const createPlanService = async (data) => {
    const msgErr = {
        not_exist_input: "Hãy gửi lên đủ thông tin của {{input}}",
    };
    if (paramsCreateRequired(data).length > 0) {
        return {
            ...responseDto,
            status: false,
            error: !utils.isArrayEmpty(paramsCreateRequired(data))
                ? msgErr.not_exist_input.replace(
                      "{{input}}",
                      paramsCreateRequired(data).join(", ")
                  )
                : paramsCreateRequired(data),
        };
    }
    const validParams = paramsCreateValid(data);
    if (validParams) {
        return {
            ...responseDto,
            status: false,
            error: validParams,
        };
    }
    const channel = await ChannelChatRepo.findChatIdById(data.key);
    if (!channel) {
        return {
            ...responseDto,
            status: false,
            error: "Thông tin xác thực không chính xác.",
        };
    }
    const updateDto = {
        creditor: data.creditors._id,
        title_plan: data.title_plan,
        // date_start: timeUtils.getTimeUtcByParam(data.date_start),
        date_start: new Date(data.date_start),
        plan_detail: data.plan_detail,
        type_action: data.type_action,
        type_cycle: data?.type_cycle,
        time_cycle: data?.time_cycle,
        channel_tele: channel.id,
        channel_fb: "",
    };

    const create = await planRepo.createPlan(updateDto);
    if (!create) {
        await BaseService.createLog(
            `Tạo plan không thành công: ${JSON.stringify(updateDto)}`,
            null,
            TYPE_LOG
        );
        return {
            ...responseDto,
            status: false,
            error: "Lỗi tạo plan",
        };
    }

    const { type, time } = displayCycle(data?.type_cycle, data?.time_cycle);
    let planDetailString = "";
    data.plan_detail.map((item, index) => {
        planDetailString += `<tr>
            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">${index + 1}</td>
            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">${item.planName}</td>
            <td style="border: 1px solid #dddddd; text-align: left; padding: 8px;">${item.planNote}</td>
        </tr>`;
    });
    const tablePlanDetail = `<table style="border-collapse: collapse; width: 100%;">
        <tr>
            <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">STT</th>
            <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Tên chi tiết</th>
            <th style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Ghi chú</th>
        </tr>
        ${planDetailString}
    </table>`;
    const paramsEmailSend = {
        full_name: data.creditors.fullName,
        title_plan: data.title_plan,
        date_start: moment(data.date_start, "DD-MM-YYYY").format("DD-MM-YYYY"),
        plan_detail: tablePlanDetail,
        type_action: data.type_action == "loop" ? "Lặp lại" : "Một lần",
        type_cycle: data?.type_cycle ? type : "",
        time_cycle: data?.time_cycle ? time : "",
    };

    let teleString = `Tạo kế hoạch [${data.title_plan}] thành công.\n`;
    teleString += `\nVới các thông tin: `;
    teleString += `\nTên kế hoạch: ${data.title_plan}`;
    teleString += `\nNgày thực hiện: ${new Date(data.date_start)}`;
    // teleString += `\nNgày thực hiện: ${timeUtils.getTimeUtcByParam(data.date_start).format("DD-MM-YYYY")}`;
    teleString += `\nKiểu thực hiện: ${data.type_action == "loop" ? "Lặp lại" : "Một lần"}`;
    teleString += `\nChu kỳ thực hiện lặp lại: ${data?.type_cycle ? type : ""}`;
    teleString += `\nThời gian lặp lại: ${data?.time_cycle ? time : ""}`;

    await Promise.all([
        // BaseService.sendMail({
        //     template: "../../template-mail/plan-create.html",
        //     params: paramsEmailSend,
        //     id: create._id,
        //     mailToSend: data.creditors.email,
        //     title: "[Noreply] Thông báo tạo kế hoạch thành công",
        // }),
        SocialService.telegramSendMessage(channel.chatId, teleString),
    ]);
    let contentSendJob = `Kế hoạch: ${data.title_plan} \nCác việc cần thực hiện:`;
    data.plan_detail.forEach((item, index) => {
        contentSendJob += `\n${index}. ${item.planName} - Ghi chú: ${item.planNote}`;
    });
    await JobService.jobAdd(
        channel.chatId,
        create.id,
        data.type_action,
        data.date_start,
        data?.type_cycle,
        data?.time_cycle,
        contentSendJob,
        data.title_plan
    );

    return {
        ...responseDto,
        data: planResource(create, "obj"),
    };
};

const checkKeyTelegramService = async (data) => {
    const key = data.key;

    const check = await ChannelChatRepo.findChatIdById(key);

    if (check.id) {
        return {
            ...responseDto,
            status: true,
            data: "Key xác thực thành công.",
        };
    }
    return {
        ...responseDto,
        error: "Key xác thực không đúng. Hãy kiểm tra lại. \nNếu như key do bot trả về vui lòng liên hệ admin.",
        status: false,
    };
};
module.exports = { createPlanService, checkKeyTelegramService };
