const MemoRepo = require("../Repositories/mongodb/MemoRepository");
const MemoPayRepo = require("../Repositories/mongodb/MemoPayRepository");
const MemoExtraRepo = require("../Repositories/mongodb/MemoExtraRepository");
const userRepo = require("../Repositories/mongodb/UserRepository");
const Utils = require("../../utils/common");
const BasicService = require("./BasicServices");
const cdnService = require("./CDNService");
const S3Service = require("./S3Service");
const { memoResource } = require("../Resources/MemoResource");
const { memoPayResource } = require("../Resources/MemoPayResource");
const { memoExtraResource } = require("../Resources/MemoExtraResource");
const ModelConstants = require("../../config/model-constants");
const { usersResource } = require("../Resources/UserResource");

const TYPE_LOG = "MEMO";

const listMemoService = async (
    searchParams = {},
    limit,
    page,
    pathname,
    userId
) => {
    let search = {};

    if (!Utils.isObjectEmpty(searchParams)) {
        search = { ...searchParams };
    }

    let offset = page - 1;

    if (offset < 0) {
        offset = 0;
    }

    const totalMemo = await MemoRepo.countTotal(userId);
    const listMemo = await MemoRepo.listMemo(search, limit, offset);

    const paginator = BasicService.paginate(totalMemo, pathname, limit, page);

    return {
        ...paginator,
        data: listMemo,
    };
};

// ================================

const createMemoService = async (data) => {
    let resDto = {
        status: true,
        id: "",
    };
    const validate = validDataMemo(data);

    if (validate.status) {
        return { ...resDto, status: false, error: validate.message };
    }
    const checkExistName = await MemoRepo.findByName(
        data.full_name,
        data.creditors
    );

    if (checkExistName) {
        const getMemoPayByMemoId = await MemoPayRepo.findMemoPayByMemoId(
            checkExistName._id,
            data.creditors
        );
        if (
            !Utils.isObjectEmpty(getMemoPayByMemoId) &&
            getMemoPayByMemoId.pay_status == ModelConstants.PAY_STATUS.WAIT
        ) {
            return {
                ...resDto,
                status: false,
                error: `Người này đã tồn tại và đang đợi xác nhận thanh toán nên không thể thêm nợ mới.
                Hãy kiểm tra id ${checkExistName._id}`,
            };
        }

        const amountTotal = checkExistName.structure.reduce(
            (total, current) => total + Number(current.price),
            0
        );
        const updateDto = {
            structure: [
                {
                    price: data.price,
                    reason: data.reason,
                    create_date: data.created_date,
                },
                ...checkExistName.structure,
            ],
            amountTotal: Number(amountTotal) + Number(data.price),
        };

        await BasicService.createLog(
            `Đang xử lý update mở rộng cho memo [${checkExistName._id}]`,
            checkExistName._id,
            TYPE_LOG,
            2
        );
        if (data?.file_upload) {
            const upload = await handleUploadCDN(
                data.file_upload,
                checkExistName._id
            );
            let fileName = upload.fileName || "";
            let imgType = upload.imgType || "";
            let imgOther = upload.imgOther || "";

            const memoExtra = await MemoExtraRepo.findMemoExtraById(
                checkExistName.memo_extra_id
            );
            if (memoExtra) {
                if (memoExtra.memo_img_qr) {
                    await cdnService.deleteImgCDN([memoExtra.memo_img_qr]);
                }

                const paramsMemoExtra = {
                    memo_img_qr: fileName,
                    memo_other_img_type: imgType,
                    memo_other_img: imgOther,
                };
                await MemoExtraRepo.updateMemoExtra(
                    memoExtra._id,
                    paramsMemoExtra
                );
            } else {
                const paramsMemoExtra = {
                    memo_id: checkExistName._id,
                    memo_img_qr: fileName,
                    memo_other_img_type: imgType,
                    memo_other_img: imgOther,
                };
                const createExtra =
                    await MemoExtraRepo.createMemoExtra(paramsMemoExtra);
                if (createExtra) {
                    updateDto.memo_extra_id = createExtra._id;
                }
            }
        }

        const update = await MemoRepo.updateMemo(checkExistName._id, updateDto);
        if (update) {
            await BasicService.createLog(
                `Update mở rộng cho memo [${checkExistName._id}] thành công.`,
                checkExistName._id,
                TYPE_LOG,
                1
            );
            return {
                ...resDto,
                id: checkExistName._id,
            };
        }

        return {
            ...resDto,
            status: false,
            error: "Lỗi tạo memo",
        };
    }

    const revertData = {
        fullName: data.full_name,
        structure: [
            {
                price: data.price,
                reason: data.reason,
                create_date: data.created_date,
            },
        ],
        amountTotal: data.price,
        creditors: data.creditors,
    };

    const insert = await MemoRepo.createMemo(revertData);

    if (insert) {
        // await handleUploadS3(data?.file_upload, insert._id);
        const upload = await handleUploadCDN(data?.file_upload, insert._id);
        let fileName = upload.fileName || "";
        let imgType = upload?.imgType || "";
        let imgOther = upload?.imgOther || "";

        await BasicService.createLog(
            `Tạo mở rộng cho memo [${insert._id}]`,
            insert._id,
            TYPE_LOG,
            1
        );
        const paramsMemoExtra = {
            memo_id: insert._id,
            memo_img_qr: fileName,
            memo_other_img_type: imgType,
            memo_other_img: imgOther,
        };
        const createExtra =
            await MemoExtraRepo.createMemoExtra(paramsMemoExtra);
        if (createExtra) {
            await MemoRepo.updateMemo(insert._id, {
                memo_extra_id: createExtra._id,
            });
        }
        return {
            ...resDto,
            id: insert._id,
        };
    }

    await BasicService.createLog(`Lỗi tạo cho memo`, "", TYPE_LOG);
    return {
        ...resDto,
        status: false,
        error: "Lỗi tạo memo",
    };
};
const handleUploadS3 = async (fileUpload, memoId) => {
    return;
    const originalname = fileUpload?.originalname;
    const bodyFile = fileUpload?.buffer;
    const contentType = fileUpload?.mimetype;
    let fileName = "";
    if (originalname?.length > 0) {
        const convertOriginalName = originalname.split(".");
        fileName = `images/${memoId}.${
            convertOriginalName[convertOriginalName.length - 1]
        }`;
        await cdnService.uploadImgCDN(fileName, bodyFile, contentType, memoId);
    }
    return { fileName };
};
const handleUploadCDN = async (fileUpload, memoId) => {
    let fileName = "";
    let imgType = "";
    let imgOther = "";

    const bodyFile = fileUpload?.buffer;
    const contentType = fileUpload?.mimetype;

    if (contentType?.length > 0) {
        const upload = await cdnService.uploadImgCDN(
            bodyFile,
            contentType,
            memoId,
            "memos"
        );

        if (upload?.asset_id) {
            fileName = upload.public_id;
            imgType = ModelConstants.MEMO_EXTRA_TYPE_OTHER.OBJECT;
            imgOther = JSON.stringify(upload);
        }
    }

    return {
        fileName,
        imgType,
        imgOther,
    };
};
const validDataMemo = (data) => {
    let isValid = {
        status: false,
        message: "",
    };

    if (!data?.full_name) {
        isValid.message = "Hãy nhập họ tên người nợ!";
        isValid.status = true;
    }

    if (!data?.price) {
        isValid.message = "Hãy nhập số tiền cần trả!";
        isValid.status = true;
    }

    if (!data?.created_date) {
        isValid.message = "Hãy nhập ngày bắt đầu nợ!";
        isValid.status = true;
    }

    if (data?.file_upload) {
        const mimeType = data?.file_upload?.mimetype;
        if (!Utils.fileUploadIsImg(mimeType)) {
            isValid.message =
                "File upload không phải file ảnh. Hãy kiểm tra lại!";
            isValid.status = true;
        }
    }

    return isValid;
};

// ================================

const detailMemoService = async (id, userId) => {
    let status = true;

    const [detail, memoPayment, memoPayList, memoExtra] = await Promise.all([
        MemoRepo.findMemoById(id, userId),
        MemoPayRepo.findMemoPayByMemoId(id, userId),
        MemoPayRepo.getListMemoPay(id, userId),
        MemoExtraRepo.findMemoExtraByMemoId(id),
    ]);

    if (!detail) {
        status = false;

        return {
            status,
            data: {},
            error: "Not found memo",
        };
    }

    return {
        status,
        data: {
            memoDetail: memoResource({ ...detail._doc }),
            memoPaymentByMemoId: memoPayResource(memoPayment, "obj"),
            listMemoPay: memoPayResource(memoPayList, "array"),
            memoExtra: memoExtraResource(memoExtra, "obj"),
        },
    };
};

// ================================

const updatedMemoService = async (id, data) => {
    const detail = await MemoRepo.findMemoById(id, data.creditors);
    if (!detail) {
        return {
            status: false,
            data: {},
            error: "Not found memo",
        };
    }

    const memoPayDetail = await MemoPayRepo.findMemoPayByMemoId(
        id,
        data.creditors
    );
    if (memoPayDetail?.pay_status == ModelConstants.PAY_STATUS.WAIT) {
        return {
            status: false,
            data: {},
            error: `Hóa đơn đang chờ xác nhận thanh toán nên không thể thêm nợ mới.`,
        };
    }

    let structure = [...detail.structure];
    let amount = Number(detail.amountTotal);
    if (data?.structure) {
        // delete structure
        let cloneStructureData = JSON.parse(data?.structure);
        let cloneStructure = [];
        cloneStructureData.map((item) => {
            cloneStructure.push({
                price: Number(item?.priceNumber),
                reason: item?.reason,
                create_date: item?.createDate,
            });
        });
        amount = cloneStructure?.reduce(
            (accumulator, currentVal) => accumulator + Number(currentVal.price),
            0
        );
        structure = [...cloneStructure];
    }

    const isNewMemo = data.price && data.reason && data.created_date;
    if (isNewMemo) {
        // update addition memo
        const validate = validDataMemo(data);

        if (validate.status) {
            return {
                status: false,
                data: detailMemoData.data,
                error: validate.message,
            };
        }

        const newStructure = {
            price: Number(data.price),
            reason: data.reason,
            create_date: data.created_date,
        };

        amount = Number(data.price) + amount;
        structure = [newStructure, ...structure];
    }

    let importStructure = [];
    if (data?.structureImport) {
        const parseStructureImport = JSON.parse(data?.structureImport);
        parseStructureImport.map((item) => {
            importStructure.push({
                price: Number(item?.priceNumber),
                reason: item?.reason,
                create_date: item?.createDate,
            });
        });

        if (importStructure.length > 0) {
            structure = [...importStructure, ...structure];
            amount = structure?.reduce(
                (accumulator, currentVal) =>
                    accumulator + Number(currentVal.price),
                0
            );
        }
    }

    let updateDto = {
        fullName: data.full_name,
        structure: structure,
        amountTotal: amount,
    };

    const update = await MemoRepo.updateMemo(id, updateDto);

    if (!update) {
        return {
            status: false,
            data: {},
            error: "Updated fail!",
        };
    }

    return {
        status: true,
        data: memoResource(update),
    };
};

// ================================

const sharingMemoService = async (userId, memoId) => {
    const isExistUser = await userRepo.findById(userId);

    if (!isExistUser) {
        return {
            status: false,
            data: {},
            error: "User not exist.",
        };
    }

    const [memoDetail, memoPayment, memoExtra] = await Promise.all([
        MemoRepo.findMemoById(memoId, userId),
        MemoPayRepo.findMemoPayByMemoId(memoId, userId),
        MemoExtraRepo.findMemoExtraByMemoId(memoId),
    ]);
    if (!memoDetail) {
        return {
            status: false,
            data: {},
            error: "Record not exist.",
        };
    }
    const convertMemoPay = memoPayment ? memoPayResource(memoPayment) : {};
    const resDto = {
        memoDetail: memoResource(memoDetail),
        memoPayment: {
            id: convertMemoPay?.id,
            payStatus: convertMemoPay?.payStatus,
            createdAt: convertMemoPay?.createdAt,
        },
        memoExtra: memoExtraResource(memoExtra, "obj"),
        user: usersResource(isExistUser, "obj"),
    };

    return {
        status: true,
        data: resDto,
    };
};

// ================================

const memoPaymentDoneService = async (id, userId) => {
    const resDto = {
        status: true,
    };

    const detail = await MemoRepo.findMemoById(id, userId);
    if (!detail) {
        return {
            ...resDto,
            status: false,
            error: "Not found memo",
        };
    }

    const memoPayDetail = await MemoPayRepo.findMemoPayByMemoId(id, userId);
    if (memoPayDetail?._id) {
        const updateMemoPayDto = {
            pay_status: ModelConstants.PAY_STATUS.DONE,
        };

        await MemoPayRepo.updateMemoPayment(
            memoPayDetail._id,
            updateMemoPayDto
        );
    }
    const updateDto = {
        amountTotal: 0,
        structure: [],
    };
    const update = await MemoRepo.updateMemo(id, updateDto);
    if (update) {
        return {
            ...resDto,
            data: update._id,
        };
    }

    return {
        ...resDto,
        status: false,
        error: "Update Fail!",
    };
};

// ================================

const updateQrImgService = async (file, data) => {
    let result = {
        status: true,
        data: {},
        error: "",
    };

    const msg = {
        not_found: "Không tìm thấy dữ liệu cần update",
        delete_fail: "Có lỗi xảy ra. Xóa ảnh cũ không thành công",
        update_fail: "Có lỗi xảy ra. Update data không thành công",
    };

    const memoExtra = await MemoExtraRepo.findMemoExtraById(data.extraId);
    if (!memoExtra) {
        return {
            ...result,
            status: false,
            error: msg.not_found,
        };
    }
    const memoExists = await MemoRepo.findMemoById(
        memoExtra.memo_id,
        data.creditors
    );

    if (!memoExists) {
        return {
            ...result,
            status: false,
            error: msg.not_found,
        };
    }
    const publicCDN = memoExtra.memo_img_qr;
    if (publicCDN) {
        const deleteImg = await cdnService.deleteImgCDN([publicCDN]);
        if (!deleteImg) {
            return {
                ...result,
                status: false,
                error: msg.delete_fail,
            };
        }
    }

    const upload = await handleUploadCDN(file, memoExtra.memo_id);
    let fileName = upload.fileName || "";
    let imgType = upload?.imgType || "";
    let imgOther = upload?.imgOther || "";

    await BasicService.createLog(
        `Update mở rộng cho memo [${memoExtra.memo_id}]`,
        memoExtra.memo_id,
        TYPE_LOG,
        1
    );
    const paramsMemoExtra = {
        memo_img_qr: fileName,
        memo_other_img_type: imgType,
        memo_other_img: imgOther,
    };
    const updateDto = await MemoExtraRepo.updateMemoExtra(
        memoExtra._id,
        paramsMemoExtra
    );

    return {
        ...result,
        data: memoExtraResource(updateDto, "obj"),
        error: updateDto ? "" : msg.update_fail,
    };
};

// ================================
module.exports = {
    createMemoService,
    listMemoService,
    detailMemoService,
    updatedMemoService,
    sharingMemoService,
    memoPaymentDoneService,
    updateQrImgService,
};
