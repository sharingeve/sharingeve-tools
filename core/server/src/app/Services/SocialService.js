const TelegramBot = require("node-telegram-bot-api");
const BaseService = require("./BasicServices");
const { responseDto } = require("../../utils/response");
const config = require("../../config/app");
const utils = require("../../utils/common");
const ChannelChatRepo = require("../Repositories/mongodb/ChannelChatRepositories");

const bot = new TelegramBot(config.telegramKey, { polling: true });
const TYPE_LOG = "TELEGRAM";

const checkExistChatId = async (chatId) => {
    return await ChannelChatRepo.findChatId(chatId);
};
const telegramAutoReply = async () => {
    try {
        bot.on("message", async function (msg) {
            const chatId = msg.chat.id;
            const text = msg.text;
            let options = {};
            let message = `Đây là bot tools. Nếu cần lấy key xác thực hãy nhập: /key`;
            let channel = await checkExistChatId(chatId);
            if (!channel) {
                channel = await ChannelChatRepo.saveChatId(chatId, "TELEGRAM");
            }
            switch (text) {
                case "/start":
                    break;
                case "/key":
                    message =
                        "Sử dụng key này để thực hiện xác nhận trên hệ thống: " +
                        channel.id;
                    break;
                default:
                    message = `Bot đang trong giai đoạn phát triển chưa có tính năng. Nội dung vừa gửi là: ${text}.`;
                    options = {
                        parse_mode: "HTML",
                    };
                    break;
            }

            bot.sendMessage(chatId, message, options);
        });
    } catch (error) {
        if (config.env != "production") {
            console.log("error", error);
        }

        await BaseService.createLog("Auto reply thất bại", null, TYPE_LOG);
    }
};
const telegramSendMessage = async (chatId, message) => {
    if (utils.isEmpty(chatId) || utils.isEmptyString(message)) {
        return "Thiếu thông tin gửi thông báo. Hãy nhập đủ: chatId và message cần gửi.";
    }
    try {
        bot.sendMessage(chatId, message);
    } catch (error) {
        if (config.env != "production") {
            console.log("error", error);
        }

        await BaseService.createLog(
            "Thông báo message thất bại",
            null,
            TYPE_LOG
        );
    }
};

module.exports = { telegramAutoReply, telegramSendMessage };
