const cdn = require("../../config/cloudinary.config");
const baseSer = require("./BasicServices");
const constants = require("../../config/constants");
const configApp = require('../../config/app');

const TYPE_LOG = "CDN";
const FOLDER_UPLOAD = configApp.env === 'production' ? 'prod' : 'dev';
/** 
 * docs: https://cloudinary.com/documentation/cloudinary_references
 * 
 * Template file upload success
 *  result {
      asset_id: '081f29d4be3b36b28d66f70c67834538',
      public_id: 'images/mmjd44j2iusrxuk2hvrk',
      version: 1708777796,
      version_id: 'c9035613ef6358713ca51ff83021e805',
      signature: '9856dd25e38a81a453d9d06e6e7b31c3d26c6f99',
      width: 1364,
      height: 2048,
      format: 'jpg',
      resource_type: 'image',
      created_at: '2024-02-24T12:29:56Z',
      tags: [],
      bytes: 131810,
      type: 'upload',
      etag: '817315db6b23e4ecb31c6c104c7ffc4d',
      placeholder: false,
      url: 'http://res.cloudinary.com/divehoap2/image/upload/v1708777796/images/mmjd44j2iusrxuk2hvrk.jpg',
      secure_url: 'https://res.cloudinary.com/divehoap2/image/upload/v1708777796/images/mmjd44j2iusrxuk2hvrk.jpg',
      folder: 'images',
      api_key: '824332644828999'
    }
*/

const uploadImgCDN = async (bodyFile, contentType, idObj, scoped = 'no_scoped') => {
    if (!bodyFile || !contentType) {
        let content = `Tải ảnh lên CDN cho memo [${idObj}] không thành công: thiếu thông tin bodyFile}`;
        if (!contentType && !bodyFile) {
            content = `Tải ảnh lên CDN cho memo [${idObj}] không thành công: thiếu thông tin bodyFile và contentType}`;
        } else if (!contentType && bodyFile) {
            content = `Tải ảnh lên CDN cho memo [${idObj}] không thành công: thiếu thông tin contentType}`;
        }

        await baseSer.createLog(content, idObj, TYPE_LOG);
        return;
    }

    const imgBase = await parseImgBase64(bodyFile, contentType, idObj);
    if (!imgBase) {
        await baseSer.createLog(`Thiếu file upload lên cdn.`, idObj, TYPE_LOG);
        return;
    }

    const result = await cdn.uploader
        .upload(imgBase, {
            folder: FOLDER_UPLOAD + `/${scoped}/${idObj}`,
            resource_type: constants.CDN_RESOURCE_TYPE.IMG,
        })
        .then(async (result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `Upload file cdn không thành công: ${JSON.stringify(err)}`,
                idObj,
                TYPE_LOG
            );
            return "";
        });

    return result;
};

const parseImgBase64 = async (bodyFile, contentType, idObj) => {
    if (!contentType) {
        await baseSer.createLog(
            `Thiếu thông tin contentType không mã hóa được img`,
            idObj,
            TYPE_LOG
        );
        return;
    }
    // https://cloudinary.com/blog/guest_post/how-to-parse-media-files-with-multer#create_the_api_route
    const b64 = bodyFile.toString("base64");

    return "data:" + contentType + ";base64," + b64;
};

const getResourceCDN = async (accessId) => {
    if (!accessId) {
        await baseSer.createLog(
            `getResourceCDN >> Dữ liệu truyền không hợp lệ [${accessId}]`,
            "",
            TYPE_LOG
        );
        return "";
    }

    return await cdn.api
        .resources_by_asset_ids(accessId) // string || string[]
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getResourceByAccessIds lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const createFolderCDN = async (nameFolder) => {
    if (!nameFolder) {
        await baseSer.createLog(
            `Tạo "${nameFolder}" không thành công trên CDN`,
            "",
            TYPE_LOG
        );
        return "";
    }

    const folder = await cdn.api
        .create_folder(nameFolder)
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `Tạo folder ${nameFolder} lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });

    return folder;
};

const getResourceFolderCDN = async (accessFolder) => {
    // Trả về tất cả nội dung được lưu trữ trực tiếp trong thư mục nội dung được chỉ định, bất kể đường dẫn public_id hay
    // loại tài nguyên của những nội dung đó. Phương thức này không trả về nội dung được lưu trữ trong các thư mục con của
    // thư mục đã chỉ định. Tên thư mục nội dung không phân biệt chữ hoa chữ thường.
    if (!accessFolder) {
        await baseSer.createLog(
            `getResourceFolderCDN >> Dữ liệu truyền không hợp lệ [${accessFolder}]`,
            "",
            TYPE_LOG
        );
        return "";
    }

    return await cdn.api
        .resources_by_asset_folder(accessFolder)
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getResourceFolderCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const deleteFolderCDN = async (folder) => {
    if (!folder) {
        await baseSer.createLog(
            `deleteFolderCDN >> Dữ liệu truyền không hợp lệ [${folder}]`,
            "",
            TYPE_LOG
        );
        return "";
    }

    return await cdn.api
        .delete_folder(folder)
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `deleteFolderCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const deleteImgCDN = async (arrPublicId) => {
    if (!Array.isArray(arrPublicId) || arrPublicId?.length < 1) {
        await baseSer.createLog(
            `Tham số truyền vào không hợp lệ : kiểu [${typeof arrPublicId}] và số lượng [${
                arrPublicId?.length
            }]`,
            "",
            TYPE_LOG
        );
        return "";
    }
    return await cdn.api
        .delete_resources(arrPublicId, {
            type: constants.CDN_TYPE.UPLOAD,
            resource_type: constants.CDN_RESOURCE_TYPE.IMG,
        })
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `deleteImgCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const getAllRootFolderCDN = async () => {
    // lấy all tất cả folder hiện có
    return await cdn.api
        .root_folders()
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getAllRootFolderCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const getSubFolderCDN = async (folder) => {
    if (!folder) {
        await baseSer.createLog(
            `Tham số truyền vào không hợp lệ  [${folder}].`,
            "",
            TYPE_LOG
        );
        return "";
    }

    // Liệt kê tên và đường dẫn đầy đủ của tất cả các thư mục con của thư mục mẹ được chỉ định.
    return await cdn.api
        .root_folders()
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getSubFolderCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const getUsageCDN = async () => {
    return await cdn.api
        .usage()
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getAllRootFolderCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

const getListResourceCDN = async (
    isPrefix = true,
    prefix = FOLDER_UPLOAD
) => {
    if (isPrefix) {
        return await cdn.api
            .resources({
                type: constants.CDN_TYPE.UPLOAD,
                prefix: prefix,
            })
            .then((result) => result)
            .catch(async (err) => {
                await baseSer.createLog(
                    `getListResourceCDN lỗi: ${JSON.stringify(err)}`,
                    "",
                    TYPE_LOG
                );
                return "";
            });
    }

    return await cdn.api
        .resources()
        .then((result) => result)
        .catch(async (err) => {
            await baseSer.createLog(
                `getListResourceCDN lỗi: ${JSON.stringify(err)}`,
                "",
                TYPE_LOG
            );
            return "";
        });
};

module.exports = {
    uploadImgCDN,
    getResourceCDN,
    createFolderCDN,
    getResourceFolderCDN,
    deleteFolderCDN,
    deleteImgCDN,
    getAllRootFolderCDN,
    getSubFolderCDN,
    getUsageCDN,
    getListResourceCDN,
};
