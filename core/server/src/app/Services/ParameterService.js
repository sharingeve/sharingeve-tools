const mongoose = require("mongoose");
const BaseService = require("./BasicServices");
const constants = require("../../config/constants");
const parameterRepo = require("../Repositories/mongodb/ParameterRepository");
const paramResource = require("../Resources/ParameterResource");
const { responseDto } = require("../../utils/response");
const Utils = require("../../utils/common");

const TYPE_LOG = "PARAMETER";
const logger = async function (content, options = {}) {
    const option = { id: null, status: 0, ip: "", useAgent: "", ...options };
    return await BaseService.createLog(
        content,
        option.id,
        TYPE_LOG,
        option.status,
        option.ip,
        option.useAgent
    );
};
const checkValid = (data, isUpdate = false) => {
    let listTypeOfString = ["name", "typeParam", "description"];
    const listTypeOfObjectId = ["creditor"];
    if (!data.hasOwnProperty("description")) {
        return "Thiếu dữ liệu truyền lên: description";
    }

    if (!isUpdate) {
        for (let h of listTypeOfObjectId) {
            if (!mongoose.isValidObjectId(data[h])) {
                return `${h} không đúng định dạng.`;
            }
        }
    } else {
        const indexName = listTypeOfString.indexOf("name");
        if (indexName > -1) {
            listTypeOfString.splice(indexName, 1);
        }
    }
    for (let i of listTypeOfString) {
        if (
            (i == "description" &&
                data[i] != "" &&
                typeof data[i] != "string") ||
            (i != "description" && typeof data[i] != "string")
        ) {
            return `${i} không đúng định dạng.`;
        }
    }
    if (
        !constants.PARAMETER.LIST_TYPE.includes(data.typeParam?.toLowerCase())
    ) {
        return "typeParam phải là 1 trong các loại sau: string, number, array, object";
    }
    const checkValueParamIsNotArray =
        data.typeParam?.toLowerCase() == constants.PARAMETER.TYPE.ARRAY &&
        !Utils.isArray(data.valueParam);

    const checkValueParamIsNotObject =
        data.typeParam?.toLowerCase() == constants.PARAMETER.TYPE.OBJECT &&
        !Utils.isObject(data.valueParam);
    const checkValueParamIsNotNumber =
        data.typeParam?.toLowerCase() == constants.PARAMETER.TYPE.NUMBER &&
        typeof data.valueParam != "number";
    const checkValueParamIsNotString =
        data.typeParam?.toLowerCase() == constants.PARAMETER.TYPE.STRING &&
        typeof data.valueParam != "string";

    if (
        checkValueParamIsNotObject ||
        checkValueParamIsNotArray ||
        checkValueParamIsNotNumber ||
        checkValueParamIsNotString
    ) {
        return "valueParam không đúng định dạng với typeParam đã khai báo.";
    }

    if (
        (typeof data.valueParam == "object" &&
            data.valueParam instanceof Array &&
            data.valueParam?.length == 0) ||
        (typeof data.valueParam == "object" &&
            Object.keys(data.valueParam).length == 0) ||
        (typeof data.valueParam == "number" &&
            !/^\d+$/i.test(data.valueParam)) ||
        (typeof data.valueParam == "string" && data.valueParam == "")
    ) {
        return "valueParam không được để trống.";
    }
    return "";
};

const create = async (data) => {
    // xử lý tạo tham sô
    const isValid = checkValid(data);
    if (isValid) {
        return {
            code: 422,
            data: { ...responseDto, status: false, error: isValid },
        };
    }
    const insertDto = {
        name: data.name,
        valueParam: JSON.stringify(data.valueParam),
        typeParam: data.typeParam,
        description: data.description,
        creditor: data.creditor,
    };
    const insert = await parameterRepo.create(insertDto);
    if (insert._id) {
        return {
            code: 201,
            data: { ...responseDto, data: paramResource(insert) },
        };
    } else if (insert?.exist) {
        return {
            code: 409,
            data: {
                ...responseDto,
                status: false,
                error: `Tên tham số là '${data.name}' đã tồn tại, vui lòng kiểm tra lại.`,
            },
        };
    }

    await logger("Lỗi tạo tham số hệ thống", {
        ip: data.ip,
        useAgent: data.useAgent,
    });
    return {
        code: 500,
        data: { ...responseDto, status: false, error: "Lỗi tạo tham số" },
    };
};

const update = async (id, data) => {
    const isValid = checkValid(data, true);
    if (isValid) {
        return {
            code: 422,
            data: { ...responseDto, status: false, error: isValid },
        };
    }
    const updateDto = {
        valueParam: JSON.stringify(data.valueParam),
        typeParam: data.typeParam,
        description: data.description,
    };
    const update = await parameterRepo.update(id, updateDto);
    if (update._id) {
        return {
            code: 200,
            data: { ...responseDto, data: paramResource(update) },
        };
    }

    await logger("Lỗi cập nhật tham số hệ thống", {
        id,
        ip: data.ip,
        useAgent: data.useAgent,
    });
    return {
        code: 500,
        data: { ...responseDto, status: false, error: "Lỗi cập nhật tham số" },
    };
};
const deleteParam = async (id, data) => {
    const deleteParam = await parameterRepo.deleteParam(id);
    if (deleteParam) {
        return {
            code: 200,
            data: {
                ...responseDto,
                data: "Xóa tham số thành công.",
            },
        };
    }
    await logger("Lỗi xóa tham số hệ thống: " + id, {
        id,
        ip: data.ip,
        useAgent: data.useAgent,
    });
    return {
        code: 500,
        data: {
            ...responseDto,
            status: false,
            error: "Lỗi xóa tham số hệ thống",
        },
    };
};

const getAll = async (data, searchParams, limit, page, pathname) => {
    let search = {};

    if (!Utils.isObjectEmpty(searchParams)) {
        search = { ...searchParams };
    }

    let offset = page - 1;

    if (offset < 0) {
        offset = 0;
    }

    const totalParam = await parameterRepo.countTotal();
    const listParams = await parameterRepo.all(search, limit, offset);

    const paginator = BaseService.paginate(totalParam, pathname, limit, page);

    return {
        code: 200,
        data: {
            ...paginator,
            data: paramResource(listParams),
        },
    };
};
const getParamById = async (id) => {
    const res = await parameterRepo.findParameter(id);
    if (res._id) {
        return {
            code: 200,
            data: { ...responseDto, data: paramResource(res) },
        };
    }

    return {
        code: 200,
        data: { ...responseDto, status: true, error: "Tham số không tồn tại" },
    };
};
const getValueParamByName = async (name) => {
    return await BaseService.paramFromName(name);
};
module.exports = {
    create,
    update,
    deleteParam,
    getAll,
    getParamById,
    getValueParamByName,
};
