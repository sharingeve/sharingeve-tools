const path = require("path");
const bcrypt = require("bcryptjs");
const handlebars = require("handlebars");
const common = require("../../utils/common");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const app = require("../../config/app");
const { resUnauthorized } = require("../../utils/response");
const constants = require("../../config/constants");
const repoSession = require("../Repositories/mongodb/SessionRepository");
const repoUser = require("../Repositories/mongodb/UserRepository");
const BasicService = require("./BasicServices");
const { usersResource } = require("../Resources/UserResource");
const cdnService = require("./CDNService");
const mailUtils = require("../../utils/nodemailer-utils");

const TYPE_LOG = "USER";

const authenticate = ({ req, res, next }) => {
    passport.authenticate(
        "local",
        // { session: false },
        async (err, user, info) => {
            const checkFindUser = await BasicService.paramFromName(
                "CHECK_FIND_USER_LOGIN"
            );
            if (err || !user) {
                if (checkFindUser == "1") {
                    await BasicService.sendNotifyTeleGram(
                        app.teleChatID,
                        err || "Không tìm thấy user"
                    );
                }
                return resUnauthorized(res);
            }
            req.login(user, { session: false }, async (error) => {
                if (error) {
                    if (checkFindUser == "1") {
                        await BasicService.sendNotifyTeleGram(
                            app.teleChatID,
                            error || "Login Có lỗi xảy ra!!!"
                        );
                    }
                    return next(error);
                }
                const accessToken = jwt.sign(
                    { id: user.id },
                    app.JWT_TOKEN_SECRET,
                    {
                        expiresIn: constants.COMMON.JWT_EXPIRED_DEFAULT,
                    }
                );
                // res.cookie("token", token, { httpOnly: true });
                req.session.data = {
                    accessToken,
                    user_id: user.id,
                };
                return res.json({ accessToken });
            });
        }
    )(req, res, next);
    // passport.serializeUser((user, done) => done(null, user.fullName));
};

const handleLogOut = async (token, userId) => {
    const removeSession = await repoSession.deleteSession(token, userId);
    if (removeSession) {
        return true;
    }

    return false;
};

const createUser = async (data) => {
    const isEmailExist = await repoUser.checkEmailExist(data.email);
    if (isEmailExist) {
        return {
            type: "error",
            content: "Email already exists. Please choose another email.",
        };
    }

    if (data?.account) {
        const isAccountExist = await repoUser.checkAccountExist(data?.account);

        if (isAccountExist) {
            return {
                type: "error",
                content:
                    "Account already exists. Please choose another account.",
            };
        }
    }

    if (data?.phoneNumber) {
        const regexPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;

        if (!regexPhone.test(data?.phoneNumber)) {
            return {
                type: "error",
                content: "Phone number is incorrect. Try again.",
            };
        }
    }

    const create = await repoUser.create(data);
    if (!create) {
        return {
            type: "error",
            content: "Create user fail!",
        };
    }

    const accessToken = jwt.sign({ id: create.id }, app.JWT_TOKEN_SECRET, {
        expiresIn: constants.COMMON.JWT_EXPIRED_DEFAULT,
    });

    return {
        type: "success",
        content: "Create user success!",
        data: create,
        accessToken,
    };
};

const toggleAdminForUser = async (id) => {
    const result = {
        status: true,
        data: {},
        error: "",
    };

    await BasicService.createLog(
        `PROCESS - Thay đổi trạng thái user [${id}]`,
        id,
        TYPE_LOG,
        2
    );
    const user = await repoUser.findById(id);
    if (!user) {
        await BasicService.createLog(
            `ERROR - Thay đổi trạng thái user [${id}] thất bại: [Không tìm thấy user]`,
            id,
            TYPE_LOG
        );
        return {
            ...result,
            status: false,
            error: `ID này không tồn tại: [${id}]`,
        };
    }
    const update = await repoUser.update(id, {
        admin: !user.admin,
    });

    if (!update) {
        return {
            ...result,
            status: false,
            error: `Có lỗi xảy ra với ID: [${id}]`,
        };
    }

    await BasicService.createLog(
        `SUCCESS - Thay đổi trạng thái user [${id}] thành công`,
        id,
        TYPE_LOG,
        1
    );
    return {
        ...result,
        data: usersResource(update, "obj"),
    };
};

const uploadAvatar = async (id, file) => {
    const listMsg = {
        not_found_user: `ID này không tồn tại: [${id}].`,
        valid_file:
            "File không đúng định dạng. Chỉ upload các file có dạng: jpg, jpeg, png, webp, gif.",
        limit_size_file: "File upload không vượt quá 2MB.",
        error_save: "Có lỗi xảy ra update avatar không thành công.",
    };
    let result = {
        status: true,
        data: {},
        error: "",
    };

    const sizeFile = file.size / 1024 / 1024 < 2;
    if (
        !["image/gif", "image/jpeg", "image/png", "image/webp"].includes(
            file.mimetype
        )
    ) {
        return {
            ...result,
            status: false,
            error: listMsg.valid_file,
        };
    } else if (!sizeFile) {
        return {
            ...result,
            status: false,
            error: listMsg.limit_size_file,
        };
    }
    const user = await repoUser.findById(id);
    if (!user) {
        return {
            ...result,
            status: false,
            error: listMsg.not_found_user,
        };
    }
    if (user.avatar) {
        await cdnService.deleteImgCDN([user.avatar]);
    }
    const upload = await handleUploadCDN(file, id);
    let avatar = upload.fileName || "";
    let avatar_info = upload.imgOther || "";

    const updateDTO = {
        avatar,
        avatar_info,
    };

    const update = await repoUser.update(id, updateDTO);
    if (!update) {
        return {
            ...result,
            status: false,
            error: listMsg.error_save,
        };
    }

    return {
        ...result,
        data: usersResource(update, "obj"),
    };
};

const handleUploadCDN = async (fileUpload, userId) => {
    let fileName = "";
    let imgOther = "";

    const bodyFile = fileUpload?.buffer;
    const contentType = fileUpload?.mimetype;

    if (contentType?.length > 0) {
        const upload = await cdnService.uploadImgCDN(
            bodyFile,
            contentType,
            userId,
            "users"
        );

        if (upload?.asset_id) {
            fileName = upload.public_id;
            imgOther = JSON.stringify(upload);
        }
    }

    return {
        fileName,
        imgOther,
    };
};

const updateUserService = async (id, data) => {
    const error = {
        not_found: `Không tìm thấy người dùng có ID: [${id}]`,
        update_fail: "Có lỗi xảy ra khi update",
        input_valid: "Dữ liệu bị thiếu hoặc bị sai kiểm tra lại.",
    };
    let result = {
        status: true,
        data: {},
        error: "",
    };
    const listKey = ["fullName", "email", "account", "phoneNumber"];
    listKey.forEach((item) => {
        if (!data.hasOwnProperty(item)) {
            return {
                ...result,
                status: false,
                error: error.input_valid,
            };
        }
    });
    const user = await repoUser.findById(id);
    if (!user) {
        return {
            ...result,
            status: false,
            error: error.not_found,
        };
    }

    const updateDTO = {
        fullName: data.full_name,
        email: data.email,
        account: data.username,
        phoneNumber: data.phone_number,
    };
    const updated = await repoUser.update(id, updateDTO);

    if (!updated || common.isObjectEmpty(updated)) {
        result.status = false;
        result.error = error.update_fail;
    }
    let sendMail = [];
    if (updated.email != user.email) {
        const emailParams = {
            old_email: user.email,
            new_email: updated.email,
            full_name: updated.fullName,
            activationLink: app.BASE_URL_FE + "/login",
            email_admin: app.ADMIN_EMAIL,
        };
        sendMail.push({
            template: "../../template-mail/update-email.html",
            params: emailParams,
            title: "[Noreply] Thông báo thay đổi email",
            id,
            mailToSend: updated.email,
        });
    }

    if (updated.account != user.account) {
        const emailParams = {
            old_account: user.account,
            new_account: updated.account,
            full_name: updated.fullName,
            activationLink: app.BASE_URL_FE + "/login",
            email_admin: app.ADMIN_EMAIL,
        };
        sendMail.push({
            template: "../../template-mail/update-account.html",
            params: emailParams,
            id,
            mailToSend: updated.email,
            title: "[Noreply] Thông báo thay đổi account",
        });
    }

    if (sendMail.length > 0) {
        await Promise.all(
            sendMail.map((item) => {
                sendMailUpdate({ ...item });
            })
        );
    }
    return {
        ...result,
        data: usersResource(updated),
    };
};

const sendMailUpdate = async ({ template, params, title, id, mailToSend }) => {
    if (!id) {
        await BasicService.createLog(
            `ERROR - Gửi email thông báo update user đến ${mailToSend} không thành công: Thiếu id truyền vào sendMailUpdate`,
            id,
            TYPE_LOG
        );
        return;
    }
    if (!template || common.isObjectEmpty(params) || !mailToSend || !title) {
        await BasicService.createLog(
            `ERROR - Gửi email thông báo update user đến ${mailToSend} không thành công: [${template}] - \n
            Tiêu đề: ${title} - \n
            Param: ${JSON.stringify(params)}`,
            id,
            TYPE_LOG
        );
        return;
    }
    title = title || "noreply";
    mailUtils.renderHtmlFile(
        path.join(__dirname, template),
        async function (err, html) {
            if (err) {
                return;
            }
            const template = handlebars.compile(html);
            const replacements = { ...params };
            const htmlToSend = template(replacements);
            await mailUtils.transporterSendMail(
                app.MAIL_FROM_ADDRESS,
                mailToSend,
                title,
                "",
                htmlToSend
            );
        }
    );
};
const updatePwUserService = async (id, data = {}) => {
    const error = {
        not_found: `Không tìm thấy người dùng có ID: [${id}]`,
        input_valid: "Dữ liệu bị thiếu hoặc bị sai kiểm tra lại.",
        update_fail: "Có lỗi xảy ra khi update",
        pass_fail_compare: "Password không trùng khớp so với password cũ",
        pass_confirm_fail:
            "Password confirm không trùng khớp so với password mới",
    };
    let result = {
        status: true,
        data: {},
        error: "",
    };
    if (
        !data.hasOwnProperty("password_current") ||
        !data.hasOwnProperty("password") ||
        !data.hasOwnProperty("confirm_password")
    ) {
        return {
            ...result,
            status: false,
            error: error.input_valid,
        };
    }
    const user = await repoUser.findById(id);
    if (!user) {
        return {
            ...result,
            status: false,
            error: error.not_found,
        };
    }
    const match = bcrypt.compareSync(
        data.password_current,
        user.password,
        async function (err, result) {
            if (err) {
                await BasicService.createLog(
                    `ERROR - Cập nhật mật khẩu ID ${id} không thành công.`,
                    id,
                    TYPE_LOG
                );
            }
            return result;
        }
    );

    if (!match) {
        return {
            ...result,
            status: false,
            error: error.pass_fail_compare,
        };
    }

    if (data.password !== data.confirm_password) {
        return {
            ...result,
            status: false,
            error: error.pass_confirm_fail,
        };
    }

    const updateDTO = {
        id,
        password: data.password,
    };
    const updated = await repoUser.update(id, updateDTO);

    if (!updated || common.isObjectEmpty(updated)) {
        result.status = false;
        result.error = error.update_fail;
    }
    return {
        ...result,
        data: usersResource(updated),
    };
};
module.exports = {
    authenticate,
    handleLogOut,
    createUser,
    toggleAdminForUser,
    uploadAvatar,
    updateUserService,
    updatePwUserService,
};
