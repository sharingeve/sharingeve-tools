const moment = require("moment");
const schedule = require("node-schedule");
const BaseService = require("./BasicServices");
const SocialService = require("./SocialService");
const teleJob = require("../job/TelegramJob");
const { responseDto } = require("../../utils/response");
const planJobRepo = require("../Repositories/mongodb/PlanJobRepositories");
const planRepo = require("../Repositories/mongodb/PlanRepository");
const chatRepo = require("../Repositories/mongodb/ChannelChatRepositories");
const constants = require("../../config/constants");
const timeUtils = require("../../utils/time");

const jobCancel = async (id) => {
    const getJob = await planJobRepo.findByIdJob(id);
    if (getJob) {
        teleJob.scheduleDropByName(getJob.nameJob);
        await planJobRepo.updateStatusJob(id, {
            statusJob: constants.JOB_PLAN.STATUS.CANCEL,
        });
        return {
            ...responseDto,
            status: true,
            error: "",
            data: "drop job success.",
        };
    }

    return {
        ...responseDto,
        status: false,
        error: "Not found job plan",
        data: "",
    };
};
const jobCancelByPlanId = async (planId) => {
    const getJob = await planJobRepo.findByIdPlanJob(planId);
    if (getJob) {
        teleJob.scheduleDropByName(getJob.nameJob);
        await planJobRepo.updateStatusJob(getJob.id, {
            statusJob: constants.JOB_PLAN.STATUS.CANCEL,
        });
        return {
            ...responseDto,
            status: true,
            error: "",
            data: "drop job success.",
        };
    }

    return {
        ...responseDto,
        status: false,
        error: "Not found job plan",
        data: "",
    };
};

const jobAdd = async (
    chatId,
    planId,
    action,
    dateStart,
    cycle,
    timeCycle,
    contentSendJob,
    titlePlan
) => {
    const date = new Date(dateStart); // +0000 ==> cho về 1 cái
    const current = new Date(); // +0700
    const isBeforeTimeStart = current.getTime() < date.getTime();
    // add
    let template = date;
    let dateStartSave = timeUtils.getTimeUtc();
    if (!isBeforeTimeStart) {
        template =
            action == "loop" ? templateCycle(cycle, timeCycle) : template;
    }
    if (isBeforeTimeStart && action != "loop") {
        dateStartSave = timeUtils.getTimeUtcByParam(date);
    }
    const addJob = await planJobRepo.addJob({
        planId: planId,
        date_start: dateStartSave,
        templateJob: isBeforeTimeStart ? current : template,
        statusJob: constants.JOB_PLAN.STATUS.WAITING,
    });
    if (!addJob?.id) {
        return;
    }
    // xử lý
    if (action != "loop") {
        // console.log("template", template);
        const runJob = await teleJob.scheduleTeleJob(
            template,
            async function () {
                await SocialService.telegramSendMessage(chatId, contentSendJob);
            }
        );
        if (runJob) {
            const updateDto = {
                nameJob: runJob.name,
                statusJob: constants.JOB_PLAN.STATUS.ACTIVE,
            };
            await planJobRepo.updateJob(addJob.id, updateDto);
        }

        return;
    }
    // update job
    if (isBeforeTimeStart) {
        const waitJob = await teleJob.scheduleTeleJob(date, async function () {
            const job = await teleJob.scheduleTeleJob(
                templateCycle(cycle, timeCycle),
                async function () {
                    await SocialService.telegramSendMessage(
                        chatId,
                        contentSendJob
                    );
                }
            );
            if (job?.name) {
                const addDto = {
                    planId: planId,
                    date_start: timeUtils.getTimeUtcByParam(date),
                    // date_start: date,
                    templateJob: templateCycle(cycle, timeCycle),
                    nameJob: job?.name,
                    statusJob: constants.JOB_PLAN.STATUS.ACTIVE,
                };
                await Promise.all([
                    planJobRepo.addJob(addDto),
                    planJobRepo.updateJob(addJob.id, {
                        statusJob: constants.JOB_PLAN.STATUS.CANCEL,
                    }),
                ]);
            } else {
                await BaseService.createLog(
                    "Job lặp tạo thất bại",
                    addJob.id,
                    "JOB_PLAN"
                );
            }
        });
        if (waitJob) {
            await Promise.all([
                SocialService.telegramSendMessage(
                    chatId,
                    `Kế hoạch [${titlePlan}] sẽ thực hiện lúc ${timeUtils.getTimeUtcByParam(date).format("DD/MM/YYYY HH:mm:ss")}`
                    // `Kế hoạch [${titlePlan}] sẽ thực hiện lúc ${date}`
                ),
                planJobRepo.updateJob(addJob.id, {
                    planId: addJob.id,
                    nameJob: waitJob?.name,
                    statusJob: constants.JOB_PLAN.STATUS.ACTIVE,
                }),
            ]);
        }
        return;
    }

    const runJob = await teleJob.scheduleTeleJob(template, async function () {
        await SocialService.telegramSendMessage(chatId, contentSendJob);
    });
    runJob &&
        (await planJobRepo.updateJob(addJob.id, {
            nameJob: runJob?.name,
            statusJob: constants.JOB_PLAN.STATUS.ACTIVE,
        }));
};
const templateCycle = (cycle, timeCycle) => {
    // https://crontab.guru/ ==> tool tính thời gian
    const templateDay = {
        minute: `*/${timeCycle} * * * *`,
        hour: `0 */${timeCycle} * * *`,
        day: `0 0 */${timeCycle} * *`,
        month: `0 0 10 */${timeCycle} *`,
        week: `0 0 * * */${timeCycle}`,
    };

    return templateDay[cycle];
};

const restoreJob = async (planIds = "", restoreAll = false) => {
    let listPlanIds = [];
    if (planIds) {
        listPlanIds = planIds.split(",");
    }
    let jobErr = [];
    if (listPlanIds.length == 0 && restoreAll) {
        const getAllJob = await planJobRepo.getAllJob();
        if (getAllJob.length == 0) {
            return {
                status: true,
                msg: "Không có job nào để chạy!",
                job_fail: jobErr,
            };
        }
        await Promise.all(
            getAllJob.map(async (job) => {
                const plan = await planRepo.findPlanById(job.planId);
                if (!plan?.id) {
                    jobErr.push({ [id]: "Không tồn tại kế hoạch!" });
                    return;
                }
                const chatChannel = await chatRepo.findChatIdById(
                    plan.channel_tele
                );
                if (!chatChannel?.id) {
                    jobErr.push({ [id]: "Không có thông tin id chat!" });
                    return;
                }
                const template = job.templateJob;
                let contentSendJob = `Kế hoạch: ${plan.title_plan} \nCác việc cần thực hiện:`;
                plan.plan_detail.forEach((item, index) => {
                    contentSendJob += `\n${index}. ${item.planName} - Ghi chú: ${item.planNote}`;
                });
                const runJob = await teleJob.scheduleTeleJob(
                    template,
                    async function () {
                        await SocialService.telegramSendMessage(
                            chatChannel.chatId,
                            contentSendJob
                        );
                    }
                );
                if (runJob?.name) {
                    const updateDto = {
                        nameJob: runJob?.name,
                    };
                    const update = await planJobRepo.updateJob(
                        job.id,
                        updateDto
                    );
                    if (!update?.id) {
                        jobErr.push({ [id]: "Cập nhật tên job thất bại!" });
                    }
                } else {
                    jobErr.push({ [id]: "Không chạy lại được job!" });
                }
            })
        );

        return {
            status: true,
            msg: "Done!",
            job_fail: jobErr,
        };
    }

    await Promise.all(
        listPlanIds.map(async (id) => {
            const jobPlan = await planJobRepo.findByIdPlanJob(id);
            if (
                !jobPlan?.id ||
                (jobPlan?.id &&
                    jobPlan?.statusJob == constants.JOB_PLAN.STATUS.CANCEL)
            ) {
                jobErr.push({ [id]: "Job đã bị hủy!" });
                return;
            }
            const plan = await planRepo.findPlanById(id);
            if (!plan?.id) {
                jobErr.push({ [id]: "Không tồn tại kế hoạch!" });
                return;
            }
            const chatChannel = await chatRepo.findChatIdById(
                plan.channel_tele
            );
            if (!chatChannel?.id) {
                jobErr.push({ [id]: "Không có thông tin id chat!" });
                return;
            }
            const template = jobPlan.templateJob;
            let contentSendJob = `Kế hoạch: ${plan.title_plan} \nCác việc cần thực hiện:`;
            plan.plan_detail.forEach((item, index) => {
                contentSendJob += `\n${index}. ${item.planName} - Ghi chú: ${item.planNote}`;
            });
            const runJob = await teleJob.scheduleTeleJob(
                template,
                async function () {
                    await SocialService.telegramSendMessage(
                        chatChannel.chatId,
                        contentSendJob
                    );
                }
            );
            if (runJob?.name) {
                const updateDto = {
                    nameJob: runJob?.name,
                };
                const update = await planJobRepo.updateJob(
                    jobPlan.id,
                    updateDto
                );
                if (!update?.id) {
                    jobErr.push({ [id]: "Cập nhật tên job thất bại!" });
                }
            } else {
                jobErr.push({ [id]: "Không chạy lại được job!" });
            }
        })
    );
    return {
        status: true,
        msg: "Done!",
        job_fail: jobErr,
    };
};

const removeJobs = async (planIds = "", isDeleteAll = false) => {
    let listPlanIds = [];
    if (planIds) {
        listPlanIds = planIds.split(",");
    }
    let listErrStopJob = [];
    if (listPlanIds.length == 0 && isDeleteAll) {
        const getAllJob = await planJobRepo.getAllJob();
        await Promise.all(
            getAllJob.map(async (job) => {
                const cancel = await jobCancel(job.id);
                if (cancel.status == false) {
                    listErrStopJob[job.id] = "Không thành công";
                }
            })
        );
        return;
    }

    await Promise.all(
        listPlanIds.map(async (id) => {
            const cancel = await jobCancelByPlanId(id);
            if (cancel.status == false) {
                listErrStopJob[job.id] = "Không thành công";
            }
        })
    );
};
const checkPlanEveryMinute = async () => {
    await teleJob.scheduleTeleJob("*/1 * * * *", async function () {
        // check 1 phút/ lần
        const listJobWait = await planJobRepo.findJobWait(new Date());
        let jobErr = [];
        let requests = [];
        await Promise.all(
            listJobWait.map(async (job) => {
                const plan = await planRepo.findPlanById(job.planId);
                if (!plan?.id) {
                    jobErr.push({ [id]: "Không tồn tại kế hoạch!" });
                    return;
                }
                const chatChannel = await chatRepo.findChatIdById(
                    plan.channel_tele
                );
                if (!chatChannel?.id) {
                    jobErr.push({ [id]: "Không có thông tin id chat!" }); // tạo db lưu lại ds bị lỗi
                    return;
                }
                const template = job.templateJob;
                let contentSendJob = `Kế hoạch: ${plan.title_plan} \nCác việc cần thực hiện:`;
                plan.plan_detail.forEach((item, index) => {
                    contentSendJob += `\n${index}. ${item.planName} - Ghi chú: ${item.planNote}`;
                });
                const runJob = await teleJob.scheduleTeleJob(
                    template,
                    async function () {
                        await SocialService.telegramSendMessage(
                            chatChannel.chatId,
                            contentSendJob
                        );
                    }
                );
                if (runJob?.name) {
                    requests.push({
                        updateOne: {
                            filter: { _id: job.id },
                            update: {
                                $set: {
                                    nameJob: runJob?.name,
                                    statusJob: constants.JOB_PLAN.STATUS.ACTIVE,
                                },
                            },
                        },
                    });
                } else {
                    jobErr.push({ [id]: "Không chạy lại được job!" });
                }
            })
        );

        if (requests.length > 0) {
            await planJobRepo.updateMultipleJob(requests);
        }
    });
    // update nếu như thời gian bẳt đầu lớn hơn currentTime
};
module.exports = {
    jobCancel,
    jobAdd,
    restoreJob,
    jobCancelByPlanId,
    removeJobs,
    checkPlanEveryMinute,
};
