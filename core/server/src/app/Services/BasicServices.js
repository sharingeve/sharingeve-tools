const path = require("path");
const handlebars = require("handlebars");
const logFunc = require("../Repositories/mongodb/LogFunctionRepository");
const modelConstant = require("../../config/model-constants");
// const Utils = require("../../utils/common");
const app = require("../../config/app");
const common = require("../../utils/common");
const mailUtils = require("../../utils/nodemailer-utils");
const parameterRepo = require("../Repositories/mongodb/ParameterRepository");

const TYPE_LOG = "LOG";

const paginate = (total_number_of_page, pathname, limit, page) => {
    if (!pathname) {
        throw new Error("Không có tên đường dẫn. Hãy bổ sung");
    }

    let prevPage = false;
    let nextPage = true;
    let linkPrev = "";
    let linkNext = "";

    let total_page = Math.ceil(total_number_of_page / limit);

    if (total_page <= 0) {
        total_page = 0;
        nextPage = false;
    }

    if (total_page > 0 && page <= 0) {
        page = 1;
    }

    if (total_page > 0 && page > 1 && total_page >= page) {
        prevPage = true;
    }

    if (total_page <= page) {
        nextPage = false;
    }

    if (nextPage) {
        linkNext = `${pathname}?limit=${limit}&page=${
            page + 1 == total_page ? total_page : page + 1
        }`;
    }

    if (prevPage) {
        linkPrev = `${pathname}?limit=${limit}&page=${
            page - 1 > 0 ? page - 1 : 0
        }`;
    }

    return {
        total: total_number_of_page,
        total_page,
        per_page: limit,
        current_page: page,
        prevPage,
        linkPrev,
        nextPage,
        linkNext,
    };
};

const createLog = async (
    content = "",
    id = "",
    type = TYPE_LOG,
    status = 0,
    ip = "",
    useAgent = ""
) => {
    let nameStatus = modelConstant.LOG_STATUS.SUCCESS;
    switch (status) {
        case 1:
            nameStatus = modelConstant.LOG_STATUS.SUCCESS;
            break;
        case 2:
            nameStatus = modelConstant.LOG_STATUS.PROCESS;
            break;
        case 3:
            nameStatus = modelConstant.LOG_STATUS.WARNING;
            break;
        default:
            nameStatus = modelConstant.LOG_STATUS.ERROR;
            break;
    }
    const paramsLog = {
        type: type,
        id_record: id,
        status: nameStatus,
        content: content ?? "Không điều tra do có nội dung ghi Log.",
        ip,
        useAgent,
    };
    await logFunc.createLogFunc(paramsLog);
};

const sendMail = async ({ template, params, title, id, mailToSend }) => {
    if (!id) {
        await this.createLog(
            `ERROR - Gửi email đến ${mailToSend} không thành công: Thiếu id truyền vào sendMail`,
            id,
            TYPE_LOG
        );
        return;
    }
    if (!template || common.isObjectEmpty(params) || !mailToSend || !title) {
        await this.createLog(
            `ERROR - Gửi email đến ${mailToSend} không thành công: [${template}] - \n
            Tiêu đề: ${title} - \n
            Param: ${JSON.stringify(params)}`,
            id,
            TYPE_LOG
        );
        return;
    }
    title = title || "noreply";
    mailUtils.renderHtmlFile(
        path.join(__dirname, template),
        async function (err, html) {
            if (err) {
                return;
            }
            const template = handlebars.compile(html);
            const replacements = { ...params };
            const htmlToSend = template(replacements);
            await mailUtils.transporterSendMail(
                app.MAIL_FROM_ADDRESS,
                mailToSend,
                title,
                "",
                htmlToSend
            );
        }
    );
};

const paramFromName = async (name) => {
    const param = await parameterRepo.findParameterByName(name);

    if (param._id) {
        return JSON.parse(param.valueParam);
    }

    return "";
};

const sendNotifyTeleGram = async (chat_id, message) => {
    // Kiểm tra giá trị của message
    if (!message) {
        console.error("Message text is empty");
        return;
    }

    const payload = {
        chat_id: chat_id,
        text: message,
    };

    const options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
        redirect: "follow",
    };

    const urlBot = `${app.telegramApi}bot${app.telegramKey}/sendMessage`;
    // console.log(urlBot, options);
    try {
        const response = await fetch(urlBot, options);
        const result = await response.json();
        if (!response.ok) {
            throw new Error(result.description || "Unknown error");
        }
        const debugParam = await paramFromName("DEBUG_SEND_LOGIN");
        // console.log(result);
        if (debugParam == "1") {
            console.log("OK: Send telegram pass!");
        }
    } catch (error) {
        console.error("Error sending message:", error);
    }
};
module.exports = {
    paginate,
    createLog,
    sendMail,
    sendNotifyTeleGram,
    paramFromName,
};
