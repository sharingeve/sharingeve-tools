const utils = require("../../utils/common");
const modelConstants = require("../../config/model-constants");
const memoPayRepo = require("../Repositories/mongodb/MemoPayRepository");
const memoRepo = require("../Repositories/mongodb/MemoRepository");
const userRepo = require("../Repositories/mongodb/UserRepository");
const convertDataMemoPay = require("../Resources/MemoPayResource");

const validatorMemoPay = (data) => {
    let valid = "";
    if (utils.isEmptyString(data?.creditors_id)) {
        valid = "Không id người cho vay kiểm tra lại";
        return valid;
    }
    if (utils.isEmptyString(data?.memo_id)) {
        valid = "Không memo id kiểm tra lại";
        return valid;
    }
    if (utils.isEmptyString(data?.bank_pay)) {
        valid = "Không có ngân hàng thanh toán";
        return valid;
    }

    if (!utils.isNumber(data?.amount)) {
        valid = "Số tiền không đúng định dạng hãy kiểm tra lại";
        return valid;
    }

    if (!utils.isNumber(data?.pay_type)) {
        valid = "Chưa có loại thanh toán hãy kiểm tra lại";
        return valid;
    }

    if (utils.isEmptyString(data?.debtor)) {
        valid = "Chưa có tên người trả nợ";
        return valid;
    }

    return valid;
};
const createMemoPayService = async (data) => {
    let resDto = {
        status: true,
        data: {},
    };

    const isValid = validatorMemoPay(data);
    if (!utils.isEmptyString(isValid)) {
        return { ...resDto, status: false, errors: isValid };
    }

    const userDetail = await userRepo.findById(data.creditors_id);
    if (!userDetail || utils.isObjectEmpty(userDetail)) {
        return {
            ...resDto,
            status: false,
            errors: "Không có tìm thấy thông tin người cho vay hoặc không tồn tại người này.",
        };
    }
    const debtor = data.debtor;
    const updateDto = {
        memo_id: data.memo_id,
        creditors: data.creditors_id,
        bank_pay: data.bank_pay,
        amount: data.amount,
        pay_type: data.pay_type,
        pay_status: modelConstants.PAY_STATUS.WAIT,
    };

    const create = await memoPayRepo.createMemoPay(
        updateDto,
        userDetail.email,
        debtor
    );
    if (utils.isObjectEmpty(create)) {
        return {
            ...resDto,
            status: false,
            error: "Tạo dữ liệu memo thanh toán bị lỗi",
        };
    }

    resDto = {
        ...resDto,
        data: convertDataMemoPay.memoPayResource(create),
    };
    return resDto;
};

const confirmPaymentPaidService = async (memoPayId, data) => {
    const result = {
        status: true,
        data: {},
    };
    if (!memoPayId) {
        return {
            ...result,
            status: false,
            error: "Không có id hóa đơn cần thanh toán hãy kiểm tra lại.",
        };
    }

    const isExistMemoPay = await memoPayRepo.isExistMemoPayById(memoPayId);
    if (!isExistMemoPay) {
        return {
            ...result,
            status: false,
            error: "Không có id hóa đơn cần thanh toán hãy kiểm tra lại.",
        };
    }
    const updateDto = {
        pay_status:
            data.isPaid == 1
                ? modelConstants.PAY_STATUS.DONE
                : modelConstants.PAY_STATUS.FAIL,
    };

    const update = await memoPayRepo.updateMemoPayment(memoPayId, updateDto);
    if (utils.isObjectEmpty(update)) {
        return {
            ...result,
            status: false,
            error: "Update trạng thái thất bại!",
        };
    }

    if (data.isPaid == 1) {
        const memoId = update.memo_id;
        const updateMemo = await memoRepo.updateMemo(memoId, {
            amountTotal: 0,
            structure: [],
        });

        if (utils.isObjectEmpty(updateMemo)) {
            await memoPayRepo.updateMemoPayment(memoPayId, {
                pay_status: modelConstants.PAY_STATUS.WAIT,
            });

            return {
                ...result,
                status: false,
                error: "Update trạng thái thất bại!",
            };
        }
    }

    return {
        ...result,
        data: convertDataMemoPay.memoPayResource(update),
    };
};

module.exports = {
    createMemoPayService,
    confirmPaymentPaidService,
};
