const app = require("../../config/app");
const { COMMON } = require("../../config/constants");

const infoSessionTbMongo = (db) => {
    return {
        uri: app.mongoURL,
        collection: "sessions",
        connection: db,
        expires: COMMON.DEFAULT_EXPIRED, // Thời gian hết hạn của session: 1 ngày (đơn vị là milliseconds)
    };
};

module.exports = {
    infoSessionTbMongo,
};
