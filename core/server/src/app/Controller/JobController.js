const jobService = require("../Services/JobService");
const BaseController = require("./BaseController");
const utils = require("../../utils/common");
const { responseDto } = require("../../utils/response");

class JobController extends BaseController {
    constructor() {
        super();
    }
    // [GET] /api/v1/...
    async index(req, res) {
        return this.responseOk(res, {});
    }
    // [POST] /api/v1/.../create
    async create(req, res) {
        const data = req.body;

        // có 2 TH
        // 1. chỉ có data: this.requestCheckDataEmpty(data)
        // 2. Có thêm 1 params khác như file: this.requestCheckDataEmpty(data, {name_input_file: value})
        const checkParams = this.requestCheckDataEmpty(data, {});

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseCreated(res, {});
    }
    // [PUT] /api/v1/.../:id/update
    async update(req, res) {
        const id = req.params.id;
        const data = req.body;

        const checkParams = this.requestCheckDataEmpty(data, { id });

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseOk(res, {});
    }
    // [DELETE] /api/v1/.../:id/delete
    async delete(req, res) {
        const id = req.params.id;

        const checkParams = this.requestCheckDataEmpty("", { id });
        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseOk(res, {});
    }
    // [DELETE] /api/v1/job/remove/:idJobPlan
    async removeJob(req, res) {
        const jobId = req.params.id;
        if (!jobId) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: "Không có id job",
            });
        }
        const dropJob = await jobService.jobCancel(jobId);
        return this.responseOk(res, dropJob);
    }
}

module.exports = new JobController();
