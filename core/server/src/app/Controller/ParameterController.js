const parameterService = require("../Services/ParameterService");
const BaseController = require("./BaseController");
const utils = require("../../utils/common");
const { responseDto } = require("../../utils/response");
const paramPolicy = require("../Policies/ParameterPolicy");
const Constants = require("../../config/constants");

class ParameterController extends BaseController {
    constructor() {
        super();
    }
    // [GET] /api/v1/parameter
    async index(req, res) {
        const pathName =
            req.protocol + "://" + req.get("host") + req.baseUrl + req.path;
        const searchName = req.query?.name || "";
        let search = {};
        if (searchName) {
            const regex = new RegExp(searchName, "i");
            search = {
                $or: [
                    { name: { $regex: regex } },
                    { description: { $regex: regex } },
                    { valueParam: { $regex: regex } },
                    { typeParam: { $regex: regex } },
                ],
            };
        }
        const limit = Number(req.query?.limit) || Constants.QUERY_LIMIT_DEFAULT;
        const page = req.query?.page
            ? Number(req.query?.page)
            : Constants.QUERY_PAGE_DEFAULT;

        const getAll = await parameterService.getAll(
            this.mergeDataWithIpAndAgent({}, req),
            search,
            limit,
            page,
            pathName
        );

        return this.returnResponseByCode(getAll.code, res, getAll.data);
    }

    // [GET] /api/v1/parameter/:id
    async findParamById(req, res) {
        const id = req.params.id;
        const creditor = req.query.creditor;

        const checkParams = this.requestCheckDataEmpty(
            { creditor: creditor },
            ["creditor"],
            { id }
        );

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        } else {
            const policyUpdate = await paramPolicy.getParamById(
                res.locals.user._id,
                creditor,
                id,
                this.getIpAddress(req),
                this.getUseAgent(req)
            );
            if (!policyUpdate) {
                return this.responseOk(res, {
                    ...responseDto,
                    status: false,
                    error: "Lấy tham số không thành công, thông tin người dùng hoặc id không khớp!",
                });
            }
        }

        const resDTO = await parameterService.getParamById(id);
        return this.returnResponseByCode(resDTO.code, res, resDTO.data);
    }

    // [POST] /api/v1/parameter/create
    async create(req, res) {
        const data = req.body;
        const checkParams = this.requestCheckDataEmpty(data, [
            "name",
            "valueParam",
            "typeParam",
            "creditor",
        ]);

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        } else if (
            !paramPolicy.createParam(res.locals.user._id, data.creditor)
        ) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: "Tạo tham số không thành công, thông tin người tạo không khớp!",
            });
        }
        const create = await parameterService.create(
            this.mergeDataWithIpAndAgent(data, req)
        );

        return this.returnResponseByCode(create.code, res, create.data);
    }
    // [PUT] /api/v1/parameter/:id/update
    async update(req, res) {
        const id = req.params.id;
        const data = req.body;

        const checkParams = this.requestCheckDataEmpty(
            data,
            ["valueParam", "typeParam"],
            { id }
        );

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        } else {
            const policyUpdate = await paramPolicy.updateParam(
                res.locals.user._id,
                data,
                id,
                this.getIpAddress(req),
                this.getUseAgent(req)
            );
            if (!policyUpdate) {
                return this.responseOk(res, {
                    ...responseDto,
                    status: false,
                    error: "Cập nhật tham số không thành công, thông tin người dùng hoặc id không khớp!",
                });
            }
        }

        const update = await parameterService.update(
            id,
            this.mergeDataWithIpAndAgent(data, req)
        );
        return this.returnResponseByCode(update.code, res, update.data);
    }
    // [DELETE] /api/v1/parameter/:id/delete
    async delete(req, res) {
        const id = req.params.id;
        const creditor = req.query.creditor;
        const checkParams = this.requestCheckDataEmpty(
            { creditor: creditor },
            ["creditor"],
            { id }
        );
        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        } else {
            const policyUpdate = await paramPolicy.updateParam(
                res.locals.user._id,
                { creditor },
                id,
                this.getIpAddress(req),
                this.getUseAgent(req)
            );
            if (!policyUpdate) {
                return this.responseOk(res, {
                    ...responseDto,
                    status: false,
                    error: "Xóa tham số không thành công, thông tin người dùng hoặc id không khớp!",
                });
            }
        }
        const removeParam = await parameterService.deleteParam(
            id,
            this.mergeDataWithIpAndAgent({}, req)
        );
        return this.returnResponseByCode(
            removeParam.code,
            res,
            removeParam.data
        );
    }
}

module.exports = new ParameterController();
