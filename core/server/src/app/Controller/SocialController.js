const SocialService = require("../Services/SocialService");
const BaseController = require("./BaseController");
const utils = require("../../utils/common");
const { responseDto } = require("../../utils/response");

// ý tưởng
/**
 * 1. Có 1 hoặc nhiều nền tảng như: facebook, zalo,
 * Note thêm: khi người dùng tạo 1 gì đó liên quan đến bot
 * cần phải đưa link đến bot để người dùng nhấn start
 * Sau khi nhấn start sẽ ren 1 key để xác nhận chatId
 * lấy key được render paste vào khung xác nhận tạo và lưu lại
 * Nếu không nhấn và không lưu ==> không được đi tiếp
 *
 * ví dụ: 1chatId - n task plan
 * 2. tạo các service tương ứng
 * 3. tạo bot tương ứng với các nền tảng
 * 4. mapping với backend
 * 5. test
 */
class SocialController extends BaseController {
    constructor() {
        super();
    }
    // [GET] /api/v1/...
    async index(req, res) {
        return this.responseOk(res, {});
    }
    // [POST] /api/v1/.../create
    async create(req, res) {
        const data = req.body;

        // có 2 TH
        // 1. chỉ có data: this.requestCheckDataEmpty(data)
        // 2. Có thêm 1 params khác như file: this.requestCheckDataEmpty(data, {name_input_file: value})
        const checkParams = this.requestCheckDataEmpty(data, {});

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseCreated(res, {});
    }
    // [PUT] /api/v1/.../:id/update
    async update(req, res) {
        const id = req.params.id;
        const data = req.body;

        const checkParams = this.requestCheckDataEmpty(data, { id });

        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseOk(res, {});
    }
    // [DELETE] /api/v1/.../:id/delete
    async delete(req, res) {
        const id = req.params.id;

        const checkParams = this.requestCheckDataEmpty("", { id });
        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        return this.responseOk(res, {});
    }
}

module.exports = new SocialController();
