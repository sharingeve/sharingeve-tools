const planService = require("../Services/PlanServices");
const BaseController = require("./BaseController");
const utils = require("../../utils/common");
const { responseDto } = require("../../utils/response");

class PlanController extends BaseController {
    constructor() {
        super();
    }
    // [POST] /api/v1/plan/store
    async create(req, res, next) {
        const data = req?.body;

        const checkParams = this.requestCheckDataEmpty(data);
        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }
        data.creditors = res.locals.user;

        const create = await planService.createPlanService(data);
        return this.responseCreated(res, create);
    }

    // [POST] /api/v1/plan/check-key
    async checkKeyTelegram(req, res) {
        const data = req?.body;
        const checkParams = this.requestCheckDataEmpty(data);
        if (checkParams) {
            return this.responseOk(res, {
                ...responseDto,
                status: false,
                error: checkParams,
            });
        }

        const isExit = await planService.checkKeyTelegramService(data);
        return this.responseOk(res, isExit);
    }
}

module.exports = new PlanController();
