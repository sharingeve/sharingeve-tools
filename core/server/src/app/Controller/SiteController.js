const jwt = require("jsonwebtoken");
const app = require("../../config/app");
const { authenticate, handleLogOut } = require("../Services/UserService");
const repoSession = require("../Repositories/mongodb/SessionRepository");
const repoUser = require("../Repositories/mongodb/UserRepository");
const awsS3 = require("../../config/aws");
const url = require("url");
const cdnService = require("../Services/CDNService");
const jobService = require("../Services/JobService");
const paramService = require("../Services/ParameterService");
const { resForbidden } = require("../../utils/response");
const constants = require("../../config/constants");
const { v4: uuidv4 } = require("uuid");
const BaseService = require("../Services/BasicServices");
const mongoose = require("mongoose");

class SiteController {
    index(req, res) {
        res.json({
            code: 200,
            token: req.session.token,
        });
    }

    async testAws(req, res) {
        const url_parts = url.parse(req.url, true);
        const query = url_parts.query;

        if (!req.params.img) {
            return res.status(constants.HTTP_STATUS.BAD_REQUEST).json({
                status: 400,
                msg: "Chưa có tên ảnh",
            });
        }

        if (!query?.pass || query?.pass !== "checkAws") {
            return resForbidden(res);
        }

        const keyName = req.params.img;
        const obj = `${app.s3BaseUrl}images/${keyName}`;
        const listObj = await awsS3.s3ListObject("images/", 5);
        return res.json({
            status: 200,
            object: obj,
            list: listObj,
        });
    }

    async testCDN(req, res) {
        const url_parts = url.parse(req.url, true);
        const query = url_parts.query;

        if (!req.params.id) {
            return res.status(constants.HTTP_STATUS.BAD_REQUEST).json({
                status: 400,
                msg: "Chưa có tên ảnh",
            });
        }

        if (!query?.pass || query?.pass !== "checkCDN") {
            return resForbidden(res);
        }

        const accessId = req.params.id; // "e5a7f531f02451ec52c3740a7040db91"
        let listObj = {};
        switch (query?.type) {
            case "all":
                // listObj = await cdnService.getListResourceCDN(true);
                listObj = await cdnService.getListResourceCDN();
                break;
            case "usage":
                listObj = await cdnService.getUsageCDN();
                break;
            case "2":
                listObj = await cdnService.getResourceFolderCDN(accessId);
                break;
            case "3":
                listObj = await cdnService.deleteFolderCDN(accessId);
                break;
            case "4":
                listObj = await cdnService.getSubFolderCDN(accessId);
                break;
            case "5":
                listObj = await cdnService.createFolderCDN(accessId);
                break;
            case "99":
                listObj = await cdnService.getAllRootFolderCDN();
                break;
            default:
                listObj = await cdnService.getResourceCDN(accessId);
                break;
        }

        return res.json({
            status: 200,
            list: listObj,
        });
    }

    // POST /login
    login(req, res, next) {
        authenticate({ req, res, next });
    }

    // [POST] /logout
    async logout(req, res) {
        const token = req.body?.token;
        const userId = req.body?.userId;
        const logOut = await handleLogOut(token, userId);

        if (!logOut) {
            return res.json({
                status: false,
                message: "Logout Fail!",
            });
        }

        res.json({
            status: true,
            message: "Logout success!",
        });
    }

    async verifyToken(req, res) {
        const token = decodeURIComponent(req.query.token);

        if (!token) {
            return res.json({
                status: false,
                message: "Token không hợp lệ.",
            });
        }

        try {
            const verifyToken = jwt.verify(token, app.JWT_TOKEN_SECRET);
            const [verifyInDb, user] = await Promise.all([
                repoSession.findSessionWithToken(token, verifyToken.id),
                repoUser.findById(verifyToken.id),
            ]);

            if (!verifyInDb || !user) {
                return res.json({
                    status: false,
                    message: "Token không hợp lệ.",
                });
            }
            return res.json({
                status: true,
                message: "Token hợp lệ.",
            });
        } catch (error) {
            return res.json({
                status: false,
                message: "Token không hợp lệ",
            });
        }
    }

    // [GET]  /api/get/hashKey
    async getHashKey(req, res) {
        const key = uuidv4();
        const hash = jwt.sign({ re_job: true }, key, {
            expiresIn: "1m",
        });
        res.json({
            status: true,
            key,
            hash,
            msg: "Key chỉ tồn tại trong 1 phút.",
        });
    }

    //[PUT] /api/restore/job
    /**
     * body:
     * restoreAll ?: boolean
     * ids ?: string
     * @param {*} req
     * @param {*} res
     * @returns
     */
    async restoreJob(req, res) {
        const planIds = req.body?.ids;
        if (planIds && planIds?.length > 0 && typeof planIds != "string") {
            res.json({
                status: false,
                error: "Tham số ids không hợp lệ. Vui lòng chỉ truyền dạng string",
                data: "",
            });
            return;
        }
        if (app.env == "production") {
            const hash = req.query?.hash;
            const key = req.query?.key;
            if (!hash || !key) {
                res.json({
                    status: false,
                    data: "Thiếu thông tin xác nhận",
                });
                return;
            }
        }

        try {
            if (app.env == "production") {
                const hashKey = jwt.verify(hash, key);
                if (!hashKey?.re_job) {
                    res.json({
                        status: false,
                        data: "Thông tin xác nhận không chính xác hoặc hết hạn. Vui lòng kiểm tra lại",
                    });
                    return;
                }
            }
            let restoreAll = req.body?.restoreAll;
            if (
                (!planIds || planIds?.length == 0) &&
                typeof restoreAll != "boolean"
            ) {
                res.json({
                    status: false,
                    data: "API không đủ bị giới hạn thực hiện.",
                });
                return;
            }
            if (planIds && planIds?.length > 0) {
                restoreAll = false;
            }
            const reJob = await jobService.restoreJob(planIds, restoreAll);
            res.json(reJob);
        } catch (error) {
            if (app.env != "production") {
                console.log("error", error);
            }
            res.json({
                status: false,
                data: "Thông tin xác nhận không chính xác hoặc hết hạn. Vui lòng kiểm tra lại",
            });
            return;
        }
    }

    // [DELETE] /api/job/all-cancel
    /**
     * body:
     * deleteAll ?: boolean
     * ids ?: string
     * @param {*} req
     * @param {*} res
     * @returns
     */
    async removeAllJob(req, res) {
        const planIds = req.body?.ids;
        if (planIds && planIds?.length > 0 && typeof planIds != "string") {
            res.json({
                status: false,
                error: "Tham số ids không hợp lệ. Vui lòng chỉ truyền dạng string",
                data: "",
            });
            return;
        }
        if (app.env == "production") {
            const hash = req.body?.hash;
            const key = req.body?.key;
            if (!hash || !key) {
                res.json({
                    status: false,
                    data: "Thiếu thông tin xác nhận",
                });
                return;
            }
        }
        try {
            if (app.env == "production") {
                const hashKey = jwt.verify(hash, key);
                if (!hashKey?.re_job) {
                    res.json({
                        status: false,
                        data: "Thông tin xác nhận không chính xác hoặc hết hạn. Vui lòng kiểm tra lại",
                    });
                    return;
                }
            }
            let deleteAll = req.body?.deleteAll;
            if (
                (!planIds || planIds?.length == 0) &&
                typeof deleteAll != "boolean"
            ) {
                res.json({
                    status: false,
                    data: "API không đủ bị giới hạn thực hiện.",
                });
                return;
            }
            if (planIds && planIds?.length > 0) {
                deleteAll = false;
            }
            await jobService.removeJobs(planIds, deleteAll);
            res.json({
                status: true,
                error: "",
                data: "Stop kế hoạch thành công!",
            });
        } catch (err) {
            if (app.env != "production") {
                console.log("error", err.message);
            }
            res.json({
                status: false,
                data: "Thông tin xác nhận không chính xác hoặc hết hạn. Vui lòng kiểm tra lại",
            });
        }
    }

    async getDateServer(req, res) {
        let date_new = new Date();
        let date_moment = require("moment")();
        const display = `JS: ${date_new} <br/> Moment: ${date_moment}`;
        res.send(display);
        return;
    }

    async checkDbConnect(req, res) {
        const check_db_param =
            await paramService.getValueParamByName("CHECK_DB");
        if (check_db_param != 1) {
            return res.json({
                status: false,
                data: "Chức năng không khả dụng.",
            });
        }
        const keyCheck = req.query?.keyCheck;
        if (!keyCheck || keyCheck != "20240626") {
            return res.json({
                status: false,
                data: "Không có khóa kiểm tra hoặc khóa bị sai.",
            });
        }
        const collectionName = mongoose.connection.name;
        await BaseService.sendNotifyTeleGram(
            app.teleChatID,
            `Database đang connect: ${collectionName}`
        );
        res.json({
            status: true,
            data: "Đã gửi thông tin db.",
        });
    }

    async pingServer(req, res) {
        res.status(200).json({
            status: true,
            message: "Pong",
        });
    }
}

module.exports = new SiteController();
