const http = require("../../utils/response");
const utils = require("../../utils/common");

class BaseController {
    constructor() {
        this.msgCheckData = {
            not_data: "Thiếu dữ liệu truyền lên. Hãy kiểm tra lại.",
            options_not_data: `Thiếu dữ liệu: {field}`,
            wrong_options:
                "[requestCheckDataEmpty] Không đúng định dạng options định dạng đúng là {key: value} hoặc ['key1','key2']",
        };
    }
    /**
     * kiểm tra body truyền vào đủ hay chưa
     * Options có 2 cách dùng
     * 1: {a: 1, b:2} dùng kiểm tra 1 biến cụ thể có trong object có giá trị hay không ==> kiểm tra tham số càn thiết phải có ngoài data truyền lên
     * 2: ['1','2'] dùng kiểm tra trong data[1] có data[2] hay không ==> kiểm tra toàn bộ tham số truyền lên có trong data
     * @param {*} data
     * @param {*} options : {key: value}
     * @returns
     */
    requestCheckDataEmpty(data, options = {}, options2 = {}) {
        if (utils.isObject(data) && utils.isObjectEmpty(data)) {
            return this.msgCheckData.not_data;
        }

        if (options instanceof Array) {
            for (let i in options) {
                let field = options[i];
                let value = data[field];

                if (
                    (typeof value == "string" && value == "") ||
                    (typeof value == "number" &&
                        !/^\d+$/i.test(JSON.stringify(value))) ||
                    (typeof value == "object" &&
                        value instanceof Array &&
                        value.length == 0) ||
                    (typeof value == "object" &&
                        Object.keys(value).length == 0) ||
                    typeof value == "undefined"
                ) {
                    return this.msgCheckData.options_not_data.replace(
                        "{field}",
                        field
                    );
                }
            }

            return this.checkOptions(options2);
        } else {
            return this.checkOptions(options);
        }
    }

    checkOptions(options) {
        if (!utils.isObjectEmpty(options)) {
            for (let key in options) {
                let field = options[key];

                if (
                    !utils.isObject(field) &&
                    !utils.isArray(field) &&
                    utils.isEmptyString(field)
                ) {
                    return this.msgCheckData.options_not_data.replace(
                        "{field}",
                        key
                    );
                }
            }
        } else if (!utils.isObject(options)) {
            return this.msgCheckData.wrong_options;
        }
    }
    responseOk(res, data) {
        if (!utils.isObject(data) || utils.isObjectEmpty(data)) {
            return this.responseNoContent(res);
        }
        return http.resOk(res, data);
    }

    responseCreated(res, data) {
        if (!utils.isObject(data) || utils.isObjectEmpty(data)) {
            return responseNoContent(res);
        }
        return http.resCreated(res, data);
    }

    responseConflict(res, data) {
        if (!utils.isObject(data) || utils.isObjectEmpty(data)) {
            return responseNoContent(res);
        }
        return http.resConflict(res, data);
    }

    responseUnprocessableEntity(res, data) {
        if (!utils.isObject(data) || utils.isObjectEmpty(data)) {
            return responseNoContent(res);
        }
        return http.resUnprocessableEntity(res, data);
    }

    responseNoContent(res) {
        return http.resNoContent(res);
    }

    responseNotFound(res) {
        return http.resNotFound(res);
    }
    responseUnauthorized(res) {
        return http.resUnauthorized(res);
    }

    responseErrorServer(res) {
        return http.errorServer(res);
    }

    responseForbidden(res, msg) {
        if (!msg) {
            return http.resForbidden(res);
        }

        return http.resForbidden(res, msg);
    }

    getIpAddress(req) {
        return req.headers["x-forwarded-for"] || req.socket.remoteAddress;
    }

    getUseAgent(req) {
        return req.headers["user-agent"];
    }

    mergeDataWithIpAndAgent(data, req) {
        return {
            ...data,
            ip: this.getIpAddress(req),
            useAgent: this.getUseAgent(req),
        };
    }

    /**
     * trả về theo http code ứng với từng response
     * @param {*} code
     * @param {*} res
     * @param {*} data 403: truyền message, còn lại là data trả về (nếu có)
     * @returns
     */
    returnResponseByCode(code, res, data) {
        switch (code) {
            case 201:
                return this.responseCreated(res, data);
            case 204:
                return this.responseNoContent(res);
            case 401:
                return this.responseUnauthorized(res);
            case 403:
                return this.responseForbidden(res, data);
            case 404:
                return this.responseNotFound(res);
            case 409:
                return this.responseConflict(res, data);
            case 422:
                return this.responseUnprocessableEntity(res, data);
            case 500:
                return this.responseErrorServer(res, data);
            default:
                return this.responseOk(res, data);
        }
    }
}

module.exports = BaseController;
