const { userValidate } = require("../../utils/validator");
const { HTTP_STATUS } = require("../../config/constants");
const { userRepository } = require("../Repositories/mongodb");
const common = require("../../utils/common");
const { usersResource } = require("../Resources/UserResource");
const { resForbidden } = require("../../utils/response");
const {
    createUser,
    toggleAdminForUser,
    uploadAvatar,
    updateUserService,
    updatePwUserService,
} = require("../Services/UserService");

class UserController {
    // [GET] /
    async index(req, res) {
        const result = await userRepository.find();
        const transformedData = usersResource(result);
        if (!transformedData) {
            return next();
        }

        return res.status(HTTP_STATUS.OK).json(transformedData);
    }

    // [GET] /show
    async show(req, res, next) {
        if (!userValidate.checkId(req.params.id)) {
            return next();
        }

        const user = await userRepository.findById(req.params.id);

        if (!user) {
            return next();
        }

        return res.status(HTTP_STATUS.OK).json(user);
    }

    // [POST] /create
    async store(req, res, next) {
        const data = req.body;
        const create = await createUser(data);
        if (create.type == "success") {
            create.data = usersResource(create.data);
            req.session.data = {
                accessToken: create.accessToken,
                user_id: create.data.id,
            };
        }
        return res.json(create);
    }

    // [PUT] /:id/update
    async update(req, res, next) {
        if (!userValidate.checkId(req.params.id)) {
            return res.json({
                status: false,
                data: {},
                error: "ID không hợp lệ.",
            });
        }

        const data = req.body;
        if (!data) {
            return res.json({
                status: false,
                data: {},
                error: "Không có dữ liệu cần update.",
            });
        }
        const updated = await updateUserService(req.params.id, data);

        return res.json(updated);
    }

    // [DELETE] /delete/:id
    async delete(req, res, next) {
        if (!userValidate.checkId(req.params.id)) {
            return next();
        }

        const deleted = await userRepository.deleteById(req.params.id);
        if (!deleted || common.isObjectEmpty(deleted)) return next();

        return res.json({ message: "delete success!!" });
    }

    // [DELETE] /destroy/:id
    async destroy(req, res, next) {
        if (!userValidate.checkId(req.params.id)) {
            return next();
        }

        const deleted = await userRepository.destroy(req.params.id);

        if (!deleted || common.isObjectEmpty(deleted)) return next();

        return res.json({ message: "delete force success!!" });
    }

    // [PATCH] /restore/:id
    async restore(req, res, next) {
        if (!userValidate.checkId(req.params.id)) {
            return next();
        }

        const restored = await userRepository.restoreById(req.params.id);

        if (!restored || common.isObjectEmpty(restored)) return next();

        return res.json({ message: "restore success!!" });
    }

    // [GET] /find
    async findUser(req, res) {
        let user = usersResource(res.locals.user);

        if (user) {
            return res.json(user);
        }

        return resForbidden(res);
    }

    // [PATCH] /api/auth/:user_id/toggle/admin
    // ex: http://localhost:3005/api/auth/656367946335ed6f488fee45/toggle/admin/
    // @params signature=MjAyNDAyMjUyMjMy
    async toggleAdmin(req, res) {
        const id = req.params.id;
        const signature = req.body?.signature;

        if (
            !signature ||
            signature != "MjAyNDAyMjUyMjMy" ||
            atob(signature) != "202402252232"
        ) {
            //btoa: en_code ==> MjAyNDAyMjUyMjMy
            return resForbidden(res, "ERROR 403 - Forbidden: Access Denied.");
        }
        if (!userValidate.checkId(id)) {
            return res.json({
                status: false,
                data: {},
                error: "ID không hợp lệ.",
            });
        }
        const update = await toggleAdminForUser(id);
        return res.json({ ...update });
    }

    // [PATCH] /api/user/:id/upload/avatar
    async uploadAvatar(req, res) {
        const id = req.params.id;
        if (!userValidate.checkId(id)) {
            return res.json({
                status: false,
                data: {},
                error: "ID không hợp lệ.",
            });
        }
        const file = req.file;
        if (!file) {
            return res.json({
                status: false,
                data: {},
                error: "Thiếu file upload. Dữ liệu không hợp lệ.",
            });
        }

        const handleUpdate = await uploadAvatar(id, file);
        return res.json(handleUpdate);
    }

    // [PATCH] /api/user/:id/upload/pw
    async updatePw(req, res) {
        const id = req.params.id;
        if (!userValidate.checkId(id)) {
            return res.json({
                status: false,
                data: {},
                error: "ID không hợp lệ.",
            });
        }
        const data = req.body;
        if (!data) {
            return res.json({
                status: false,
                data: {},
                error: "Dữ liệu truyền vào không hợp lệ.",
            });
        }

        const handleUpdate = await updatePwUserService(id, data);
        return res.json(handleUpdate);
    }
}

module.exports = new UserController();
