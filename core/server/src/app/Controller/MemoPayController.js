const utils = require("../../utils/common");
const memoPayService = require("../Services/MemoPayServices");

class MemoPayController {
    // [POST] /memo-pay/create
    async store(req, res) {
        const data = req.body;

        if (utils.isObjectEmpty(data)) {
            return res.json({
                status: false,
                data: {},
                error: "The transmitted params is invalid, please try again",
            });
        }
        const create = await memoPayService.createMemoPayService(data);

        return res.json({ ...create });
    }

    // [PATCH] /memo/confirm/:id/paid
    async confirmPaid(req, res) {
        const id = req.params.id;
        const data = req.body;

        if (utils.isObjectEmpty(data)) {
            return res.json({
                status: false,
                data: {},
                error: "Không tìm thấy hóa đơn. Hãy kiểm tra lại",
            });
        }

        const confirmPaymentPaid =
            await memoPayService.confirmPaymentPaidService(id, data);

        return res.json({ ...confirmPaymentPaid });
    }
}

module.exports = new MemoPayController();
