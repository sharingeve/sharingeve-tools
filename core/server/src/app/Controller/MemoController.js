const memoService = require("../Services/MemoServices");
const Constants = require("../../config/constants");
const utils = require("../../utils/common");
class MemoController {
    // [GET] /memo/list
    async list(req, res) {
        const pathName =
            req.protocol + "://" + req.get("host") + req.baseUrl + req.path;
        const searchName = req.query?.name || "";
        const searchDate = req.query?.dateBegin || "";
        const user = res.locals.user;

        let search = {
            creditors: user._id,
            structure: { $ne: [] },
            amountTotal: { $ne: 0 },
        };

        if (searchName) {
            search.fullName = searchName;
        }

        if (searchDate) {
            search["structure.create_date"] = searchDate;
        }

        const limit = Number(req.query?.limit) || Constants.QUERY_LIMIT_DEFAULT;
        const page = req.query?.page
            ? Number(req.query?.page)
            : Constants.QUERY_PAGE_DEFAULT;

        const getListMemo = await memoService.listMemoService(
            search,
            limit,
            page,
            pathName,
            user._id
        );

        return res.json({
            status: true,
            ...getListMemo,
        });
    }

    // [GET] /memo/detail/:id
    async show(req, res) {
        const id = req.params.id;
        if (!id) {
            return res.json({
                status: false,
                data: {},
                error: "Not found id memo",
            });
        }
        const user = res.locals.user;
        const result = await memoService.detailMemoService(id, user._id);

        return res.json({ ...result });
    }

    // [POST] /memo/create
    async create(req, res, next) {
        const data = req.body;
        const file = req.file;
        if (file != "undefined") {
            data.file_upload = file;
        }

        if (!data) {
            return res.json({
                status: false,
                id: undefined,
            });
        }
        const user = res.locals.user;
        data.creditors = user._id;

        const createMemo = await memoService.createMemoService(data);

        res.json(createMemo);
    }

    // [PUT] /memo/:id/updated
    async updated(req, res) {
        const id = req.params.id;
        if (!id) {
            return res.json({
                status: false,
                data: {},
                error: "Not found id memo",
            });
        }
        const data = req.body;
        const user = res.locals.user;
        data.creditors = user._id;

        const update = await memoService.updatedMemoService(id, data);
        return res.json({ ...update });
    }

    //[PUT] /memo/:id/payment/done
    async paymentDone(req, res) {
        const id = req.params.id;
        if (!id) {
            return res.json({
                status: false,
                data: {},
                error: "Not found id memo",
            });
        }

        const user = res.locals.user;
        const update = await memoService.memoPaymentDoneService(id, user._id);

        return res.json(update);
    }

    // [GET] /api/share/:userId/memo/:memoId
    async publicShow(req, res) {
        const memoId = req.params.memoId;
        const userId = req.params.userId;

        if (!memoId || !userId) {
            return res.json({
                status: false,
                data: {},
                error: "Passed parameters are not enough. The passed parameters must include userId and memoId",
            });
        }

        const memoShare = await memoService.sharingMemoService(userId, memoId);

        return res.json(memoShare);
    }

    // [PATCH] /memo/upload-qr
    async uploadQr(req, res) {
        const data = req.body;
        const file = req.file;
        if (!file) {
            return res.json({
                status: false,
                data: {},
                error: "Thiếu file truyền lên",
            });
        }

        if (utils.isObjectEmpty(data)) {
            return res.json({
                status: false,
                data: {},
                error: "Không có dữ liệu mở rộng truyền lên. Hãy kiểm tra lại",
            });
        }
        const user = res.locals.user;
        data.creditors = user._id;
        const updateQrImg = await memoService.updateQrImgService(file, data);

        return res.json({ ...updateQrImg });
    }
}

module.exports = new MemoController();
