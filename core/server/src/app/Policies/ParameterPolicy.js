const basePolicy = require("./BasePolicy");
const parameterRepo = require("../Repositories/mongodb/ParameterRepository");

const createParam = (userId, creditor) => userId == creditor;
const updateParam = async (userId, data, paramId, ip, useAgent) => {
    return (
        createParam(userId, data.creditor) &&
        (await parameterRepo.checkIdExist(paramId, { ip, useAgent }))
    );
};
const getParamById = async (userId, creditor, paramId, ip, useAgent) => {  
    return (
        createParam(userId, creditor) &&
        (await parameterRepo.checkIdExist(paramId, { ip, useAgent }))
    );
};

module.exports = {
    createParam,
    updateParam,
    getParamById,
};
