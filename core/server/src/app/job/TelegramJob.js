const BaseJob = require("./BaseJob");

const scheduleTeleJob = async (cronPattern, callback) => {
    return await BaseJob.addJob(cronPattern, callback);
};

const scheduleDropByName = (nameJob) => {
    return BaseJob.removeJobByName(nameJob);
};

module.exports = { scheduleTeleJob, scheduleDropByName };
