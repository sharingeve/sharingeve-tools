const { v4: uuidv4 } = require("uuid");
const cron = require("node-schedule");

const addJob = async (cronPattern, callback) => {
    const job = cron.scheduleJob(cronPattern, callback);

    return job;
};

const removeJobById = (id) => {
    const job = id;
    if (job) {
        return job.cancel();
    }
};
const removeJobByName = (name) => {
    if (name) {
        return cron.cancelJob(name);
    }
};
module.exports = { addJob, removeJobById, removeJobByName };
