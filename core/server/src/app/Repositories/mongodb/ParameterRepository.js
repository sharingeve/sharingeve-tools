const baseRepo = require("../BasicRepository");
const parameterModel = require("../../Model/Parameter");

const TYPE_LOG = "PARAMETER";
class ParameterRepository extends baseRepo {
    async create(data) {
        const isExistName = await this.checkNameUnique(data.name);
        if (isExistName) {
            return { exist: true };
        }
        const create = new parameterModel(data);

        return await create.save().catch(async (err) => {
            const context = "Tạo parameter bị lỗi: " + err;

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "create parameter: >>");
        });
    }

    async findParameter(id) {
        return await parameterModel
            .findById(id)
            .exec()
            .catch(async (err) => {
                const context = "findParameter: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "Parameter: >>");
            });
    }

    async findParameterByName(name) {
        return await parameterModel
            .findOne({ name: name })
            .exec()
            .catch(async (err) => {
                const context = "findParameterByName: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "findParameterByName: >>");
            });
    }

    async checkNameUnique(name) {
        const count = await parameterModel
            .countDocuments({ name: name })
            .exec()
            .catch(async (err) => {
                const context =
                    "Kiểm tra tên tham số tồn tại theo tên: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(
                    err,
                    "Kiểm tra tên tham số tồn tại theo tên: >>"
                );
            });
        return count > 0;
    }

    async checkIdExist(id, options = {}) {
        const count = await parameterModel
            .countDocuments({ _id: id })
            .exec()
            .catch(async (err) => {
                const context =
                    "Kiểm tra tên tham số tồn tại theo id: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(
                    err,
                    "Kiểm tra tên tham số tồn tại theo id: >>"
                );
            });
        if (count == 0) {
            await this.logError(
                TYPE_LOG,
                null,
                "Không tồn tại tham số: " + id,
                options?.ip,
                options?.useAgent
            );
        }
        return count > 0;
    }

    async update(id, data) {
        try {
            return await parameterModel
                .findOneAndUpdate({ _id: id }, data, {
                    new: true,
                })
                .then((data) => {
                    if (!data) {
                        return {};
                    }

                    return data;
                });
        } catch (err) {
            const context = "Cập nhật tham số: [" + err + "]";

            await this.logError(TYPE_LOG, id, context);
            this.renderError(err, "Lỗi cập nhật tham số:: >>");
        }
    }

    async deleteParam(id) {
        const actionDel = await parameterModel
            .deleteOne({ _id: id })
            .exec()
            .catch(async (err) => {
                const context = "Xóa tham số: [" + err + "]";

                await this.logError(TYPE_LOG, id, context);
                this.renderError(err, "Lỗi Xóa tham số:: >>");
            });

        return actionDel.deletedCount > 0;
    }

    async all(querySearch, limit, offset) {
        const query = await parameterModel
            .find(querySearch)
            .limit(limit)
            .skip(offset * limit)
            .sort({ updatedAt: -1 })
            .then((data) => data)
            .catch((err) => this.renderError(err, "Lấy tất cả tham số: >>"));

        return query;
    }

    async countTotal() {
        return await parameterModel.find({}).countDocuments();
    }
}

module.exports = new ParameterRepository();
