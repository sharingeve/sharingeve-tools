const BasicRepository = require("../BasicRepository");
const logFunc = require("../../Model/LogFunction");

class LogFunctionRepositories extends BasicRepository {
    async findLogFuncByIdRecord(idRecord) {
        return await memoExtra
            .findOne({ id_record: idRecord })
            .sort({ createdAt: -1 })
            .exec()
            .catch((err) =>
                this.renderError(
                    err,
                    "findLogFuncByIdRecord by idRecord err: >>"
                )
            );
    }

    async createLogFunc(data) {
        const insert = new logFunc(data);

        return await insert.save();
        // .catch((err) => this.renderError(err, "createLogFunc err: >>"));
    }
}

module.exports = new LogFunctionRepositories();
