const baseRepo = require("../BasicRepository");
const jobPlan = require("../../Model/PlanJob");
const constants = require("../../../config/constants");
const TYPE_LOG = "JOB_PLAN";
class PlanJobRepositories extends baseRepo {
    async getAllJob() {
        return await jobPlan
            .find({ statusJob: { $ne: constants.JOB_PLAN.STATUS.CANCEL } })
            .catch(async (err) => {
                const context = "getAllJob plan: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "getAllJob plan: >>");
            });
    }
    async addJob(data) {
        if (Object.keys(data).length == 0) {
            const context = "addJob: Không tồn tại data thêm job plan";

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "addJob Plan: >>");
            return {};
        }
        const created = new jobPlan(data);
        return created.save().catch(async (err) => {
            const context = "addJob plan: [" + err + "]";

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "addJob plan: >>");
        });
    }

    async findByIdJob(id) {
        if (!id) {
            const context = "findByIdJob: Không id job plan";

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "findByIdJob Plan: >>");
            return {};
        }
        return await jobPlan
            .findById(id)
            .exec()
            .catch(async (err) => {
                const context = "findByIdJob: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "findByIdJob plan: >>");
            });
    }

    async findByIdPlanJob(id) {
        if (!id) {
            const context = "findByIdPlanJob: Không planId job plan";

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "findByIdPlanJob PlanId: >>");
            return {};
        }
        return await jobPlan
            .findOne({ planId: id })
            .exec()
            .catch(async (err) => {
                const context = "findByIdPlanJob: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "findByIdPlanJob plan: >>");
            });
    }

    async updateStatusJob(id, data) {
        if (!id) {
            const context = "updateStatusJob: Không id job plan";

            await this.logError(TYPE_LOG, null, context);
            this.renderError("Không có id job", "updateStatusJob Plan: >>");
            return {};
        }
        return await jobPlan
            .findOneAndUpdate({ _id: id }, data, { new: true })
            .exec()
            .catch(async (err) => {
                const context = "updateStatusJob: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "updateStatusJob plan: >>");
            });
    }

    async updateJob(id, data) {
        if (!id) {
            const context = "updateJob: Không id job plan";

            await this.logError(TYPE_LOG, null, context);
            this.renderError("Không có id job", "updateJob Plan: >>");
            return {};
        }
        return await jobPlan
            .findOneAndUpdate({ _id: id }, data, { new: true })
            .exec()
            .catch(async (err) => {
                const context = "updateJob: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "updateJob plan: >>");
            });
    }

    async findJobWait(currentDate = undefined) {
        if (!currentDate) {
            currentDate = new Date();
        }
        // $ne: not equal
        // $gte: greater than or equal to ==> lớn hơn hoặc bằng
        // $lte: less than or equal to ==> nhở hơn hoặc bằng
        return await jobPlan
            .find({
                statusJob: constants.JOB_PLAN.STATUS.WAITING,
                date_start: { $lte: currentDate },
            })
            .catch(async (err) => {
                const context = "findJobWait plan: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "findJobWait plan: >>");
            });
    }

    async updateMultipleJob(requests) {
        return await jobPlan
            .bulkWrite(requests)
            .then((result) => {
                return result.modifiedCount;
            })
            .catch(async (err) => {
                const context = "updateMultipleJob plan: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "updateMultipleJob plan: >>");
            });
    }
}

module.exports = new PlanJobRepositories();
