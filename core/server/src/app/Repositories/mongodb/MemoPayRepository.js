const memoPay = require("../../Model/MemoPay");
const BasicRepository = require("../BasicRepository");

class MemoPayRepositories extends BasicRepository {
    async getListMemoPay(memoId, userId) {
        return await memoPay
            .find({ memo_id: memoId, creditors: userId })
            .sort({ createdAt: "desc" })
            .exec()
            .then((data) => {
                if (!data) {
                    return {};
                }

                return data;
            })
            .catch((err) => this.renderError(err, "get list memo_pay >>>"));
    }
    async createMemoPay(data, creditorsEmail, debtor) {
        const insert = new memoPay(data);

        return await insert
            .save({
                creditorsEmail,
                debtor,
                memo_id: data.memo_id,
                amount: data.amount,
                bank: data.bank_pay,
            })
            .catch((err) => this.renderError(err, "create memo pay: >>"));
    }

    async findMemoPayByMemoId(memoId, userId) {
        return await memoPay
            .findOne({ memo_id: memoId, creditors: userId })
            .sort({ createdAt: -1 })
            .exec()
            .catch((err) =>
                this.renderError(err, "find memo pay by memo_id: >>")
            );
    }

    async updateMemoPayment(id, data) {
        try {
            return await memoPay
                .findOneAndUpdate({ _id: id }, data, {
                    new: true,
                })
                .then((data) => {
                    if (!data) {
                        return {};
                    }

                    return data;
                });
        } catch (error) {
            return this.renderError(err, "update memo payment: >>");
        }
    }

    async isExistMemoPayById(id) {
        return await memoPay
            .findById(id)
            .countDocuments()
            .exec()
            .catch((err) => this.renderError(err, "isExistMemoPayById: >>"));
    }
}

module.exports = new MemoPayRepositories();
