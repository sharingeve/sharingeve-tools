const BasicRepository = require("../BasicRepository");
const UserModel = require("../../Model/User");

// const privateField = `-admin -__v -deleted`;

class UserRepository extends BasicRepository {
    // find all user
    async findWithDeleted() {
        return await UserModel.findWithDeleted()
            .sort({ updatedAt: -1 })
            .then((data) => data)
            .catch((err) => this.renderError(err, "find all user: >>"));
    }

    async find() {
        return await UserModel.find({})
            .sort({ updatedAt: -1 })
            .then((data) => data)
            .catch((err) => this.renderError(err, "find user: >>"));
    }

    async findById(id) {
        return await UserModel.findById(id)
            .exec()
            .then((user) => user)
            .catch((err) => this.renderError(err, "find user by id: >>"));
    }

    async findByAccountOrEmail(username) {
        return await UserModel.findOne({
            $or: [{ account: username }, { email: username }],
        })
            .exec()
            .catch((err) => this.renderError(err, "find user by account: >>"));
    }

    async findDeletedById(id) {
        return await UserModel.findOneDeleted({ _id: id })
            .then((user) => user)
            .catch((err) => this.renderError(err, "find user deleted : >>"));
    }

    // create user
    async create(input) {
        const insert = new UserModel(input);
        return await insert
            .save()
            .catch((err) => this.renderError(err, "create user by id: >>"));
    }

    // update user
    async update(id, data) {
        return await UserModel.findOneAndUpdate({ _id: id }, data, {
            new: true,
        })
            .then((data) => data)
            .catch(async (err) => {
                const content = `Update user [${id}] lỗi: ${err}`;
                await this.logError("USER", id, content);
                this.renderError(err, "update user by id: >>");
            });
    }

    // restore user
    async restoreById(id) {
        const userExits = await this.checkExistUser(id);
        if (userExits) return {};

        return await UserModel.restore({ _id: id })
            .then((data) => data)
            .catch((err) => this.renderError(err, "restore user by id: >>"));
    }

    // delete user
    async deleteById(id) {
        const userExits = await this.checkExistUser(id, true);
        if (userExits) return {};

        return await UserModel.deleteById(id)
            .then((data) => data)
            .catch((err) => this.renderError(err, "delete user by id: >>"));
    }

    // destroy user
    async destroy(id) {
        const userExits = await this.checkExistUser(id);
        if (userExits) return {};

        return await UserModel.deleteOne({ _id: id })
            .then((data) => data)
            .catch((err) => this.renderError(err, "delete user by id: >>"));
    }

    async checkExistUser(id, softDel = false) {
        const findUser = await UserModel.findOneWithDeleted({ _id: id });

        if (softDel) return !findUser || (findUser && findUser?.deleted);

        return !findUser || (findUser && !findUser?.deleted);
    }

    async checkEmailExist(emailReq) {
        const isExistEmail = await UserModel.findOneWithDeleted({
            email: emailReq,
        }).catch((err) => this.renderError(err, "checkEmailExist: >>"));
        return isExistEmail;
    }

    async checkAccountExist(accountReq) {
        const isExistAccount = await UserModel.findOneWithDeleted({
            account: accountReq,
        }).catch((err) => this.renderError(err, "checkAccountExist: >>"));
        return isExistAccount;
    }

    isAdmin(user) {
        if (user?._id) {
            return user.admin;
        }

        return false;
    }
}

module.exports = new UserRepository();
