const baseRepo = require("../BasicRepository");
const planModel = require("../../Model/Plan");

const TYPE_LOG = "PLAN";
class PlanRepositories extends baseRepo {
    async createPlan(data) {
        const create = new planModel(data);
        return await create.save().catch(async (err) => {
            const context = "Tạo plan bị lỗi: " + err;

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "create plan: >>");
        });
    }
    async findPlanById(id) {
        return planModel
            .findOne({ _id: id })
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm theo id lỗi: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "find plan by id: >>");
            });
    }
    async findPlanByTeleChatId(chatId) {
        return planModel
            .findOne({ tele_chatId: chatId })
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm theo tele chatId lỗi: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "create plan: >>");
            });
    }

    async findPlanByFbChatId(chatId) {
        return planModel
            .findOne({ fb_chatId: chatId })
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm theo fb chatId lỗi: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "create plan: >>");
            });
    }
}

module.exports = new PlanRepositories();
