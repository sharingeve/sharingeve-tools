const BasicRepository = require("../BasicRepository");
const memoExtra = require("../../Model/MemoExtra");

class MemoExtraRepositories extends BasicRepository {
    async findMemoExtraById(id) {
        return await memoExtra
            .findById(id)
            .sort({ createdAt: -1 })
            .exec()
            .catch(async (err) => {
                const content = `Không tìm thấy mở rộng memo ${id} lỗi: ${err}`;
                await this.logError("memo_extra", id, content);
                this.renderError(err, "findMemoExtraById by memo_id err: >>");
            });
    }

    async findMemoExtraByMemoId(memoId) {
        return await memoExtra
            .findOne({ memo_id: memoId })
            .sort({ createdAt: -1 })
            .exec()
            .catch(async (err) => {
                const content = `Tìm kiếm memo extra theo [${memoId}] lỗi: ${err}`;
                await this.logError("memo", memoId, content);
                this.renderError(
                    err,
                    "findMemoExtraByMemoId by memo_id err: >>"
                );
            });
    }

    async createMemoExtra(data) {
        const insert = new memoExtra(data);

        return await insert.save().catch(async (err) => {
            const content = `Tạo mở rộng thất bại cho memo [${data.memo_id}]: ${err}`;
            await this.logError("memo", data.memo_id, content);
            this.renderError(err, "createMemoExtra err: >>");
        });
    }

    async updateMemoExtra(id, data) {
        try {
            return await memoExtra
                .findOneAndUpdate({ _id: id }, data, {
                    new: true,
                })
                .then((data) => {
                    if (!data) {
                        return {};
                    }

                    return data;
                });
        } catch (error) {
            const content = `Cập nhật mở rộng thất bại cho memo [${id}]: ${err}`;
            await this.logError("memo", id, content);
            this.renderError(err, "updateMemoExtra err: >>");
        }
    }
}

module.exports = new MemoExtraRepositories();
