const baseRepo = require("../BasicRepository");
const templateModel = require("../../Model/TemplateModel");

const TYPE_LOG = "Template";
class TemplateRepositories extends baseRepo {
    async template(id) {
        return templateModel.findById(id)
            .exec()
            .catch(async (err) => {
                const context = "Template: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "Template: >>");
            });
    }
}

module.exports = new TemplateRepositories();
