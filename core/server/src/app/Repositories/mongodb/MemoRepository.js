const BasicRepository = require("../BasicRepository");
const MemoModel = require("../../Model/Memo");

class MemoRepositories extends BasicRepository {
    async listMemo(querySearch, limit, offset) {
        const query = await MemoModel.find(querySearch)
            .limit(limit)
            .skip(offset * limit)
            .sort({ updatedAt: -1 })
            .select({ fullName: 1, amountTotal: 1 })
            .then((data) => data)
            .catch((err) => this.renderError(err, "find list memo: >>"));

        return query;
    }

    async countTotal(userId) {
        return await MemoModel.find({
            creditors: userId,
            structure: { $ne: [] },
            amountTotal: { $ne: 0 },
        }).countDocuments();
    }

    async createMemo(data) {
        const insert = new MemoModel(data);

        return await insert
            .save()
            .catch((err) => this.renderError(err, "create memo: >>"));
    }

    async findByName(name, userId) {
        try {
            return await MemoModel.findOne({
                fullName: name,
                creditors: userId,
            }).exec();
        } catch (error) {
            this.renderError(error, "find name memo: >>");
        }
    }

    async findMemoById(id, userId) {
        try {
            const detail = await MemoModel.findOne({
                _id: id,
                creditors: userId,
            }).exec();

            return detail;
        } catch (err) {
            return this.renderError(err, "detail memo: >>");
        }
    }

    async updateMemo(id, data) {
        try {
            return await MemoModel.findOneAndUpdate({ _id: id }, data, {
                new: true,
            }).then((data) => {
                if (!data) {
                    return {};
                }

                return data;
            });
        } catch (error) {
            return this.renderError(error, "update memo: >>");
        }
    }
}

module.exports = new MemoRepositories();
