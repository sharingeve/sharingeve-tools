const baseRepo = require("../BasicRepository");
const ChannelChatModel = require("../../Model/ChannelChat");
const ObjectId = require("mongoose").Types.ObjectId;
const TYPE_LOG = "ChannelChat";
class ChannelChatRepositories extends baseRepo {
    async findChatIdById(id) {
        if (!ObjectId.isValid(id)) {
            return {};
        }
        return ChannelChatModel.findById(id)
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm chatId lỗi: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "Channel ChatId by Id: >>");
            });
    }
    async checkExistsChatId(chatId) {
        return ChannelChatModel.findOne({ chatId })
            .countDocuments()
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm chatId: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "ChannelChat by chatId: >>");
            });
    }
    async findChatId(chatId) {
        return ChannelChatModel.findOne({ chatId })
            .exec()
            .catch(async (err) => {
                const context = "Tìm kiếm chatId: [" + err + "]";

                await this.logError(TYPE_LOG, null, context);
                this.renderError(err, "ChannelChat by chatId: >>");
            });
    }
    async saveChatId(chatId, channel) {
        const channelSave = new ChannelChatModel({
            chatId,
            channel,
        });
        return await channelSave.save().catch(async (err) => {
            const context = "Tạo chatId lỗi: [" + err + "]";

            await this.logError(TYPE_LOG, null, context);
            this.renderError(err, "ChannelChat created: >>");
        });
    }
}

module.exports = new ChannelChatRepositories();
