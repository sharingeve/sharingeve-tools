const BasicRepository = require("../BasicRepository");
const SessionModel = require("../../Model/Session");

class SessionRepository extends BasicRepository {
    async findSessionWithToken(token, user_id) {
        return await SessionModel.findOne({
            "session.data.accessToken": token,
            "session.data.user_id": user_id,
        })
            .exec()
            .catch((err) => this.renderError(err, "findSessionWithToken : >>"));
    }

    async deleteSession(token, user_id) {
        return await SessionModel.deleteOne({
            "session.data.accessToken": token,
            "session.data.user_id": user_id,
        }).catch((err) => this.renderError(err, "deleteSession: >>"));
    }
}

module.exports = new SessionRepository();
