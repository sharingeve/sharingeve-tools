const mongoose = require("mongoose");
const logFunc = require("../Model/LogFunction");
const modelConstants = require("../../config/model-constants");
const config = require("../../config/app");
class BasicRepository {
    renderError(error, title = "Error") {
        if (config.env != "production") {
            logger.error(title, error);
        }
        return {};
    }
    async logError(type, id_record, content, ip = "", useAgent = "") {
        let paramsLog = {
            type: type ?? "Log",
            status: modelConstants.LOG_STATUS.ERROR,
            id_record: id_record,
            // id_record: id_record ?? new mongoose.Types.ObjectId(),
            content:
                content ??
                "Không có content truyền vào từ ${id_record} vì thế không check được log chi tiết",
            ip,
            useAgent,
        };

        const insert = new logFunc(paramsLog);
        await insert.save();
    }
}

module.exports = BasicRepository;
