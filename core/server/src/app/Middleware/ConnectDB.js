const mongoose = require('mongoose');
const {resForbidden,  errorServer } = require("../../utils/response");

module.exports.checkConnectDB = async function (req, res, next) {

    try {
        if (mongoose.connection.readyState == 1) {
            return next();
        }

        return errorServer(res);

    } catch (error) {
        console.log("error", error);
        return resForbidden(res);
    }
};
