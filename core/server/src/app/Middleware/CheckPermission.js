const app = require("../../config/app");
const baseService = require("../Services/BasicServices");
const {
    resUnauthorized,
    resForbidden,
    errorServer,
} = require("../../utils/response");
const repoUser = require("../Repositories/mongodb/UserRepository");
const repoSession = require("../Repositories/mongodb/SessionRepository");
var mongoose = require("mongoose");
const config = require("../../config/app");

async function logger(content, ip, useAgent, status = 3) {
    return await baseService.createLog(
        content,
        null,
        "MIDDLEWARE",
        status,
        ip,
        useAgent
    );
}
module.exports = async function (req, res, next) {
    const ipRemote = req.headers["x-forwarded-for"] || req.socket.remoteAddress;
    const useAgent = req.headers["user-agent"];
    const permission = res.locals.user;

    if (!permission?._id) {
        await logger(
            `Check quyền có lỗi: Chưa có thông tin đăng nhập`,
            ipRemote,
            useAgent
        );
        return resUnauthorized(res);
    }

    try {
        if (repoUser.isAdmin(permission)) {
            next();
            return;
        }

        await logger(
            `Tài khoản không có quyền: ${permission._id} - ${permission.email} - ${permission.account}`,
            ipRemote,
            useAgent
        );
        return resUnauthorized(
            res,
            "Tài khoản không đủ điều kiện sử dụng chức năng!"
        );
    } catch (error) {
        if (config.env != "production") {
            console.log("error", error);
        }
        await logger(`Check quyền có lỗi: ${error}`, ipRemote, useAgent, 0);
        return errorServer(res, "Có lỗi xảy ra! Vui lòng thử lại sau.");
    }
};
