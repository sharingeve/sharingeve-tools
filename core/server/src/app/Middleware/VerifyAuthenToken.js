const jwt = require("jsonwebtoken");
const app = require("../../config/app");
const {
    resUnauthorized,
    resForbidden,
    errorServer,
} = require("../../utils/response");
const repoUser = require("../Repositories/mongodb/UserRepository");
const repoSession = require("../Repositories/mongodb/SessionRepository");
var mongoose = require("mongoose");
const config = require("../../config/app");

module.exports.authenToken = async function (req, res, next) {
    const authHeader = req.headers["authorization"];

    if (!authHeader) {
        return resUnauthorized(res);
    }

    const token = authHeader && authHeader.split(" ")[1];

    if (!token) {
        return resUnauthorized(res);
    }

    try {
        const verifyToken = jwt.verify(token, app.JWT_TOKEN_SECRET);
        const verifyInDb = await repoSession.findSessionWithToken(
            token,
            verifyToken.id
        );

        if (!verifyInDb) {
            return resUnauthorized(res);
        }

        const user = await repoUser.findById(verifyToken.id);

        if (!user) {
            return resUnauthorized(res);
        }

        res.locals.user = user;

        next();
    } catch (error) {
        if (config.env != "production") {
            console.log("error", error);
        }
        return resUnauthorized(res, "Token hết hạn hoặc không tồn tại.");
    }
};
