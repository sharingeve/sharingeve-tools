const crypto = require("crypto");
const app = require("../config/app");
const BasicService = require("../app/Services/BasicServices");

const isEmpty = (data) => {
    return data === undefined || data === null;
};

const isEmptyString = (data) => {
    return data.length === 0 || data.trim() === "" || !data || isEmpty(data);
};

const isArray = (data) => {
    return (
        data instanceof Array ||
        Array.isArray(data) ||
        Object.prototype.toString.call(data) === "[object Array]"
    );
};

const isObject = (data) => {
    return (
        data instanceof Object ||
        typeof obj === "object" ||
        Object.prototype.toString.call(data) === "[object Object]"
    );
};

const checkForHexRegExp = (char) => {
    const regex = new RegExp("^[0-9a-fA-F]{24}$");
    return regex.test(char);
};

const isArrayEmpty = (arr) => {
    return (
        isEmpty(arr) ||
        !isArray(arr) ||
        typeof arr !== "object" ||
        arr.length === 0
    );
};

const isObjectEmpty = (obj) => {
    return isEmpty(obj) || !isObject(obj) || Object.keys(obj).length === 0;
};

const isNumber = (val) => {
    return /^\+?\-?\d+$/.test(val) && Number.isInteger(Number(val)) && !isNaN(Number(val));
};

const formatNumberWithThreeDigits = (num) => {
    var str = num.toString().split(".");
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, "$1,");
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, "$1 ");
    }
    return str.join(".");
};

const fileUploadIsImg = (type) => {
    const validImgTypes = ["image/gif", "image/jpeg", "image/png"];
    return validImgTypes.includes(type);
};

const generateFileName = (bytes = 32) =>
    crypto.randomBytes(bytes).toString("hex");

const convertDataToArray = (values) => {
    let data = [];

    if (isEmptyString(values)) {
        return data;
    }

    try {
        const convert = JSON.parse(values);
        if (typeof convert != "object") {
            data.splice(data.length, 0, convert);
            return data;
        }

        for (let i in convert) {
            const val = convert[i];
            data.splice(data.length, 0, val);
        }

        return data;
    } catch (error) {
        (async () =>
            await BasicService.createLog(
                "Lỗi convert data to array: " + error,
                null,
                "FUNCTION"
            ))();
        if (app.env != "production")
            console.error("Lỗi convert data to array >>>", error);
        return [];
    }
};
const convertDataToObject = (values) => {
    let data = {};

    if (isEmptyString(values)) {
        return data;
    }

    try {
        const convert = JSON.parse(values);
        if (typeof convert != "object") {
            data = { data: values };
            return data;
        }

        for (let i in convert) {
            const val = convert[i];
            data[i] =  val ;
        }

        return data;
    } catch (error) {
        (async () =>
            await BasicService.createLog(
                "Lỗi convert data to object: " + error,
                null,
                "FUNCTION"
            ))();
        if (app.env != "production")
            console.error("Lỗi convert data to object >>>", error);
        return [];
    }
};
module.exports = {
    isEmpty,
    isEmptyString,
    isObjectEmpty,
    isArray,
    isObject,
    checkForHexRegExp,
    isNumber,
    formatNumberWithThreeDigits,
    isArrayEmpty,
    fileUploadIsImg,
    generateFileName,
    convertDataToArray,
    convertDataToObject,
};
