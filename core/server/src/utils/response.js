const { HTTP_STATUS, VALIDATED } = require("../config/constants");

const responseDto = {
    status: true,
    error: "",
    data: [],
};

const resUnauthorized = (res, msg = VALIDATED.HTTP.UNAUTHORIZED) => {
    return res.status(HTTP_STATUS.UNAUTHORIZED).json({
        status: false,
        error: msg,
        data: [],
    });
};

const resForbidden = (res, msg = VALIDATED.HTTP.FORBIDDEN) => {
    return res.status(HTTP_STATUS.FORBIDDEN).json({
        error: msg,
        status: false,
        data: [],
    });
};

const errorServer = (res) => {
    return res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).json({
        status: false,
        error: VALIDATED.HTTP.INTERNAL_SERVER_ERROR,
        data: [],
    });
};

const resOk = (res, data) => {
    return res.status(HTTP_STATUS.OK).json(data);
};

const resCreated = (res, data) => {
    return res.status(HTTP_STATUS.CREATED).json(data);
};

const resConflict = (res, data) => {
    return res.status(HTTP_STATUS.CONFLICT).json(data);
};

const resUnprocessableEntity = (res, data) => {
    return res.status(HTTP_STATUS.UNPROCESSABLE_ENTITY).json(data);
};

const resNoContent = (res) => {
    return res.status(HTTP_STATUS.OPTIONS).json({
        status: true,
        error: "",
        data: "Yêu cầu được tiếp nhận không có nội dung trả về",
    });
};
const resNotFound = (res) => {
    return res.status(HTTP_STATUS.NOT_FOUND).json({
        status: true,
        error: "",
        data: "Không tồn tại api",
    });
};

module.exports = {
    resUnauthorized,
    resForbidden,
    errorServer,
    resOk,
    resCreated,
    resNoContent,
    responseDto,
    resConflict,
    resUnprocessableEntity,
    resNotFound
};
