const common = require("./common");
const mongoose = require("mongoose");

const userValidate = {
    checkId: function (id) {
        return id && mongoose.isValidObjectId(id);
    },
};
module.exports = {
    userValidate,
};
