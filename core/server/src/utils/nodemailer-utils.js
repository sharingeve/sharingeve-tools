const fs = require("fs");
const configNodeMail = require("../config/nodemailer");
const logFunc = require("../app/Repositories/mongodb/LogFunctionRepository");
const modelConstant = require("../config/model-constants");
const configApp = require("../config/app");

const TYPE_LOG = "MAIL";

const transporterSendMail = (emailFrom, emailTo, subject, text, html) => {
    const mailOptions = {
        from: emailFrom,
        to: emailTo,
        subject,
        text,
        html,
    };

    const send = new Promise(function (resolve, reject) {
        configNodeMail.sendMail(mailOptions, function (error, info) {
            const paramLog = {
                type: TYPE_LOG,
                id_record: null,
                status: modelConstant.LOG_STATUS.SUCCESS,
                content: "",
            };
            if (error) {
                if (configApp.env != "production") {
                    console.log(error);
                }
                logFunc.createLogFunc({
                    ...paramLog,
                    status: modelConstant.LOG_STATUS.ERROR,
                    content: `Send mail dến ${emailTo} không thành công [${error.message}]. \n Tiêu đề: ${subject}`,
                });
                reject(error);
            } else {
                if (configApp.env != "production") {
                    console.log("Email sent: " + info.response);
                }

                logFunc.createLogFunc({
                    ...paramLog,
                    content: `Send mail dến ${emailTo} thành công [${info.response}]. \n Tiêu đề: ${subject}`,
                });
                resolve(info.response);
            }
        });
    });

    return send;
};

const renderHtmlFile = (path, callback) => {
    fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
        if (err) {
            return callback(err);
        }

        callback(null, html);
    });
};

module.exports = {
    transporterSendMail,
    renderHtmlFile,
};
