const common = require("./common");

class Logger {
  info(...args) {
    console.log("======= INFO ========");
    console.log(...args);
    console.log("===============");
  }
  warning(...args) {
    console.warn("======= WARN ========");
    console.warn(...args);
    console.warn("===============");
  }

  error(...args) {
    console.error("======= ERROR ========");
    console.error(...args);
    console.error("===============");
  }

  groupInfo(nameGroup, listLog) {
    console.group(
      `=============== START GROUP INFO: ${nameGroup} ===============`
    );
    if (common.isArray(listLog)) {
      listLog.forEach((item, index) => console.log(`Debug ${index}: `, item));
    }

    if (!common.isArray(listLog) && !common.isEmpty(listLog)) {
      console.log("Debug", listLog);
    }

    if (common.isEmpty(listLog)) {
      console.log("Not data debug");
    }
    console.groupEnd(`=============== END: ${nameGroup} ===============`);
  }

  groupWarn(nameGroup, listLog) {
    console.group(
      `=============== START GROUP WARN: ${nameGroup} ===============`
    );
    if (common.isArray(listLog)) {
      listLog.forEach((item, index) => console.warn(`Debug ${index}: `, item));
    }

    if (!common.isArray(listLog) && !common.isEmpty(listLog)) {
      console.warn("Debug", listLog);
    }

    if (common.isEmpty(listLog)) {
      console.warn("Not data debug");
    }
    console.groupEnd(`=============== END: ${nameGroup} ===============`);
  }

  groupError(nameGroup, listLog) {
    console.group(
      `=============== START GROUP ERROR: ${nameGroup} ===============`
    );
    if (common.isArray(listLog)) {
      listLog.forEach((item, index) => console.error(`Debug ${index}: `, item));
    }

    if (!common.isArray(listLog) && !common.isEmpty(listLog)) {
      console.error("Debug", listLog);
    }

    if (common.isEmpty(listLog)) {
      console.error("Not data debug");
    }
    console.groupEnd(`=============== END: ${nameGroup} ===============`);
  }
}

module.exports = new Logger();
