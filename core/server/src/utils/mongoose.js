module.exports = {
    mogooseToObject: function (mongoose) {
        return mongoose ? mongoose.toObject() : mongoose;
    },
    multilMogooseToObject: function (mongoose) {
        return mongoose.map((item) => item.toObject());
    },
};
