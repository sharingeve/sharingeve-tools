const moment = require("moment");
const getTime = (tz = "Asia/Ho_chi_Minh") => moment().tz(tz);
const getTimeByParam = (date, tz = "Asia/Ho_chi_Minh") => moment(date).tz(tz);
const getTimeUtcByParam = (date, utc = "+7") => moment(date).utc(utc);
const getTimeUtc = (utc = "+7") => moment().utc(utc);
// const convertTZ = (date, tzString = "Asia/Ho_chi_Minh") => {
//     return new Date(
//         (typeof date === "string" ? new Date(date) : date).toLocaleString(
//             "en-US",
//             { timeZone: tzString }
//         )
//     );
// };
const compareTwoDateAfter = (end, start, compareTime = false) => {
    if (start instanceof Date && end instanceof Date) {
        const day1 = start.getDate();
        const day2 = end.getDate();
        const month1 = start.getMonth();
        const month2 = end.getMonth();
        const year1 = start.getFullYear();
        const year2 = end.getFullYear();

        const isCompareDay = year1 == year2 && month1 == month2 && day1 > day2;
        const isCompareMonth = year1 == year2 && month1 > month2;
        const isCompareYear = year1 > year2;

        if (compareTime) {
            const hour1 = start.getHours();
            const hour2 = end.getHours();
            const minute1 = start.getMinutes();
            const minute2 = end.getMinutes();
            const second1 = start.getSeconds();
            const second2 = end.getSeconds();

            const isCompareHour = hour1 > hour2;
            const isCompareMinute = hour1 == hour2 && minute1 > minute2;
            const isCompareSecond =
                hour1 == hour2 && minute1 == minute2 && second1 > second2;
            return (
                (isCompareYear || isCompareMonth || isCompareDay) &&
                (isCompareHour || isCompareMinute || isCompareSecond)
            );
        }
        return isCompareYear || isCompareMonth || isCompareDay;
    }

    if (!start && !end) {
        return false;
    }

    const date1 = moment(start);
    const date2 = moment(end);

    const day1 = date1.date();
    const day2 = date2.date();
    const month1 = date1.month();
    const month2 = date2.month();
    const year1 = date1.year();
    const year2 = date2.year();

    const isCompareDay = year1 == year2 && month1 == month2 && day1 > day2;
    const isCompareMonth = year1 == year2 && month1 > month2;
    const isCompareYear = year1 > year2;
    if (compareTime) {
        const hour1 = date1.hour();
        const hour2 = date2.hour();
        const minute1 = date1.minute();
        const minute2 = date2.minute();
        const second1 = date1.second();
        const second2 = date2.second();

        const isCompareHour = hour1 > hour2;
        const isCompareMinute = hour1 == hour2 && minute1 > minute2;
        const isCompareSecond =
            hour1 == hour2 && minute1 == minute2 && second1 > second2;
        return (
            (isCompareYear || isCompareMonth || isCompareDay) &&
            (isCompareHour || isCompareMinute || isCompareSecond)
        );
    }
    return isCompareYear || isCompareMonth || isCompareDay;
};
const compareTwoDateBefore = (start, end, compareTime = false) => {
    if (start instanceof Date && end instanceof Date) {
        const day1 = start.getDate();
        const day2 = end.getDate();
        const month1 = start.getMonth();
        const month2 = end.getMonth();
        const year1 = start.getFullYear();
        const year2 = end.getFullYear();

        const isCompareDay = year1 == year2 && month1 == month2 && day1 < day2;
        const isCompareMonth = year1 == year2 && month1 < month2;
        const isCompareYear = year1 < year2;

        if (compareTime) {
            const hour1 = start.getHours();
            const hour2 = end.getHours();
            const minute1 = start.getMinutes();
            const minute2 = end.getMinutes();
            const second1 = start.getSeconds();
            const second2 = end.getSeconds();

            const isCompareHour = hour1 < hour2;
            const isCompareMinute = hour1 == hour2 && minute1 < minute2;
            const isCompareSecond =
                hour1 == hour2 && minute1 == minute2 && second1 < second2;
            return (
                (isCompareYear || isCompareMonth || isCompareDay) &&
                (isCompareHour || isCompareMinute || isCompareSecond)
            );
        }
        return isCompareYear || isCompareMonth || isCompareDay;
    }

    if (!start && !end) {
        return false;
    }

    const date1 = moment(start);
    const date2 = moment(end);

    const day1 = date1.date();
    const day2 = date2.date();
    const month1 = date1.month();
    const month2 = date2.month();
    const year1 = date1.year();
    const year2 = date2.year();

    const isCompareDay = year1 === year2 && month1 == month2 && day1 < day2;
    const isCompareMonth = year1 === year2 && month1 < month2;
    const isCompareYear = year1 < year2;

    if (compareTime) {
        const hour1 = date1.hour();
        const hour2 = date2.hour();
        const minute1 = date1.minute();
        const minute2 = date2.minute();
        const second1 = date1.second();
        const second2 = date2.second();

        const isCompareHour = hour1 < hour2;
        const isCompareMinute = hour1 == hour2 && minute1 < minute2;
        const isCompareSecond =
            hour1 == hour2 && minute1 == minute2 && second1 < second2;
        return (
            (isCompareYear || isCompareMonth || isCompareDay) &&
            (isCompareHour || isCompareMinute || isCompareSecond)
        );
    }
    return isCompareYear || isCompareMonth || isCompareDay;
};
const compareTwoDateSame = (start, end, compareTime = false) => {
    if (start instanceof Date && end instanceof Date) {
        const day1 = start.getDate();
        const day2 = end.getDate();
        const month1 = start.getMonth();
        const month2 = end.getMonth();
        const year1 = start.getFullYear();
        const year2 = end.getFullYear();

        const isCompareDay =
            year1 === year2 && month1 == month2 && day1 == day2;

        if (compareTime) {
            const hour1 = start.getHours();
            const hour2 = end.getHours();
            const minute1 = start.getMinutes();
            const minute2 = end.getMinutes();
            const second1 = start.getSeconds();
            const second2 = end.getSeconds();

            const isCompareSecond =
                hour1 == hour2 && minute1 == minute2 && second1 == second2;
            return isCompareDay && isCompareSecond;
        }
        return isCompareDay;
    }

    if (!start && !end) {
        return false;
    }

    const date1 = moment(start);
    const date2 = moment(end);

    const day1 = date1.date();
    const day2 = date2.date();
    const month1 = date1.month();
    const month2 = date2.month();
    const year1 = date1.year();
    const year2 = date2.year();

    const isCompareDay = year1 === year2 && month1 == month2 && day1 == day2;

    if (compareTime) {
        const hour1 = date1.hour();
        const hour2 = date2.hour();
        const minute1 = date1.minute();
        const minute2 = date2.minute();
        const second1 = date1.second();
        const second2 = date2.second();

        const isCompareSecond =
            hour1 == hour2 && minute1 == minute2 && second1 == second2;
        return isCompareDay && isCompareSecond;
    }

    return isCompareDay;
};
module.exports = {
    getTime,
    getTimeUtc,
    getTimeByParam,
    getTimeUtcByParam,
    compareTwoDateAfter,
    compareTwoDateBefore,
    compareTwoDateSame,
    // convertTZ
};
