const express = require("express");
const cors = require("cors");
const passport = require("passport");
const session = require("express-session");
const compression = require("compression");
const MongoDBStore = require("connect-mongodb-session")(session);

global.logger = require("./utils/logger");
const config = require("./config/app");
const { jwtStrategy, localStrategy } = require("./config/passport");
const constants = require("./config/constants");
const db = require("./config/database");
const routes = require("./routes");
const { infoSessionTbMongo } = require("./app/Services/MongoService");
const middlewareConnectDB = require("./app/Middleware/ConnectDB");
const SocialService = require("./app/Services/SocialService");
const JobService = require("./app/Services/JobService");

const connectDB = db.connect(constants.DB_CONNECTION.MONGO);
const momentTz = require("moment-timezone");
momentTz.tz.setDefault("Asia/Ho_Chi_Minh");
const app = express();
const store = new MongoDBStore(infoSessionTbMongo(connectDB));
store.on("error", function (error) {
    logger.error("Session store error:", error);
});
app.set("trust proxy", true);
app.disable("x-powered-by");
app.use(cors(), (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", ["*"]);
    //   res.setHeader("Access-Control-Allow-Origin", origin);

    res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,PATCH,DELETE");

    res.setHeader("Access-Control-Allow-Credentials", "true");

    next();
});
app.use(
    compression({
        level: 6,
        threshold: 1024, // 1MB thì nén
        filter: function (req, res) {
            if (req.headers["x-no-compression"]) {
                return false;
            }
            return compression.filter(req, res);
        },
    })
);
app.use(express.json());
app.use(
    session({
        saveUninitialized: false,
        secret: config.JWT_TOKEN_SECRET,
        resave: false,
        cookie: {
            maxAge: constants.COMMON.DEFAULT_EXPIRED,
        },
        store: store,
    })
);
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
jwtStrategy(passport);
localStrategy(passport);
// serializeUser(passport);
app.use(middlewareConnectDB.checkConnectDB);
routes(app);
SocialService.telegramAutoReply();

// (async () => {
//     JobService.checkPlanEveryMinute();
// })(); ==> xử lý tại lúc tạo
app.listen(config.port, () => {
    if (config.env === "production") {
        console.log(`Server is running ${config.base_url}`);
        return;
    }

    console.log(`Server is running http:/127.0.0.1:${config.port}`);
});
