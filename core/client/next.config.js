module.exports = async (phase, { defaultConfig }) => {
    /**
     * @type {import('next').NextConfig}
     */
    const nextConfig = {
        // i18n: {
        //     locales: ["en-US"],
        //     defaultLocale: "en-US",
        // },
        compiler: {
            // see https://styled-components.com/docs/tooling#babel-plugin for more info on the options.
            styledComponents: {
                ssr: true,
                minify: true,
                transpileTemplateLiterals: true,
                pure: true,
            },
        },
        experimental: {
            optimizePackageImports: ["antd", "@ant-design/icons"],
        },
        eslint: {
            // Warning: This allows production builds to successfully complete even if
            // your project has ESLint errors.
            // ignoreDuringBuilds: true,
            dirs: ["src"],
        },
        // typescript: {
        //     // !! WARN !!
        //     // Dangerously allow production builds to successfully complete even if
        //     // your project has type errors.
        //     // !! WARN !!
        //     ignoreBuildErrors: true,
        // },
        transpilePackages: [
            "antd",
            "@ant-design/icons",
            "@ant-design/icons-svg",
        ],
        reactStrictMode: true,
        images: {
            remotePatterns: [
                {
                    protocol: "https",
                    hostname: "img.vietqr.io",
                    port: "",
                    pathname: "/image/**",
                },
            ],
        },
        // async rewrites() {
        //     // https://nextjs.org/docs/app/api-reference/next-config-js/rewrites#header-cookie-and-query-matching
        //     return [
        //         {
        //             source: "/:path*",
        //             has: [
        //                 {
        //                     type: "host",
        //                     value: "res.cloudinary.com",
        //                 },
        //             ],
        //             destination: "/image/:path*",
        //         },
        //     ];
        // },
        env: {
            API_URL: process.env.API_URL,
            SERVER_URL_DOCKER: process.env.SERVER_URL_DOCKER,
            BASE_URL: process.env.NEXT_APP_BASE_URL,
            API_VIET_QR: process.env.API_VIET_QR,
            URI_IMAGE_QR: process.env.URI_IMAGE_QR,
            TELE_BOT_URL: process.env.TELE_BOT_URL
        },
    };
    return nextConfig;
};
