import Route, {
    instanceApi as RouteApi,
    fnCustomAxios as customAxios,
} from "@/lib/axios";
import { showLogCatch } from "@/lib/utils";

const axiosGet = async (pathname: string, options?: any) => {
    const callApi: any = await Route.get(pathname, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

const axiosPost = async (pathname: string, data?: any) => {
    const callApi: any = await Route.post(pathname, data);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

export const webRoute = { axiosGet, axiosPost };

// ===============================================

const apiGet = async (pathname: string, options?: any) => {
    const callApi: any = await RouteApi.get(pathname, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

const apiPost = async (pathname: string, data?: any, options?: any) => {
    const callApi: any = await RouteApi.post(pathname, data, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

const apiPut = async (pathname: string, data?: any, options?: any) => {
    const callApi: any = await RouteApi.put(pathname, data, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

const apiDelete = async (pathname: string, options?: any) => {
    const callApi: any = await RouteApi.delete(pathname, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

const apiPatch = async (pathname: string, data?: any, options?: any) => {
    const callApi: any = await RouteApi.patch(pathname, data, options);
    if (callApi == "re-login") {
        setTimeout(() => window.location.reload(), 800)
        return {
            status: false,
            error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
        };
    }

    return callApi;
};

export const apiRoute = { apiGet, apiPost, apiPut, apiDelete, apiPatch };

// ========
const fetchGet = async (pathname: string, options?: any, config?: any) => {
    try {
        const res = await fetch(
            `${process.env.API_URL}/api/v1${pathname}` +
                new URLSearchParams({ ...options }),
            {
                method: "GET",
                cache: "no-store",
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    Authorization: `Bearer ${localStorage.getItem(
                        "accessToken"
                    )}`,
                    ...config,
                },
            }
        );

        if (!res.ok) {
            throw new Error("Failed to fetch data");
        }
        return await res.json();
    } catch (error) {
        console.log("error", error);
    }
};
const fetchPost = async (pathname: string, options?: any, config?: any) => {
    try {
        const res = await fetch(`${process.env.API_URL}/${pathname}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
                ...config,
            },
            body: JSON.stringify({ ...options }),
        });

        if (!res.ok) {
            throw new Error("Failed to fetch data");
        }

        return res.json();
    } catch (error) {
        showLogCatch(error);
    }
};

const fetchPut = async (pathname: string, options?: any, config?: any) => {
    try {
        const res = await fetch(`${process.env.API_URL}/${pathname}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
                ...config,
            },
            body: JSON.stringify({ ...options }),
        });

        if (!res.ok) {
            throw new Error("Failed to fetch data");
        }

        return res.json();
    } catch (error) {
        showLogCatch(error);
    }
};
const fetchPatch = async (pathname: string, options?: any, config?: any) => {
    try {
        const res = await fetch(`${process.env.API_URL}/${pathname}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
                ...config,
            },
            body: JSON.stringify({ ...options }),
        });

        if (!res.ok) {
            throw new Error("Failed to fetch data");
        }
        return res.json();
    } catch (error) {
        showLogCatch(error);
    }
};
const fetchDelete = async (pathname: string, options?: any, config?: any) => {
    try {
        const res = await fetch(`${process.env.API_URL}/${pathname}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
                ...config,
            },
            body: JSON.stringify({ ...options }),
        });

        if (!res.ok) {
            throw new Error("Failed to fetch data");
        }

        return res.json();
    } catch (error) {
        showLogCatch(error);
    }
};

export { fetchGet, fetchPost, fetchPut, fetchPatch, fetchDelete, customAxios };
