import Constants from "@/lib/constants/constants";

export const routes = {
    [Constants.AUTHEN.PROFILE.KEY]: "/dashboard/profile",
    [Constants.MENU_CHILD.M01.SM_01.KEY]: "/dashboard/courses",
    [Constants.MENU_CHILD.M01.SM_01.KEY]: "/dashboard/courses",
    [Constants.MENU_CHILD.M03.SM_01.KEY]: "/dashboard/nikki/plan",
    [Constants.MENU_CHILD.M03.SM_02.KEY]: "/dashboard/nikki/event_day",
    [Constants.MENU_KEY.M03]: "/dashboard/memo",
    [Constants.MENU_KEY.M07]: "/dashboard/params",
    [Constants.DEFAULT_HOME]: "/",
};
