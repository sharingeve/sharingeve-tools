import { v4 as uuidv4 } from "uuid";
import { errorNotify } from "@/components/atoms/Notification";
import { i18nVi } from "@/lib/i18n/vi";
import { apiRoute } from "@/routers";
import { userIdNotExists } from "./ProfileInfoHandler";
import { setLoadingRedux } from "@/store/common/actions";
import { notifyUpdate } from "./ProfileHeadHandler";

export const passwordRule: any[] = [
    {
        required: true,
        message: i18nVi.PASSWORD_REQUIRED,
    },
    {
        min: 8,
        message: i18nVi.CM_BOTH_MAX_MIN.replace(
            /{max_length}|{min_length}/g,
            (matched) => {
                return matched === "{max_length}" ? "16" : "8";
            }
        ),
    },
];

export const passwordConfirmRule: any[] = [
    {
        required: true,
        message: i18nVi.CM_PASSWORD_CONFIRM_REQUIRED_VALID,
    },
    ({ getFieldValue }: { getFieldValue: any }) => ({
        validator(_: any, value: any) {
            if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
            }
            return Promise.reject(new Error(i18nVi.CM_PASSWORD_CONFIRM_VALID));
        },
    }),
];

export const handleUpdatePw = async (
    user: any,
    data: any,
    dispatch: any,
    form: any
) => {
    const keyNotify = uuidv4();
    if (!user?.id) {
        userIdNotExists(keyNotify);
        return;
    }
    await dispatch(setLoadingRedux(true));

    const updated: any = await apiRoute.apiPatch(
        `/user/${user.id}/update/pw`,
        data
    );
    await dispatch(setLoadingRedux(false));
    localStorage.setItem("keyNotify", keyNotify);
    if (updated.status) {
        notifyUpdate(keyNotify);
        form.resetFields();
        return;
    }

    errorNotify({
        message: "Thông báo",
        description: updated.error,
        duration: 5,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};
