import { v4 as uuidv4 } from "uuid";
import { errorNotify, successNotify } from "@/components/atoms/Notification";
import { apiRoute } from "@/routers";
import { setUser } from "@/store/users/actions";
import { userIdNotExists } from "./ProfileInfoHandler";

export const uploadAvatarHandle = async (
    userID: string,
    previewImg: any,
    setPreviewImg: any,
    setDisabledBtn: any,
    dispatch: any,
    setIsLoading: any,
    setRandom: any
) => {
    const keyNotify = uuidv4();
    if (!userID) {
        userIdNotExists(keyNotify);
        return;
    }
    if (!previewImg) {
        localStorage.setItem("keyNotify", keyNotify);
        errorNotify({
            message: "Thông báo",
            description:
                "Không có file upload hoặc có lỗi xảy ra. Vui lòng upload file ảnh mới để cập nhật avatar.",
            duration: 5,
            key: keyNotify,
            onClose: () => {
                localStorage.removeItem("keyNotify");
            },
        });
        return;
    }

    setIsLoading(true);
    const formData = new FormData();
    formData.append("update_avatar", previewImg);
    const updated: any = await apiRoute.apiPatch(
        `/user/${userID}/upload/avatar`,
        formData
    );

    setPreviewImg(null);
    setDisabledBtn(true);
    // xử lý hiển thị
    if (updated.status) {
        await dispatch(setUser(updated.data));
        setRandom(Date.now());
        notifyUpdate(keyNotify);
        return setIsLoading(false);
    }

    localStorage.setItem("keyNotify", keyNotify);
    errorNotify({
        message: "Thông báo",
        description: updated.error,
        duration: 5,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
    setIsLoading(false);
};

export const notifyUpdate = (keyNotify: string) => {
    successNotify({
        message: "Thông báo",
        description: "Cập nhật thành công",
        duration: 5,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};
