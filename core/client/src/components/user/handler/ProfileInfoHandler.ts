import { v4 as uuidv4 } from "uuid";
import { errorNotify } from "@/components/atoms/Notification";
import { i18nVi } from "@/lib/i18n/vi";
import { apiRoute } from "@/routers";
import { handleLogOutAsync } from "@/store/users/actions";

export const fullNameRule: any[] = [
    {
        required: true,
        message: i18nVi.FULL_NAME_REQUIRED,
    },
];
export const emailRule: any[] = [
    {
        type: "email",
        message: i18nVi.CM_EMAIL_VALID,
    },
    {
        required: true,
        message: i18nVi.EMAIL_REQUIRED,
    },
];
export const accountRule: any[] = [
    {
        required: false,
        message: i18nVi.CM_ACCOUNT_VALID_REQUIRE,
    },
];

export const phoneNumberRule: any[] = [
    {
        pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
        message: i18nVi.CM_PHONE_NUMBER_VALID,
    },
];

export const handleUpdateInfo = async (
    user: any,
    setIsLoading: any,
    data: any,
    dispatch: any
) => {
    const keyNotify = uuidv4();
    if (!user?.id) {
        userIdNotExists(keyNotify);
        return;
    }
    setIsLoading(true);

    const updated: any = await apiRoute.apiPut(`/user/${user.id}/update`, data);
    if (updated.status) {
        if (data.email != user.email || data.username != user.account) {
            await dispatch(
                handleLogOutAsync({
                    token: localStorage.getItem("accessToken"),
                    userId: user.id,
                })
            );
        }
        alert("Cập nhật thông tin thành công");
        window.location.reload();
        return;
    }

    setIsLoading(false);
    localStorage.setItem("keyNotify", keyNotify);
    errorNotify({
        message: "Thông báo",
        description: updated.error,
        duration: 5,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};

export const userIdNotExists = (keyNotify: string) => {
    localStorage.setItem("keyNotify", keyNotify);
    errorNotify({
        message: "Thông báo",
        description: `ID user không tồn tại.`,
        duration: 5,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};
