export interface IProfile {
    id: string;
    full_name: string;
    account: string;
    email: string;
    isClient: boolean;
    phoneNumber: string;
    updatedAt: string;
}

export interface IProfileInfo {
    full_name: string;
    username: string;
    email: string;
    phone_number: string;
    is_client: string;
}

export interface IProfilePassword {
    password: any;
    password_current: any;
    confirm_password: any;
}
