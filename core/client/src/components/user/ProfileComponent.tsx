"use client";

import { useState } from "react";
import LoadingUI from "../molecules/LoadingUI";
import ProfileHead from "./element/ProfileHead";
import ProfileInfo from "./element/ProfileInfo";
import { Typography } from "antd";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { userDetail } from "@/store/users/selector";
import ProfilePassword from "./element/ProfilePassword";

export default function ProfileComponent() {
    const dispatch = useAppDispatch();
    const user: any = useAppSelector(userDetail);
    const [isLoading, setIsLoading] = useState(false);

    return (
        <div>
            {isLoading && <LoadingUI isOpen={isLoading} />}
            <ProfileHead user={user} setIsLoading={setIsLoading} />
            <Typography>
                <Typography.Title level={5}>Thông tin đăng ký</Typography.Title>
                <ProfileInfo user={user} setIsLoading={setIsLoading} />
            </Typography>
            <Typography>
                <Typography.Title level={5}>Thay đổi mật khẩu</Typography.Title>
                <ProfilePassword user={user} dispatch={dispatch} />
            </Typography>
        </div>
    );
}
