import FieldButton from "@/components/atoms/Button";
import FieldInput from "@/components/atoms/Input";
import { i18nVi } from "@/lib/i18n/vi";
import { templateGetItem } from "@/lib/utils";
import { useState } from "react";
import {
    handleUpdatePw,
    passwordConfirmRule,
    passwordRule,
} from "../handler/ProfilePwhandler";
import { IProfilePassword } from "../interface/ProfileInterface";
import Form from "@/components/molecules/form";

export default function ProfilePassword({
    user,
    dispatch,
}: {
    user: any;
    dispatch: any;
}) {
    const [initValInfo, setInitValInfo] = useState<IProfilePassword>();
    const [isUpdate, setIsUpdate] = useState(false);

    const baseInput = [
        templateGetItem(
            <FieldInput
                placeholder="********"
                type="password"
                name="password_current"
                scoped="passwordInput"
            />,
            i18nVi.PASSWORD_CURRENT_FIELD,
            "password_current",
            passwordRule
        ),
        templateGetItem(
            <FieldInput
                placeholder="********"
                type="password"
                name="password"
                scoped="passwordInput"
            />,
            i18nVi.PASSWORD_FIELD,
            "password",
            passwordRule
        ),
        templateGetItem(
            <FieldInput
                placeholder="********"
                type="password"
                name="confirm_password"
                scoped="passwordInput"
            />,
            i18nVi.CONFIRM_PASSWORD_FIELD,
            "confirm_password",
            passwordConfirmRule,
            {},
            ["password"],
            true
        ),
        templateGetItem(
            <FieldButton
                type="primary"
                htmlType="submit"
                className="btn_submit_update"
                nameBtn="Thay đổi mật khẩu"
                disabled={!isUpdate}
            />,
            undefined,
            undefined,
            [],
            {
                className: "btn-update",
            }
        ),
    ];

    const handlerSubmit = async (data: any, form: any) => {
        await handleUpdatePw(user, data, dispatch, form);
        setIsUpdate(false);
    };

    return (
        <div>
            <Form
                nameForm="profile_form_password"
                className={"profile-form"}
                autoComplete="off"
                items={baseInput}
                onFinish={handlerSubmit}
                onValuesChange={() => setIsUpdate(true)}
                initialValues={initValInfo || {}}
                layout="vertical"
                isNotReset="skip"
            />
        </div>
    );
}
