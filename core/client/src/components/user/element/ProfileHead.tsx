import { useEffect, useState } from "react";
import { Image, Typography } from "antd";
import ImgUploadCus from "@/components/molecules/img-upload-cus";
import clsx from "clsx";
import style from "../css/Profile.module.css";
import FieldButton from "@/components/atoms/Button";
import { uploadAvatarHandle } from "../handler/ProfileHeadHandler";
import { useAppDispatch } from "@/hooks/redux-hook";

export default function ProfileHead({
    user,
    setIsLoading,
}: {
    user: any;
    setIsLoading: any;
}) {
    const [idUser, setIdUser] = useState("");
    const [avatar, setAvatar] = useState("/assets/img/not_found_image.png");
    const [disabledBtn, setDisabledBtn] = useState(true);
    const [previewImg, setPreviewImg] = useState<any>();
    const [random, setRandom] = useState<number>();
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (user?.id) {
            setIdUser(user?.id);
            setAvatar(user?.avatar);
        }
    }, [user]);

    return (
        <Typography className={clsx(style.wrapper)}>
            <p>ID: {idUser}</p>
            <Image
                width={100}
                src={
                    (avatar || "/assets/img/not_found_image.png") + `?${random}`
                }
                alt={"avatar_user"}
                placeholder={
                    <Image
                        preview={false}
                        src="/assets/img/not_found_image.png"
                        width={100}
                        alt="preview_lazy"
                        className={clsx(style.user_avatar, style.img_blur)}
                    />
                }
                className={clsx(style.user_avatar)}
            />

            <ImgUploadCus
                txtBtn="Upload avatar"
                previewImg={previewImg}
                setPreviewImg={setPreviewImg}
                classWrapper={clsx(style.preview_img_wrapper)}
                className={clsx(style.preview_img)}
                sizeImgPreview={100}
                setDisabledBtn={setDisabledBtn}
            />

            <FieldButton
                nameBtn="Cập nhật Avatar"
                type={"primary"}
                className={clsx(style.preview_img_btn)}
                disabled={disabledBtn}
                onClick={async () =>
                    await uploadAvatarHandle(
                        idUser,
                        previewImg,
                        setPreviewImg,
                        setDisabledBtn,
                        dispatch,
                        setIsLoading,
                        setRandom
                    )
                }
            />
        </Typography>
    );
}
