import { useEffect, useState } from "react";
import { templateGetItem } from "@/lib/utils";
import { i18nVi } from "@/lib/i18n/vi";
import {
    accountRule,
    emailRule,
    fullNameRule,
    handleUpdateInfo,
    phoneNumberRule,
} from "../handler/ProfileInfoHandler";
import { IProfileInfo } from "../interface/ProfileInterface";
import FieldInput from "@/components/atoms/Input";
import Form from "@/components/molecules/form";
import FieldButton from "@/components/atoms/Button";
import { useAppDispatch } from "@/hooks/redux-hook";

export default function ProfileInfo({
    user,
    setIsLoading,
}: {
    user: any;
    setIsLoading: any;
}) {
    const [initValInfo, setInitValInfo] = useState<IProfileInfo>();
    const [isUpdate, setIsUpdate] = useState(false);
    const dispatch: any = useAppDispatch();

    useEffect(() => {
        if (user.id) {
            setInitValInfo({
                full_name: user.full_name || "",
                username: user.account || "",
                email: user.email || "",
                phone_number: user.phoneNumber ? "0" + user.phoneNumber : "",
                is_client: user.isClient ? "Cơ bản" : "Admin",
            });
        }
    }, [user]);

    const baseInput = [
        templateGetItem(
            <FieldInput
                placeholder={i18nVi.FULL_NAME_FIELD}
                name="full_name"
            />,
            i18nVi.FULL_NAME_FIELD,
            "full_name",
            fullNameRule
        ),
        templateGetItem(
            <FieldInput placeholder={i18nVi.EMAIL_FIELD} name="email" />,
            i18nVi.EMAIL_FIELD,
            "email",
            emailRule
        ),
        templateGetItem(
            <FieldInput placeholder={i18nVi.ACCOUNT_FIELD} name="account" />,
            i18nVi.ACCOUNT_FIELD,
            "username",
            accountRule
        ),
        templateGetItem(
            <FieldInput placeholder="0123456789" name="phone_number" />,
            i18nVi.PHONE_NUMBER_FIELD,
            "phone_number",
            phoneNumberRule
        ),
        templateGetItem(
            <FieldInput name="is_client" disabled={true} />,
            i18nVi.CLIENT,
            "is_client"
        ),
        templateGetItem(
            <FieldButton
                type="primary"
                htmlType="submit"
                className="btn_submit_update"
                nameBtn="Cập nhật"
                disabled={!isUpdate}
            />,
            undefined,
            undefined,
            [],
            {
                className: "btn-update",
            }
        ),
    ];

    const handlerSubmit = async (data: any, form: any) => {
        await handleUpdateInfo(user, setIsLoading, data, dispatch);
    };

    return (
        <Form
            nameForm="profile_form"
            className={"profile-form"}
            autoComplete="off"
            items={baseInput}
            onFinish={handlerSubmit}
            onValuesChange={() => setIsUpdate(true)}
            initialValues={initValInfo || {}}
            layout="vertical"
            isNotReset="skip"
        />
    );
}
