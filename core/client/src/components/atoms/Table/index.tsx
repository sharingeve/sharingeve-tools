import { useEffect, useRef, useState } from "react";
import clsx from "clsx";
import style from "./table.module.css";
import LoadingUI from "@/components/molecules/LoadingUI";
import { useRouter } from "next/navigation";

export default function Table({
    cols,
    thead,
    data,
    page,
    page_next,
    page_prev,
    page_first,
    page_last,
    total,
    paginate_center,
    paginate_start,
    paginate_end,
    disabledPrevPage,
    disabledNextPage,
    blurInputPaginate,
    isLoading,
    setIsLoading,
    linkAdd,
    positionBtnRight,
    searchHide,
    txtSearch,
    blurSearch,
    txtPlaceholderSearch,
}: {
    cols: string[];
    thead: string[];
    data: React.ReactNode;
    page?: string;
    total?: string;
    paginate_center?: boolean;
    paginate_start?: boolean;
    paginate_end?: boolean;
    page_next?: any;
    page_prev?: any;
    page_first?: any;
    page_last?: any;
    disabledPrevPage?: boolean;
    disabledNextPage?: boolean;
    isLoading: boolean;
    blurInputPaginate: (e?: any) => void;
    linkAdd: string;
    positionBtnRight?: boolean;
    setIsLoading: any;
    searchHide?: boolean;
    txtSearch?: string;
    txtPlaceholderSearch?: string;
    blurSearch?: (value?: string) => Promise<any>;
}) {
    const router = useRouter();
    const current_page = useRef<HTMLInputElement>(null);
    const total_page = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (page && current_page.current) {
            current_page.current.value = page;
        }
        if (total && total_page.current) {
            total_page.current.value = total;
        }
    }, [page, total, data]);
    const paginate_position = () => {
        if (paginate_center) {
            return style.justify_center;
        }
        if (paginate_start) {
            return style.justify_start;
        }
        if (paginate_end) {
            return style.justify_end;
        }
    };
    const handleOnClickBtn = () => {
        setIsLoading(true);
        router.push(linkAdd, { scroll: false });
    };
    return (
        <div className={clsx(style.table_wrapper)}>
            {isLoading && <LoadingUI isOpen={isLoading} />}
            {!searchHide && (
                <>
                    <label htmlFor="table_search" className={clsx(style.label)}>
                        {txtSearch ? txtSearch : "Tìm kiếm"}
                    </label>
                    <input
                        id="table_search"
                        type="text"
                        className={clsx(style.input)}
                        onBlur={async (e) => {
                            if (blurSearch) {
                                await blurSearch(e.target.value);
                            }
                        }}
                        placeholder={txtPlaceholderSearch}
                    />
                </>
            )}
            {linkAdd && (
                <button
                    className={clsx(
                        style.btn,
                        style.btn_primary,
                        positionBtnRight && style.btn_right
                    )}
                    onClick={handleOnClickBtn}
                >
                    Thêm mới
                </button>
            )}
            <table id={clsx(style.table)}>
                <colgroup>
                    {cols.map((col, index) => (
                        <col width={col + "%"} key={`col_table_${index}`} />
                    ))}
                </colgroup>
                <thead>
                    <tr>
                        {thead.map((head, index) => (
                            <th key={`thead_${index}`}>{head}</th>
                        ))}
                        <th></th>
                    </tr>
                </thead>
                <tbody id={clsx(style.tbody)}>{data}</tbody>
            </table>
            <div className={clsx(style.paginate, paginate_position())}>
                <div
                    className={clsx(
                        style.paginate_first_page,
                        page == "1" && style.disabled
                    )}
                    title="First"
                >
                    <a href={"#"} onClick={page_first}>
                        {"<<"}
                    </a>
                </div>
                <div
                    className={clsx(
                        style.paginate_prev_page,
                        disabledPrevPage && style.disabled
                    )}
                    title="Previous"
                >
                    <a href={"#"} onClick={page_prev}>
                        {"<"}
                    </a>
                </div>
                <div className={clsx(style.paginate_input_page)}>
                    <input
                        type="text"
                        defaultValue={page}
                        className="paginate_page"
                        onBlur={blurInputPaginate}
                        ref={current_page}
                        readOnly={page == total && total == "1"}
                    />
                    <span>&nbsp;/&nbsp;</span>
                    <input
                        type="text"
                        defaultValue={total}
                        ref={total_page}
                        disabled
                        className="paginate_page_total"
                    />
                </div>
                <div
                    className={clsx(
                        style.paginate_next_page,
                        disabledNextPage && style.disabled
                    )}
                    title="Next"
                >
                    <a href={"#"} onClick={page_next}>
                        {">"}
                    </a>
                </div>
                <div
                    className={clsx(
                        style.paginate_last_page,
                        total == page && style.disabled
                    )}
                    title="Last"
                >
                    <a href="#" onClick={page_last}>
                        {">>"}
                    </a>
                </div>
            </div>
        </div>
    );
}
