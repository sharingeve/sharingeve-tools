import { Fragment, FocusEvent, ChangeEvent, useState } from "react";
import { IElement } from "../../FormInterface";
import clsx from "clsx";
import { validateEl } from "../ElementHandle";

export default function InputCustom({
    label,
    type,
    placeholder,
    name,
    blurInput,
    changeInput,
    defaultVal,
    styleEl,
    classEl,
    focus,
    maxInput,
    minInput,
    required,
    validate,
    disabled,
    readonly,
}: IElement) {
    const [message_Err, setMessage_Err] = useState("");
    const handleOnBlur = (e: FocusEvent<HTMLInputElement, Element>) => {
        const input = e.target as HTMLInputElement;
        const nextElement = input.nextElementSibling;
        if (nextElement && nextElement.id === "err_check_form") {
            nextElement.remove();
        }
        if (
            required &&
            validateEl.required &&
            validateEl.required(
                e,
                typeof defaultVal == "string" ? defaultVal : ""
            )
        ) {
            setMessage_Err("Vui lòng không bỏ trống ô này.");
            return;
        }
        if (validate?.custom_valid) {
            setMessage_Err("");
            validate.custom_valid(e, setMessage_Err);
            if (message_Err) {
                return;
            }
        }
        setMessage_Err("");
        blurInput && blurInput(e);
    };
    const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
        changeInput && changeInput(e);
    };
    return (
        <Fragment>
            {label && (
                <label
                    htmlFor={name + "_id"}
                    className={clsx(required && "label_required")}
                >
                    <span className="label_title">{label}</span>
                    {required && <span className="label_start">&nbsp;*</span>}
                </label>
            )}
            <input
                className={classEl}
                style={styleEl}
                type={type ? type : "text"}
                placeholder={placeholder}
                name={name}
                id={name + "_id"}
                onBlur={(e) => handleOnBlur(e)}
                onChange={(e) => handleOnChange(e)}
                defaultValue={defaultVal}
                autoFocus={focus}
                minLength={minInput}
                maxLength={maxInput}
                data-require={required ? "true" : "false"}
                disabled={disabled}
                readOnly={readonly}
            />
            {message_Err && (
                <p id={name + "_error"} className={clsx("error_display")}>
                    {message_Err}
                </p>
            )}
        </Fragment>
    );
}
