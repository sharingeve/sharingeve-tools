import React, { useState } from "react";
import { IElement, optionSelect } from "../../FormInterface";
import clsx from "clsx";
import { validateEl } from "../ElementHandle";

export default function SelectCus({
    label,
    name,
    blurInput,
    changeInput,
    dataSelect,
    txtDefaultSelect,
    selected,
    styleEl,
    classEl,
    focus,
    required,
    validate,
    disabled
}: IElement) {
    const [message_Err, setMessage_Err] = useState("");
    const handleOnBlur = (e: React.FocusEvent<HTMLSelectElement, Element>) => {
        const select = e.target as HTMLSelectElement;
        const nextElement = select.nextElementSibling;
        if (nextElement && nextElement.id === "err_check_form") {
            nextElement.remove();
        }
        let defaultVal = typeof selected != "undefined" ? selected : "";
        if (
            required &&
            validateEl.required &&
            validateEl.required(e, defaultVal)
        ) {
            setMessage_Err("Vui lòng không bỏ trống ô này.");
            return;
        }
        if (validate?.custom_valid) {
            setMessage_Err("");
            validate.custom_valid(e, setMessage_Err);
            if (message_Err) {
                return;
            }
        }
        setMessage_Err("");
        blurInput && blurInput(e);
    };
    const handleOnChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const select = e.target as HTMLSelectElement;
        const nextElement = select.nextElementSibling;
        if (nextElement && nextElement.id === "err_check_form") {
            nextElement.remove();
        }
        if (nextElement && nextElement.id === "err_check_form") {
            nextElement.remove();
        }
        let defaultVal = typeof selected != "undefined" ? selected : "";
        if (
            required &&
            validateEl.required &&
            validateEl.required(e, defaultVal)
        ) {
            setMessage_Err("Vui lòng không bỏ trống ô này.");
            return;
        }
        if (validate?.custom_valid) {
            setMessage_Err("");
            validate.custom_valid(e, setMessage_Err);
            if (message_Err) {
                return;
            }
        }
        setMessage_Err("");
        changeInput && changeInput(e);
    };
    return (
        <React.Fragment>
            {label && (
                <label
                    htmlFor={name + "_id"}
                    className={clsx(required && "label_required")}
                >
                    <span className="label_title">{label}</span>
                    {required && <span className="label_start">&nbsp;*</span>}
                </label>
            )}
            <select
                className={classEl}
                style={styleEl}
                id={`${name}_id`}
                onBlur={(e) => handleOnBlur(e)}
                onChange={(e) => handleOnChange(e)}
                defaultValue={selected}
                autoFocus={focus}
                data-require={required ? "true" : "false"}
                name={name}
                disabled={disabled}
            >
                <option value={""}>
                    {txtDefaultSelect ? txtDefaultSelect : "-- Chưa chọn --"}
                </option>
                {dataSelect &&
                    dataSelect.map((item: optionSelect) => (
                        <option value={item.value} key={item.value}>
                            {item.label}
                        </option>
                    ))}
            </select>
            {message_Err && (
                <p id={name + "_error"} className={clsx("error_display")}>
                    {message_Err}
                </p>
            )}
        </React.Fragment>
    );
}
