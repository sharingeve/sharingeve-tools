import React, { useState } from "react";
import { IElement } from "../../FormInterface";
import clsx from "clsx";
import { validateEl } from "../ElementHandle";

export default function TextareaCus({
    name,
    label,
    placeholder,
    rowsTextarea,
    colsTextarea,
    defaultVal,
    blurInput,
    changeInput,
    resizeTextareaNone,
    styleEl,
    classEl,
    focus,
    required,
    validate,
    disabled,
    readonly
}: IElement) {
    const [message_Err, setMessage_Err] = useState("");
    const handleOnBlur = (
        e: React.FocusEvent<HTMLTextAreaElement, Element>
    ) => {
        const textarea = e.target as HTMLTextAreaElement;
        const nextElement = textarea.nextElementSibling;
        if (nextElement && nextElement.id === "err_check_form") {
            nextElement.remove();
        }
        if (required && validateEl.required && validateEl.required(e, typeof defaultVal == 'string' ? defaultVal : '')) {
            setMessage_Err("Vui lòng không bỏ trống ô này.");
            return;
        }
        if (validate?.custom_valid) {
            setMessage_Err("");
            validate.custom_valid(e, setMessage_Err);
            if (message_Err) {
                return;
            }
        }
        setMessage_Err("");
        blurInput && blurInput(e);
    };
    const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        changeInput && changeInput(e);
    };
    return (
        <React.Fragment>
            {label && (
                <label
                    htmlFor={name + "_id"}
                    className={clsx(required && "label_required")}
                >
                    <span className="label_title">{label}</span>
                    {required && <span className="label_start">&nbsp;*</span>}
                </label>
            )}
            <textarea
                style={{
                    resize: resizeTextareaNone ? "none" : "vertical",
                    ...styleEl,
                }}
                className={classEl}
                id={`${name}_id`}
                name={name}
                placeholder={placeholder}
                defaultValue={defaultVal}
                cols={colsTextarea}
                rows={rowsTextarea || 3}
                onBlur={(e) => handleOnBlur(e)}
                onChange={(e) => handleOnChange(e)}
                autoFocus={focus}
                data-require={required ? "true" : "false"}
                disabled={disabled}
                readOnly={readonly}
            ></textarea>
            {message_Err && (
                <p id={name + "_error"} className={clsx("error_display")}>
                    {message_Err}
                </p>
            )}
        </React.Fragment>
    );
}
