import { validateForm } from "../FormInterface";

export const validateEl: validateForm = {
    required(e, defaultVal) {
        const btnSubmit = document.getElementById("frm_btn_submit");
        if (e?.target?.value == "") {
            btnSubmit?.setAttribute("disabled", "disabled");
            btnSubmit?.classList.add("disabled");
            return true;
        }
        if (e?.target?.value != defaultVal) {
            btnSubmit?.removeAttribute("disabled");
            btnSubmit?.classList.remove("disabled");
        }
        return false;
    },
    custom_valid(e: any) {},
};

export const callBackDisabledBtnSubmit = () => {
    const btnSubmit = document.getElementById("frm_btn_submit");
    btnSubmit?.setAttribute("disabled", "disabled");
    btnSubmit?.classList.add("disabled");
};
