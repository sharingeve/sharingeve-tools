import React, { Fragment } from "react";
import clsx from "clsx";
import style from "./form.module.css";
import { FormProps, itemProps } from "./FormInterface";
import { i18nVi } from "@/lib/i18n/vi";
import InputCustom from "./elements/Input";
import SelectCus from "./elements/Select";
import TextareaCus from "./elements/Textarea";

export default function Form({ data, items }: FormProps) {
    const handleOnSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const form = e.target as HTMLFormElement;
        const requiredElements = form.querySelectorAll('[data-require="true"]');
        document.querySelectorAll(".error_display").forEach((item) => {
            if (!item.classList.contains("error_hold")) {
                item.remove();
            }
        });
        if (requiredElements.length > 0) {
            let stopForm = false;
            requiredElements.forEach((item: any) => {
                if (item.value == "") {
                    stopForm = true;
                    item.insertAdjacentHTML(
                        "afterend",
                        `<p id="err_check_form" class="error_display">
                            Vui lòng không bỏ trống ô này.
                        </p>`
                    );
                }
            });
            if (stopForm) {
                return;
            }
        }
        const values = Object.fromEntries(new FormData(form).entries());
        data.doSubmit(values);
    };
    const positionBtnClass = () => {
        switch (data.positionBtn) {
            case "center":
                return style.justify_center_button;
            case "right":
                return style.justify_right_button;
            default:
                return style.justify_left_button;
        }
    };
    const displayDOMForm = (item: itemProps) => {
        const options = item.options;
        switch (item.type) {
            case "select":
                return (
                    <SelectCus
                        label={options.label}
                        name={options.name}
                        txtDefaultSelect={options.txtDefaultSelect}
                        dataSelect={options.dataSelect}
                        selected={options.selected}
                        blurInput={options.blurInput}
                        changeInput={options.changeInput}
                        styleEl={options.styleEl}
                        classEl={options.classEl}
                        focus={options.focus}
                        required={options.required}
                        validate={options.validate}
                        disabled={options.disabled}
                    />
                );
            case "textarea":
                return (
                    <TextareaCus
                        name={options.name}
                        label={options.label}
                        rowsTextarea={options.rowsTextarea}
                        colsTextarea={options.colsTextarea}
                        resizeTextareaNone={options.resizeTextareaNone}
                        blurInput={options.blurInput}
                        changeInput={options.changeInput}
                        defaultVal={options.defaultVal}
                        styleEl={options.styleEl}
                        classEl={options.classEl}
                        focus={options.focus}
                        placeholder={options.placeholder}
                        required={options.required}
                        validate={options.validate}
                        disabled={options.disabled}
                        readonly={options.readonly}
                    />
                );
            default:
                return (
                    <InputCustom
                        type={options.type}
                        label={options.label}
                        name={options.name}
                        blurInput={options.blurInput}
                        changeInput={options.changeInput}
                        defaultVal={options.defaultVal}
                        placeholder={options.placeholder}
                        styleEl={options.styleEl}
                        classEl={options.classEl}
                        focus={options.focus}
                        maxInput={options.maxInput}
                        minInput={options.minInput}
                        required={options.required}
                        validate={options.validate}
                        disabled={options.disabled}
                        readonly={options.readonly}
                    />
                );
        }
    };
    return (
        <Fragment>
            <div className={clsx(style.wrapper_button, positionBtnClass())}>
                <button
                    onClick={(e) => {
                        data.doBack(e);
                    }}
                    className={data.classBtnBack}
                    style={data.styleBtnBack}
                    id={
                        "frm_btn_back" +
                        (data.idBtnBack ? " " + data.idBtnBack : "")
                    }
                    disabled={data.disabledBtnBack}
                >
                    {data.textBtnBack ? data.textBtnBack : i18nVi.TXT_BACK}
                </button>
                <button
                    className={data.classBtnSubmit}
                    style={data.styleBtnSubmit}
                    id={
                        "frm_btn_submit" +
                        (data.idBtnSubmit ? " " + data.idBtnSubmit : "")
                    }
                    form={data.idForm ? data.idForm : "frm_main"}
                    disabled={data.disabledBtnSubmit}
                    type="submit"
                >
                    {data.isUpdate ? i18nVi.TXT_UPDATE : i18nVi.TXT_ADD}
                </button>
            </div>
            <p className="error_display error_hold hide" id="err_frm"></p>
            <form
                action=""
                onSubmit={(e) => handleOnSubmit(e)}
                id={data.idForm ? data.idForm : "frm_main"}
            >
                {items.map((item) => (
                    <div
                        className={clsx(
                            style.form_group,
                            item.hiddenParent && "hide"
                        )}
                        key={item.type + Math.random()}
                    >
                        {displayDOMForm(item)}
                    </div>
                ))}
            </form>
        </Fragment>
    );
}
