interface Form {
    idForm?: string;
    doSubmit: <T>(e: T) => void;
    doBack: <T>(e: T) => void;
}
type positionBtn = "left" | "center" | "right";
interface Button {
    isUpdate?: boolean;
    textBtnBack?: string;
    positionBtn?: positionBtn;
    classBtnSubmit?: string;
    styleBtnSubmit?: {};
    classBtnBack?: string;
    styleBtnBack?: {};
    idBtnSubmit?: string;
    idBtnBack?: string;
    disabledBtnBack?: boolean;
    disabledBtnSubmit?: boolean;
}

export type TForm = Form & Button;

export type itemProps = {
    type: string;
    options: IElement;
    hiddenParent?: boolean;
};
export interface FormProps {
    data: TForm;
    items: itemProps[];
}

export type optionSelect = {
    label: string;
    value: string | number;
};
export type validateForm = {
    required?: (e: any, defaultVal: string | number) => boolean;
    custom_valid?: (e: any, setMessage_Err: any) => void;
};
export interface IElement {
    name: string;
    label?: string;
    type?: string;
    defaultVal?: string;
    placeholder?: string;
    dataSelect?: optionSelect[];
    txtDefaultSelect?: string;
    selected?: string | number;
    rowsTextarea?: number;
    colsTextarea?: number;
    resizeTextareaNone?: boolean;
    blurInput?: <T>(e: T) => void;
    changeInput?: <T>(e: T) => void;
    classEl?: string;
    styleEl?: object;
    focus?: boolean;
    maxInput?: number;
    minInput?: number;
    required?: boolean;
    validate?: validateForm;
    disabled?: boolean,
    readonly?: boolean
}
