import React from "react";
import { Button } from "antd";
import type { ConfigProviderProps } from "antd";

type SizeType = ConfigProviderProps["componentSize"];
interface IFieldButton {
    nameBtn: React.ReactNode;
    icon?: React.ReactNode;
    id?: string;
    className?: string;
    htmlType?: any;
    type?: any;
    onClick?: () => void;
    style?: object;
    disabled?: any;
    size?: SizeType;
    ghost?: boolean;
    danger?: boolean;
}

const FieldButton: React.FC<IFieldButton> = ({ nameBtn, ...props }) => {
    return <Button {...props}>{nameBtn}</Button>;
};

export default FieldButton;
