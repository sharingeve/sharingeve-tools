import React from "react";
import { Input } from "antd";

interface IFieldInput {
    name: string;
    id?: string;
    className?: string;
    type?: string;
    placeholder?: string;
    value?: string;
    defaultValue?: string;
    scoped?: string;
    onBlur?: any;
    onChange?: any;
    readOnly?: any;
    disabled?: any;
}

const FieldInput: React.FC<IFieldInput> = ({ scoped, ...props }) => {
    const renderUI: any = () => {
        switch (scoped) {
            case 'passwordInput':
                return <Input.Password {...props} />;
            default:
                return <Input {...props}/>;
        }
    };
    return renderUI();
};

export default FieldInput;
