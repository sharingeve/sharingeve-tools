"use client";

import React, { useEffect } from "react";
import { useRouter } from "next/navigation";
import { Layout } from "antd";
import { LogoutOutlined } from "@ant-design/icons";
import clsx from "clsx";
import styles from "./main-dashboard.module.css";
import { usePathname, useSearchParams } from "next/navigation";
import { destroyNotify } from "../Notification";
import LoadingUI from "@/components/molecules/LoadingUI";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { getLoadingRedux } from "@/store/common/selectors";
import { setLoadingRedux } from "@/store/common/actions";
import { logoutExpiredToken } from "@/store/users/actions";
import Constants from "@/lib/constants/constants";
import { handleLogOut } from "@/layouts/Header/HeaderHandler";
import { accessToken, userDetail } from "@/store/users/selector";

const { Content } = Layout;

const MainDashboard = ({ children }: { children: React.ReactNode }) => {
    const pathname = usePathname();
    const searchParams = useSearchParams();
    const dispatch = useAppDispatch();
    const token = useAppSelector(accessToken);
    const user: any = useAppSelector(userDetail);
    const loading = useAppSelector(getLoadingRedux);
    const router = useRouter();

    useEffect(() => {
        dispatch(setLoadingRedux(false));
        if (
            typeof window !== "undefined" &&
            window.localStorage.getItem("keyNotify")
        ) {
            const keyDestroy = localStorage.getItem("keyNotify") ?? "";
            destroyNotify(keyDestroy);
        }

        if (searchParams.get("logout") == "1" && pathname == "/login") {
            dispatch(logoutExpiredToken());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pathname, searchParams]);

    return (
        <Content className={clsx(styles.wrapper)}>
            {loading && <LoadingUI isOpen={loading} />}
            <div className={clsx(styles.logout)}>
                <label
                    onClick={async () =>
                        await handleLogOut(dispatch, router, {
                            token,
                            userId: user?.id,
                        })
                    }
                >
                    <LogoutOutlined />
                    {Constants.AUTHEN.LOG_OUT.LABEL}
                </label>
            </div>
            <div className={clsx(styles.content)}>{children}</div>
        </Content>
    );
};

export { MainDashboard };
