"use client";

import React, { useEffect } from "react";
import { App, ConfigProvider, Layout } from "antd";
import clsx from "clsx";
import styles from "./main.module.css";
import Menus from "@/components/molecules/menu";
import { StyleProvider } from "@ant-design/cssinjs";
import { usePathname, useSearchParams } from "next/navigation";
import { destroyNotify } from "../Notification";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { getLoadingRedux } from "@/store/common/selectors";
import { setLoadingRedux } from "@/store/common/actions";
import LoadingUI from "@/components/molecules/LoadingUI";
import { logoutExpiredToken } from "@/store/users/actions";

const { Content } = Layout;

const Main = ({ children }: { children: React.ReactNode }) => {
    const pathname = usePathname();
    const searchParams = useSearchParams();
    const dispatch = useAppDispatch();
    const loading = useAppSelector(getLoadingRedux);
    const { locale, theme } = React.useContext(ConfigProvider.ConfigContext);
    React.useLayoutEffect(() => {
        ConfigProvider.config({
            holderRender: (children) => (
                <StyleProvider hashPriority="high">
                    <ConfigProvider
                        prefixCls="static"
                        iconPrefixCls="icon"
                        locale={locale}
                        theme={theme}
                    >
                        <App
                            message={{ maxCount: 1 }}
                            notification={{ maxCount: 1 }}
                        >
                            {children}
                        </App>
                    </ConfigProvider>
                </StyleProvider>
            ),
        });
    }, [locale, theme]);

    useEffect(() => {
        dispatch(setLoadingRedux(false));
        if (
            typeof window !== "undefined" &&
            window.localStorage.getItem("keyNotify")
        ) {
            const keyDestroy = localStorage.getItem("keyNotify") ?? "";
            destroyNotify(keyDestroy);
        }

        if (searchParams.get('logout') == '1' && pathname == '/login') {
            dispatch(logoutExpiredToken());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pathname, searchParams]);

    return (
        <Layout className={clsx(styles.layout)}>
            <Menus />
            <Layout className={clsx(styles.layout_content)}>
                <Content className={clsx(styles.content)}>
                    {loading && <LoadingUI isOpen={loading} />}
                    {children}
                </Content>
            </Layout>
        </Layout>
    );
};

export default Main;
