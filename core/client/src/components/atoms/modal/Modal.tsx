import { Modal } from "antd";
// import { ModalFooterRender } from "antd/es/modal/interface";

interface IModal {
    open: boolean;
    title: string;
    content: React.ReactNode;
    onCallBack?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    onCancelCallBack?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    footer?: any;
    customOkText?: React.ReactNode;
    customCancelText?: React.ReactNode;
    className?: string;
    zIndex?: number;
    okBtnProps?: any;
    cancelBtnProps?: any;

}

export default function ModalComponent({
    open,
    title,
    content,
    onCallBack,
    onCancelCallBack,
    footer,
    customOkText,
    customCancelText,
    className,
    zIndex,
    okBtnProps,
    cancelBtnProps
}: IModal) {
    const handleOk = (e: any) => {
        return onCallBack ? onCallBack(e) : () => {};
    };
    const handleCancel = (e: any) => {
        return onCancelCallBack ? onCancelCallBack(e) : () => {};
    };
    return (
        <div>
            <Modal
                open={open}
                title={title}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={footer}
                okText={customOkText || 'Đồng ý'}
                okButtonProps={okBtnProps}
                cancelButtonProps={cancelBtnProps}
                cancelText={customCancelText || 'Hủy bỏ'}
                className={className}
                zIndex={zIndex ?? 1000}
            >
                {content}
            </Modal>
        </div>
    );
}
