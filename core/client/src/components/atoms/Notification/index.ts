import { isEmptyObject } from "@/lib/utils";
import { notification } from "antd";

type ConfigType = Parameters<typeof notification.config>[0];
type errorNotifyType = Parameters<typeof notification.error>[0];
type warnNotifyType = Parameters<typeof notification.warning>[0];
type successNotifyType = Parameters<typeof notification.success>[0];
type openNotifyType = Parameters<typeof notification.open>[0];
type infoNotifyType = Parameters<typeof notification.info>[0];
type destroyNotifyType = Parameters<typeof notification.destroy>[0];

const configNotify = (config: ConfigType) => {
    return notification.config(config);
};

export const destroyNotify = (keyDestroy: destroyNotifyType) => {
    if (keyDestroy) {
        return notification.destroy(keyDestroy);
    }
};

export const openNotify = (config: openNotifyType, options?: ConfigType) => {
    let configOptions = {
        maxCount: 1,
    };
    if (!isEmptyObject(options)) {
        configOptions = { ...configOptions, ...options };
    }

    configNotify(configOptions);
    return notification.open(config);
};

export const infoNotify = (config: infoNotifyType, options?: ConfigType) => {
    let configOptions = {
        maxCount: 1,
    };
    if (!isEmptyObject(options)) {
        configOptions = { ...configOptions, ...options };
    }

    configNotify(configOptions);
    return notification.open(config);
};

export const errorNotify = (config: errorNotifyType, options?: ConfigType) => {
    let configOptions = {
        maxCount: 1,
    };
    if (!isEmptyObject(options)) {
        configOptions = { ...configOptions, ...options };
    }

    configNotify(configOptions);
    return notification.error(config);
};

export const warningNotify = (config: warnNotifyType, options?: ConfigType) => {
    let configOptions = {
        maxCount: 1,
    };
    if (!isEmptyObject(options)) {
        configOptions = { ...configOptions, ...options };
    }

    configNotify(configOptions);
    return notification.warning(config);
};

export const successNotify = (
    config: successNotifyType,
    options?: ConfigType
) => {
    let configOptions = {
        maxCount: 1,
    };
    if (!isEmptyObject(options)) {
        configOptions = { ...configOptions, ...options };
    }

    configNotify(configOptions);
    return notification.success(config);
};
