import { v4 as uuidv4 } from "uuid";
import { errorNotify, successNotify } from "@/components/atoms/Notification";
import {
    deleteParamRequest,
    initParamsRequest,
    redirectPaginate,
    searchParamsRequest,
} from "@/service/params-service";

export const initParams = async (
    setParams: any,
    setPage: any,
    setTotalPage: any,
    setLinkNext: any,
    setLinkPrev: any
) => {
    const res = await initParamsRequest();

    setParams(res.data);
    setPage(res.current_page);
    setTotalPage(res.total_page);
    setLinkNext(res.nextPage);
    setLinkPrev(res.prevPage);
};

export const paginatePage = async (
    setIsLoading: any,
    page: any,
    setPage: any,
    setLinkPrev: any,
    setLinkNext: any,
    setParams: any,
    totalPage?: any,
    nextPrev?: number
) => {
    setIsLoading(true);
    let pageHandle = parseInt(page);
    if (nextPrev == 1) {
        pageHandle = pageHandle - 1;
        if (pageHandle < 1) {
            pageHandle = 1;
        }
    } else if (nextPrev == 2) {
        pageHandle = pageHandle + 1;
        if (pageHandle > parseInt(totalPage)) {
            pageHandle = totalPage;
        }
    }
    const data = await redirectPaginate(pageHandle);

    setPage(data.current_page);
    setLinkPrev(data.prevPage);
    setLinkNext(data.nextPage);
    setParams(data.data);
    setIsLoading(false);
};

const deleteParam = async (id: string | null, user_id: string) => {
    if (!id) {
        return false;
    }
    return await deleteParamRequest(id, user_id);
};
export const handleDeleteParam = async (paramId: string, user_id: string) => {
    const result = await deleteParam(paramId, user_id);
    const keyNotify = uuidv4();
    localStorage.setItem("keyNotify", keyNotify);
    if (result) {
        successNotify({
            message: "Thông báo",
            description: "Xoá tham số thành công.",
            key: keyNotify,
            onClose: () => {
                localStorage.removeItem("keyNotify");
            },
        });

        return true;
    }
    errorNotify({
        message: "Thông báo",
        description: result,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
    return false;
};

export const handleSearch = async (
    value: string,
    setParams: any,
    setPage: any,
    setTotalPage: any,
    setLinkNext: any,
    setLinkPrev: any
) => {
    const res = await searchParamsRequest(value);

    setParams(res.data);
    setPage(res.current_page);
    setTotalPage(res.total_page);
    setLinkNext(res.nextPage);
    setLinkPrev(res.prevPage);
};
