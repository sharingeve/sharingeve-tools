import {
    itemProps,
    optionSelect,
    TForm,
} from "@/components/atoms/Form/FormInterface";
import { isNumber } from "@/lib/utils";
import {
    createParamsRequest,
    getParamById,
    updateParamsRequest,
} from "@/service/params-service";

export const TData = <T>(options: T): T & TForm => {
    return {
        positionBtn: "right",
        doSubmit() {},
        doBack() {},
        ...options,
    };
};

const typeParams: optionSelect[] = [
    { label: "Array", value: "array" },
    { label: "Object", value: "object" },
    { label: "Number", value: "number" },
    { label: "String", value: "string" },
];
type DataHtmlForm = {
    user_id: string;
    param?: dataInput;
    isUpdate?: boolean;
};
type nameInput = {
    creditor: string;
    description: string;
    name: string;
    typeParam: string;
    valueParam: any;
};
export type dataInput = {
    description: string;
    name: string;
    type: string;
    value: any;
    id: string;
};
export const getParam = (user_id: string, param_id?: string | null) => {
    return new Promise(async (resolve, reject) => {
        if (param_id) {
            try {
                const res = await getParamById(param_id, user_id);
                if (typeof res == "object") {
                    resolve(res);
                    return;
                }

                reject(res);
                return;
            } catch (error) {
                reject(error);
            }
        } else {
            reject("Không tìm thấy tham số.");
        }
    });
};
export const htmlForm: (data: DataHtmlForm) => itemProps[] = (data) => {
    const htmlDOM: itemProps[] = [
        {
            type: "text",
            options: {
                name: "name",
                focus: true,
                label: "Tên tham số",
                required: true,
                defaultVal: data.param ? data.param.name : "",
                validate: {
                    custom_valid(e, setMessage_Err) {},
                },
                disabled: data.isUpdate ? true : false,
            },
        },
        {
            type: "select",
            options: {
                name: "typeParam",
                label: "Loại tham số",
                dataSelect: typeParams,
                required: true,
                selected: data.param ? data.param.type : "",
            },
        },
        {
            type: "text",
            options: {
                name: "valueParam",
                label: "Giá trị tham số",
                required: true,
                defaultVal:
                    data.param && (data.param?.value || data.param?.value == 0)
                        ? typeof data.param.value == "object"
                            ? JSON.stringify(data.param.value)
                            : data.param.value
                        : "",
            },
        },
        {
            type: "textarea",
            options: {
                name: "description",
                label: "Mô tả tham số",
                defaultVal: data.param ? data.param.description : "",
            },
        },
        {
            type: "text",
            options: {
                type: "hidden",
                name: "creditor",
                defaultVal: data.user_id,
            },
            hiddenParent: true,
        },
    ];
    return htmlDOM;
};

const validSubmit = (values: nameInput) => {
    switch (values.typeParam) {
        case "array":
            const correctedInputArray = values.valueParam.replace(/'/g, '"');
            try {
                return (
                    typeof JSON.parse(correctedInputArray) == "object" &&
                    JSON.parse(correctedInputArray) instanceof Array
                );
            } catch (error) {
                return false;
            }
        case "object":
            const correctedInputObj = values.valueParam.replace(/'/g, '"');
            try {
                return (
                    typeof JSON.parse(correctedInputObj) == "object" &&
                    !(JSON.parse(correctedInputObj) instanceof Array)
                );
            } catch (error) {
                return false;
            }
        case "number":
            return isNumber(values.valueParam);
        default:
            return typeof values.valueParam == "string";
    }
};
const displayErr = (message = "", display = false) => {
    const err_p = document.querySelector("#err_frm");
    if (err_p) {
        err_p.classList.add("hide");
        if (display) {
            err_p.classList.remove("hide");
        }
        err_p.innerHTML = message;
    }
};
export const handleSubmit = async (
    values: nameInput,
    isUpdate = false,
    param_id?: string | null
) => {
    if (!validSubmit(values)) {
        displayErr(
            "Giá trị nhập không khớp với loại đã chọn. Hãy kiểm tra lại",
            true
        );
        return isUpdate
            ? "Cập nhật tham số không thành công."
            : "Thêm tham số không thành công.";
    }
    displayErr();
    const data = { ...values };
    if (values.typeParam == "array" || values.typeParam == "object") {
        const correctedInput = values.valueParam.replace(/'/g, '"');
        const convertValues = JSON.parse(correctedInput);
        data.valueParam = convertValues;
    } else if (values.typeParam == "number") {
        data.valueParam = parseInt(values.valueParam);
    }
    return isUpdate
        ? param_id
            ? await updateParamsRequest(param_id, data)
            : "Không tìm thấy tham số để cập nhật"
        : await createParamsRequest(data);
};
