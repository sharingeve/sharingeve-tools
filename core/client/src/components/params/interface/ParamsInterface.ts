import { TypeDispatch } from "@/hooks/redux-hook";

export interface IHomeParams {
    id: string;
    name: string;
    value: string;
    type: string;
    description: string;
}

export interface IEditParam<R> {
    router: R;
    dispatch: TypeDispatch;
    user_id: string;
    param_id?: string | null;
}
