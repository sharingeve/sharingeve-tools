import { v4 as uuidv4 } from "uuid";
import Form from "@/components/atoms/Form";
import { handleSubmit, htmlForm, TData } from "../handler/EditParamHandle";
import { redirectNoScroll, toggleDisplayLoading } from "@/lib/utils";
import { IEditParam } from "../interface/ParamsInterface";
import { errorNotify, successNotify } from "@/components/atoms/Notification";

export default function CreateParamElement({
    router,
    dispatch,
    user_id,
}: IEditParam<any>) {
    const typeData = TData({
        // doSubmit(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        async doSubmit(values: any) {
            toggleDisplayLoading(dispatch, true);

            const res = await handleSubmit(values);
            const keyNotify = uuidv4();
            localStorage.setItem("keyNotify", keyNotify);
            if (typeof res == "string") {
                toggleDisplayLoading(dispatch, false);
                return errorNotify({
                    message: "Thông báo",
                    description: res,
                    key: keyNotify,
                    onClose: () => {
                        localStorage.removeItem("keyNotify");
                    },
                });
            }
            successNotify({
                message: "Thông báo",
                description: "Thêm tham số thành công.",
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });

            setTimeout(() => {
                router.push("/dashboard/params", { scroll: true });
            }, 3000);
        },
        doBack(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
            toggleDisplayLoading(dispatch, true);
            redirectNoScroll(router, "/dashboard/params");
        },
        classBtnSubmit: "btn_primary",
        disabledBtnSubmit: true,
    });
    return <Form data={typeData} items={htmlForm({ user_id })} />;
}
