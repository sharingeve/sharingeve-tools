import { v4 as uuidv4 } from "uuid";
import Form from "@/components/atoms/Form";
import {
    getParam,
    handleSubmit,
    htmlForm,
    dataInput,
    TData,
} from "../handler/EditParamHandle";
import React, { useEffect, useState } from "react";
import { IEditParam } from "../interface/ParamsInterface";
import { redirectNoScroll, toggleDisplayLoading } from "@/lib/utils";
import { errorNotify, successNotify } from "@/components/atoms/Notification";
import { callBackDisabledBtnSubmit } from "@/components/atoms/Form/elements/ElementHandle";

export default function UpdateParamElement({
    router,
    dispatch,
    user_id,
    param_id,
}: IEditParam<any>) {
    const [param, setParam] = useState<dataInput>({
        name: "",
        type: "",
        value: "",
        description: "",
        id: "",
    });
    useEffect(() => {
        toggleDisplayLoading(dispatch, true);
        const res = getParam(user_id, param_id);
        res.then((item: any) => {
            if (typeof item == "object") {
                setParam(item);
            } else {
                setParam({
                    name: "",
                    type: "",
                    value: "",
                    description: "",
                    id: "",
                });
            }
        });
        toggleDisplayLoading(dispatch, false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [param_id, user_id]);
    const typeData = TData({
        isUpdate: true,
        async doSubmit(values: any) {
            toggleDisplayLoading(dispatch, true);

            const res = await handleSubmit(values, true, param_id);
            const keyNotify = uuidv4();
            localStorage.setItem("keyNotify", keyNotify);
            toggleDisplayLoading(dispatch, false);
            if (typeof res == "string") {
                return errorNotify({
                    message: "Thông báo",
                    description: res,
                    key: keyNotify,
                    onClose: () => {
                        localStorage.removeItem("keyNotify");
                    },
                });
            }

            callBackDisabledBtnSubmit();
            setParam(res);
            successNotify({
                message: "Thông báo",
                description: "Cập nhật tham số thành công.",
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });
        },
        doBack(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
            toggleDisplayLoading(dispatch, true);
            redirectNoScroll(router, "/dashboard/params");
        },
        classBtnSubmit: "btn_primary",
        disabledBtnSubmit: true,
    });
    return (
        <Form
            data={typeData}
            items={htmlForm({ user_id, param, isUpdate: true })}
        />
    );
}
