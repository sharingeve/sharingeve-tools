import React, { useEffect, useState } from "react";
import { IEditParam, IHomeParams } from "./interface/ParamsInterface";
import Table from "../atoms/Table";
import {
    handleDeleteParam,
    handleSearch,
    initParams,
    paginatePage,
} from "./handler/home_param_handle";
import Link from "next/link";

export default function HomeParamComponent({
    router,
    dispatch,
    user_id,
}: Omit<IEditParam<any>, "param_id">) {
    const [params, setParams] = useState<IHomeParams[] | []>([]);
    const [page, setPage] = useState<string>("1");
    const [totalPage, setTotalPage] = useState<string>("1");
    const [linkNext, setLinkNext] = useState<boolean>();
    const [linkPrev, setLinkPrev] = useState<boolean>();
    const [isLoading, setIsLoading] = useState<boolean>(false);

    useEffect(() => {
        setIsLoading(true);
        (async () => {
            await initParams(
                setParams,
                setPage,
                setTotalPage,
                setLinkNext,
                setLinkPrev
            );
            setIsLoading(false);
        })();
    }, []);
    const handleOnBlurInputPaginate = async (e: any) => {
        let value = e.target.value;
        if (value > totalPage) {
            e.target.value = totalPage;
            value = totalPage;
        } else if (value < 1) {
            e.target.value = 1;
            value = 1;
        }
        if (page == value) {
            return;
        }
        await paginatePage(
            setIsLoading,
            value,
            setPage,
            setLinkPrev,
            setLinkNext,
            setParams
        );
    };

    const handleRedirectPaginate = async (
        page: string | number,
        nextPre?: number
    ) => {
        if (nextPre) {
            return await paginatePage(
                setIsLoading,
                page,
                setPage,
                setLinkPrev,
                setLinkNext,
                setParams,
                totalPage,
                nextPre
            );
        }
        return await paginatePage(
            setIsLoading,
            page,
            setPage,
            setLinkPrev,
            setLinkNext,
            setParams
        );
    };

    const clickDelete = async (
        e: any,
        param_id: string,
        name_param: string
    ) => {
        router.replace("dashboard/params");
        if (
            confirm(
                "Kiểm tra lại có chắc muốn xóa tham số `" +
                    name_param +
                    "` này không?"
            )
        ) {
            setIsLoading(true);
            const res = await handleDeleteParam(param_id, user_id);
            if (!res) {
                e.preventDefault();
                setIsLoading(false);
                return;
            }

            await initParams(
                setParams,
                setPage,
                setTotalPage,
                setLinkNext,
                setLinkPrev
            );
            setIsLoading(false);
        }
    };

    return (
        <Table
            isLoading={isLoading}
            setIsLoading={setIsLoading}
            linkAdd={"/dashboard/params?page=create"}
            positionBtnRight={true}
            page={page}
            total={totalPage}
            blurSearch={async (value) => {
                setIsLoading(true);
                if (value) {
                    await handleSearch(
                        value,
                        setParams,
                        setPage,
                        setTotalPage,
                        setLinkNext,
                        setLinkPrev
                    );
                } else {
                    await initParams(
                        setParams,
                        setPage,
                        setTotalPage,
                        setLinkNext,
                        setLinkPrev
                    );
                }
                setIsLoading(false);
            }}
            paginate_center={true}
            blurInputPaginate={handleOnBlurInputPaginate}
            page_last={async () => await handleRedirectPaginate(totalPage)}
            page_next={async () => await handleRedirectPaginate(page, 2)}
            page_prev={async () => handleRedirectPaginate(page, 1)}
            page_first={async () => await handleRedirectPaginate(1)}
            disabledNextPage={!linkNext}
            disabledPrevPage={!linkPrev}
            cols={["5", "20", "20", "20", "25", "10"]}
            thead={["ID", "Tên", "Loại", "Giá trị", "Mô tả"]}
            data={
                params.length > 0 ? (
                    params.map((item, index) => (
                        <tr key={"data_table_body_" + index}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.type}</td>
                            <td>
                                {typeof item.value == "object"
                                    ? JSON.stringify(item.value)
                                    : item.value}
                            </td>
                            <td>{item.description}</td>
                            <td>
                                <Link
                                    href={`?page=update&id=${item.id}`}
                                    onClick={() => setIsLoading(true)}
                                >
                                    Sửa
                                </Link>
                                <Link
                                    href={`#!`}
                                    onClick={async (e) =>
                                        await clickDelete(e, item.id, item.name)
                                    }
                                >
                                    Xóa
                                </Link>
                            </td>
                        </tr>
                    ))
                ) : (
                    <tr>
                        <td colSpan={6} align="center">
                            Không có dữ liệu tham số
                        </td>
                    </tr>
                )
            }
        />
    );
}
