import CreateParamElement from "./elements/CreateParamElement";
import UpdateParamElement from "./elements/UpdateParamElement";

export default function UpdateParamComponent({
    page,
    id,
    user_id,
    router,
    dispatch,
}: {
    page: string;
    id?: string | null;
    user_id: string;
    router: any;
    dispatch: any;
}) {
    const displayComponent = () => {
        if (page == "create") {
            return (
                <CreateParamElement
                    router={router}
                    dispatch={dispatch}
                    user_id={user_id}
                />
            );
        } else {
            return (
                <UpdateParamElement
                    router={router}
                    dispatch={dispatch}
                    user_id={user_id}
                    param_id={id}
                />
            );
        }
    };
    return displayComponent();
}
