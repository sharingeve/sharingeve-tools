/* eslint-disable react-hooks/rules-of-hooks */
import { usePapaParse } from "react-papaparse";

export const headerMemoPayment = [
    { label: "Ngày thanh toán", key: "createAt" },
    { label: "Tổng tiền", key: "amount" },
    { label: "Ngân hàng", key: "bankName" },
    { label: "Hình thức", key: "payType" },
    { label: "Trạng thái", key: "payStatus" },
    {
        label: "Ngày xác nhận",
        key: "updateAt",
    },
];

export const headerTemplateMemoDetail = [
    { label: "Tiền nợ", key: "priceNumber" },
    { label: "Lý do", key: "reason" },
    { label: "Ngày nợ", key: "createDate" },
];

const checkTemplateMemoDetail = (arr: any[]) => {
    const templateMemoDetail = ["Tiền nợ", "Lý do", "Ngày nợ"];
    return arr.filter((item) => !templateMemoDetail.includes(item));
};

const setKeyForData = (arr: any[]) => {
    const listKey = ["priceNumber", "reason", "createDate"];

    return listKey.reduce((obj: any, key: any, index: any) => {
        obj[key] = arr[index]; // {}.key = arr[0]
        return obj;
    }, {});
};

export const handlerImportCsv = (fileAsText: any) => {
    const { readString } = usePapaParse();
    let dataRead: {
        error: boolean;
        data: any[];
    } = {
        error: false,
        data: [],
    };
    readString(fileAsText, {
        // header: true,
        complete: function (results: any) {
            const { data, errors } = results;
            // console.log("errors.length", errors.length);
            if (errors.length !== 0) {
                dataRead = { ...dataRead, error: true, data: errors };
                return;
            }
            // check header
            if (data.length <= 1) {
                dataRead = {
                    ...dataRead,
                    error: true,
                    data: ["Csv rỗng không có data"],
                };
                return;
            }
            if (
                data.length > 1 &&
                data[0]?.length == 3 &&
                checkTemplateMemoDetail(data[0]).length == 0
            ) {
                let cloneData = [...data];
                cloneData.shift();
                let newData: any[] = [];
                for (let i in cloneData) {
                    newData.push(setKeyForData(cloneData[i]));
                }
                dataRead = { ...dataRead, data: newData };
                return;
            }

            dataRead = {
                ...dataRead,
                error: true,
                data: ["Template không đúng định dạng"],
            };
        },
    });

    return dataRead;
};
