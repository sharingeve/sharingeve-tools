import { apiUpdateQrPayment } from "@/service/memo-service";

export const handleFinishUploadImgPayment = async (
    extraId: string,
    previewImg: any
) => {
    // https://github.com/meech-ward/s3-get-put-and-delete
    // https://www.youtube.com/watch?v=eQAIojcArRY&ab_channel=SamMeech-Ward
    const formData = new FormData();
    formData.append("extraId", extraId);
    formData.append("qr_upload", previewImg);

    const upload: any = await apiUpdateQrPayment("/memo/upload-qr", formData, {
        headers: { "Content-Type": "multipart/form-data;charset=UTF-8" },
    });

    if (upload?.status) {
        return {
            status: true,
            data: upload.data,
            msg: "Update QR thành công",
        };
    }

    return {
        status: false,
        data: {},
        msg: upload?.error,
    };
};

export const resetFiled = (
    setIsOpenModalAddQr: any,
    setDisabledAddBtn: any,
    setPreviewImg: any
) => {
    setIsOpenModalAddQr(false);
    setDisabledAddBtn(true);
    setPreviewImg(null);
};
