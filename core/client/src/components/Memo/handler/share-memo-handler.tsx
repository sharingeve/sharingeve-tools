import ConfigApp from "@/lib/config";
import MemoConstants from "@/lib/constants/memo-constants";
import vietQrConstants from "@/lib/constants/vietqr-constants";
import { createMemoPay } from "@/service/memo-service";
import { Datum } from "@/service/vietqr-service";
import { apiGetListBank } from "@/service/vietqr-service";
import { setNotify } from "@/store/common/actions";

export interface DataType {
    key: React.Key;
    price: string;
    priceNumber: number;
    reason: string;
    createDate: any;
}

export interface IModalContent {
    memoId: string;
    debtor?: string;
    amount: number;
    selectPay: any;
    setSelectPay: any;
    selectQR: any;
    setSelectQR: any;
    listBankFromVietQR: Datum[];
}

export const handlePay = (setOpenModal: any) => {
    setOpenModal(true);
};

export const checkPaymentProgress = (status: any) => {
    return status ? status === MemoConstants.MEMO_PAY.PAY_STATUS.WAIT : false;
};

/**
 * call api get list bank from https://api.vieqr.com/list-banks/
 *
 *
 */
export const initListBank = async () => {
    return await apiGetListBank();
};

type TImgQrOptions = {
    amount: number | string;
    addInfo: string;
    accountName: number | string;
};

const renderImgQr = ({
    bankBinOrShortName,
    numberBank,
    template,
    options,
}: {
    bankBinOrShortName: string | undefined;
    numberBank: string;
    template?: string;
    options: TImgQrOptions;
}) => {
    const bankId = bankBinOrShortName;
    const accountNo = numberBank;
    const templateSelect = template ?? vietQrConstants.THEME.COMPACT_2;

    return `${
        ConfigApp.env.uri_img_qr
    }/${bankId}-${accountNo}-${templateSelect}.jpg?amount=${
        options.amount
    }&addInfo=${encodeURIComponent(
        options.addInfo
    )}&accountName=${encodeURIComponent(options.accountName)}`;
};
export const pathQrBank = (
    bank: string,
    isQR = false,
    listBankFromVietQR?: Datum[],
    data?: any
) => {
    let listPathImgQr = {
        [MemoConstants.LIST_QR.TECH.name]: "/assets/img/tech_qr.jpg",
        [MemoConstants.LIST_QR.MBB.name]: "/assets/img/mbb_qr.jpg",
        [MemoConstants.LIST_QR.VCB.name]: "/assets/img/vcb_qr.jpg",
    };
    if (isQR) {
        const listCodeAndInfoBank = {
            [MemoConstants.LIST_QR.TECH.name]: {
                code: MemoConstants.LIST_QR.TECH.code,
                number: MemoConstants.LIST_NUMBER_BANK.TECH,
            },
            [MemoConstants.LIST_QR.MBB.name]: {
                code: MemoConstants.LIST_QR.MBB.code,
                number: MemoConstants.LIST_NUMBER_BANK.MBB,
            },
            [MemoConstants.LIST_QR.VCB.name]: {
                code: MemoConstants.LIST_QR.VCB.code,
                number: MemoConstants.LIST_NUMBER_BANK.VCB,
            },
        };
        const detailBank = listBankFromVietQR?.find(
            (item) => item.code == listCodeAndInfoBank[bank].code
        );

        const options: TImgQrOptions = {
            amount: data?.amount || 0,
            addInfo: data?.desc || "",
            accountName: MemoConstants.OWNER_CARD,
        };

        return renderImgQr({
            bankBinOrShortName: detailBank?.shortName,
            numberBank: listCodeAndInfoBank[bank].number,
            options,
        });
    }

    return listPathImgQr[bank];
};

export const handleConfirmPay = async (
    userId: string,
    memoId: string,
    amount: number,
    debtor: string,
    bank: string,
    selectPay: string,
    setIsLoading: any,
    dispatch: any,
    setPaymentProgress: any,
    setOpenModal: any
) => {
    setIsLoading(true);
    const payType = selectPay == MemoConstants.LIST_PAY.BANK_TRANSFER ? 1 : 0;
    const resDto = {
        bank_pay: bank,
        pay_type: payType,
        amount,
        debtor,
        memo_id: memoId,
        creditors_id: userId,
    };
    const res = await createMemoPay("api/share/memo-pay/create", resDto);
    if (!res.status) {
        dispatch(
            setNotify({
                display: true,
                content:
                    "Đang có sự cố trang sẽ reload lại sau 5s để cập nhật thông tin. Hoặc liên hệ với người cho vay để kiểm tra lại!",
                type: "error",
            })
        );
        setTimeout(() => {
            window.location.reload();
        }, 5000);
        return false;
    }

    setIsLoading(false);
    setOpenModal(false);
    dispatch(
        setNotify({
            display: true,
            content:
                "Thông tin đã gửi đến người cho vay để kiểm tra. Cảm ơn đã xác nhận thanh toán.",
            type: "success",
        })
    );
    return setPaymentProgress(true);
};
