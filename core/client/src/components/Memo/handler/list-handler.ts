import { errorNotify, successNotify } from "@/components/atoms/Notification";
import { apiRoute, fetchGet } from "@/routers";
import { apiUpdatedDoneMemo } from "@/service/memo-service";

export const initMemoData = async () => {
    const apiCall = await fetchGet("/memo/list");

    if (apiCall?.status) {
        return apiCall;
    }
};

export const handleRedirectPaginate = async (limit: number, page: number) => {
    const apiCall = await apiRoute.apiGet(
        `/memo/list?limit=${limit}&page=${page}`
    );

    if (apiCall?.status) {
        return apiCall;
    }
};

export const handlePaymentDone = async (e: any, id: string) => {
    e.preventDefault();
    const pathUpdate = `/memo/${id}/payment/done`;
    const update: any = await apiUpdatedDoneMemo(pathUpdate);
    localStorage.setItem("keyNotify", "update_thanh_toan");
    if (!update.status) {
        errorNotify({
            message: "Thông báo",
            description: update.error,
            key: "update_thanh_toan",
            onClose: () => {
                localStorage.removeItem("keyNotify");
            },
        });
        return;
    }

    successNotify({
        message: "Thông báo",
        description: `Hoàn tất thanh toán ${id}`,
        key: "update_thanh_toan",
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};
