import FieldButton from "@/components/atoms/Button";
import FieldInput from "@/components/atoms/Input";
import ConfigApp from "@/lib/config";
import MemoConstants from "@/lib/constants/memo-constants";
import { i18nVi } from "@/lib/i18n/vi";
import {
    actionUpload,
    formatMoney,
    formatNumberWithThreeDigits,
    isEmptyObject,
    isValidDate,
    templateGetItem,
} from "@/lib/utils";
import {
    apiConfirmUpdateMemoPayment,
    apiDetailMemo,
} from "@/service/memo-service";
import MemoActions from "@/store/memo/memo-actions";
import { DatePicker, Upload, notification } from "antd";
import dayjs from "dayjs";
import React from "react";
import { handlerImportCsv } from "./csv-handler";
import { InboxOutlined } from "@ant-design/icons";

const fields = ["full_name", "price", "reason", "created_date", "img_qr"];
export const formBasicMemo = (router: any, isNew: boolean, isEdit: boolean) => [
    templateGetItem(
        <>
            <FieldButton
                onClick={() =>
                    router.push("/dashboard/memo", {
                        scroll: false,
                    })
                }
                type="default"
                style={{ marginRight: 16 }}
                nameBtn="Trở về"
            />
            <FieldButton
                type="primary"
                htmlType="submit"
                className="memo_form_button"
                disabled={!isEdit}
                nameBtn={isNew ? "Thêm mới" : "Cập nhật"}
            />
        </>,
        undefined,
        undefined,
        [],
        {}
    ),
    templateGetItem(
        <FieldInput placeholder={i18nVi.FULL_NAME_FIELD} name="full_name" />,
        i18nVi.FULL_NAME_FIELD,
        "full_name",
        inputRules.fullName()
    ),
    templateGetItem(
        <FieldInput placeholder={`Tiền nợ`} name="price" />,
        "Tiền nợ",
        "price",
        inputRules.price(isNew)
    ),
    templateGetItem(
        <FieldInput placeholder={`Lý do`} name="reason" />,
        "Lý do",
        "reason",
        inputRules.reason(isNew)
    ),
    templateGetItem(
        <DatePicker name="created_date" />,
        "Ngày nợ",
        "created_date",
        inputRules.created_at()
    ),
    templateGetItem(
        <Upload.Dragger
            name="img_qr"
            // action="https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188"
            customRequest={actionUpload}
            disabled={!isNew}
            listType="picture"
            fileList={undefined}
            maxCount={1}
        >
            <p className="ant-upload-drag-icon">
                <InboxOutlined />
            </p>
            <p className="ant-upload-text">
                Chọn hoặc kéo ảnh cần upload vào đây
            </p>
            <p className="ant-upload-hint">Chỉ upload 1 ảnh mỗi lần chọn</p>
        </Upload.Dragger>,
        "Ảnh QR",
        "img_qr",
        undefined,
        undefined,
        undefined,
        undefined,
        !isNew,
        handlerFile
    ),
];
const handlerFile = (e: any) => {
    console.log("e", e);
    if (Array.isArray(e)) {
        return e;
    }
    return e?.fileList[0]?.originFileObj;
};

const inputRules: { [key: string]: (isNew?: boolean) => any[] } = {
    fullName: function (): any[] {
        return [
            {
                required: true,
                message: i18nVi.FULL_NAME_REQUIRED,
            },
        ];
    },
    price: function (isNew?: boolean): any[] {
        return [
            {
                required: isNew ?? true,
                message: `Nhập số tiền nợ`,
            },
            {
                pattern: /^-?\d+$/,
                message: "Ký tự không hợp lệ. Hãy nhập ký tự là sô!",
            },
        ];
    },
    reason: function (isNew?: boolean): any[] {
        return [
            {
                required: isNew ?? true,
                message: `Nhập số lý do vay`,
            },
        ];
    },
    created_at: function (): any[] {
        return [
            ({ getFieldValue }: { getFieldValue: any }) => ({
                validator(_: any, value: any) {
                    if (value) {
                        return Promise.resolve();
                    }

                    if (
                        (getFieldValue("price") ||
                            getFieldValue("reason") ||
                            getFieldValue("full_name")) &&
                        !value
                    ) {
                        return Promise.reject(new Error("Nhập ngày nợ"));
                    }
                },
            }),
        ];
    },
};

/**
 * init memo detail by id
 *
 * @param {*} memoId
 */

export const initMemoDetailHandler = async (memoId: string) => {
    if (!memoId) return {};

    const res = await apiDetailMemo(`/memo/detail/${memoId}`);
    return res;
};

const getDataImportPassCondition = (data: any[]) => {
    const filter = data.filter(
        (item: any) =>
            /^\d+$/.test(item.priceNumber) && isValidDate(item.createDate)
    );

    return filter.map((item) => ({
        priceNumber: Number(item.priceNumber),
        reason: item.reason,
        createDate: dayjs(new Date(item.createDate)).format(
            "YYYY-MM-DD HH:mm:ss"
        ),
    }));
};

export const handleUpdatedMemo = async (
    data: any,
    scoped = MemoConstants.MEMO_UI.CREATE,
    form: any,
    importCsvAsText: string,
    formCsvRef: any,
    initialValues: any
) => {
    let result = {
        status: true,
    };
    if (scoped == MemoConstants.MEMO_UI.CREATE) {
        const memoInsert: any = await MemoActions.createMemo(data);
        if (!memoInsert.status) {
            return { ...result, status: false, data: memoInsert.error };
        }

        return { ...result, data: memoInsert.id };
    }

    // updated;
    const isValid = validateFormMemo(form, data);

    if (isValid) {
        return {
            ...result,
            status: false,
            data: "Vui lòng điền đầy đủ các input bắt buộc.",
        };
    }
    if (importCsvAsText) {
        const dataStructure = handlerImportCsv(importCsvAsText);
        formCsvRef.current.reset();
        // update to data send
        if (!dataStructure.error) {
            const dataPassCondition = getDataImportPassCondition(
                dataStructure.data
            );
            if (dataPassCondition.length > 0) {
                data.structureImport = JSON.stringify([...dataPassCondition]);
            }
        }

        if (dataStructure.error && dataStructure.data.length >= 1) {
            alert("Có lỗi upload xảy ra. Không import thành công.");
            form.setFieldsValue(initialValues);
            return {
                ...result,
                status: false,
                data: dataStructure.data[0],
            };
        }
    }

    const memoUpdated: any = await MemoActions.updatedMemo(data);
    if (!memoUpdated?.status) {
        return { ...result, status: false, data: memoUpdated.error };
    }
    return { ...result, data: memoUpdated };
};

type TMemoForm = {
    fullName: string;
    price?: string;
    created_date?: string;
    reason?: string;
};

export const validateFormMemo = (formAttr: any, dataForm: TMemoForm) => {
    const isNewMemo = dataForm?.price || dataForm?.reason;
    let isError = false;
    if (!isNewMemo) {
        return isError;
    }

    let valid = {};
    if (!dataForm?.price || !dataForm?.created_date || !dataForm?.reason) {
        isError = true;
        if (!dataForm?.price) {
            valid = Object.assign(valid, {
                name: "price",
                errors: ["Nhập số tiền nợ"],
            });
        }

        if (!dataForm?.created_date) {
            valid = Object.assign(valid, {
                name: "created_date",
                errors: ["Nhập ngày cho vay"],
            });
        }

        if (!dataForm?.reason) {
            valid = Object.assign(valid, {
                name: "reason",
                errors: ["Nhập số lý do vay"],
            });
        }
    }

    if (
        !isEmptyObject(valid) &&
        dataForm?.price &&
        !/^-?\d+$/.test(dataForm?.price)
    ) {
        isError = true;
        valid = {
            value: dataForm?.price,
            errors: ["Ký tự không hợp lệ. Hãy nhập ký tự là sô!"],
        };
    }
    if (isError) {
        formAttr.setFields([valid]);
    }

    return isError;
};

export const handleSharing = (uriShare: string, setTooltipSharing: any) => {
    if (!uriShare) {
        return setTooltipSharing("Copy fail!");
    }

    navigator.clipboard.writeText(uriShare);
    setTooltipSharing("Copied!");
};

export const handleConfirmPayment = async (
    payId: string,
    isPaid: number,
    setPaymentPaidLast: any,
    setIsLoading: any
) => {
    const uriConfirm = `/memo/confirm/${payId}/paid`;

    const res: any = await apiConfirmUpdateMemoPayment(uriConfirm, { isPaid });

    if (!res.status) {
        setIsLoading(false);
        notification.error({
            message: "Thông báo",
            description: res.error,
            duration: 5,
        });
        return;
    }

    setPaymentPaidLast({});
    const descNotify =
        isPaid == 1 ? "Xác nhận thành công" : "Xác nhận chưa thanh toán";
    notification.success({
        message: "Thông báo",
        description: descNotify,
        duration: 5,
    });

    setTimeout(() => {
        setIsLoading(false);
        window.location.href = ConfigApp.env.uri_base + "/dashboard/memo";
    }, 3000);
};

export const memoTabActive = (tab: boolean, scoped: boolean) => {
    if (scoped) {
        return tab ? "primary" : "default";
    }

    return !tab ? "primary" : "default";
};

export interface PaymentMemoDataType {
    key: React.Key;
    createAt: any;
    amount: any;
    bankName: string;
    payType: string;
    payStatus: string;
    updateAt: any;
    amountString: string;
}

export const columnsPaymentMemo = [
    { title: "Ngày thanh toán", dataIndex: "createAt", key: "createAt" },
    { title: "Tổng tiền", dataIndex: "amount", key: "amount" },
    { title: "Ngân hàng", dataIndex: "bankName", key: "bankName" },
    { title: "Hình thức", dataIndex: "payType", key: "payType" },
    { title: "Trạng thái", dataIndex: "payStatus", key: "payStatus" },
    {
        title: "Ngày xác nhận",
        dataIndex: "updateAt",
        key: "updateAt",
        render: (
            _: any,
            { payStatus, updateAt }: { payStatus: string; updateAt: any }
        ) => {
            if (payStatus !== MemoConstants.MEMO_PAY.PAY_STATUS.WAIT) {
                return <>{updateAt}</>;
            }

            return "";
        },
    },
];

export const handleActiveTab = (
    setTabDetailMemo: any,
    isDetailMemo: boolean
) => {
    setTabDetailMemo(isDetailMemo);

    if (isDetailMemo) {
    }
};

export const initMemoPayList = (
    dataListMemoPay: any[],
    setListMemoPay: any
) => {
    let convertColumnsMemoPay: any[] = [];
    dataListMemoPay.map((item, index) =>
        convertColumnsMemoPay.push({
            key: `${index}-${item.id}`,
            createAt: dayjs(item?.createdAt).format("YYYY-MM-DD"),
            amount: formatMoney("vi-VN", "VND", 0).format(item?.amount),
            amountString: formatNumberWithThreeDigits(item?.amount),
            bankName: item?.bankPay,
            payType: item?.payType,
            payStatus: item?.payStatus,
            updateAt: dayjs(item?.updatedAt).format("YYYY-MM-DD"),
        })
    );
    setListMemoPay(convertColumnsMemoPay);
};

export const handleOnBlurFrm = async (
    el: any,
    frm: any,
    setIsLoading?: any,
    setIsEdit?: any
) => {
    const isError = frm
        .getFieldsError(fields)
        ?.filter((item: any) => item.errors?.length > 0);

    if (isError?.length > 0) {
        setIsEdit(false);
        return;
    }
};
