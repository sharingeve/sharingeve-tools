interface IUpdateMemo {
    scoped?: string;
    id?: string;
}

interface UpdateDataType {
    key: React.Key;
    price: string;
    priceNumber: number;
    reason: string;
    paymentProgress: boolean;
    createDate: any;
}
