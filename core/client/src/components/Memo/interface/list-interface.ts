interface ListDataType {
    key: React.Key;
    id: string;
    name: string;
    amount: string;
}

interface IListMemo {}
