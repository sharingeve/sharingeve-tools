"use client";

import React, { useEffect, useState } from "react";
import { Button, Table } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useRouter } from "next/navigation";
import {
    handlePaymentDone,
    handleRedirectPaginate,
    initMemoData,
} from "./handler/list-handler";
import Link from "next/link";
import type { PaginationProps } from "antd";
import LoadingUI from "../molecules/LoadingUI";
import { formatMoney } from "@/lib/utils";

const ListMemo: React.FC<IListMemo> = ({}) => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [listMemo, setListMemo] = useState<ListDataType[]>([]);
    const [totalRecord, setTotalRecord] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const [isReload, setIsReload] = useState("");

    const columns: ColumnsType<ListDataType> = [
        {
            title: "Tên",
            dataIndex: "name",
            key: "name",
            render: (_, { id, name }) => (
                <Link href={`/dashboard/memo/${id}`}>{name}</Link>
            ),
            // fixed: "left",
        },
        { title: "Tổng tiền", dataIndex: "amount", key: "amount" },
        {
            title: "Action",
            key: "operation",
            fixed: "right",
            width: 200,
            render: (_, { id, name }) => (
                <>
                    <Link href={`/dashboard/memo/${id}`}>Chi tiết</Link> &nbsp;/
                    &nbsp;
                    <Link
                        href={"#"}
                        onClick={async (e) => {
                            const confirmDone = confirm(
                                `Hãy chắc chắn rằng ${name} đã thanh toán. Kiểm tra lại lần nữa cho chắc!`
                            );
                            if (!confirmDone) return;
                            await handlePaymentDone(e, id);
                            setIsReload(new Date().toString());
                        }}
                    >
                        Thanh Toán
                    </Link>
                </>
            ),
        },
    ];

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            const result = await initMemoData();
            let dataInit: ListDataType[] = [];
            if (result?.data) {
                result?.data?.forEach((item: any, index: number) => {
                    dataInit.push({
                        key: item?._id + index,
                        id: item?._id,
                        name: item?.fullName,
                        amount: formatMoney("vi-VN", "VND", 0).format(
                            item?.amountTotal
                        ),
                    });
                });
                setTotalRecord(result?.total);
            }

            if (dataInit.length > 0) {
                setListMemo(dataInit);
            }
            setIsLoading(false);
        })();

        return () => {
            setListMemo([]);
        };
    }, [isReload]);

    const handleAdd = () => {
        return router.push("/dashboard/memo/create", { scroll: false });
    };

    const redirectPaginate: PaginationProps["onChange"] = async (page) => {
        setIsLoading(true);
        const reloadData: any = await handleRedirectPaginate(10, page);
        let dataInit: ListDataType[] = [];
        if (reloadData?.data) {
            reloadData?.data?.forEach((item: any, index: number) => {
                dataInit.push({
                    key: item?._id + index,
                    id: item?._id,
                    name: item?.fullName,
                    amount: formatMoney("vi-VN", "VND", 0).format(
                        item?.amountTotal
                    ),
                });
            });
            setTotalRecord(reloadData?.total);
        }

        if (dataInit.length > 0) {
            setListMemo(dataInit);
        }
        setCurrentPage(page);
        setIsLoading(false);
    };
    return (
        <>
            <LoadingUI isOpen={isLoading} />
            <Button
                onClick={handleAdd}
                type="primary"
                style={{ marginBottom: 16 }}
            >
                Thêm nợ mới
            </Button>
            <Table
                columns={columns}
                dataSource={listMemo}
                pagination={{
                    defaultCurrent: currentPage,
                    total: totalRecord,
                    onChange: redirectPaginate,
                    hideOnSinglePage: true,
                }}
            />
        </>
    );
};

export default ListMemo;
