/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import { Button, Image, Table, Tooltip, Typography } from "antd";
import { v4 as uuidv4 } from "uuid";
import { ShareAltOutlined, CheckOutlined } from "@ant-design/icons";
import type { ColumnsType } from "antd/es/table";
import clsx from "clsx";
import styles from "./css/update.module.css";
import Form from "../molecules/form";
import {
    PaymentMemoDataType,
    columnsPaymentMemo,
    formBasicMemo,
    handleActiveTab,
    handleOnBlurFrm,
    handleSharing,
    handleUpdatedMemo,
    initMemoDetailHandler,
    initMemoPayList,
    memoTabActive,
} from "./handler/updated-handler";
import { notFound, useRouter } from "next/navigation";
import MemoConstants from "@/lib/constants/memo-constants";
import { Fragment, useEffect, useRef, useState } from "react";
import dayjs from "dayjs";
import { useAppSelector } from "@/hooks/redux-hook";
import { userDetail } from "@/store/users/selector";
import { formatMoney, showLogCatch } from "@/lib/utils";
import { checkPaymentProgress } from "./handler/share-memo-handler";
import ConfirmPayUpdate from "./element/ConfirmPayUpdate";
import LoadingUI from "../molecules/LoadingUI";
import FieldButton from "../atoms/Button";
import {
    destroyNotify,
    errorNotify,
    successNotify,
    warningNotify,
} from "../atoms/Notification";
import HistoryMemoPayment from "./element/HistoryMemoPayment";
import CSVUploadMemo from "./element/CSVUploadMemo";
import { handlePaymentDone } from "./handler/list-handler";
import ModalComponent from "../atoms/modal";
import UploadImgPayment from "./element/UploadImgPayment";
import { MainDashboard } from "../atoms/main";

export default function UpdatedMemo({ scoped, id }: IUpdateMemo) {
    const formCsvRef = useRef();
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [initialValues, setInitialValues] = useState({
        full_name: "",
        price: "",
        reason: "",
        created_date: dayjs(new Date()),
    });
    const [structureMemo, setStructureMemo] = useState<UpdateDataType[]>([]);
    const [listMemoPay, setListMemoPay] = useState<any[]>([]);
    const [memoExtra, setMemoExtra] = useState<any>({});
    const [amountMemo, setAmountMemo] = useState(0);
    const [person, setPerson] = useState("");
    const [changeStructure, setChangeStructure] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const detailUser: any = useAppSelector(userDetail);
    const [tooltipSharing, setTooltipSharing] = useState("");
    const [paymentPaidLast, setPaymentPaidLast] = useState<any>({});
    const [tabDetailMemo, setTabDetailMemo] = useState(true);
    const [importCsvAsText, setImportCsvAsText] = useState("");
    const [isReload, setIsReload] = useState("");
    const [isOpenModal, setIsOpenModal] = useState(false);

    const handleDelete = (key: any) => {
        setIsEdit(true);
        let newStructure = structureMemo.filter((item) => item.key != key);
        setStructureMemo(newStructure);
        setChangeStructure(true);
    };

    const columns: ColumnsType<UpdateDataType> = [
        { title: "Ngày nợ", dataIndex: "createDate", key: "createDate" },
        { title: "Lý do", dataIndex: "reason", key: "reason" },
        { title: "Tiền nợ", dataIndex: "price", key: "price" },
        {
            title: "Action",
            key: "operation",
            fixed: "right",
            width: 200,
            render: (_, record: UpdateDataType) => {
                return (
                    <>
                        {record.paymentProgress ? (
                            <button disabled>Xóa</button>
                        ) : (
                            <button onClick={() => handleDelete(record.key)}>
                                Xóa
                            </button>
                        )}
                    </>
                );
            },
        },
    ];

    const columnsMemoPay: ColumnsType<PaymentMemoDataType> = columnsPaymentMemo;

    useEffect(() => {
        if (id) {
            setIsLoading(true);
            initMemoDetailHandler(id)
                .then((item: any) => {
                    const data = item.data.memoDetail;
                    if (!data?.id) {
                        const keyNotify = uuidv4();
                        localStorage.setItem("keyNotify", keyNotify);
                        warningNotify({
                            message: "Thông báo",
                            description:
                                "ID này không tồn tại. Hệ thống từ di chuyển sang trang tạo sau 5s nữa.",
                            key: keyNotify,
                            duration: 5,
                            onClose: () => {
                                localStorage.removeItem("keyNotify");
                            },
                        });
                        setTimeout(
                            () =>
                                router.push("/dashboard/memo/create", {
                                    scroll: false,
                                }),
                            5030
                        );
                        return;
                    }
                    const dataMemoPayment = item.data.memoPaymentByMemoId;
                    const dataListMemoPay = item.data.listMemoPay;
                    const dataMemoExtra = item.data.memoExtra;
                    if (dataMemoPayment?.id) {
                        setPaymentPaidLast(dataMemoPayment);
                    }

                    if (
                        Array.isArray(dataListMemoPay) &&
                        dataListMemoPay.length > 0
                    ) {
                        initMemoPayList(dataListMemoPay, setListMemoPay);
                    }

                    let structureData: UpdateDataType[] = [];
                    if (Object.keys(data).length > 0) {
                        setAmountMemo(data?.amount);
                        setPerson(data?.full_name);
                        data?.structure?.map((i: any, index: number) =>
                            structureData.push({
                                key: `${index}-${i?.price}`,
                                price: formatMoney("vi-VN", "VND", 0).format(
                                    i?.price
                                ),
                                priceNumber: Number(i?.price),
                                reason: i?.reason,
                                createDate: dayjs(i?.createDate).format(
                                    "YYYY-MM-DD"
                                ),
                                paymentProgress: checkPaymentProgress(
                                    dataMemoPayment.payStatus
                                ),
                            })
                        );
                        setStructureMemo(
                            structureData.sort((a: any, b: any) =>
                                dayjs(b.createDate).diff(a.createDate)
                            )
                        );
                        setInitialValues((prev) => ({
                            ...prev,
                            full_name: data?.full_name,
                        }));
                    }
                    setMemoExtra(dataMemoExtra);
                    setIsLoading(false);
                })
                .catch((error: any) => {
                    setIsLoading(false);
                    showLogCatch(error, 'Lỗi khởi tạo dữ liệu');
                });

            setTooltipSharing(
                `${process.env.BASE_URL}/share/${detailUser.id}/memo/${id}`
            );
        }

        return () => {
            setStructureMemo([]);
            setAmountMemo(0);
            setPerson("");
            setPaymentPaidLast({});
            setImportCsvAsText("");
        };
    }, [id, isReload]);

    useEffect(() => {
        if (["Copied!", "Copy fail!"].includes(tooltipSharing) && id) {
            setTimeout(() => {
                setTooltipSharing(
                    `${process.env.BASE_URL}/share/${detailUser.id}/memo/${id}`
                );
            }, 500);
        }
    }, [tooltipSharing]);

    const handleOnFinish = async (value: any, form?: any) => {
        form.resetFields(["img_qr"]);
        if (id) {
            value.id = id;
        }
        if (changeStructure) {
            value.structure = JSON.stringify(structureMemo);
        }
        if (localStorage.getItem("keyNotify")) {
            const keyDestroy = localStorage.getItem("keyNotify") ?? "";
            destroyNotify(keyDestroy);
        }

        setIsLoading(true);
        setIsEdit(false);
        const idMemo: any = await handleUpdatedMemo(
            value,
            scoped,
            form,
            importCsvAsText,
            formCsvRef,
            initialValues
        );

        const keyNotify = uuidv4();
        localStorage.setItem("keyNotify", keyNotify);
        setImportCsvAsText("");
        if (!idMemo.status) {
            errorNotify({
                duration: 5,
                message: "Thông báo",
                description: idMemo.data,
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });
            setImportCsvAsText("");
            return setIsLoading(false);
        }

        if (scoped == MemoConstants.MEMO_UI.CREATE) {
            return router.push(`/dashboard/memo/${idMemo.data}`, {
                scroll: false,
            });
        }

        // updated
        const data = idMemo.data.data;
        let structureData: UpdateDataType[] = [];
        if (Object.keys(data).length > 0) {
            setAmountMemo(data?.amount);
            setPerson(data?.full_name);
            data?.structure?.map((i: any, index: number) =>
                structureData.push({
                    key: `${index}-${i?.price}`,
                    price: formatMoney("vi-VN", "VND", 0).format(i?.price),
                    priceNumber: Number(i?.price),
                    reason: i?.reason,
                    createDate: dayjs(i?.createDate).format("YYYY-MM-DD"),
                    paymentProgress: false,
                })
            );
            setStructureMemo(
                structureData.sort((a: any, b: any) =>
                    dayjs(b.createDate).diff(a.createDate)
                )
            );

            successNotify({
                message: "Thông báo",
                description: "Cập nhật nợ thành công!",
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });
            return setIsLoading(false);
        }

        warningNotify({
            message: "Thông báo",
            description: "Không có nợ nào được thêm mới!",
            key: keyNotify,
            onClose: () => {
                localStorage.removeItem("keyNotify");
            },
        });
        setIsLoading(false);
    };

    return (
        <div>
            <LoadingUI isOpen={isLoading} />
            {Object.keys(paymentPaidLast).length === 0 ||
            paymentPaidLast?.payStatus !==
                MemoConstants.MEMO_PAY.PAY_STATUS.WAIT ? (
                <Form
                    nameForm={`updated_form`}
                    autoComplete="off"
                    items={formBasicMemo(
                        router,
                        scoped == MemoConstants.MEMO_UI.CREATE,
                        isEdit
                    )}
                    onFinish={handleOnFinish}
                    initialValues={initialValues}
                    layout="vertical"
                    onValuesChange={() => setIsEdit(true)}
                    onBlur={handleOnBlurFrm}
                    setIsLoading={setIsLoading}
                    setIsEdit={setIsEdit}
                />
            ) : (
                <ConfirmPayUpdate
                    router={router}
                    paymentMemo={paymentPaidLast}
                    setPaymentPaidLast={setPaymentPaidLast}
                    setIsLoading={setIsLoading}
                />
            )}
            {scoped === MemoConstants.MEMO_UI.UPDATED && (
                <Fragment>
                    <span>-----------Total-------------</span>
                    <br />
                    <div>
                        Tổng nợ của <strong>{person}</strong>:{" "}
                        <span style={{ color: "red" }}>
                            {formatMoney("vi-VN", "VND", 0).format(amountMemo)}
                        </span>
                    </div>
                    {amountMemo != 0 && (
                        <>
                            <Tooltip title={tooltipSharing}>
                                <Button
                                    onClick={() =>
                                        handleSharing(
                                            tooltipSharing,
                                            setTooltipSharing
                                        )
                                    }
                                    icon={<ShareAltOutlined />}
                                    type="link"
                                />
                            </Tooltip>
                            {paymentPaidLast?.payStatus !==
                                MemoConstants.MEMO_PAY.PAY_STATUS.WAIT && (
                                <>
                                    <Tooltip title="Đã thanh toán">
                                        <Button
                                            icon={<CheckOutlined />}
                                            type="link"
                                            onClick={() => setIsOpenModal(true)}
                                        />
                                    </Tooltip>
                                    <UploadImgPayment
                                        memoExtra={memoExtra}
                                        setMemoExtra={setMemoExtra}
                                    />
                                    {isOpenModal && (
                                        <ModalComponent
                                            open={isOpenModal}
                                            title="Xác nhận"
                                            content={
                                                <p>
                                                    Hãy chắc chắn rằng{" "}
                                                    <strong>{person}</strong> đã
                                                    thanh toán. Kiểm tra lại lần
                                                    nữa cho chắc!
                                                </p>
                                            }
                                            onCallBack={async (e: any) => {
                                                const cloneId: any = id;
                                                await handlePaymentDone(
                                                    e,
                                                    cloneId
                                                );
                                                setIsReload(
                                                    new Date().toString()
                                                );
                                                setIsOpenModal(false);
                                            }}
                                            onCancelCallBack={() =>
                                                setIsOpenModal(false)
                                            }
                                        />
                                    )}
                                </>
                            )}
                        </>
                    )}
                    <Typography.Paragraph className="tab_memo_update">
                        <FieldButton
                            nameBtn="Chi tiết nợ"
                            type={memoTabActive(tabDetailMemo, true)}
                            onClick={() =>
                                handleActiveTab(setTabDetailMemo, true)
                            }
                            style={{ marginRight: 5 }}
                            className={
                                tabDetailMemo
                                    ? clsx(styles.btn_memo_detail)
                                    : ""
                            }
                        />

                        {Object.keys(paymentPaidLast).length > 0 && (
                            <HistoryMemoPayment
                                tabDetailMemo={tabDetailMemo}
                                setTabDetailMemo={setTabDetailMemo}
                                listMemoPay={listMemoPay}
                                memoId={id}
                            />
                        )}

                        <CSVUploadMemo
                            setImportCsvAsText={setImportCsvAsText}
                            setIsEdit={setIsEdit}
                            ref={formCsvRef}
                        />
                    </Typography.Paragraph>

                    <Typography.Paragraph>
                        <strong className={clsx(styles.used_qr)}>
                            QR đang dùng (click vào để zoom):{" "}
                        </strong>
                        {memoExtra?.imgQr ? (
                            <Image
                                width={100}
                                src={memoExtra?.imgQr}
                                alt={"qr_memo_" + id}
                                placeholder={
                                    <Image
                                        preview={false}
                                        src="/assets/img/not_found_image.png"
                                        width={100}
                                        alt="loading_preview"
                                    />
                                }
                            />
                        ) : (
                            "Chưa có mã QR"
                        )}
                    </Typography.Paragraph>

                    <Table
                        columns={tabDetailMemo ? columns : columnsMemoPay}
                        dataSource={tabDetailMemo ? structureMemo : listMemoPay}
                        pagination={{ hideOnSinglePage: true }}
                    />
                </Fragment>
            )}
        </div>
    );
}
