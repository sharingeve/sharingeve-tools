"use client";

import ModalComponent from "@/components/atoms/modal/Modal";
import { BankOutlined } from "@ant-design/icons";
import { Button, Tooltip } from "antd";
import { useState } from "react";
import FormUploadImgPayment from "./FormUploadImgPayment";
import {
    handleFinishUploadImgPayment,
    resetFiled,
} from "../handler/upload-img-payment";
import { errorNotify, successNotify } from "@/components/atoms/Notification";
import { v4 as uuidv4 } from "uuid";
import LoadingUI from "@/components/molecules/LoadingUI";

export default function UploadImgPayment({
    memoExtra,
    setMemoExtra,
}: {
    memoExtra: any;
    setMemoExtra: any;
}) {
    // https://vietqr.io/intro
    const [isLoading, setIsLoading] = useState(false);
    const [disabledAddBtn, setDisabledAddBtn] = useState(true);
    const [isOpenModalWarn, setIsOpenModalWarn] = useState(false);
    const [isOpenModalAddQr, setIsOpenModalAddQr] = useState(false);
    const [previewImg, setPreviewImg] = useState<any>();

    const handleUpdate = async () => {
        setIsLoading(true);
        const result = await handleFinishUploadImgPayment(
            memoExtra.id,
            previewImg
        );
        resetFiled(
            setIsOpenModalAddQr,
            setDisabledAddBtn,
            setPreviewImg
        );
        const keyNotify = uuidv4();
        localStorage.setItem("keyNotify", keyNotify);
        setIsLoading(false);
        if (result.status) {
            setMemoExtra(result.data);
            return successNotify({
                message: "Thông báo",
                description: result.msg,
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });
        }

        return errorNotify({
            message: "Thông báo",
            description: result.msg,
            key: keyNotify,
            onClose: () => {
                localStorage.removeItem("keyNotify");
            },
        });
    };
    return (
        <>
            {isLoading && <LoadingUI isOpen={isLoading} />}
            <Tooltip title="Mã QR ngân hàng">
                <Button
                    icon={<BankOutlined />}
                    type="link"
                    onClick={() => setIsOpenModalWarn(true)}
                />
            </Tooltip>
            {/* Thông báo trước khi thêm QR */}
            {isOpenModalWarn && (
                <ModalComponent
                    open={isOpenModalWarn}
                    title="Lưu ý"
                    content={
                        <p>
                            Ảnh upload lên phải là ảnh QR code của ngân hàng mà
                            bạn dùng <br />
                            Vì nó sẽ dùng để cho người khác quét mã QR trả tiền
                            cho bạn <br />
                            Trong trường hợp ảnh upload không phải mã QR ngân
                            hàng bạn dùng thì link chia sẻ sẽ chỉ có tác dụng
                            liệt kê các khoản nợ mà bạn đã note.
                            <br />
                            Cảm ơn đã đọc!
                        </p>
                    }
                    customOkText="Tiếp tục"
                    onCallBack={() => {
                        setIsOpenModalWarn(false);
                        setIsOpenModalAddQr(true);
                    }}
                    onCancelCallBack={() => setIsOpenModalWarn(false)}
                />
            )}
            {/* Form thêm mới QR */}
            {isOpenModalAddQr && (
                <ModalComponent
                    open={isOpenModalAddQr}
                    title="QR thanh toán"
                    content={
                        <FormUploadImgPayment
                            imgQr={memoExtra.imgQr}
                            setDisabledAddBtn={setDisabledAddBtn}
                            previewImg={previewImg}
                            setPreviewImg={setPreviewImg}
                        />
                    }
                    customOkText="Thêm"
                    okBtnProps={{ disabled: disabledAddBtn }}
                    onCallBack={async () => await handleUpdate()}
                    onCancelCallBack={() =>
                        resetFiled(
                            setIsOpenModalAddQr,
                            setDisabledAddBtn,
                            setPreviewImg
                        )
                    }
                    zIndex={9}
                />
            )}
        </>
    );
}
