import React from "react";
import clsx from "clsx";
import styles from "../css/confirm-pay.module.css";
import FieldButton from "@/components/atoms/Button";
import { Typography } from "antd";
import { handleConfirmPayment } from "../handler/updated-handler";
import MemoConstants from "@/lib/constants/memo-constants";
import { formatMoney } from "@/lib/utils";
import dayjs from "dayjs";

export default function ConfirmPayUpdate({
    router,
    paymentMemo,
    setPaymentPaidLast,
    setIsLoading,
}: {
    paymentMemo: any;
    router: any;
    setPaymentPaidLast: any;
    setIsLoading: any;
}) {
    return (
        <div className="btn-back-wrapper">
            <FieldButton
                onClick={() =>
                    router.push("/dashboard/memo", {
                        scroll: false,
                    })
                }
                type="default"
                style={{ marginRight: 16 }}
                nameBtn="Trở về"
            />
            <Typography.Title
                level={5}
                className={clsx(styles.pay_confirm_title)}
            >
                Mục này đã được thanh toán. Hãy kiểm tra và xác nhận đã nhận
                được tiền hay chưa?
            </Typography.Title>
            <Typography.Paragraph className={clsx(styles.pay_confirm_action)}>
                <Typography.Paragraph>
                    <p>Ngân hàng: {paymentMemo.bankPay}</p>
                    <p>
                        Số tiền:{" "}
                        {formatMoney("vi-VN", "VND", 0).format(
                            paymentMemo.amount
                        )}
                    </p>
                    <p>Hình thức: {paymentMemo.payType}</p>
                    <p>
                        Ngày thanh toán:{" "}
                        {dayjs(paymentMemo.createdAt).format(
                            `DD-MM-YYYY HH:mm:ss`
                        )}
                    </p>
                </Typography.Paragraph>
                <FieldButton
                    onClick={async () =>
                        handleConfirmPayment(
                            paymentMemo.id,
                            MemoConstants.MEMO_PAY.PAY_PAID.NOT_PAID,
                            setPaymentPaidLast,
                            setIsLoading
                        )
                    }
                    type="default"
                    style={{ marginRight: 16 }}
                    nameBtn="Chưa Thanh toán"
                />
                <FieldButton
                    onClick={async () =>
                        handleConfirmPayment(
                            paymentMemo.id,
                            MemoConstants.MEMO_PAY.PAY_PAID.PAID,
                            setPaymentPaidLast,
                            setIsLoading
                        )
                    }
                    type="primary"
                    nameBtn="Đã thanh toán"
                />
            </Typography.Paragraph>
        </div>
    );
}
