/* eslint-disable react-hooks/exhaustive-deps */
import { useMemo, useState } from "react";
import Image from "next/image";
import { Typography, type RadioChangeEvent, Radio } from "antd";
import { IModalContent, pathQrBank } from "../handler/share-memo-handler";
import MemoConstants from "@/lib/constants/memo-constants";
import clsx from "clsx";
import styles from "../css/share-memo.module.css";
import { formatMoney } from "@/lib/utils";

const ShareModalComponent = ({
    memoId,
    debtor,
    amount,
    selectPay,
    setSelectPay,
    listBankFromVietQR,
    selectQR,
    setSelectQR,
}: IModalContent) => {
    const data = useMemo(
        () => ({ amount, desc: `${debtor} trả nợ - ${memoId}` }),
        []
    );

    const [isQR, setIsQR] = useState(true);
    // const [showImg, setShowImg] = useState(true);
    const [infoBank, setInfoBank] = useState({
        path:
            pathQrBank(
                MemoConstants.LIST_QR.TECH.name,
                true,
                listBankFromVietQR,
                data
            ) ?? "",
        bankNumber: MemoConstants.LIST_NUMBER_BANK.TECH,
        nameOwner: MemoConstants.OWNER_CARD,
    });

    const listPathImgQr = (qr: boolean) => ({
        [MemoConstants.LIST_QR.TECH.name]: {
            path: pathQrBank(
                MemoConstants.LIST_QR.TECH.name,
                qr,
                listBankFromVietQR,
                data
            ),
            number: MemoConstants.LIST_NUMBER_BANK.TECH,
        },
        [MemoConstants.LIST_QR.MBB.name]: {
            path: pathQrBank(
                MemoConstants.LIST_QR.MBB.name,
                qr,
                listBankFromVietQR,
                data
            ),
            number: MemoConstants.LIST_NUMBER_BANK.MBB,
        },
        [MemoConstants.LIST_QR.VCB.name]: {
            path: pathQrBank(
                MemoConstants.LIST_QR.VCB.name,
                qr,
                listBankFromVietQR,
                data
            ),
            number: MemoConstants.LIST_NUMBER_BANK.VCB,
        },
    });

    const onChangeSelectPay = ({ target: { value } }: RadioChangeEvent) => {
        setIsQR(value == MemoConstants.LIST_PAY.BANK_QR);
        const infoBankNew = {
            path: listPathImgQr(value == MemoConstants.LIST_PAY.BANK_QR)[
                selectQR
            ].path,
            bankNumber: listPathImgQr(value == MemoConstants.LIST_PAY.BANK_QR)[
                selectQR
            ].number,
        };
        setInfoBank((prev) => ({ ...prev, ...infoBankNew }));

        // setShowImg(value == MemoConstants.LIST_PAY.BANK_TRANSFER);
        setSelectPay(value);
    };

    const onChangeSelectQR = ({ target: { value } }: RadioChangeEvent) => {
        const infoBankNew = {
            path: listPathImgQr(isQR)[value].path,
            bankNumber: listPathImgQr(isQR)[value].number,
        };

        setSelectQR(value);
        setInfoBank((prev) => ({ ...prev, ...infoBankNew }));
    };

    // const handleChangeBank = (value: { value: string; label: ReactNode }) => {
    //     console.log(value, typeof value);

    //     const bankDetail: Datum | undefined = listBankFromVietQR.find(
    //         (item) => item.id == Number(value)
    //     );

    //     console.log("bankDetail", bankDetail);
    // };

    return (
        <>
            <Radio.Group
                options={[
                    MemoConstants.LIST_PAY.BANK_QR,
                    MemoConstants.LIST_PAY.BANK_TRANSFER,
                ]}
                value={selectPay}
                onChange={onChangeSelectPay}
            />
            <div className={clsx(styles.select_qr)}>
                <Typography.Title level={5}>
                    Chọn QR thanh toán
                </Typography.Title>
                <Radio.Group
                    className={clsx(styles.radio_select_qr)}
                    options={[
                        MemoConstants.LIST_QR.TECH.name,
                        MemoConstants.LIST_QR.MBB.name,
                        MemoConstants.LIST_QR.VCB.name,
                    ]}
                    value={selectQR}
                    onChange={onChangeSelectQR}
                />
            </div>
            {infoBank.path && (
                <div className={clsx(styles.img_qr_wrapper)}>
                    <Image
                        className={clsx(styles.img_qr)}
                        src={infoBank.path}
                        alt="qr_bank"
                        width={0}
                        height={0}
                        sizes="100vw"
                    />
                    <div className={clsx(styles.info_bank)}>
                        <p>Chủ tài khoản: {infoBank.nameOwner}</p>
                        <p>Số tài khoản: {infoBank.bankNumber}</p>
                        <p>
                            Số tiền trả:{" "}
                            {formatMoney("vi-VN", "VND", 0).format(amount)}
                        </p>
                    </div>
                </div>
            )}
            {/* {false && (
                <div>
                    <Typography.Title level={5}>
                        Chọn ngân hàng
                    </Typography.Title>
                    <Select
                        className={clsx(styles.bank_selected)}
                        showSearch
                        defaultValue={bankList[0].value}
                        placeholder={"Search to Select"}
                        optionFilterProp="children"
                        options={bankList}
                        onChange={handleChangeBank}
                        filterOption={(input, option) =>
                            (option?.label.toLowerCase() ?? "").includes(
                                input.toLowerCase()
                            )
                        }
                        filterSort={(optionA, optionB) =>
                            (optionA?.label ?? "")
                                .toLowerCase()
                                .localeCompare(
                                    (optionB?.label ?? "").toLowerCase()
                                )
                        }
                    />
                </div>
            )} */}
        </>
    );
};

export default ShareModalComponent;
