import { forwardRef, useImperativeHandle, useRef } from "react";
import FieldButton from "@/components/atoms/Button";
import { type UploadProps, Upload, message, Form, Button } from "antd";
import { UploadOutlined, DownloadOutlined } from "@ant-design/icons";
import clsx from "clsx";
import styles from "../css/update.module.css";
import ReactCSVComponent from "@/components/molecules/csv/ReactCSVComponent";
import dayjs from "dayjs";
import { headerTemplateMemoDetail } from "../handler/csv-handler";
import { actionUpload } from "@/lib/utils";

const CSVUploadMemo = (props: any, ref: any) => {
    const btnResetRef = useRef<any>();

    useImperativeHandle(ref, () => ({
        // protected function output
        reset() {
            btnResetRef.current.click();
        },
    }));

    const propsUpload: UploadProps = {
        name: "file",
        maxCount: 1,
        // action: "https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188",
        customRequest: actionUpload,
        // headers: {
        //     authorization: "authorization-text",
        // },
        onChange(info) {
            if (info.file.status !== "uploading") {
                // console.log(info.file, info.fileList);
                if (info.file.type !== "text/csv") {
                    return;
                }
            }
            if (info.file.status === "done") {
                message.success(`${info.file.name} file uploaded successfully`);
                props.setIsEdit(true);
            } else if (info.file.status === "error") {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        beforeUpload: (file) => {
            const reader = new FileReader();

            reader.onload = (e: any) => {
                // console.log(e.target.result);
                props.setImportCsvAsText(e.target.result);
            };
            reader.readAsText(file);

            // Prevent upload
            // return false;
        },
        accept: ".csv", // application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel
    };
    const normFile = (e: any) => {
        if (Array.isArray(e)) {
            return e;
        }
        return e?.fileList;
    };
    return (
        <>
            <ReactCSVComponent
                titleLinkCSV={
                    <FieldButton
                        nameBtn={"Tải mẫu csv chi tiết nợ"}
                        type="dashed"
                        danger
                        icon={<DownloadOutlined />}
                    />
                }
                filename={`template-memo-structure-${dayjs(
                    new Date().toLocaleString("vi-VN", {
                        timeZone: "Asia/Ho_Chi_Minh",
                    })
                ).format("YYYYMMDDHHmmss")}.csv`}
                headerCSV={headerTemplateMemoDetail}
                csvData={[
                    {
                        priceNumber: 100000,
                        reason: "Lý do nợ",
                        createDate: dayjs(new Date()).format("YYYY-MM-DD"),
                    },
                ]}
                className={clsx(styles.btn_template_export)}
            />
            <Form
                className={clsx(styles.btn_memo_import)}
                onFinish={() => {
                    alert("Không được phép sửa đổi cấu trúc upload");
                    location.reload();
                }}
            >
                <Form.Item
                    getValueFromEvent={normFile}
                    valuePropName="fileList"
                    name="upload"
                    noStyle
                >
                    <Upload {...propsUpload}>
                        <FieldButton
                            nameBtn="Thêm nhiều chi tiết nợ"
                            type="dashed"
                            danger
                            icon={<UploadOutlined />}
                        />
                    </Upload>
                </Form.Item>
                <Form.Item className="hide">
                    <Button ref={btnResetRef} htmlType="reset"></Button>
                </Form.Item>
            </Form>
        </>
    );
};

export default forwardRef(CSVUploadMemo);
