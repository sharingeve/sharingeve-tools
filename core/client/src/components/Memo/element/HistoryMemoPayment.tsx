import React, { useEffect, useState } from "react";
import { DownloadOutlined } from "@ant-design/icons";
import clsx from "clsx";
import styles from "../css/history_memo_payment.module.css";
import FieldButton from "@/components/atoms/Button";
import { handleActiveTab, memoTabActive } from "../handler/updated-handler";
import { headerMemoPayment } from "../handler/csv-handler";
import ReactCSVComponent from "@/components/molecules/csv/ReactCSVComponent";
import dayjs from "dayjs";
import MemoConstants from "@/lib/constants/memo-constants";

export default function HistoryMemoPayment({
    tabDetailMemo,
    setTabDetailMemo,
    listMemoPay,
    memoId,
}: {
    tabDetailMemo: any;
    setTabDetailMemo: any;
    listMemoPay: any;
    memoId: any;
}) {
    const [csvData, setCsvData] = useState<any[]>([]);
    useEffect(() => {
        let newData: any[] = [];
        listMemoPay.map((item: any) => {
            newData.push({
                createAt: item.createAt,
                amount: item.amountString,
                bankName: item.bankName,
                payType: item.payType,
                payStatus: item.payStatus,
                updateAt:
                    item.payStatus !== MemoConstants.MEMO_PAY.PAY_STATUS.WAIT
                        ? item.updateAt
                        : "",
            });
        });

        setCsvData(newData);
    }, [listMemoPay]);
    return (
        <>
            <FieldButton
                nameBtn="Lịch sử thanh toán"
                type={memoTabActive(tabDetailMemo, false)}
                onClick={() => handleActiveTab(setTabDetailMemo, false)}
                className={!tabDetailMemo ? clsx(styles.history_active) : ""}
            />

            <ReactCSVComponent
                titleLinkCSV={
                    <FieldButton
                        nameBtn={"Xuất báo cáo giao dịch"}
                        type="dashed"
                        danger
                        icon={<DownloadOutlined />}
                    />
                }
                filename={`memo-payment-${memoId}-${dayjs(new Date()).format(
                    "YYYYMMDDHHmmss"
                )}.csv`}
                headerCSV={headerMemoPayment}
                csvData={csvData}
                className={clsx(styles.btn_history_export)}
            />
        </>
    );
}
