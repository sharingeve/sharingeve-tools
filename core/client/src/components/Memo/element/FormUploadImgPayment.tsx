"use client";

import { Image } from "antd";
import clsx from "clsx";
import styles from "../css/form-upload-pay.module.css";
import ImgUploadCus from "@/components/molecules/img-upload-cus";

export default function FormUploadImgPayment({
    imgQr,
    setDisabledAddBtn,
    previewImg,
    setPreviewImg,
}: {
    imgQr: string;
    setDisabledAddBtn: any;
    previewImg: any;
    setPreviewImg: any;
}) {
    return (
        <div className={clsx(styles["form-upload-wrapper"])}>
            <Image
                width={200}
                src={imgQr ? imgQr : "/assets/img/not_found_image.png"}
                alt="img_qr"
                loading="lazy"
                placeholder={
                    <Image
                        preview={false}
                        src="/assets/img/not_found_image.png"
                        width={200}
                        alt="loading_preview"
                    />
                }
            />
            <br /> <br />
            <ImgUploadCus
                txtBtn="Upload QR khác"
                setDisabledBtn={setDisabledAddBtn}
                previewImg={previewImg}
                setPreviewImg={setPreviewImg}
            />
        </div>
    );
}
