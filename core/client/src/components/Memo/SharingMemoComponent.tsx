/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import { useEffect, useState } from "react";
import { Typography, Table, Button, notification, Image } from "antd";
import type { ColumnsType } from "antd/es/table";
import { formatMoney, isEmptyArray } from "@/lib/utils";
import dayjs from "dayjs";
import clsx from "clsx";
import styles from "./css/share-memo.module.css";
import ModalComponent from "../atoms/modal";
import {
    DataType,
    checkPaymentProgress,
    handleConfirmPay,
    handlePay,
    initListBank,
} from "./handler/share-memo-handler";
import MemoConstants from "@/lib/constants/memo-constants";
import { Datum } from "@/service/vietqr-service";
import ShareModalComponent from "./element/ShareModalComponent";
import LoadingUI from "../molecules/LoadingUI";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { i18nVi } from "@/lib/i18n/vi";
import { getNotify } from "@/store/common/selectors";
import { resetNotify } from "@/store/common/actions";

export default function SharingMemoComponent({
    memo,
    memoId,
    userId,
}: {
    memo: any;
    memoId: string;
    userId: string;
}) {
    const detailMemo = memo?.data?.memoDetail;
    const detailMemoPayment = memo?.data?.memoPayment;
    const detailMemoExtra = memo?.data?.memoExtra;
    const userDetail = memo?.data?.user;

    const dispatch = useAppDispatch();
    const contextNotify: any = useAppSelector(getNotify);
    const [isLoading, setIsLoading] = useState(false);
    const [openModal, setOpenModal] = useState(false);
    const [selectPay, setSelectPay] = useState(MemoConstants.LIST_PAY.BANK_QR);
    const [selectQR, setSelectQR] = useState(MemoConstants.LIST_QR.TECH.name);
    const [listBankFromVietQR, setListBankFromVietQR] = useState<Datum[]>([]);
    const [columns, setColumns] = useState<ColumnsType<DataType>>([]);
    const [defaultData, setDefaultData] = useState<DataType[]>([]);
    const [paymentProgress, setPaymentProgress] = useState(
        checkPaymentProgress(detailMemoPayment?.payStatus)
    );

    useEffect(() => {
        if (!userDetail.isClient) {
            setIsLoading(true);
            initListBank()
                .then((data: Datum[]) => {
                    if (!isEmptyArray(data)) {
                        let optionsBank: any[] = [];
                        let sortData = data.sort((a, b) => a.id - b.id);
                        sortData.map((item) => {
                            optionsBank.push({
                                value: item.id,
                                label: `${item.shortName}-${item.name}`,
                            });
                        });
                        setListBankFromVietQR(sortData);
                    }
                    setIsLoading(false);
                })
                .catch((error) => {
                    setIsLoading(false);
                    console.log(
                        "SharingMemoComponent error >>",
                        error?.message
                    );
                });
        }

        return () => {
            setSelectPay(MemoConstants.LIST_PAY.BANK_QR);
            setListBankFromVietQR([]);
        };
    }, []);

    useEffect(() => {
        if (memo?.status) {
            const fields = [
                {
                    title: "Ngày nợ",
                    dataIndex: "createDate",
                    key: "createDate",
                },
                { title: "Lý do", dataIndex: "reason", key: "reason" },
                { title: "Tiền nợ", dataIndex: "price", key: "price" },
            ];
            setColumns(fields);

            let structureData: DataType[] = [];
            detailMemo?.structure?.map((i: any, index: number) => {
                structureData.push({
                    key: `${index}-${i?.price}`,
                    price: formatMoney("vi-VN", "VND", 0).format(i?.price),
                    priceNumber: Number(i?.price),
                    reason: i?.reason,
                    createDate: dayjs(i?.createDate).format("YYYY-MM-DD"),
                });
            });
            setDefaultData(
                structureData.sort((a: any, b: any) =>
                    dayjs(b.createDate).diff(a.createDate)
                )
            );
        }
    }, [memo]);

    useEffect(() => {
        if (contextNotify.display) {
            notification.open({
                type: contextNotify.type,
                message: "Thông báo",
                description: contextNotify.content,
                duration: 5,
                onClose: () => {
                    dispatch(resetNotify());
                },
            });
        }
    }, [contextNotify]);

    return (
        <div>
            {isLoading && <LoadingUI isOpen={isLoading} />}
            <>
                <Typography.Title className={clsx(styles["full-name"])}>
                    <span>{detailMemo.full_name}</span>
                </Typography.Title>
                <Typography.Title level={3} className={clsx(styles.amount)}>
                    <span>
                        Tổng:{" "}
                        {formatMoney("vi-VN", "VND", 0).format(
                            detailMemo.amount
                        )}
                    </span>
                </Typography.Title>
                {paymentProgress ? (
                    <Typography.Title
                        level={5}
                        className={clsx(styles.payment_in_progress)}
                    >
                        <span>{i18nVi.PAYMENT_INPROGRESS}</span>
                    </Typography.Title>
                ) : (
                    <div className={clsx(styles.btn_pay)}>
                        {!userDetail.isClient ? (
                            <Button onClick={() => handlePay(setOpenModal)}>
                                Thanh Toán
                            </Button>
                        ) : (
                            <Image
                                className={
                                    detailMemoExtra?.imgQr
                                        ? ""
                                        : clsx(styles.img_qr)
                                }
                                width={150}
                                src={
                                    detailMemoExtra?.imgQr
                                        ? detailMemoExtra?.imgQr
                                        : "/assets/img/not_found_image.png"
                                }
                                alt="img_qr"
                                loading="lazy"
                                placeholder={
                                    <Image
                                        preview={false}
                                        src="/assets/img/not_found_image.png"
                                        width={200}
                                        alt="loading_preview"
                                    />
                                }
                            />
                        )}
                    </div>
                )}
                <Table
                    columns={columns}
                    dataSource={defaultData}
                    pagination={{
                        hideOnSinglePage: true,
                    }}
                />

                {openModal && (
                    <ModalComponent
                        open={openModal}
                        zIndex={99}
                        title={"Thanh toán tiền nợ"}
                        customOkText={"Thanh toán"}
                        customCancelText={"Hủy bỏ"}
                        content={
                            <ShareModalComponent
                                memoId={memoId}
                                debtor={detailMemo.full_name}
                                amount={detailMemo.amount || 0}
                                selectPay={selectPay}
                                setSelectPay={setSelectPay}
                                listBankFromVietQR={listBankFromVietQR}
                                selectQR={selectQR}
                                setSelectQR={setSelectQR}
                            />
                        }
                        onCallBack={() =>
                            handleConfirmPay(
                                userId,
                                memoId,
                                detailMemo?.amount,
                                detailMemo?.full_name,
                                selectQR,
                                selectPay,
                                setIsLoading,
                                dispatch,
                                setPaymentProgress,
                                setOpenModal
                            )
                        }
                        onCancelCallBack={() => {
                            setOpenModal(false);
                            setSelectPay(MemoConstants.LIST_PAY.BANK_QR);
                            setSelectQR(MemoConstants.LIST_QR.TECH.name);
                        }}
                    />
                )}
            </>
        </div>
    );
}
