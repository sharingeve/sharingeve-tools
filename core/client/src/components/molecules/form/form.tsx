"use client";

import React, { useEffect } from "react";
import { Form } from "antd";
import { FormType, formErrorStyle } from "./form-handle";

const FormComponent: React.FC<FormType> = ({
    nameForm,
    items,
    onFinish,
    onBlur,
    error,
    listResetFields,
    layout,
    initialValues,
    isResetForm,
    setIsLoading,
    setIsEdit,
    isNotReset,
    ...props
}) => {
    const [form] = Form.useForm();
    const handleFinish = (values: any) => {
        onFinish(values, form);
        if (!isNotReset) {
            if (isResetForm) {
                form.resetFields(listResetFields || []);
            } else {
                form.setFieldsValue(initialValues);
            }
        }
    };

    const handleOnBlur = async (e: any) => {
        if (typeof onBlur !== "undefined") {
            await onBlur(e, form, setIsLoading, setIsEdit);
        }
    };

    useEffect(() => {
        form.setFieldsValue(initialValues);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [initialValues]);

    return (
        <Form
            form={form}
            name={nameForm}
            onFinish={handleFinish}
            onBlur={handleOnBlur}
            layout={layout}
            {...props}
        >
            {error !== "" && <p style={{ ...formErrorStyle }}>{error}</p>}

            {items.length > 0 &&
                items.map((item, index) => {
                    return (
                        <Form.Item
                            className={item?.hidden ? "hide" : ""}
                            label={item?.label}
                            name={item?.name}
                            rules={item?.rule}
                            key={`${nameForm}-${index}`}
                            dependencies={item?.dependencies}
                            getValueFromEvent={item?.getValueFromEvent}
                            {...item?.custom}
                        >
                            {item.field}
                        </Form.Item>
                    );
                })}
        </Form>
    );
};

export default FormComponent;
