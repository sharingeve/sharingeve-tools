export interface FormType {
    nameForm: string;
    items: any[];
    onFinish: any;
    autoComplete?: string;
    className?: string;
    error?: string;
    initialValues: object;
    listResetFields?: string[];
    layout?: any;
    onValuesChange?: any,
    isResetForm?: boolean,
    onBlur?: any;
    onChange?: any;
    setIsLoading?: any;
    setIsEdit?: any;
    isNotReset?: 'skip';
}

export const formErrorStyle: any = {
    marginBottom: "12px",
    color: "red",
    textAlign: "center",
    fontSize: "16px",
};

