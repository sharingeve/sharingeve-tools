"use client";

import React, { Fragment, useEffect, useState } from "react";
import type { MenuProps } from "antd";
import { Menu, notification } from "antd";
import {
    activeMenu,
    handleRedirect,
    initDisplayMenuAfterLogin,
    items,
} from "./MenuHandler";
import { useRouter, usePathname } from "next/navigation";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { accessToken, isAuth, userDetail } from "@/store/users/selector";
import LoadingUI from "../LoadingUI";
import { getNotify } from "@/store/common/selectors";
import { resetNotify } from "@/store/common/actions";
import Constants from "@/lib/constants/constants";

const Menus: React.FC = () => {
    const router = useRouter();
    const dispatch = useAppDispatch();
    const pathname = usePathname();

    const token = useAppSelector(accessToken);
    const checkAuth = useAppSelector(isAuth);
    const user: any = useAppSelector(userDetail);
    const notify: any = useAppSelector(getNotify);

    const [current, setCurrent] = useState("");
    const [listMenu, setListMenu] = useState(items);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    React.useEffect(() => {
        if (checkAuth) {
            initDisplayMenuAfterLogin(listMenu, user, setListMenu);
        } else {
            setListMenu(items);
        }
        activeMenu(pathname, setCurrent);

        return () => {
            setCurrent("");
        };
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user, checkAuth]);

    useEffect(() => {
        if (notify.display && !notify.isAuth) {
            notification.open({
                type: notify.type,
                message: "Thông báo",
                description: notify.content,
                duration: 3,
                onClose: () => {
                    dispatch(resetNotify());
                },
            });
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [notify]);

    const onClick: MenuProps["onClick"] = async (e) => {
        if (e.key != Constants.MENU_KEY.M05) {
            setCurrent(e.key);
        }
        await handleRedirect(
            e.key,
            router,
            dispatch,
            {
                token,
                userId: user?.id,
            },
            setIsLoading,
            checkAuth
        );
    };

    return (
        <Fragment>
            <LoadingUI isOpen={isLoading} />
            <Menu
                onClick={async (e) => await onClick(e)}
                selectedKeys={[current]}
                mode="horizontal"
                items={listMenu}
            />
        </Fragment>
    );
};

export default Menus;
