```
{
    label: Constants.MENU.M01,
    key: Constants.MENU_KEY.M01,
    icon: <ReadOutlined />,
    children: [
        {
            type: "group",
            children: [
                {
                    label: Constants.MENU_CHILD.M01.SM_01.LABEL,
                    key: Constants.MENU_CHILD.M01.SM_01.KEY,
                },
                {
                    label: Constants.MENU_CHILD.M01.SM_02.LABEL,
                    key: Constants.MENU_CHILD.M01.SM_02.KEY,
                },
                {
                    label: Constants.MENU_CHILD.M01.SM_03.LABEL,
                    key: Constants.MENU_CHILD.M01.SM_03.KEY,
                },
                {
                    label: Constants.MENU_CHILD.M01.SM_04.LABEL,
                    key: Constants.MENU_CHILD.M01.SM_04.KEY,
                },
            ],
        },
    ],
},

```
