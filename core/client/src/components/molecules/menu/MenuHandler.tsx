import Link from "next/link";
import type { MenuProps } from "antd";
import {
    ReadOutlined,
    FileTextOutlined,
    ExportOutlined,
    HomeOutlined,
} from "@ant-design/icons";
import Constants from "@/lib/constants";
import React from "react";
import { handleLogOutAsync } from "@/store/users/actions";
import { setNotify } from "@/store/common/actions";
import { loginGoToBack } from "@/lib/utils";
import ConfigApp from "@/lib/config";

export const items: MenuProps["items"] = [
    {
        label: Constants.MENU.M00,
        key: Constants.MENU_KEY.M00,
        icon: <HomeOutlined />,
        disabled: false,
    },
    {
        label: Constants.MENU.M01,
        key: Constants.MENU_KEY.M01,
        icon: <ReadOutlined />,
        children: [
            {
                label: Constants.MENU_CHILD.M01.SM_01.LABEL,
                key: Constants.MENU_CHILD.M01.SM_01.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_02.LABEL,
                key: Constants.MENU_CHILD.M01.SM_02.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_03.LABEL,
                key: Constants.MENU_CHILD.M01.SM_03.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_04.LABEL,
                key: Constants.MENU_CHILD.M01.SM_04.KEY,
            },
        ],
    },
    {
        label: Constants.MENU.M06,
        key: Constants.MENU_KEY.M06,
        icon: <FileTextOutlined />,
        disabled: false,
    },
    {
        label: (
            <Link href={ConfigApp.env.other_url} target="_blank">
                {Constants.MENU.M05}
            </Link>
        ),
        icon: <ExportOutlined />,
        key: Constants.MENU_KEY.M05,
    },
    {
        label: Constants.AUTHEN.LOGIN.LABEL,
        key: Constants.AUTHEN.LOGIN.KEY,
        style: {
            position: "absolute",
            right: 0,
        },
    },
];

export const activeMenu = (
    pathname: String,
    setCurrent: React.Dispatch<React.SetStateAction<string>>
) => {
    switch (pathname) {
        case "/login":
        case "/register":
            setCurrent(Constants.AUTHEN.LOGIN.KEY);
            break;
        default:
            if (location.pathname == "/") {
                setCurrent("home");
            }
            break;
    }
};

export const initDisplayMenuAfterLogin = (
    listMenu: any,
    user: any,
    setListMenu: any
) => {
    const newListMenu = listMenu?.filter(
        // tạo function xử lý riêng
        (item: any) =>
            ![
                Constants.AUTHEN.LOGIN.KEY,
                Constants.AUTHEN.PROFILE.KEY,
            ].includes(item?.key)
    );
    newListMenu?.push({
        label: user?.full_name ?? Constants.AUTHEN.PROFILE.LABEL,
        key: Constants.AUTHEN.PROFILE.KEY,
        children: [
            {
                label: Constants.AUTHEN.PROFILE.LABEL,
                key: "sm_" + Constants.AUTHEN.PROFILE.KEY,
            },
            {
                label: Constants.AUTHEN.LOG_OUT.LABEL,
                key: Constants.AUTHEN.LOG_OUT.KEY,
            },
        ],
        style: {
            position: "absolute",
            right: 0,
        },
    });
    setListMenu(newListMenu);
};

export const handleRedirect = async (
    key: string,
    router: any,
    dispatch: any,
    options: object,
    setIsLoading: any,
    checkAuth: any
) => {
    setIsLoading(true);
    switch (key) {
        case Constants.AUTHEN.LOGIN.KEY:
            return router.push("/login", { scroll: false });
        case Constants.MENU_CHILD.M01.SM_01.KEY:
            if (checkAuth) {
                return router.push("/dashboard/courses", { scroll: false });
            }

            const urlGoBack = new URL("/dashboard/courses", location.href);
            return loginGoToBack(urlGoBack);
        case Constants.AUTHEN.LOG_OUT.KEY:
            return await handleLogOut(dispatch, router, options, setIsLoading);
        case "sm_" + Constants.AUTHEN.PROFILE.KEY:
            return router.push("/dashboard/profile", { scroll: false });
        default:
            return router.push("/", { scroll: false });
    }
};

export const handleLogOut = async (
    dispatch: any,
    router: any,
    options: object,
    setIsLoading: any
) => {
    setIsLoading(true);
    const logout = await dispatch(handleLogOutAsync(options));
    setIsLoading(false);
    if (logout) {
        dispatch(
            setNotify({
                display: true,
                content: "Đăng xuất thành công.",
                type: "success",
            })
        );
        return router.push("/", { scroll: false });
    }

    return;
};
