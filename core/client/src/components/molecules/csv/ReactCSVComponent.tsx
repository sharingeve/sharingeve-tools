"use client";

import React from "react";
import { CSVLink } from "react-csv";
import { IReactCSV } from "./handler/react-csv-handler";

// https://www.npmjs.com/package/react-csv?activeTab=readme
export default function ReactCSVComponent({
    titleLinkCSV,
    csvData,
    headerCSV,
    filename,
    className,
}: IReactCSV) {
    return (
        <CSVLink
            href="#"
            suppressHydrationWarning={true}
            data={csvData}
            headers={headerCSV}
            filename={filename}
            className={className}
        >
            {titleLinkCSV}
        </CSVLink>
    );
}
