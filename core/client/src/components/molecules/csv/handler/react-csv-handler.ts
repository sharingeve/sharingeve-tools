import { ReactNode } from "react";

export interface IReactCSV {
    titleLinkCSV: ReactNode;
    csvData: any[];
    filename: string;
    headerCSV?: any[];
    className?: any;
    target?: any;
}
