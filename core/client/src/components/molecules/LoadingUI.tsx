"use client";

import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import clsx from "clsx";
import styles from "./css/molecules.module.css";

type ILoading = {
    isOpen: boolean;
};

const LoadingUI: React.FC<ILoading> = ({ isOpen }: ILoading) => {
    return (
        <Spin
            indicator={
                <LoadingOutlined
                    style={{
                        fontSize: 70,
                        color: "white",
                    }}
                    spin
                />
            }
            spinning={isOpen}
            size="large"
            fullscreen
            className={clsx(styles.disabled_select)}
        />
    );
};

export default LoadingUI;
