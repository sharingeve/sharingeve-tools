import { Button, Image } from "antd";
import { Fragment, useEffect, useRef } from "react";
import { handleBtnUp, handleFileChange } from "./handler";

export default function ImgUploadCus({
    txtBtn,
    accept,
    setDisabledBtn,
    previewImg,
    setPreviewImg,
    className,
    classWrapper,
    sizeImgPreview,
}: {
    txtBtn: string;
    accept?: string;
    setDisabledBtn?: any;
    previewImg: any;
    setPreviewImg: any;
    className?: string;
    classWrapper?: string;
    sizeImgPreview?: number;
}) {
    const fileRef = useRef<any>();

    useEffect(() => {
        return () => {
            previewImg && URL.revokeObjectURL(previewImg.preview);
        };
    }, [previewImg]);

    return (
        <div className={classWrapper ? classWrapper : ""}>
            <Button onClick={() => handleBtnUp(fileRef)}>{txtBtn}</Button>
            <input
                type="file"
                className={"hide "}
                accept={accept ? accept : "image/*"}
                id="preview_img_input"
                onChange={() =>
                    handleFileChange(fileRef, setPreviewImg, setDisabledBtn)
                }
                ref={fileRef}
            />
            {previewImg && (
                <Fragment>
                    <br /> <br />
                    <Image
                        width={sizeImgPreview ? sizeImgPreview : 200}
                        src={previewImg.preview}
                        alt="img_upload_preview"
                        className={className ? className : ""}
                    />
                </Fragment>
            )}
        </div>
    );
}
