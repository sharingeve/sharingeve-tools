import { v4 as uuidv4 } from "uuid";
import { errorNotify } from "@/components/atoms/Notification";

export const handleBtnUp = (fileRef: any) => {
    fileRef.current.click();
};

export const handleFileChange = (
    fileRef: any,
    setPreviewImg: any,
    setDisabledBtn?: any
) => {
    // https://www.youtube.com/watch?v=Fnb3GbY9FUY&t=472s&ab_channel=F8Official
    const [file] = fileRef.current.files; // e = fileRef ==> e.target.files[0]
    if (file) {
        const isFile2M =
            file.size / 1024 / 1024 < 2 &&
            (file.type === "image/jpeg" || file.type === "image/png"); // byte / kb / mb
        if (!isFile2M) {
            const keyNotify = uuidv4();
            localStorage.setItem("keyNotify", keyNotify);
            errorNotify({
                message: "Thông báo",
                description: "Chỉ upload file ảnh nhỏ hơn 2MB.",
                duration: 5,
                key: keyNotify,
                onClose: () => {
                    localStorage.removeItem("keyNotify");
                },
            });
            return;
        }

        file.preview = URL.createObjectURL(file);
        setPreviewImg(file);
        setDisabledBtn && setDisabledBtn(false);
    }
};
