import { v4 as uuidv4 } from "uuid";
import { errorNotify } from "@/components/atoms/Notification";
import { ITypeAction, ITypeActive } from "../interface/ListInterface";
import dayjs from "dayjs";
import { checkKeyTelegram, createPlan } from "@/service/plan-service";

export const radioGroup = [
    { value: "only", label: "Một lần" },
    { value: "loop", label: "Lặp lại" },
];
export const radioGroupActive = [
    { value: "minute", label: "Phút" },
    { value: "hour", label: "Giờ" },
    { value: "day", label: "Ngày" },
    { value: "week", label: "Tuần" },
    { value: "month", label: "Tháng" },
];
export const listSelectActive: { [key: string]: any } = {
    minute: [
        { value: "5", label: "5 phút" },
        { value: "10", label: "10 phút" },
        { value: "15", label: "15 phút" },
        { value: "30", label: "30 phút" },
    ],
    hour: [
        { value: "1", label: "1 giờ" },
        { value: "2", label: "2 giờ" },
        { value: "4", label: "4 giờ" },
        { value: "6", label: "6 giờ" },
        { value: "8", label: "8 giờ" },
        { value: "12", label: "12 giờ" },
    ],
    day: [
        { value: "0", label: "0-1 giờ hàng ngày" },
        { value: "6", label: "6-7 giờ hàng ngày" },
        { value: "8", label: "8-9 giờ hàng ngày" },
        { value: "10", label: "10-11 giờ hàng ngày" },
        { value: "12", label: "12 giờ hàng ngày" },
    ],
    week: [
        { value: "2", label: "Thứ 2 hàng tuần" },
        { value: "3", label: "Thứ 3 hàng tuần" },
        { value: "4", label: "Thứ 4 hàng tuần" },
        { value: "5", label: "Thứ 5 hàng tuần" },
        { value: "6", label: "Thứ 6 hàng tuần" },
        { value: "7", label: "Thứ 7 hàng tuần" },
        { value: "8", label: "Chủ nhật hàng tuần" },
    ],
    month: [
        { value: "1", label: "Ngày 1 hàng tháng" },
        { value: "5", label: "Ngày 5 hàng tháng" },
        { value: "10", label: "Ngày 10 hàng tháng" },
        { value: "15", label: "Ngày 15 hàng tháng" },
        { value: "20", label: "Ngày 20 hàng tháng" },
        { value: "25", label: "Ngày 25 hàng tháng" },
        { value: "last", label: "Ngày cuối tháng" },
    ],
};
export const onChangeValues = ({
    type_action,
    type_cycle,
    setTypeAction,
    setTypeActive,
    setListSelect,
    form,
    setDisabledBtn,
}: {
    type_action: any;
    type_cycle?: any;
    setTypeAction: any;
    setTypeActive: any;
    setListSelect: any;
    form: any;
    setDisabledBtn: any;
}) => {
    setDisabledBtn(false);
    if (type_action) {
        setTypeAction(type_action);
        if (type_action == "only") {
            setListSelect(listSelectActive.day);
            form.resetFields(["type_cycle", "time_cycle"]);
        }
    }
    if (type_cycle) {
        setTypeActive(type_cycle);
        setListSelect(listSelectActive[type_cycle]);
        form.setFieldValue("time_cycle", listSelectActive[type_cycle][0].value);
    }
};
export const onSubmitHandle = async (
    data: any,
    setDisabledBtn: any,
    form: any,
    setTypeAction: any,
    setTypeActive: any,
    keyConfirm: string
) => {
    setDisabledBtn(true);
    console.log("data", data);
    const keyNotify = uuidv4();
    if (!data.plan_detail || data.plan_detail?.length < 1) {
        displayError(
            keyNotify,
            "Không thêm thành công do thiếu thông tin: kế hoạch chi tiết."
        );
        return;
    }

    console.log("data", data);
    const dateStart = dayjs(data.date_start, "DD/MM/YYYY");
    const timeStart = dayjs(data.time_start, "HH:MM:SS");
    const dateNewFormat = dayjs(
        dateStart.format("DD/MM/YYYY") + " " + timeStart.format("HH:mm:ss"),
        "DD/MM/YYYY HH:mm:ss"
    );
    const today = dayjs();
    if (
        dateStart.year() < today.year() ||
        (dateStart.year() === today.year() &&
            dateStart.month() < today.month()) ||
        (dateStart.year() === today.year() &&
            dateStart.month() === today.month() &&
            dateStart.date() < today.date())
    ) {
        displayError(
            keyNotify,
            "Không thể chọn ngày thực hiện trong quá khứ. Hãy kiểm tra lại."
        );
        return;
    }

    if (dateNewFormat.diff(today, "hour") < 2) {
        displayError(
            keyNotify,
            "Hãy chọn thời gian thực hiện sau ít nhất là 2 giờ so với giờ hiện tại."
        );
        return;
    }
    //
    form.resetFields();
    setTypeAction("only");
    setTypeActive("day");
    data.date_start = dateNewFormat;
    data.key = keyConfirm;
    const create = await createPlan(data);
};
export const displayError = (
    keyNotify: any,
    description: string,
    message = "Thông báo",
    duration = 5
) => {
    localStorage.setItem("keyNotify", keyNotify);
    errorNotify({
        message: message,
        description: description,
        duration: duration,
        key: keyNotify,
        onClose: () => {
            localStorage.removeItem("keyNotify");
        },
    });
};

export const checkKeyConfirm = async (keyConfirm: string) => {
    return await checkKeyTelegram(keyConfirm);
};
