import { setLoadingRedux } from "@/store/common/actions";

export const redirectUpdatePage = async (dispatch: any, router: any) => {
    await dispatch(setLoadingRedux(true));

    return router.push('/dashboard/nikki/plan/update?page=create', {scroll: false})


}