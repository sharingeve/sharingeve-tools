"use client";

import { useSearchParams } from "next/navigation";
import { useEffect, useLayoutEffect, useState } from "react";
import CreatePlan from "./element/CreatePlan";
import UpdatePlan from "./element/UpdatePlan";

export default function UpdatePlanComponent() {
    const searchParams = useSearchParams();
    const [isCreatePage, setIsCreatePage] = useState(1);
    const [idPlan, setIdPlan] = useState<string>("");

    useLayoutEffect(() => {
        if (searchParams.get("page") == "update" && searchParams.get("pid")) {
            setIsCreatePage(0);
            setIdPlan(searchParams.get("pid") || "");
        }
    }, [searchParams]);

    return isCreatePage ? <CreatePlan /> : <UpdatePlan id={idPlan} />;
}
