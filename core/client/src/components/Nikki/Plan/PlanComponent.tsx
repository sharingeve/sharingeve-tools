"use client";

import type { ColumnsType } from "antd/es/table";
import { PlanDataType } from "./interface/ListInterface";
import { Button, Table } from "antd";
import { useEffect, useState } from "react";
import { useAppDispatch } from "@/hooks/redux-hook";
import { useRouter } from "next/navigation";
import { redirectUpdatePage } from "./handler/PlanHandle";

export default function PlanComponent() {
    const dispatch = useAppDispatch();
    const router = useRouter();
    
    //https://aithietke.com/tao-ung-dung-giao-tiep-voi-bot-telegram-bang-node-js-docker-va-deploy-len-ec2-aws-phan-1/
    const [sourceTable, setSourceTable] = useState<PlanDataType[]>();
    const [totalRecord, setTotalRecord] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);

    const columns: ColumnsType<PlanDataType> = [
        { title: "Kế hoạch", dataIndex: "namePlan", key: "namePlan" },
        {
            title: "Ngày lên kế hoạch",
            dataIndex: "dateStart",
            key: "dateStart",
        },
        {
            title: "Action",
            key: "operation",
            fixed: "right",
            // width: 200,
            // render: (_, { }) => (),
        },
    ];
    return (
        <div>
            <Button
                onClick={async () => {await redirectUpdatePage(dispatch, router)}}
                type="primary"
                style={{ marginBottom: 16 }}
            >
                Thêm kế hoạch mới
            </Button>
            <Table
                columns={columns}
                dataSource={sourceTable}
                pagination={{
                    defaultCurrent: currentPage,
                    total: totalRecord,
                    onChange: () => {},
                    hideOnSinglePage: true,
                }}
            />
        </div>
    );
}
