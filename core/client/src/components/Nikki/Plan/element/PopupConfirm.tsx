import ModalComponent from "@/components/atoms/modal/Modal";
import { FC, Fragment, useState } from "react";
import {
    checkKeyConfirm,
    displayError,
    onSubmitHandle,
} from "../handler/CreatePlanHandle";
import FieldInput from "@/components/atoms/Input";
import { v4 as uuidv4 } from "uuid";
import Link from "next/link";
import ConfigApp from "@/lib/config";

interface IFormConfirmKey {
    setKeyConfirm: any;
}

const FormConfirmKey: FC<IFormConfirmKey> = ({
    setKeyConfirm,
}: {
    setKeyConfirm: any;
}) => {
    return (
        <div>
            <p
                style={{
                    fontStyle: "italic",
                }}
            >
                <span style={{ color: "red" }}>(*) </span>
                Hãy truy cập&nbsp;
                <Link href={ConfigApp.env.bot_tele} target="_blank">
                    tại đây
                </Link>
                &nbsp;và sử dụng <strong>/key</strong> để lấy key xác nhận
            </p>
            <FieldInput
                name="key_confirm"
                placeholder="Nhập key xác nhận thông báo tự động"
                onBlur={(e: any) => {
                    const val = e.target.value;

                    if (val) {
                        setKeyConfirm(val);
                    }
                }}
            />
        </div>
    );
};

export default function PopupConfirm({
    openModal,
    setOpenModal,
    setDisabledBtn,
    form,
    setTypeAction,
    setTypeActive,
    resultData,
}: {
    openModal: any;
    setOpenModal: any;
    setDisabledBtn: any;
    form: any;
    setTypeAction: any;
    setTypeActive: any;
    resultData: any;
}) {
    const [keyConfirm, setKeyConfirm] = useState<string>("");

    return (
        <Fragment>
            {openModal && (
                <ModalComponent
                    open={openModal}
                    title="Hãy xác nhận key xác nhận"
                    content={<FormConfirmKey setKeyConfirm={setKeyConfirm} />}
                    onCallBack={async () => {
                        if (!keyConfirm) {
                            displayError(
                                uuidv4(),
                                "Thiếu key confirm hãy kiểm tra lại."
                            );
                            return;
                        }
                        const keyExist: any = await checkKeyConfirm(keyConfirm);
                        if (keyExist.status) {
                            setOpenModal(false);
                            await onSubmitHandle(
                                resultData,
                                setDisabledBtn,
                                form,
                                setTypeAction,
                                setTypeActive,
                                keyConfirm
                            );
                        } else {
                            displayError(uuidv4(), keyExist.error);
                            return;
                        }
                    }}
                    onCancelCallBack={() => setOpenModal(false)}
                />
            )}
            {/* create form input key chat */}
        </Fragment>
    );
}
