import { notFound } from "next/navigation";
import { useEffect } from "react";

export default function UpdatePlan({ id }: { id: string }) {
    useEffect(() => {
        // set init data
    }, []);

    if (!id) {
        return notFound();
    }

    return <div>UpdatePlan: {id}</div>;
}
