import { DatePicker, Form, Input, Radio, Select, Space } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import {
    onChangeValues,
    radioGroup,
    radioGroupActive,
    listSelectActive,
} from "../handler/CreatePlanHandle";
import FieldButton from "@/components/atoms/Button";
import { Fragment, useState } from "react";
import { ITypeAction, ITypeActive } from "../interface/ListInterface";
import dayjs from "dayjs";
import PopupConfirm from "./PopupConfirm";

export default function CreatePlan() {
    const [form] = Form.useForm();
    const [openModal, setOpenModal] = useState(false);
    const [typeAction, setTypeAction] = useState<ITypeAction | "only">("only");
    const [typeActive, setTypeActive] = useState<ITypeActive | "day">("day");
    const [listSelect, setListSelect] = useState(listSelectActive.day);
    const [disabledBtn, setDisabledBtn] = useState(true);
    const [initValueFrm, setInitValueFrm] = useState({
        type_action: typeAction,
        type_cycle: typeActive,
        time_cycle: listSelectActive.day[0].value,
        date_start: dayjs().add(2.5, 'hour'),
        time_start: dayjs().add(2.5, 'hour'),
    });
    const [resultData, setResultData] = useState([]);

    return (
        <>
            <Form
                form={form}
                name="dynamic_form_nest_item"
                onFinish={(value: any) => {
                    setOpenModal(true);
                    setResultData(value);
                }}
                autoComplete="off"
                onValuesChange={({ type_action, type_cycle }) => {
                    onChangeValues({
                        type_action,
                        type_cycle,
                        setTypeAction,
                        setTypeActive,
                        setListSelect,
                        form,
                        setDisabledBtn,
                    });
                }}
                initialValues={initValueFrm}
            >
                <Form.Item
                    name={"title_plan"}
                    label="Tên kế hoạch"
                    rules={[
                        {
                            required: true,
                            message: "Hãy nhập kế hoạch",
                        },
                    ]}
                >
                    <Input placeholder="Nhập tên kế hoạch" />
                </Form.Item>
                <Form.Item
                    name={"date_start"}
                    label="Ngày thực hiện kế hoạch"
                    rules={[
                        {
                            required: true,
                            message: "Hãy chọn ngày thực hiện kế hoạch",
                        },
                    ]}
                >
                    <DatePicker format={"DD-MM-YYYY"} />
                </Form.Item>
                <Form.Item
                    name={"time_start"}
                    label="Thời gian bắt đầu thông báo"
                    rules={[
                        {
                            required: true,
                            message: "Hãy chọn thời gian bắt đầu thông báo",
                        },
                    ]}
                >
                    <DatePicker picker="time" format="HH:mm" />
                </Form.Item>
                <Form.Item name="type_action" label="Thực hiện">
                    <Radio.Group>
                        {radioGroup.map((item) => (
                            <Radio value={item.value} key={item.value}>
                                {item.label}
                            </Radio>
                        ))}
                    </Radio.Group>
                </Form.Item>
                {typeAction && typeAction != "only" && (
                    <Fragment>
                        <Form.Item name="type_cycle" label="Chọn chu kỳ lặp">
                            <Radio.Group>
                                {radioGroupActive.map((item) => (
                                    <Radio value={item.value} key={item.value}>
                                        {item.label}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item name="time_cycle" label="Chọn thời gian lặp">
                            <Select>
                                {listSelect.map((item: any) => (
                                    <Select.Option
                                        value={item.value}
                                        key={item.value}
                                    >
                                        {item.label}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Fragment>
                )}
                <Form.List name="plan_detail">
                    {(fields, { add, remove }) => (
                        <>
                            <p style={{ fontStyle: "italic" }}>
                                <span style={{ color: "red" }}>(*)&nbsp;</span>
                                Hãy nhập chi tiết kế hoạch trước khi nhấn lưu
                            </p>
                            {fields.map(
                                ({ key, name, ...restField }, index) => (
                                    <Space
                                        key={key}
                                        style={{
                                            display: "flex",
                                            marginBottom: 8,
                                        }}
                                        align="baseline"
                                    >
                                        <Form.Item
                                            {...restField}
                                            name={[name, "planName"]}
                                            label={"Kế hoạch " + (index + 1)}
                                            rules={[
                                                {
                                                    required: true,
                                                    message:
                                                        "Hãy nhập chi tiết thực hiện",
                                                },
                                            ]}
                                        >
                                            <Input
                                                placeholder={`Chi tiết cần thực hiện`}
                                            />
                                        </Form.Item>
                                        <Form.Item
                                            {...restField}
                                            name={[name, "planNote"]}
                                        >
                                            <Input placeholder={"Ghi chú"} />
                                        </Form.Item>
                                        <MinusCircleOutlined
                                            onClick={() => remove(name)}
                                        />
                                    </Space>
                                )
                            )}
                            <Form.Item>
                                <FieldButton
                                    type={"dashed"}
                                    nameBtn="Bổ sung chi tiết kế hoạch"
                                    onClick={() => add()}
                                    icon={<PlusOutlined />}
                                />
                            </Form.Item>
                        </>
                    )}
                </Form.List>
                <Form.Item>
                    <FieldButton
                        htmlType="submit"
                        type="primary"
                        nameBtn="Thêm mới kế hoạch"
                        disabled={disabledBtn}
                    />
                </Form.Item>
            </Form>
            {openModal && (
                <PopupConfirm
                    openModal={openModal}
                    setOpenModal={setOpenModal}
                    resultData={resultData}
                    setDisabledBtn={setDisabledBtn}
                    form={form}
                    setTypeAction={setTypeAction}
                    setTypeActive={setTypeActive}
                />
            )}
        </>
    );
}
