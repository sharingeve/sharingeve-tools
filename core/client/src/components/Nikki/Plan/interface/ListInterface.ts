export interface PlanDataType {
    namePlan: string;
    dateStart?: any;
}

export interface ITypeAction {
    loop: string;
    only: string;
}

export interface ITypeActive {
    minute: string;
    hour: string;
    day: string;
    week: string;
    month: string;
}
