import { PayloadAction } from "@reduxjs/toolkit";
import { initialState } from "./slice";
import { ICommon, IReduceNotify } from "./common-interface.redux";

const SET_NOTIFY = (state: ICommon, action: PayloadAction<IReduceNotify>) => {
    state.notify = { ...state.notify, ...action.payload };
};

const RESET_NOTIFY = (state: ICommon) => {
    state.notify = { ...initialState.notify };
};

const SET_LOADING = (state: ICommon, action: PayloadAction<boolean>) => {
    state.loading = action.payload;
};

export const commonReducer = { SET_NOTIFY, RESET_NOTIFY, SET_LOADING };
