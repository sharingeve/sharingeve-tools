import { IReduceNotify } from "./common-interface.redux";
import { commonSlice } from "./slice";

const { SET_NOTIFY, RESET_NOTIFY, SET_LOADING } = commonSlice.actions;

const resetNotify = () => (dispatch: any) => {
    return dispatch(RESET_NOTIFY());
};

const setNotify = (notify: IReduceNotify) => (dispatch: any) => {
    return dispatch(SET_NOTIFY(notify));
};

const setLoadingRedux = (loading: boolean) => (dispatch: any) => {
    return dispatch(SET_LOADING(loading));
};

export { setNotify, resetNotify, setLoadingRedux };
