export interface ICommon {
    notify: {
        content: string;
        type: string;
        config?: object;
        display: boolean;
        isAuth?: boolean;
    };
    loading: boolean;
}

export interface IReduceNotify {
    content: string;
    type: 'error' | 'success' | 'warning';
    config?: object;
    display: boolean;
    isAuth?: boolean;
}
