import { createSlice } from "@reduxjs/toolkit";
import { commonReducer } from "./reducers";
import { ICommon } from "./common-interface.redux";

export const initialState: ICommon = {
    notify: {
        content: "",
        type: "",
        config: {},
        display: false,
        isAuth: false,
    },
    loading: false,
};

export const commonSlice = createSlice({
    name: "CommonSlice",
    initialState,
    reducers: commonReducer,
});

export default commonSlice.reducer;
