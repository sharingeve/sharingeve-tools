import { RootState } from "..";

export const getNotify = (state: RootState) => state.common.notify;
export const getLoadingRedux = (state: RootState) => state.common.loading;
