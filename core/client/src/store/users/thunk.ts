import userService from "@/service/user-service";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const loginAsync = createAsyncThunk("users/login", async (data: any) => {
    const response: any = await userService.login("/api/auth/login", data);

    return response;
});

export const getUserAsync = createAsyncThunk("users/setUser", async () => {
    const findUser: any = await userService.findUser("/user/find");
    return findUser;
});
