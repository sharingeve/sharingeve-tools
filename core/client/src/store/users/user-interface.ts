type UserType = {
    id: string;
    full_name: string;
    email: string;
    account: string;
    isClient: boolean;
    phoneNumber: string;
    updatedAt: string;
};
export default interface IUserRedux {
    isAuth: boolean;
    token: string;
    user: UserType;
    user_id: string;
}

export interface ILogIn {
    username: string;
    password: string;
}
