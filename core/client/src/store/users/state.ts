import IUserRedux from "./user-interface";

const initialUserState: IUserRedux = {
    isAuth: false,
    token: "",
    user: {
        id: "",
        full_name: "",
        email: "",
        account: "",
        isClient: true,
        phoneNumber: "",
        updatedAt: "",
    },
    user_id: "",
};

export default initialUserState;
