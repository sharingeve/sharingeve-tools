import { userSlice } from "./slice";
import { getUserAsync, loginAsync } from "./thunk";
import userService from "@/service/user-service";
import initialUserState from "./state";
import { removeTokenCookie, setTokenCookie } from "@/hooks/authentication";
import { showLogCatch } from "@/lib/utils";

const { SET_USER, SET_TOKEN } = userSlice.actions;

const setUser = (user: object) => async (dispatch: any) => {
    if (Object.keys(user).length > 0) {
        return await dispatch(SET_USER(user));
    }
};

const setAuthenticate = (user: any) => async (dispatch: any) => {
    const accessToken = localStorage.getItem("accessToken");

    if (accessToken && user?.id) {
        await Promise.all([
            dispatch(SET_TOKEN(accessToken)),
            dispatch(setUser(user)),
        ]);
        return;
    }
    localStorage.removeItem("accessToken");
};

const handleLoginAsync = (data: any) => async (dispatch: any) => {
    try {
        const handle = await dispatch(loginAsync(data));
        if (handle.payload) {
            await dispatch(getUserAsync());
            return true;
        }

        return false;
    } catch (error) {
        showLogCatch("handleLoginAsync error >>" + error);
        return false;
    }
};

const handleLogOutAsync = (options: object) => async (dispatch: any) => {
    try {
        const res = await userService.logOut("/api/auth/logout", options);
        if (!res) {
            return false;
        }

        await Promise.all([
            dispatch(SET_TOKEN("")),
            dispatch(setUser(initialUserState.user)),
            localStorage.removeItem("accessToken"),
            removeTokenCookie(),
        ]);
        return true;
    } catch (error: any) {
        showLogCatch("handleLogOutAsync error >>" + error.message);
        return false;
    }
};

const logoutExpiredToken = () => async (dispatch: any) => {
    try {
        await Promise.all([
            dispatch(SET_TOKEN("")),
            dispatch(setUser(initialUserState.user)),
            localStorage.removeItem("accessToken"),
            removeTokenCookie(),
        ]);
    } catch (error: any) {
        showLogCatch("logoutExpiredToken error >>" + error.message);
    }
};

const handleCreateUser = (options: object) => async (dispatch: any) => {
    const result = {
        status: false,
    };

    try {
        const res: any = await userService.createUser(
            "/api/auth/create",
            options
        );
        if (res.type == "success") {
            await Promise.all([
                setTokenCookie(res.accessToken),
                dispatch(dispatch(SET_TOKEN(res.accessToken))),
                dispatch(dispatch(SET_USER(res.data))),
            ]);
            localStorage.setItem("accessToken", res.accessToken);
            result.status = true;
        }

        return { ...res, ...result };
    } catch (error: any) {
        return { ...result, data: error.message };
    }
};

export {
    setUser,
    handleLoginAsync,
    setAuthenticate,
    handleLogOutAsync,
    handleCreateUser,
    logoutExpiredToken,
};
