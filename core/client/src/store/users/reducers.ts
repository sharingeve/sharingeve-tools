import { PayloadAction } from "@reduxjs/toolkit";
import IUserRedux, { ILogIn } from "./user-interface";

const SET_USER: (state: IUserRedux, action: PayloadAction<object>) => void = (
    state,
    action
) => {
    state.user = {
        id: "",
        full_name: "",
        email: "",
        account: "",
        isClient: true,
        phoneNumber: "",
        updatedAt: "",
        ...action.payload,
    };
    state.user_id = state.user.id;
};

const SET_TOKEN = (state: IUserRedux, action: PayloadAction<string>) => {
    if (action.payload) {
        state.isAuth = true;
    } else {
        state.isAuth = false;
    }
    state.token = action.payload;
};

export const UserReducers = {
    SET_USER,
    SET_TOKEN,
};
