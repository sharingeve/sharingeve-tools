import { createSlice } from "@reduxjs/toolkit";
import initialUserState from "./state";
import { UserReducers } from "./reducers";
import { getUserAsync, loginAsync } from "./thunk";
import { setTokenCookie } from "@/hooks/authentication";

export const userSlice = createSlice({
    name: "UserRedux",
    initialState: initialUserState,
    reducers: UserReducers,
    extraReducers(builder) {
        builder
            .addCase(loginAsync.pending, (state, action) => {
                state.isAuth = false;
                state.token = "";
            })
            .addCase(loginAsync.fulfilled, (state, action) => {
                if (action.payload.accessToken) {
                    state.isAuth = true;
                    setTokenCookie(action.payload.accessToken);
                    localStorage.setItem(
                        "accessToken",
                        action.payload.accessToken
                    );
                    state.token = action.payload.accessToken;
                }
            })
            .addCase(loginAsync.rejected, (state, action) => {
                state.isAuth = false;
                state.token = "";
                console.log("builder reject", action.payload);
            })
            .addCase(getUserAsync.fulfilled, (state, action) => {
                state.user = { ...action.payload };
            });
    },
});

export default userSlice.reducer;
