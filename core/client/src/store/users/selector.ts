import { RootState } from "..";

export const userDetail = (state: RootState) => state.users.user;
export const isAuth = (state: RootState) => state.users.isAuth;
export const accessToken = (state: RootState) => state.users.token;
export const userId = (state: RootState) => state.users.user_id;
