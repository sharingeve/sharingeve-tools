import { RootState } from "..";

export const getMemoDetail = (state: RootState) => state.memos.memoDetail;
export const getListMemo = (state: RootState) => state.memos.listMemo;
