import { IMemoRedux } from "./memo-interface";
import { PayloadAction } from "@reduxjs/toolkit";

const SET_MEMO_DETAIL = (state: IMemoRedux, action: PayloadAction<object>) => {
    state.memoDetail = { ...action.payload };
};
const SET_LIST_MEMO = (state: IMemoRedux, action: PayloadAction<any[]>) => {
    state.listMemo = [...action.payload];
};

export const MemoReducers = { SET_MEMO_DETAIL, SET_LIST_MEMO };
