import { createSlice } from "@reduxjs/toolkit";
import { MemoReducers } from "./memo-reducers";
import { IMemoRedux } from "./memo-interface";

const initialMemoState: IMemoRedux = {
    memoDetail: {},
    listMemo: [],
};

export const resetMemoState: IMemoRedux = {
    memoDetail: {},
    listMemo: [],
};

export const memoSlice = createSlice({
    name: "MemoRedux",
    initialState: initialMemoState,
    reducers: MemoReducers,
});

export default memoSlice.reducer;
