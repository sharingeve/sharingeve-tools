import { isEmptyObject } from "@/lib/utils";
import { memoSlice } from ".";
import {
    apiCreateMemo,
    apiDetailMemo,
    apiListMemo,
    apiUpdatedMemo,
} from "@/service/memo-service";

const { SET_MEMO_DETAIL, SET_LIST_MEMO } = memoSlice.actions;

const setMemoDetail = (id: string) => async (dispatch: any) => {
    const res = await apiDetailMemo("/");
    if (!res) {
        return {};
    }

    return await dispatch(SET_MEMO_DETAIL(res));
};

const setListMemo = () => async (dispatch: any) => {
    const res: any = await apiListMemo("/");
    if (!res) {
        return [];
    }

    return await dispatch(SET_LIST_MEMO(res));
};

const initMemoDetail = (data: object) => async (dispatch: any) => {
    return await dispatch(SET_MEMO_DETAIL(data));
};

const initListMemo = (data: any[]) => async (dispatch: any) => {
    return await dispatch(SET_LIST_MEMO(data));
};

const createMemo = async (data: any) => {
    const formData = new FormData();
    formData.append("full_name", data?.full_name);
    formData.append("price", data?.price);
    formData.append("reason", data?.reason);
    formData.append("created_date", data?.created_date);
    formData.append("img_qr", data?.img_qr);
    const createMemo: any = await apiCreateMemo("/memo/create", formData, {
        headers: { "Content-Type": "multipart/form-data;charset=UTF-8" },
    });

    if (isEmptyObject(createMemo)) {
        return { status: false };
    }

    return createMemo;
};

const updatedMemo = async (data: any) => {
    const id = data?.id;
    if (isEmptyObject(data) || !id) {
        return { status: false };
    }

    const uri = `/memo/${id}/updated`;

    const updated = await apiUpdatedMemo(uri, data);

    return updated;
};

const MemoActions = {
    setMemoDetail,
    setListMemo,
    initMemoDetail,
    initListMemo,
    createMemo,
    updatedMemo,
};
export default MemoActions;
