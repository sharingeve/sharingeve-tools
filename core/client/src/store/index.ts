"use client";

import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./users/slice";
import commonReducer from "./common/slice";
import memoReducers from "./memo";

export const store = configureStore({
    reducer: {
        common: commonReducer,
        users: userReducer,
        memos: memoReducers,
    },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
