"use client";

import React from "react";
import { Layout } from "antd";

export default function Footer() {
    return (
        <Layout.Footer style={{ textAlign: "center", zIndex: 99 }}>
            Kojin Tools ©2024 Created by TungNT
        </Layout.Footer>
    );
}
