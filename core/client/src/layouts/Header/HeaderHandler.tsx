import Link from "next/link";
import type { MenuProps } from "antd";
import {
    ReadOutlined,
    ExportOutlined,
    HomeOutlined,
    CarryOutOutlined,
    ScheduleOutlined,
    SaveOutlined,
    FlagOutlined,
} from "@ant-design/icons";
import Constants from "@/lib/constants/constants";
import { handleLogOutAsync } from "@/store/users/actions";
import { setLoadingRedux, setNotify } from "@/store/common/actions";
import { routes } from "@/routers/routes";
import ConfigApp from "@/lib/config";
import { getKeyByValue } from "@/lib/utils";

// handler menu hamburger
export const handleRedirect = async (
    key: string,
    router: any,
    dispatch: any
) => {
    await dispatch(setLoadingRedux(true));
    if (key === Constants.MENU_KEY.M05) return;

    return router.push(routes[key] ?? "/", { scroll: false });
};
export const items: MenuProps["items"] = [
    {
        icon: null,
        label: Constants.AUTHEN.PROFILE.LABEL,
        key: Constants.AUTHEN.PROFILE.KEY,
    },
    {
        icon: <HomeOutlined />,
        label: Constants.MENU.M00,
        key: Constants.MENU_KEY.M00,
    },
    {
        icon: <ReadOutlined />,
        label: Constants.MENU.M01,
        key: Constants.MENU_KEY.M01,
        children: [
            {
                label: Constants.MENU_CHILD.M01.SM_01.LABEL,
                key: Constants.MENU_CHILD.M01.SM_01.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_02.LABEL,
                key: Constants.MENU_CHILD.M01.SM_02.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_03.LABEL,
                key: Constants.MENU_CHILD.M01.SM_03.KEY,
            },
            {
                label: Constants.MENU_CHILD.M01.SM_04.LABEL,
                key: Constants.MENU_CHILD.M01.SM_04.KEY,
            },
        ],
    },
    {
        label: Constants.MENU.M02,
        key: Constants.MENU_KEY.M02,
        icon: <CarryOutOutlined />,
        children: [
            {
                label: Constants.MENU_CHILD.M03.SM_01.LABEL,
                key: Constants.MENU_CHILD.M03.SM_01.KEY,
            },
            {
                label: Constants.MENU_CHILD.M03.SM_02.LABEL,
                key: Constants.MENU_CHILD.M03.SM_02.KEY,
            },
        ],
    },
    {
        label: Constants.MENU.M03,
        key: Constants.MENU_KEY.M03,
        icon: <ScheduleOutlined />,
        disabled: false,
    },
    {
        label: Constants.MENU.M04,
        key: Constants.MENU_KEY.M04,
        icon: <SaveOutlined />,
        disabled: false,
    },
    {
        icon: <ExportOutlined />,
        label: (
            <Link href={ConfigApp.env.other_url} target="_blank">
                {Constants.MENU.M05}
            </Link>
        ),
        key: Constants.MENU_KEY.M05,
    },
];
const itemsAdmin: MenuProps["items"] = [
    {
        label: Constants.MENU.M07,
        key: Constants.MENU_KEY.M07,
        icon: <FlagOutlined />,
        disabled: false,
    },
];
export const handleAddMenuAdmin = (listMenu: any, setListMenu: any) => {
    const newList = [...listMenu, ...itemsAdmin];
    setListMenu(newList);
};
export const activeMenu = (
    pathname: string,
    setCurrent: React.Dispatch<React.SetStateAction<string>>
) => {
    const menuActive = getKeyByValue(routes, pathname) ?? "";
    if (menuActive) {
        return setCurrent(menuActive);
    }

    return setCurrent(Constants.MENU_CHILD.M01.SM_01.KEY);
};

export const handleLogOut = async (
    dispatch: any,
    router: any,
    options: object
) => {
    const logout = await dispatch(handleLogOutAsync(options));

    if (logout) {
        dispatch(
            setNotify({
                display: true,
                content: "Đăng xuất thành công.",
                type: "success",
            })
        );
        return router.push("/", { scroll: false });
    }

    return;
};
