/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import React, { useEffect, useState } from "react";
import type { MenuProps } from "antd";
import { Layout, Menu, notification } from "antd";
import Constants from "@/lib/constants";
import {
    activeMenu,
    handleRedirect,
    items,
    handleAddMenuAdmin,
} from "./HeaderHandler";
import { usePathname } from "next/navigation";
import { useRouter } from "next/navigation";
import { useAppDispatch, useAppSelector } from "@/hooks/redux-hook";
import { userDetail } from "@/store/users/selector";
import { getNotify } from "@/store/common/selectors";
import { resetNotify } from "@/store/common/actions";

const { Sider } = Layout;

const Header: React.FC = () => {
    const [current, setCurrent] = useState(Constants.MENU_CHILD.M01.SM_01.KEY);
    const [listMenu, setListMenu] = useState(items);
    const pathName: string = usePathname();
    const router = useRouter();
    const dispatch = useAppDispatch();
    const user: any = useAppSelector(userDetail);
    const notify: any = useAppSelector(getNotify);

    const hanlderOnClick: MenuProps["onClick"] = async (e) => {
        if (e.key == current) {
            return;
        }
        setCurrent(e.key);
        await handleRedirect(e.key, router, dispatch);
    };

    useEffect(() => {
        activeMenu(pathName, setCurrent);
        if (user?.isClient == false) {
            handleAddMenuAdmin(listMenu, setListMenu);
        }
    }, []);

    useEffect(() => {
        if (notify.display && notify.isAuth) {
            notification.open({
                type: notify.type,
                message: "Thông báo",
                description: notify.content,
                duration: 3,
                onClose: () => {
                    dispatch(resetNotify());
                },
            });
        }
    }, [notify]);

    return (
        <Sider
            breakpoint="lg"
            collapsedWidth="0"
            onBreakpoint={(broken) => {
                // console.log(broken);
            }}
            onCollapse={(collapsed, type) => {
                // console.log(collapsed, type);
            }}
        >
            <div className="demo-logo-vertical" />
            <Menu
                theme="dark"
                mode="inline"
                selectedKeys={[current]}
                defaultSelectedKeys={[Constants.MENU_CHILD.M01.SM_01.KEY]}
                defaultOpenKeys={[Constants.MENU_KEY.M01]}
                items={listMenu}
                onClick={hanlderOnClick}
            />
        </Sider>
    );
};

export default Header;
