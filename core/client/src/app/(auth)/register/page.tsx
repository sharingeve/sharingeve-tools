"use client";

import React, { useEffect, useState } from "react";
import { notification } from "antd";
import Main from "@/components/atoms/main";
import Form from "@/components/molecules/form";
import { formBasic, initialValues, listInputReset } from "./register-hanlde";
import { useRouter } from "next/navigation";
import styles from "./register.module.css";
import clsx from "clsx";
import { useAppDispatch } from "@/hooks/redux-hook";
import { handleCreateUser } from "@/store/users/actions";
import LoadingUI from "@/components/molecules/LoadingUI";
import { destroyNotify, successNotify } from "@/components/atoms/Notification";
import Constants from "@/lib/constants/constants";

export default function RegisterComponent() {
    const router = useRouter();
    const dispatch = useAppDispatch();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isCreateSuccess, setIsCreateSuccess] = useState(false);

    useEffect(() => {
        if (isCreateSuccess) {
            setTimeout(() => {
                destroyNotify("create_user_done");
                router.push(Constants.DEFAULT_HOME, { scroll: false });
            }, 1000);
            return;
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isCreateSuccess]);

    const onFinish = async (values: any) => {
        setIsLoading(true);
        const create: any = await dispatch(handleCreateUser(values));
        setIsLoading(false);
        if (!create.status) {
            notification.open({
                type: create?.type || "error",
                message: "Thông báo",
                description: create?.content || "Lỗi tạo user",
                duration: 5,
            });
            return;
        }

        successNotify({
            message: "Thông báo",
            description: "Tạo user thành công",
            key: "create_user_done",
        });
        setIsCreateSuccess(true);
    };

    return (
        <Main>
            <LoadingUI isOpen={isLoading} />
            <Form
                nameForm="normal_register"
                className={clsx(styles.regist_form)}
                autoComplete="off"
                items={formBasic(router, isCreateSuccess)}
                onFinish={onFinish}
                initialValues={initialValues}
                listResetFields={listInputReset}
                layout="vertical"
                isResetForm={true}
            />
        </Main>
    );
}
