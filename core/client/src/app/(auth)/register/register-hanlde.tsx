import React from "react";
import clsx from "clsx";
import FieldButton from "@/components/atoms/Button";
import FieldInput from "@/components/atoms/Input";
import { i18nVi } from "@/lib/i18n/vi";
import styles from "./register.module.css";

export const initialValues: any = {};
export const listInputReset: string[] = [
    "full_name",
    "email",
    "username",
    "password",
    "confirm_password",
    "phone_number",
];

const getItem = (
    field: React.ReactNode,
    label?: string,
    name?: string,
    rule?: any[],
    custom?: object,
    dependencies?: any[],
    hasFeedback?: boolean
) => ({
    field,
    label,
    name,
    rule,
    custom,
    dependencies,
    hasFeedback,
});

export const formBasic = (router: any, isCreateSuccess: any) => [
    getItem(
        <FieldInput placeholder={i18nVi.FULL_NAME_FIELD} name="full_name" />,
        i18nVi.FULL_NAME_FIELD,
        "full_name",
        fullNameRule
    ),
    getItem(
        <FieldInput placeholder={i18nVi.EMAIL_FIELD} name="email" />,
        i18nVi.EMAIL_FIELD,
        "email",
        emailRule
    ),
    getItem(
        <FieldInput placeholder={i18nVi.ACCOUNT_FIELD} name="account" />,
        i18nVi.ACCOUNT_FIELD,
        "username",
        accountRule
    ),
    getItem(
        <FieldInput
            placeholder="********"
            type="password"
            name="password"
            scoped="passwordInput"
        />,
        i18nVi.PASSWORD_FIELD,
        "password",
        passwordRule
    ),
    getItem(
        <FieldInput
            placeholder="********"
            type="password"
            name="confirm_password"
            scoped="passwordInput"
        />,
        i18nVi.CONFIRM_PASSWORD_FIELD,
        "confirm_password",
        passwordConfirmRule,
        {},
        ["password"],
        true
    ),
    getItem(
        <FieldInput placeholder="0123456789" name="phone_number" />,
        i18nVi.PHONE_NUMBER_FIELD,
        "phone_number",
        phoneNumberRule
    ),
    getItem(
        <FieldButton
            type="primary"
            htmlType="submit"
            className="regist_form_button"
            nameBtn="Đăng ký"
            disabled={isCreateSuccess}
        />,
        undefined,
        undefined,
        [],
        btnRegistCustom
    ),
    getItem(
        groupBtnAction(router),
        undefined,
        undefined,
        [],
        groupBtnActionCustom
    ),
];

// UI
const groupBtnAction = (router: any) => (
    <React.Fragment>
        <FieldButton
            className={clsx(styles.regist_form_button)}
            nameBtn="Đã có tài khoản"
            onClick={() => router.push("/login", { scroll: false })}
        />
        &nbsp; &nbsp;
        <span>OR</span>
        <FieldButton
            type="link"
            className="forgot_form_button"
            nameBtn="Forgot password"
            onClick={() => router.push("/reset", { scroll: false })}
        />
    </React.Fragment>
);

// validate rule
const fullNameRule: any[] = [
    {
        required: true,
        message: i18nVi.FULL_NAME_REQUIRED,
    },
];
const emailRule: any[] = [
    {
        type: "email",
        message: i18nVi.CM_EMAIL_VALID,
    },
    {
        required: true,
        message: i18nVi.EMAIL_REQUIRED,
    },
];
const accountRule: any[] = [
    {
        required: false,
        message: i18nVi.CM_ACCOUNT_VALID_REQUIRE,
    },
];

const phoneNumberRule: any[] = [
    {
        pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
        message: i18nVi.CM_PHONE_NUMBER_VALID,
    },
];
const passwordRule: any[] = [
    {
        required: true,
        message: i18nVi.PASSWORD_REQUIRED,
    },
    {
        min: 8,
        message: i18nVi.CM_BOTH_MAX_MIN.replace(
            /{max_length}|{min_length}/g,
            (matched) => {
                return matched === "{max_length}" ? "16" : "8";
            }
        ),
    },
];

const passwordConfirmRule: any[] = [
    {
        required: true,
        message: i18nVi.CM_PASSWORD_CONFIRM_REQUIRED_VALID,
    },
    ({ getFieldValue }: { getFieldValue: any }) => ({
        validator(_: any, value: any) {
            if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
            }
            return Promise.reject(new Error(i18nVi.CM_PASSWORD_CONFIRM_VALID));
        },
    }),
];

// field custom
const btnRegistCustom = {
    className: clsx(styles.action_btn_signUp),
};
const groupBtnActionCustom = {
    className: clsx(styles.action_btn_loginAndForgot),
};
