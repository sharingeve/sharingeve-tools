"use client";

import React, { useState } from "react";
import clsx from "clsx";
import Main from "@/components/atoms/main";
import styles from "./login.module.css";
import Form from "@/components/molecules/form";
import { fieldBasic, initialValues, listInputReset } from "./login-handle";
import { useRouter } from "next/navigation";
import { constantsAuth } from "@/lib/constants";
import { useAppDispatch } from "@/hooks/redux-hook";
import { handleLoginAsync } from "@/store/users/actions";
import LoadingUI from "@/components/molecules/LoadingUI";

const LoginPage: React.FC = () => {
    const router = useRouter();
    const dispatch = useAppDispatch();
    const [error, setError] = useState<string>("");
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const onFinish = async (values: any) => {
        setIsLoading(true);
        const login = await dispatch(handleLoginAsync(values));
        if (!login) {
            setIsLoading(false);
            return setError(constantsAuth.LABEL_ERROR_LOGIN);
        }

        setError("");
        setTimeout(() => window.location.reload(), 300);
    };

    return (
        <Main>
            <LoadingUI isOpen={isLoading} />
            <Form
                nameForm="normal_login"
                className={clsx(styles.login_form)}
                autoComplete="off"
                items={fieldBasic(router)}
                onFinish={onFinish}
                error={error}
                initialValues={initialValues}
                listResetFields={listInputReset}
                layout="vertical"
                isResetForm={true}
            />
        </Main>
    );
};

export default LoginPage;
