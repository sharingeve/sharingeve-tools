import React from "react";
import clsx from "clsx";
import styles from "./login.module.css";
import { i18nVi } from "@/lib/i18n/vi";
import FieldInput from "@/components/atoms/Input";
import FieldButton from "@/components/atoms/Button";

export const initialValues: IDefaultValues = { username: "", password: "" };
export const listInputReset: string[] = ["password"];

export const fieldBasic = (router: any) => [
    {
        label: "Email",
        name: "username",
        rule: [
            {
                required: true,
                message: i18nVi.EMAIL_REQUIRED,
            },
            // {
            //     type: 'email',
            //     message: 'Hãy nhập email',
            // },
        ],
        field: <FieldInput placeholder="Email" name="username" />,
    },
    {
        label: "Password",
        name: "password",
        rule: [
            {
                required: true,
                message: i18nVi.PASSWORD_REQUIRED,
            },
            // {
            //     min: 2,
            //     message: i18nVi.CM_BOTH_MAX_MIN.replace(
            //         /{max_length}|{min_length}/g,
            //         (matched) => {
            //             return matched === "{max_length}" ? "32" : "1";
            //         }
            //     ),
            // },
        ],
        field: (
            <FieldInput
                placeholder="********"
                type="password"
                name="password"
            />
        ),
    },
    {
        field: (
            <FieldButton
                type="primary"
                htmlType="submit"
                className="login_form_button"
                nameBtn="Log In"
            />
        ),
        custom: {
            className: clsx(styles.action_btn_signIn),
        },
    },
    {
        field: (
            <React.Fragment>
                <FieldButton
                    className={clsx(styles.register_form_button)}
                    nameBtn="Create a new Account"
                    onClick={() => router.push("/register", { scroll: false })}
                />
                <span>OR</span>
                <FieldButton
                    type="link"
                    className="forgot_form_button"
                    nameBtn="Forgot password"
                    onClick={() => router.push("/reset", { scroll: false })}
                />
            </React.Fragment>
        ),
        custom: {
            className: clsx(styles.action_btn_registAndForgot),
        },
    },
];

export const setTokenToCookie = (token: string) => {};
