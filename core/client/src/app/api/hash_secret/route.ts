import { CacheSingleton } from "@/lib/cache.lib";
import { CryptoInstance } from "@/lib/crypto.lib";
import { TimeLibSingleton } from "@/lib/time.lib";
import { LogSingleton } from "@/lib/logs.lib";

export async function POST(request: Request) {
    const timestamp = TimeLibSingleton.getEpoch();
    const data = await request.json();
    let secret = `secret_${timestamp}_`;
    if (data && data?.secretKey) {
        secret += data.secretKey;
    }

    const { privateKey } = CryptoInstance.createPrivateAndPublicKey(
        timestamp,
        secret
    );

    return Response.json({
        code: 200,
        epoch: timestamp,
        privateKey: encodeURIComponent(privateKey),
    });
}

export async function GET(request: Request) {
    const forwarded = request.headers.get("x-forwarded-for");
    const ip = forwarded ? forwarded.split(/, /)[0] : null;
    const useAgent = request.headers.get("user-agent");
    const url = new URL(request.url);
    const search: URLSearchParams = new URLSearchParams(url.search);
    const sign = search.get("sign");
    const epoch = search.get("epoch");
    const secretKey = search.get("secretKey");
    if (!sign || !epoch) {
        return Response.redirect(`${process.env.BASE_URL}`);
    }
    const resDTO = CryptoInstance.verifiedHash(sign, epoch, secretKey);
    LogSingleton.logger(
        TimeLibSingleton.getEpoch(),
        JSON.stringify({
            ip,
            useAgent,
            type: 'hash',
            epoch,
            message: 'Kiểm tra hash: ' + JSON.stringify(resDTO),
        })
    );

    return Response.json(resDTO);
}
