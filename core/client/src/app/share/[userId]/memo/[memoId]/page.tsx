import SharingMemoComponent from "@/components/Memo/SharingMemoComponent";
import Main from "@/components/atoms/main";
import ConfigApp from "@/lib/config";
import { apiShareDetailMemo } from "@/service/memo-service";
import { notFound } from "next/navigation";
import React from "react";

const getMemoDetail = async (userId: any, memoId: any) => {
    try {
        if (!userId || !memoId) {
            return {};
        }

        const memoDetail = await apiShareDetailMemo(
            `${ConfigApp.env.uri_backend}/api/share/${userId}/memo/${memoId}`
        );

        return memoDetail;
    } catch (error: any) {
        return {
            status: true,
            data: {},
            error: error?.message,
        };
    }
};

export default async function ShareMemo({ params }: { params: any }) {
    const memo = await getMemoDetail(params?.userId, params?.memoId);

    if (memo?.data?.memoDetail?.amount == 0 || !memo?.status) {
        return <Main>{notFound()}</Main>;
    }

    return (
        <Main>
            <SharingMemoComponent
                memoId={params?.memoId}
                memo={memo}
                userId={params?.userId}
            />
        </Main>
    );
}
