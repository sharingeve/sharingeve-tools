export interface UserModel {
    id: string;
    full_name: string;
    email: string;
    account: string;
    createdAt: string;
    updatedAt: string;
}
