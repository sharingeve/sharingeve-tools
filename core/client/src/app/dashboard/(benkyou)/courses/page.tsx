import React from "react";
import { MainDashboard } from "@/components/atoms/main";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Khóa học - kojin tools",
    description: "Danh sách các khóa học free được tổng hợp nhiều nguồn",
};

export default function CoursePage() {
    return <MainDashboard>CoursePage</MainDashboard>;
}
