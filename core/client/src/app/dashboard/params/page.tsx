"use client";
import { MainDashboard } from "@/components/atoms/main";
import { useSearchParams } from "next/navigation";
import HomeParamComponent from "@/components/params/HomeParamComponent";
import UpdateParamComponent from "@/components/params/UpdateParamComponent";
import { useAppSelector } from "@/hooks/redux-hook";
import { userId } from "@/store/users/selector";
import useRouterAndDispatch from "@/hooks/dispatch-route-hook";

export default function ParamsPage() {
    const user_id = useAppSelector(userId);
    const searchParams = useSearchParams();
    const { router, dispatch } = useRouterAndDispatch();
    const displayComponent = () => {
        const page = searchParams.get("page");
        const id = searchParams.get("id");
        switch (page) {
            case "create":
                return (
                    <UpdateParamComponent
                        router={router}
                        dispatch={dispatch}
                        page={page}
                        user_id={user_id}
                    />
                );
            case "update":
                return (
                    <UpdateParamComponent
                        router={router}
                        dispatch={dispatch}
                        page={page}
                        id={id}
                        user_id={user_id}
                    />
                );
            default:
                return (
                    <HomeParamComponent
                        router={router}
                        dispatch={dispatch}
                        user_id={user_id}
                    />
                );
        }
    };
    return <MainDashboard>{displayComponent()}</MainDashboard>;
}
