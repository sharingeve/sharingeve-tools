import { Suspense } from "react";
import Loading from "../loading";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Tham số hệ thống",
    description: "Các tham số cấu hình",
};

export default function ParamsLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return <Suspense fallback={<Loading />}>{children}</Suspense>;
}
