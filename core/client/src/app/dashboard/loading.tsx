"use client";

import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

const Loading: React.FC = () => {
    return (
        <Spin
            indicator={
                <LoadingOutlined
                    style={{ fontSize: 70, color: "white" }}
                    spin
                />
            }
            spinning={true}
            size="large"
            fullscreen
        />
    );
};

export default Loading;
