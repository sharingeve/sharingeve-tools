import UpdatedMemo from "@/components/Memo/UpdateComponent";
import { MainDashboard } from "@/components/atoms/main";
import MemoConstants from "@/lib/constants/memo-constants";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Quản lý các khoản nợ - chi tiết",
};

export default async function DetailMemo({
    params,
}: {
    params: { id: string };
}) {
    return (
        <MainDashboard>
            <UpdatedMemo
                scoped={MemoConstants.MEMO_UI.UPDATED}
                id={params.id}
            />
        </MainDashboard>
    );
}
