import { MainDashboard } from "@/components/atoms/main";
import UpdatedMemo from "@/components/Memo/UpdateComponent";
import MemoConstants from "@/lib/constants/memo-constants";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Quản lý các khoản nợ - tạo mới",
};


export default function MemoPage() {
    return (
        <MainDashboard>
            <UpdatedMemo scoped={MemoConstants.MEMO_UI.CREATE} />
        </MainDashboard>
    );
}
