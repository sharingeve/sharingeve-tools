/* eslint-disable react-hooks/rules-of-hooks */
import { MainDashboard } from "@/components/atoms/main";
import ListMemo from "@/components/Memo/ListComponent";

export default async function MemoPage() {
    return (
        <MainDashboard>
            <ListMemo />
        </MainDashboard>
    );
}
