import { Suspense } from "react";
import Loading from "../loading";
import type { Metadata } from "next";

export const metadata: Metadata = {
    title: "Quản lý các khoản nợ",
    description: "Tổng hợp các khoản nợ đã cho vay và được cho vay",
};

export default function MemoLayout({
    children,
}: {
    children: React.ReactNode;
}) {
    return <Suspense fallback={<Loading />}>{children}</Suspense>;
}
