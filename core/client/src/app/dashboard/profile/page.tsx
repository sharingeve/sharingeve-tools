import ProfileComponent from "@/components/user/ProfileComponent";
import { MainDashboard } from "@/components/atoms/main";

export default function ProfilePage() {
    return (
        <MainDashboard>
            <ProfileComponent />
        </MainDashboard>
    );
}
