import PlanComponent from "@/components/Nikki/Plan/PlanComponent";
import { MainDashboard } from "@/components/atoms/main";
import type { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
    title: "Quản lý các kế hoạch đã lên",
    description:
        "Thống kê chi tiết các sự kiện đã được lên và mức độ hoàn thành hay chưa hoàn thành của kế hoạch",
};

export default function NikkiPlanPage() {
    // return <MainDashboard><PlanComponent/></MainDashboard>;
    return "Chức năng đang được nâng cấp hãy thử lại sau.";
}
