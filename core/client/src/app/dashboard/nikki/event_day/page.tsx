import EventDayComponent from "@/components/Nikki/EventDay/EventDayComponent";
import type { Metadata } from "next";
import React from "react";

export const metadata: Metadata = {
    title: "Thống kê sự kiện diễn ra trong ngày",
    description: "Thống kê chi tiết các sự kiện đã xảy ra trong ngày",
};

export default function NikkiEventDayPage() {
    return <EventDayComponent />;
}
