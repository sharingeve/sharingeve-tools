import { Suspense } from "react";
import { Layout } from "antd";
import type { Metadata } from "next";
import "../globals.css";
import Loading from "../loading";

export const metadata: Metadata = {
    title: "Công cụ hệ thống - Kojin Tools",
    description: "Công cụ hệ thống",
};

export default function DashboardLayout({
    children,
}: {
    children: React.ReactNode;
}) {

    return (
        <Suspense fallback={<Loading />}>
            <Layout id="app">
                <Layout>{children}</Layout>
            </Layout>
        </Suspense>
    );
}
