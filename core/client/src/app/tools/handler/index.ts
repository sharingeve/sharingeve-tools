import {
    pingServerService,
    checkDB as CheckDBService,
} from "@/service/tools.service";

export const pingServer = async () => {
    const res = await pingServerService();

    if (res.status) {
        alert(res.message);
        return;
    }

    alert("Down");
};
export const checkDB = async () => {
    const res = await CheckDBService();

    if (res.status) {
        alert(res.data);
        return;
    }

    alert("Không gửi được thông tin.");
};
