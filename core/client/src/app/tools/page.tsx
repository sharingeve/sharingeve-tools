"use client";
import React from "react";
import { pingServer, checkDB } from "./handler";

export default function ToolPage() {
    return (
        <div>
            <h2>Danh công cụ</h2>
            <ul>
                <li>
                    <button onClick={async (e) => await pingServer()}>
                        Ping Server
                    </button>
                </li>
                <br />
                <li>
                    <button onClick={async (e) => await checkDB()}>
                        Check connect DB
                    </button>
                </li>
            </ul>
        </div>
    );
}
