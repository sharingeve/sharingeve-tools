"use client";

import Main from "@/components/atoms/main";

export default function HomePage() {
    return <Main>Home Page</Main>;
}
