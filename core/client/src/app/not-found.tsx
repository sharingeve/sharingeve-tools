import Main from "@/components/atoms/main";
import Link from "next/link";
import "./globals.css";

export default function NotFound() {
    return (
        <Main>
            <div className="wrapper-not-found">
                <h1 className="next-error-h1 next-error-custom">404</h1>
                <div
                    style={{
                        display: "inline-block",
                    }}
                >
                    <div>
                        <h2 className="next-error-content">
                            This page could not be found.
                        </h2>
                        <Link href="/" style={{ marginLeft: "5px" }}>
                            Go to home
                        </Link>
                    </div>
                </div>
            </div>
        </Main>
    );
}
