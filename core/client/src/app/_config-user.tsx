/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import { useAppDispatch } from "@/hooks/redux-hook";
import { setAuthenticate } from "@/store/users/actions";
import { useEffect } from "react";

export default function ConfigUser({ userDetail }: { userDetail: any }) {
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (userDetail?.id) {
            (async () => await dispatch(setAuthenticate(userDetail)))();
        }
    }, [userDetail]);

    return <></>;
}
