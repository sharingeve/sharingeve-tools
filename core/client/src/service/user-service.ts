import ConfigApp from "@/lib/config";
import { isEmptyObject } from "@/lib/utils";
import { apiRoute, webRoute } from "@/routers";

const login = async (path: string, data: any) => {
    const res = await webRoute.axiosPost(path, data);

    return res;
};

const findUser = async (path: string, options = {}) => {
    const res = await apiRoute.apiGet(path, options);

    return res;
};

const findUserServer = async (path: string, token: string) => {
    const res = await fetch(`${ConfigApp.env.uri_backend}/api/v1/${path}`, {
        cache: "no-store",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
        },
    });

    return await res.json();
};

const getUserDetail = async (token: string) => {
    try {
        if (!token) {
            return {};
        }
        const user = await findUserServer("user/find", token);

        return user;
    } catch (error) {
        console.log(error);
        return {};
    }
};

const transferOptions = (options: any) => {
    let newOptions: any = {};

    for (let key in options) {
        if (
            options[key] == undefined ||
            options[key] == null ||
            typeof options[key] == "undefined"
        ) {
            continue;
        }

        switch (key) {
            case "full_name":
                newOptions.fullName = options[key]?.trim();
                break;
            case "email":
                newOptions.email = options[key]?.trim();
                break;
            case "password":
                newOptions.password = options[key]?.trim();
                break;
            case "username":
                newOptions.account = options[key]?.trim();
                break;
            case "phone_number":
                newOptions.phoneNumber = options[key]?.trim();
                break;
            default:
                continue;
        }
    }

    return newOptions;
};

const createUser = async (path: string, options: object) => {
    let transfer = transferOptions(options);
    if (isEmptyObject(transfer)) {
        return {
            type: "error",
            content: "Create user fail!",
        };
    }

    const res = await webRoute.axiosPost(path, transfer);

    return res;
};

const logOut = async (path: string, options = {}) => {
    const res = await webRoute.axiosPost(path, options);

    return res;
};

const userService = { login, findUser, logOut, createUser, getUserDetail };

export default userService;
