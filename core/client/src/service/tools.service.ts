import ConfigApp from "@/lib/config";
import { isEmptyObject } from "@/lib/utils";
import { apiRoute, webRoute } from "@/routers";

export const pingServerService = async () => {
    const res = await webRoute.axiosGet("/ping");

    return res;
};
export const checkDB = async () => {
    const res = await webRoute.axiosGet(
        "/api/db/connect?" + new URLSearchParams({ keyCheck: "20240626" })
    );

    return res;
};
