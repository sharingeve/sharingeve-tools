import ConfigApp from "@/lib/config";
import { isEmptyObject } from "@/lib/utils";
import { apiRoute, customAxios } from "@/routers";

const checkKeyTelegram = async (key: string, config?: any) => {
    const res = await apiRoute.apiPost(
        "/plan/check-key",
        {
            key,
        },
        config
    );

    return res;
};

const createPlan = async (data: object, config?: any) => {
    const res = await apiRoute.apiPost("/plan/store", data, config);

    return res;
};

export { checkKeyTelegram, createPlan };
