import { config } from "./../middleware";
import ConfigApp from "@/lib/config";
import { isEmptyObject } from "@/lib/utils";
import { apiRoute, customAxios } from "@/routers";

const initParamsRequest = async (config?: any) => {
    const res = await apiRoute.apiGet("/parameter", config);

    return res;
};

const searchParamsRequest = async (name: string, config?: any) => {
    const res = await apiRoute.apiGet(
        "/parameter?name=" + encodeURIComponent(name),
        config
    );

    return res;
};

const redirectPaginate = async (page: string | number, config?: any) => {
    const res = await apiRoute.apiGet(
        "/parameter?page=" + encodeURIComponent(page),
        config
    );
    return res;
};

const createParamsRequest = async (data: any, config?: any) => {
    const res = await apiRoute.apiPost("/parameter/create", data, config);

    if (res.status) {
        return res.data;
    }

    return res.error;
};
const updateParamsRequest = async (id: string, data: any, config?: any) => {
    const res = await apiRoute.apiPut(
        "/parameter/" + id + "/update",
        data,
        config
    );

    if (res.status) {
        return res.data;
    }

    return res.error;
};
const getParamById = async (id: string, user_id: string, config?: any) => {
    const res = await apiRoute.apiGet(
        "/parameter/" +
            encodeURIComponent(id) +
            "?creditor=" +
            encodeURIComponent(user_id),
        config
    );
    if (res.status) {
        return res.data;
    }

    return res.error;
};
const deleteParamRequest = async (
    id: string,
    user_id: string,
    config?: any
) => {
    const res = await apiRoute.apiDelete(
        "/parameter/" +
            encodeURIComponent(id) +
            "/delete?creditor=" +
            encodeURIComponent(user_id),
        config
    );

    return res.status;
};
export {
    initParamsRequest,
    redirectPaginate,
    createParamsRequest,
    updateParamsRequest,
    getParamById,
    deleteParamRequest,
    searchParamsRequest,
};
