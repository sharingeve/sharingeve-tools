import ConfigApp from "@/lib/config";
import { isEmptyObject } from "@/lib/utils";
import { apiRoute, customAxios } from "@/routers";

const apiListMemo = async (path: string, config?: any) => {
    const res = await apiRoute.apiGet(path, config);

    return res;
};

const apiDetailMemo = async (path: string, config?: any) => {
    const res = await apiRoute.apiGet(path, config);

    return res;
};

const apiCreateMemo = async (path: string, data: object, config?: any) => {
    // if (isEmptyObject(data)) {
    //     return { status: false };
    // }
    

    const res = await apiRoute.apiPost(path, data, config);

    return res;
};

const apiUpdatedMemo = async (path: string, data: object, config?: any) => {
    if (isEmptyObject(data)) {
        return { status: false };
    }

    const res = await apiRoute.apiPut(path, data, config);

    return res;
};

const apiUpdatedDoneMemo = async (path: string, data?: any, config?: any) => {
    const res = await apiRoute.apiPut(path, data, config);

    return res;
};

const apiShareDetailMemo = async (path: string) => {
    const res = await fetch(path, { cache: "no-store" });

    return res.json();
};

const createMemoPay = async (path: string, data: object) => {
    const uri = `${ConfigApp.env.uri_api}/${path}`;
    const res = await customAxios.methodPost({ uri, data });

    return res;
};

const apiConfirmUpdateMemoPayment = async (path: string, data: object, config?: any) => {
    return await apiRoute.apiPatch(path, data, config);
};

const apiUpdateQrPayment = async (path: string, data: object, config?: any) => {
    return await apiRoute.apiPatch(path, data, config)
}

export {
    apiListMemo,
    apiDetailMemo,
    apiCreateMemo,
    apiUpdatedMemo,
    apiShareDetailMemo,
    createMemoPay,
    apiConfirmUpdateMemoPayment,
    apiUpdatedDoneMemo,
    apiUpdateQrPayment
};
