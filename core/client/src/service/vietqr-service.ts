import ConfigApp from "@/lib/config";
import { showLogCatch } from "@/lib/utils";
import { customAxios } from "@/routers";

export interface Datum {
    id: number;
    name: string;
    code: string;
    bin: string;
    shortName: string;
    logo: string;
    transferSupported: number;
    lookupSupported: number;
    short_name: string;
    support: number;
    isTransfer: number;
    swift_code: string;
}

export interface BankApi {
    code: string;
    desc: string;
    data: Datum[];
}

const apiGetListBank = async () => {
    try {
        const paramsQuery = {
            uri: `${ConfigApp.env.uri_viet_qr}v2/banks/`,
        };

        const res: any = await customAxios.methodGet(paramsQuery);
        if (!res?.code || res?.code !== "00") {
            return [];
        }

        return res?.data;
    } catch (error: any) {
        showLogCatch(`get api list bank ${error.message}`);
    }
};

export { apiGetListBank };
