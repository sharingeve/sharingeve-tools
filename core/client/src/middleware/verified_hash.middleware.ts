import { NextRequest } from "next/server";

interface IResult {
    status: boolean;
    code: number;
    message: string;
}
export const verifiedHash = async (request: NextRequest): Promise<boolean> => {
    const baseURL = request.nextUrl.origin;

    const queryRequest = request.nextUrl.searchParams;
    const sign = queryRequest.get("sign");
    const epoch = queryRequest.get("epoch");
    const secretKey = queryRequest.get("secretKey");
    if (!sign || !epoch) {
        return false;
    }

    try {
        const apiCheckHash =
            `${baseURL}/api/hash_secret?` +
            new URLSearchParams({
                secretKey: secretKey || "",
                epoch,
                sign,
            });
        const res = await fetch(apiCheckHash);
        const result: IResult = await res.json();
        return result.status;
    } catch (error) {
        console.log("error", error);
        return false;
    }
};

export default verifiedHash;
