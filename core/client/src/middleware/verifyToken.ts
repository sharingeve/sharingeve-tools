import ConfigApp from "@/lib/config";
import userService from "@/service/user-service";

export const verifyToken = async (accessToken: string) => {
    if (accessToken) {
        const getUser: any = await userService.findUser("/user/find");

        if (getUser) {
            return getUser;
        }
    }

    return {};
};

export const verifyTokens = async (token: string) => {
    if (!token) return false;

    const verify = await fetch(
        `${
            ConfigApp.env.uri_backend
        }/api/auth/verify-token/?${new URLSearchParams({
            token: encodeURIComponent(token),
        })}`,
        {
            method: "POST",
            cache: "no-store",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
        }
    )
        .then((res) => res.json())
        .then((data) => data)
        .catch((err) => console.log("err", err));

    return verify?.status;
};
