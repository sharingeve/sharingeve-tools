export const isPublicPath = (path: any) => {
    const listPath =
        ["/login", "/register", "/reset"].find((item) => item === path) || [];

    return listPath?.length > 0;
};
