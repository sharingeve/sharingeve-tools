import { useRouter } from "next/navigation";
import { useAppDispatch } from "./redux-hook";

const useRouterAndDispatch = () => {
    const router = useRouter();
    const dispatch = useAppDispatch();

    return { router, dispatch };
};

export default useRouterAndDispatch;
