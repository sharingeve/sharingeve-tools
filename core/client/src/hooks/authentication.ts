"use server";

import { cookies } from "next/headers";

export async function setTokenCookie(token: string) {
    if (token) {
        cookies().set("accessToken", token, {
            maxAge: 60 * 60 * 4,
            httpOnly: true,
            path: "/",
        });
    }
}

export async function removeTokenCookie() {
    return cookies().delete("accessToken");
}

export const getTokenCookie = () => {
    return cookies().get("accessToken")?.value || "";
};
