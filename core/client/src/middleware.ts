import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { isPublicPath } from "./middleware/publicPath-middleware";
import { verifyTokens } from "./middleware/verifyToken";
import HashMiddleware from "./middleware/verified_hash.middleware";

export async function middleware(request: NextRequest) {
    const baseURL = request.nextUrl;
    const token = request.cookies.get("accessToken")?.value || "";
    const isVerifyToken = await verifyTokens(token);

    if (request.nextUrl.pathname.startsWith("/tools")) {
        const verifiedHash = await HashMiddleware(request);
        if (!verifiedHash) {
            return NextResponse.rewrite(new URL('/', request.url)); // rewrite nội dung page ứng với url truyền không đổi tên đường dẫn url
        }
        return NextResponse.next();
    }

    if (isPublicPath(request.nextUrl.pathname) && token && isVerifyToken) {
        const redirect = request.nextUrl.searchParams.get("redirectUrl");
        if (redirect) {
            return NextResponse.redirect(redirect);
        }
        return NextResponse.redirect(new URL("/dashboard/courses", baseURL));
    }

    if (!isPublicPath(request.nextUrl.pathname) && !token && !isVerifyToken) {
        return NextResponse.redirect(
            new URL(
                "/login/?" +
                    new URLSearchParams({
                        redirectUrl: request.url,
                        logout: "1",
                    }),
                baseURL
            )
        );
    }

    // return NextResponse.next();

    // const response = NextResponse.next();
    // response.cookies.delete("accessToken");

    // return response;
}

export const config = {
    // matcher: ["/((?!api|_next/static|_next/image|.*\\.png$|favicon.ico).*)"], // compare ! route regex
    matcher: [
        "/dashboard/:path*",
        // "/api/:path*",
        "/login",
        "/register",
        "/reset",
        "/tools/:path*",
    ], // middleware for route match
};
