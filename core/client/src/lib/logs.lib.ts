import fs from "fs";
import path from "path";
import { TimeLibSingleton } from "./time.lib";

interface LogData {
    [key: string]: string;
}
class LogLib {
    private static instance: LogLib | null = null;
    private filePath: string;
    private log: LogData;
    private new_log: LogData;

    constructor(filePath: string) {
        if (LogLib.instance) {
            throw new Error("Use LogLib.getInstance() instead of new.");
        }
        this.filePath = filePath;
        this.log = this.loadLogFromFile();
        this.new_log = {};
    }

    private loadLogFromFile(): LogData {
        try {
            const data = fs.readFileSync(this.filePath, "utf-8");
            return JSON.parse(data) as LogData;
        } catch (error) {
            return {};
        }
    }

    private saveLogToFile(): void {
        fs.writeFileSync(
            this.filePath,
            JSON.stringify(this.log, null, 2),
            "utf-8"
        );
    }
    private saveLogNewToFile(): void {
        fs.writeFileSync(
            this.filePath,
            JSON.stringify(this.new_log, null, 2),
            "utf-8"
        );
    }

    private set(key: string, value: any): void {
        if (Object.keys(this.new_log).length > 0) {
            this.new_log = this.loadLogFromFile();
            this.log = this.new_log;
        }
        this.new_log[key] = value;
        this.saveLogNewToFile();
    }

    get<T>(key: string): T | null {
        let value = this.log[key] || null;
        return value !== undefined ? (value as T) : null;
    }

    remove(key: string): void {
        delete this.log[key];
        this.saveLogToFile();
    }

    clear(): void {
        this.log = {};
        this.saveLogToFile();
    }

    has(key: string): boolean {
        return key in this.log;
    }

    logger(key: string, value: any) {
        return this.set(key, value);
    }

    getAllLogByDate(logDate: string) {
        const logFolderPath = path.join(process.cwd(), "src", "logs");
        const logFilePath = path.join(
            logFolderPath,
            "log_" + logDate + ".json"
        );
        try {
            const data = fs.readFileSync(logFilePath, "utf-8");
            return JSON.parse(data) as LogData;
        } catch (error) {
            return {};
        }
    }

    static getInstance(filePath: string): LogLib {
        if (LogLib.instance === null) {
            LogLib.instance = new LogLib(filePath);
        }

        return LogLib.instance;
    }
}

function checkFile(pathLog: string): string {
    if (process.env.NODE_ENV == "production") {
        if (!fs.existsSync(pathLog)) {
            fs.mkdir(
                path.resolve(pathLog),
                { recursive: true, mode: 0o755 },
                (error: any) => {
                    if (error) {
                        throw error;
                    }
                }
            );
        }
    } else {
        if (!fs.existsSync(pathLog)) {
            fs.mkdir(path.resolve(pathLog), { mode: 0o755 }, (error: any) => {
                if (error) {
                    throw error;
                }
            });
        }
    }

    return pathLog;
}

export { LogLib }; // export class

let logFolderPath = "";
if (process.env.NODE_ENV == "production") {
    logFolderPath = checkFile(path.join(process.cwd(), "src", "/tmp/logs"));
} else {
    logFolderPath = checkFile(path.join(process.cwd(), "src", "logs"));
}
const logFilePath = path.join(
    logFolderPath,
    "log_" + TimeLibSingleton.getYYYYMMDD() + ".json"
);
// const logFilePath = path.join(__dirname, TimeLibSingleton.getYYYYMMDD() + '.json'); // tại .next/server/app/api
export const LogSingleton = LogLib.getInstance(logFilePath); // use singleton design patterns
