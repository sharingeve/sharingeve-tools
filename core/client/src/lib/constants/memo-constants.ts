const MemoConstants = {
    MEMO_UI: {
        LIST: "list",
        CREATE: "create",
        UPDATED: "update",
    },
    LIST_PAY: {
        BANK_TRANSFER: "Chuyển khoản",
        BANK_QR: "Tạo QR thanh toán",
        COD: "Đưa trực tiếp",
    },
    LIST_QR: {
        MBB: {
            name: "mbb",
            code: "MB",
        },
        VCB: {
            name: "vietcombank",
            code: "VCB",
        },
        TECH: {
            name: "techcombank",
            code: "TCB",
        },
    },
    LIST_NUMBER_BANK: {
        MBB: "0337882657",
        VCB: "1021027019",
        TECH: "19038342371017",
    },
    OWNER_CARD: "NGUYEN THANH TUNG",
    MEMO_PAY: {
        PAY_STATUS: {
            WAIT: "WAIT",
            FAIL: "FAIL",
            DONE: "DONE",
        },
        PAY_TYPE: {
            VIETQR: 0,
            QR_BANK: 0,
        },
        PAY_PAID: {
            PAID: 1,
            NOT_PAID: 0
        }
    },
};

export default MemoConstants;
