const Constants = {
    DEFAULT_HOME: "/dashboard/courses",
    MENU: {
        M00: "Trang chủ",
        M01: "Học tập",
        M02: "Nhật ký",
        M03: "Note",
        M04: "Lưu trữ",
        M05: "Khác",
        M06: "CV",
        M07: "Tham số",
    },
    MENU_KEY: {
        M00: "home",
        M01: "learn",
        M02: "diary",
        M03: "note",
        M04: "storage",
        M05: "other",
        M06: "cv",
        M07: "params"
    },
    MENU_CHILD: {
        M01: {
            SM_01: {
                LABEL: "Khóa học",
                KEY: "couses",
            },
            SM_02: {
                LABEL: "Từ điển",
                KEY: "lib",
            },
            SM_03: {
                LABEL: "Bài thi thử",
                KEY: "test",
            },
            SM_04: {
                LABEL: "Postcards",
                KEY: "postcard",
            },
        },
        M03: {
            SM_01: {
                LABEL: "Kế hoạch đã lên",
                KEY: "plan",
            },
            SM_02: {
                LABEL: "Sự kiện hàng ngày",
                KEY: "event_day",
            },
        },
    },
    AUTHEN: {
        LOGIN: {
            LABEL: "Đăng nhập",
            KEY: "login",
        },
        REGIST: {
            LABEL: "Đăng ký",
            KEY: "regist",
        },
        REST_PASS: {
            LABEL: "Quên mật khẩu?",
            KEY: "reset_pass",
        },
        PROFILE: {
            LABEL: "Thông tin cá nhân",
            KEY: "profile",
        },
        LOG_OUT: {
            LABEL: "Đăng xuất",
            KEY: "logout",
        },
    },
};

export default Constants;
