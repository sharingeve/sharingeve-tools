const vietQrConstants = {
    THEME: {
        COMPACT_2: "compact2", // QR, logo, thông tin chuyển khoản
        COMPACT: "compact", // QR kèm logo vietQR, ngân hàng
        QR_ONLY: "qr_only", // chỉ qr
        print: "print", // mã QR, các logo và đầy đủ thông tin chuyển khoản
    },
};

export default vietQrConstants;
