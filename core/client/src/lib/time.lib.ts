import SHA256 from "crypto-js/sha256";

class TimeLib {
    private static folderInstance: TimeLib | null = null;
    private date: Date;
    constructor() {
        if (TimeLib.folderInstance) {
            throw new Error("Use TimeLib.getInstance() instead of new.");
        }
        this.date = new Date();
    }

    static getInstance(): TimeLib {
        if (TimeLib.folderInstance === null) {
            TimeLib.folderInstance = new TimeLib();
        }

        return TimeLib.folderInstance;
    }

    fullYear() {
        return this.date.getFullYear();
    }
    month() {
        return this.date.getMonth() + 1 < 10
            ? `0${this.date.getMonth() + 1}`
            : this.date.getMonth() + 1;
    }
    day() {
        return this.date.getDate() < 10
            ? `0${this.date.getDate()}`
            : this.date.getDate();
    }
    hours() {
        return this.date.getHours() < 10
            ? `0${this.date.getHours()}`
            : this.date.getHours();
    }
    minutes() {
        return this.date.getMinutes() < 10
            ? `0${this.date.getMinutes()}`
            : this.date.getMinutes();
    }
    second() {
        return this.date.getSeconds() < 10
            ? `0${this.date.getSeconds()}`
            : this.date.getSeconds();
    }
    getYYYYMMDDHHIISS() {
        return `${this.fullYear()}${this.month()}${this.day()}${this.hours()}${this.minutes()}${this.second()}`;
    }
    getYYYYMMDD() {
        return `${this.fullYear()}${this.month()}${this.day()}`;
    }
    getHHIISS() {
        return `${this.hours()}${this.minutes()}${this.second()}`;
    }
    getNameFolderByDate(folder: string) {
        return `${folder}${this.getYYYYMMDD()}`;
    }
    createFolderByDateHash(folder: string) {
        return SHA256(`${this.getNameFolderByDate(folder)}`);
    }
    /**
     *
     * @param timer : timer equal minutes
     * @returns
     */
    getEpoch(timer = 15) {
        return new Date(new Date().getTime() + timer * 60000)
            .getTime()
            .toString(); // expired 15 minutes
    }
}

export { TimeLib };

export const TimeLibSingleton = TimeLib.getInstance();
