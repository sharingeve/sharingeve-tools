import CryptoJS from "crypto-js";
import Base64 from "crypto-js/enc-base64";
import { CacheSingleton } from "./cache.lib";

// https://www.npmjs.com/package/crypto-js
class CryptoLib {
    private static CryptoInstance: CryptoLib | null = null;
    constructor() {
        if (CryptoLib.CryptoInstance) {
            throw new Error("Use CryptoLib.getInstance() instead of new.");
        }
    }
    static getInstance(): CryptoLib {
        if (this.CryptoInstance === null) {
            this.CryptoInstance = new CryptoLib();
        }
        return this.CryptoInstance;
    }

    createPrivateKey(epoch: string, secret: string): string {
        let splitKey = epoch + "|";
        if (secret) {
            splitKey += secret;
        }

        return CryptoJS.AES.encrypt(splitKey, epoch).toString();
    }

    decryptPrivateKey(privateKey: string, epoch: string): string {
        const decrypt = CryptoJS.AES.decrypt(privateKey, epoch);

        return decrypt.toString(CryptoJS.enc.Utf8);
    }

    encryptSHA256(secret: string) {
        return CryptoJS.SHA256(secret);
    }

    createPublicKey(privateKey: string, nonce: string) {
        const hashDigest = CryptoJS.SHA256(nonce + privateKey);
        const publicKey = Base64.stringify(
            CryptoJS.HmacSHA512(nonce + hashDigest, privateKey)
        );
        CacheSingleton.set(nonce, publicKey);
        return publicKey;
    }

    createPrivateAndPublicKey(epoch: string, secret: string) {
        const privateKey = this.createPrivateKey(epoch, secret);
        const publicKey = this.createPublicKey(privateKey, epoch);
        return {
            privateKey: privateKey ? privateKey : "",
            publicKey: publicKey ? publicKey : "",
        };
    }

    verifiedHash(sign: string, epoch: string, secretKey: string | null) {
        const decrypt = this.decryptPrivateKey(sign, epoch);
        const explodeHash = decrypt.split("|");
        const deEpoch = explodeHash[0];
        const deSecret = explodeHash[1];
        if (!deSecret || !deEpoch) {
            return {
                status: false,
                code: 400,
                message: "Không tồn tại mã đối chiếu.",
            };
        }

        const current = new Date().getTime();
        if (current > parseInt(deEpoch)) {
            return {
                status: false,
                code: 400,
                message: "Mã đối chiếu hết hạn.",
            };
        }

        const deSecretKey = explodeHash[1]?.replace(`secret_${epoch}_`, "");
        if (deSecretKey && secretKey != deSecretKey) {
            return {
                status: false,
                code: 400,
                message: "Không tồn tại mã đối chiếu.",
            };
        }

        const publicKey = CacheSingleton.get(epoch);
        const hashPrivateKey = this.createPublicKey(sign, epoch);
        if (publicKey != hashPrivateKey) {
            return {
                status: false,
                code: 400,
                message: "Không tồn tại mã đối chiếu.",
            };
        }

        return { status: true, code: 200, message: "Mã đối chiếu tồn tại." };
    }
}

export { CryptoLib };
export const CryptoInstance = CryptoLib.getInstance();
