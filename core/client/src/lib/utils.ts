import { TypeDispatch } from "@/hooks/redux-hook";
import { setLoadingRedux } from "@/store/common/actions";

const formatNumberWithThreeDigits = (num: any) => {
    var str = num.toString().split(".");
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, "$1,");
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, "$1 ");
    }
    return str.join(".");
};

const formatMoney = (
    country: string,
    currency: string,
    minimumFractionDigits?: number
) => {
    return new Intl.NumberFormat(country, {
        style: "currency",
        currency: currency,
        minimumFractionDigits: minimumFractionDigits ?? 0,
    });
};

const isValidDate = (dateString: string) => {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regEx)) return false; // Invalid format
    var d = new Date(dateString);
    var dNum = d.getTime();
    if (!dNum && dNum !== 0) return false; // NaN value, Invalid date
    return d.toISOString().slice(0, 10) === dateString;
};

const loginGoToBack = (urlGoBack: any) => {
    const urlLogin = new URL("/login", window.location.href);
    if (urlGoBack) {
        urlLogin.searchParams.set("redirectUrl", urlGoBack);
        urlLogin.searchParams.set("logout", "1");
    }
    return window.location.assign(urlLogin);
};

const isObject = (data: any) => {
    return (
        Object.prototype.toString.call(data) === "[object Object]" ||
        data instanceof Object ||
        typeof data === "object"
    );
};

const isEmptyObject = (obj: any) => {
    return !isObject(obj) || Object.keys(obj).length === 0;
};

const isEmptyArray = (arr: any) => {
    return typeof arr !== "object" || !Array.isArray(arr) || arr?.length === 0;
};

const getKeyByValue = (obj: any, value: string) => {
    if (!isObject(obj)) {
        return "";
    }

    return Object.keys(obj).find(
        (key) => obj[key] === value || value.includes(obj[key])
    );
};

const templateGetItem = (
    field: any,
    label?: string,
    name?: string,
    rule?: any,
    custom?: object,
    dependencies?: any[],
    hasFeedback?: boolean,
    hidden?: boolean,
    getValueFromEvent?: any
) => ({
    field,
    label,
    name,
    rule,
    custom,
    dependencies,
    hasFeedback,
    hidden,
    getValueFromEvent,
});

const actionUpload = async (options: any) => {
    const { onSuccess, onError, file, onProgress } = options;

    onSuccess(null, file);
    // xử lý lưu lại nếu cần
    // https://github.com/react-component/upload#customrequest
    // https://stackoverflow.com/questions/58128062/using-customrequest-in-ant-design-file-upload
};

const showLogCatch = (error: any, msg = "Có lỗi xảy ra") => {
    if (process.env.NODE_ENV != "production") {
        console.error(error);
    } else {
        console.error(msg);
    }
};

const toggleDisplayLoading = (dispatch: TypeDispatch, value: boolean) => {
    dispatch(setLoadingRedux(value));
};
const redirectNoScroll = (route: any, path: string) => route.push(path);
const redirectCustom = (route: any, path: string) => route.push(path);
const isNumber = (val: any) => {
    return /^\d+$/i.test(val);
};
export {
    formatNumberWithThreeDigits,
    formatMoney,
    isValidDate,
    loginGoToBack,
    isEmptyObject,
    isEmptyArray,
    getKeyByValue,
    templateGetItem,
    actionUpload,
    showLogCatch,
    toggleDisplayLoading,
    redirectNoScroll,
    redirectCustom,
    isNumber
};
