export const i18nVi = {
    FULL_NAME_FIELD: "Họ tên",
    EMAIL_FIELD: "Email",
    ACCOUNT_FIELD: "Tài khoản",
    PASSWORD_FIELD: "Mật khẩu",
    PASSWORD_CURRENT_FIELD: "Mật khẩu hiện tại",
    CONFIRM_PASSWORD_FIELD: "Xác nhận mật khẩu",
    PHONE_NUMBER_FIELD: "Số điện thoại",
    FULL_NAME_REQUIRED: "Vui lòng nhập họ tên của bạn!",
    EMAIL_REQUIRED: "Vui lòng nhập Email của bạn!",
    PASSWORD_REQUIRED: "Vui lòng nhập mật khẩu của bạn!",
    CM_MAXLEN: "Mật khẩu không vượt quá {max_length} ký tự.",
    CM_MINLEN: "Mật khẩu phải từ {min_length} ký tự trở lên.",
    CM_BOTH_MAX_MIN:
        "Mật khẩu phải có độ dài từ {min_length} đến {max_length} ký tự.",
    CM_EMAIL_VALID: "Địa chỉ Email không hợp lệ.",
    CM_ACCOUNT_VALID_REQUIRE: "Không được để trống tài khoản",
    CM_PHONE_NUMBER_VALID: "Số điện thoại bị sai!",
    CM_PASSWORD_CONFIRM_REQUIRED_VALID: "Vui lòng xác nhận mật khẩu của bạn!",
    CM_PASSWORD_CONFIRM_VALID: "Mật khẩu mới bạn nhập không khớp!",
    PAYMENT_INPROGRESS:
        "Mục này đã thanh toán. Đang đợi người cho vay kiểm tra!",
    CLIENT: "Loại tài khoản",
    TXT_BACK: "Trở lại",
    TXT_UPDATE: "Cập nhật",
    TXT_ADD: "Thêm mới",
};
