export const i18nEn = {
    EMAIL_REQUIRED: "Please input your Email!",
    PASSWORD_REQUIRED: "Please input your Password!",
    CM_MAXLEN: "Password length must be less than {max_length} characters.",
    CM_MINLEN: "Password must have a length shorter than {min_length} characters.",
    CM_BOTH_MAX_MIN: "Password must have a length between {min_length} and {max_length} characters.",
    CM_EMAIL_VALID: "The input is not valid E-mail"
};
