const ConfigApp = {
    env: {
        environment: process.env.NODE_ENV || "development",
        uri_viet_qr: process.env.API_VIET_QR || "https://api.vietqr.io/",
        uri_api: process.env.API_URL || "http://localhost:3000",
        uri_backend: process.env.SERVER_URL_DOCKER || "http://tool.server:3000",
        uri_base: process.env.BASE_URL || "http://localhost:3000",
        uri_img_qr: process.env.URI_IMAGE_QR || "https://img.vietqr.io/image",
        other_url: process.env.OTHER_URL || "https://sharingeve.com/",
        bot_tele: process.env.TELE_BOT_URL || "http://localhost:3000"
    },
};
export default ConfigApp;
