import axios from "axios";
import { showLogCatch } from "./utils";

const ininstance = axios.create({
    baseURL: process.env.API_URL,
});

const instanceApi = axios.create({
    baseURL: `${process.env.API_URL}/api/v1`,
});

// https://viblo.asia/p/cung-minh-tao-boilerplate-cho-du-an-nextjs-v12-phan-1-mui-v5-emotion-cache-va-axios-hooks-GyZJZnk2Jjm
ininstance.interceptors.request.use(async (config: any) => {
    config.headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
        ...config.headers,
    };

    if (localStorage.getItem("accessToken")) {
        config.headers["Authorization"] = `Bearer ${localStorage.getItem(
            "accessToken"
        )}`;
    }

    return config;
});

ininstance.interceptors.response.use(
    (response) => {
        if (response.status === 401) {
            return "re-login";
        }
        if (response.status === 200 && response.data) {
            return response.data;
        }

        return response;
    },
    (error) => {
        if (error.response.status == 401) {
            setTimeout(() => window.location.reload(), 800)
            return {
                status: false,
                error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
            };
        } else {
            showLogCatch(`-------- ${error.message}`);
            return error.response;
        }
    }
);

// instanceApi
instanceApi.interceptors.request.use(async (config: any) => {
    config.headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
        ...config.headers,
    };

    if (localStorage.getItem("accessToken")) {
        config.headers["Authorization"] = `Bearer ${localStorage.getItem(
            "accessToken"
        )}`;
    }

    return config;
});

instanceApi.interceptors.response.use(
    (response) => {
        if (response.status === 401) {
            return "re-login";
        }
        if (response.status === 200 && response.data) {
            return response.data;
        }

        return response;
    },
    (error) => {
        if (error.response.status == 401) {
            setTimeout(() => window.location.reload(), 800)
            return {
                status: false,
                error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
            };
        } else {
            showLogCatch(`>>>>> ${error.message}`, "Gọi api không thành công");
            return error.response;
        }
    }
);

// init http methods
const methodGet = async ({
    uri,
    queryParams,
    headersOptions,
}: {
    uri: string;
    queryParams?: object;
    headersOptions?: object;
}) => {
    return await axios
        .get(uri, {
            params: { ...queryParams },
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                ...headersOptions,
            },
        })
        .then((res) => {
            if (res.status === 401) {
                setTimeout(() => window.location.reload(), 800);
                return {
                    status: false,
                    error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
                };
            }
            let responseOK = res && res.status === 200 && res.data;
            if (responseOK) {
                return res.data;
            }

            return res;
        })
        .catch((error) => showLogCatch(`>>> ${error}`));
};

const methodPost = async ({
    uri,
    data,
    queryParams,
    headersOptions,
}: {
    uri: string;
    data: object;
    queryParams?: object;
    headersOptions?: object;
}) => {
    return await axios
        .post(
            uri,
            { ...data },
            {
                params: { ...queryParams },
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    ...headersOptions,
                },
            }
        )
        .then((res) => {
            if (res.status === 401) {
                setTimeout(() => window.location.reload(), 800)
                return {
                    status: false,
                    error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
                };
            }
            let responseOK = res && res.status === 200 && res.data;
            if (responseOK) {
                return res.data;
            }

            return res;
        })
        .catch((error) => showLogCatch(`>>> ${error}`));
};

const methodPut = async ({
    uri,
    data,
    queryParams,
    headersOptions,
}: {
    uri: string;
    data: object;
    queryParams?: object;
    headersOptions?: object;
}) => {
    return await axios
        .put(
            uri,
            { ...data },
            {
                params: { ...queryParams },
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    ...headersOptions,
                },
            }
        )
        .then((res) => {
            if (res.status === 401) {
                setTimeout(() => window.location.reload(), 800)
                return {
                    status: false,
                    error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
                };
            }
            let responseOK = res && res.status === 200 && res.data;
            if (responseOK) {
                return res.data;
            }

            return res;
        })
        .catch((error) => showLogCatch(`>>> ${error}`));
};
const methodPatch = async ({
    uri,
    data,
    queryParams,
    headersOptions,
}: {
    uri: string;
    data: object;
    queryParams?: object;
    headersOptions?: object;
}) => {
    return await axios
        .patch(
            uri,
            { ...data },
            {
                params: { ...queryParams },
                headers: {
                    "Content-Type": "application/json",
                    Accept: "application/json",
                    ...headersOptions,
                },
            }
        )
        .then((res) => {
            if (res.status === 401) {
                setTimeout(() => window.location.reload(), 800)
                return {
                    status: false,
                    error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
                };
            }
            let responseOK = res && res.status === 200 && res.data;
            if (responseOK) {
                return res.data;
            }

            return res;
        })
        .catch((error) => showLogCatch(`>>> ${error}`));
};
const methodDelete = async ({
    uri,
    queryParams,
    headersOptions,
}: {
    uri: string;
    queryParams?: object;
    headersOptions?: object;
}) => {
    return await axios
        .delete(uri, {
            params: { ...queryParams },
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                ...headersOptions,
            },
        })
        .then((res) => {
            if (res.status === 401) {
                window.location.reload();
                return {
                    status: false,
                    error: "Yêu cầu bị từ chối. Xác thực không hợp lệ.",
                };
            }
            let responseOK = res && res.status === 200 && res.data;
            if (responseOK) {
                return res.data;
            }

            return res;
        })
        .catch((error) => showLogCatch(`>>> ${error}`));
};

const fnCustomAxios = {
    methodGet,
    methodPost,
    methodPut,
    methodPatch,
    methodDelete,
};

export default ininstance;
export { instanceApi, fnCustomAxios };
