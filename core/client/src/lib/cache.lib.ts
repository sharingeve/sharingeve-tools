import fs from "fs";
import path from "path";
import { TimeLibSingleton } from "./time.lib";

interface CacheData {
    [key: string]: string;
}
class CacheLib {
    private static instance: CacheLib | null = null;
    private filePath: string;
    private cache: CacheData;

    constructor(filePath: string) {
        if (CacheLib.instance) {
            throw new Error("Use CacheLib.getInstance() instead of new.");
        }
        this.filePath = filePath;
        this.cache = this.loadCacheFromFile();
    }

    private loadCacheFromFile(): CacheData {
        try {
            const data = fs.readFileSync(this.filePath, "utf-8");
            return JSON.parse(data) as CacheData;
        } catch (error) {
            return {};
        }
    }

    private saveCacheToFile(): void {
        fs.writeFileSync(
            this.filePath,
            JSON.stringify(this.cache, null, 2),
            "utf-8"
        );
    }

    set(key: string, value: any): void {
        this.cache[key] = value;
        this.saveCacheToFile();
    }

    get<T>(key: string): T | null {
        let value = this.cache[key] || null;
        return value !== undefined ? (value as T) : null;
    }

    remove(key: string): void {
        delete this.cache[key];
        this.saveCacheToFile();
    }

    clear(): void {
        this.cache = {};
        this.saveCacheToFile();
    }

    has(key: string): boolean {
        return key in this.cache;
    }

    static getInstance(filePath: string): CacheLib {
        if (CacheLib.instance === null) {
            CacheLib.instance = new CacheLib(filePath);
        }

        return CacheLib.instance;
    }
}

function checkFile(pathLog: string): string {
    if (process.env.NODE_ENV == "production") {
        if (!fs.existsSync(pathLog)) {
            fs.mkdir(
                path.resolve(pathLog),
                { recursive: true, mode: 0o755 },
                (error: any) => {
                    if (error) {
                        throw error;
                    }
                }
            );
        }
    } else {
        if (!fs.existsSync(pathLog)) {
            fs.mkdir(path.resolve(pathLog), { mode: 0o755 }, (error: any) => {
                if (error) {
                    throw error;
                }
            });
        }
    }

    return pathLog;
}

export { CacheLib }; // export class

let cacheFolderPath = "";
if (process.env.NODE_ENV == "production") {
    cacheFolderPath = checkFile(path.join(process.cwd(), "src", "/tmp/cache"));
} else {
    cacheFolderPath = checkFile(path.join(process.cwd(), "src", "cache"));
}
const cacheFilePath = path.join(
    cacheFolderPath,
    TimeLibSingleton.createFolderByDateHash("cache") + ".json"
);
// const cacheFilePath = path.join(__dirname, TimeLibSingleton.getYYYYMMDD() + '.json'); // tại .next/server/app/api
export const CacheSingleton = CacheLib.getInstance(cacheFilePath); // use singleton design patterns
