describe("Test create memo", () => {
    // beforeEach(() => {
    //     cy.request("GET", "http://localhost:3005").its("body").as("StatusBe");
    // });
    it("Memo page", function () {
        // const be = this.StatusBe;

        // cy.visit("/");
        // cy.contains("Đăng nhập").click();

        cy.login("20231126", "20231126");
        cy.visit("/dashboard/memo/create");
        cy.get("#updated_form_full_name").type("test cypress");
        cy.get("#updated_form_price").type(1000);
        cy.get("#updated_form_reason").type("reason test cypress");
        // cy.get('#updated_form_created_date').type('2023-04-07');
        cy.get("input[type=file]").selectFile(
            "/Users/tungnt/Documents/share/images/1/300001127_176528951540680_4918543917839342611_n.jpg",
            // { action: "drag-drop" }
            { force: true }
        );
        cy.intercept("POST", "http://localhost:3005/api/v1/memo/create").as(
            "memoCreate"
        );
        cy.get("#updated_form").submit();
        cy.wait(8000);
        cy.wait("@memoCreate").then((interception) => {
            let idMemo = interception.response.body?.id;
            if (idMemo) {
                cy.task("log", `${idMemo} --> pass`);
                cy.url().should("include", `/dashboard/memo/${idMemo}`);
            }
        }).should('exist');
    });
});
