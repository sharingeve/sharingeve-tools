context("GET /", () => {
    it("Home page", () => {
        cy.request("GET", "http://localhost:3005")
            .then((res) => {
                expect(res.status).to.eq(200);
                expect(res.body.code).to.eq(200);
            })
            .should("exist");
    });
});
