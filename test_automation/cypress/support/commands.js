// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add("login", (email, password) => {
    cy.session("LoginSession", () => {
        cy.visit("/login");

        // set giá trị cho email
        cy.get("#normal_login_username").type(`${email}`);
        // cy.get("#normal_login_username").should("have.value", "20231126");
        // set giá trị cho pw
        cy.get("#normal_login_password").type(`${password}{enter}`);
        // cy.get("#normal_login_password").should("have.value", "20231126");
        cy.url().should("include", "/dashboard");
        cy.getCookie("accessToken").should("exist");
        // cy.get(".ant-layout-content > div").should(
        //     "contain.text",
        //     "CoursePage"
        // );
    });
});
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
